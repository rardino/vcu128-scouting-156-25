onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+xdma_debug_ila -L xilinx_vip -L xpm -L xil_defaultlib -L xilinx_vip -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.xdma_debug_ila xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {xdma_debug_ila.udo}

run -all

endsim

quit -force
