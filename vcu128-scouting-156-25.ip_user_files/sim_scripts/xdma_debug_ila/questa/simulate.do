onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib xdma_debug_ila_opt

do {wave.do}

view wave
view structure
view signals

do {xdma_debug_ila.udo}

run -all

quit -force
