vlib modelsim_lib/work
vlib modelsim_lib/msim

vlib modelsim_lib/msim/xilinx_vip
vlib modelsim_lib/msim/xpm
vlib modelsim_lib/msim/gtwizard_ultrascale_v1_7_7
vlib modelsim_lib/msim/xil_defaultlib
vlib modelsim_lib/msim/blk_mem_gen_v8_4_4
vlib modelsim_lib/msim/xdma_v4_1_4

vmap xilinx_vip modelsim_lib/msim/xilinx_vip
vmap xpm modelsim_lib/msim/xpm
vmap gtwizard_ultrascale_v1_7_7 modelsim_lib/msim/gtwizard_ultrascale_v1_7_7
vmap xil_defaultlib modelsim_lib/msim/xil_defaultlib
vmap blk_mem_gen_v8_4_4 modelsim_lib/msim/blk_mem_gen_v8_4_4
vmap xdma_v4_1_4 modelsim_lib/msim/xdma_v4_1_4

vlog -work xilinx_vip -64 -incr -sv -L xilinx_vip "+incdir+/opt/Xilinx/Vivado/2019.2/data/xilinx_vip/include" \
"/home/opt/Xilinx/Vivado/2019.2/data/xilinx_vip/hdl/axi4stream_vip_axi4streampc.sv" \
"/home/opt/Xilinx/Vivado/2019.2/data/xilinx_vip/hdl/axi_vip_axi4pc.sv" \
"/home/opt/Xilinx/Vivado/2019.2/data/xilinx_vip/hdl/xil_common_vip_pkg.sv" \
"/home/opt/Xilinx/Vivado/2019.2/data/xilinx_vip/hdl/axi4stream_vip_pkg.sv" \
"/home/opt/Xilinx/Vivado/2019.2/data/xilinx_vip/hdl/axi_vip_pkg.sv" \
"/home/opt/Xilinx/Vivado/2019.2/data/xilinx_vip/hdl/axi4stream_vip_if.sv" \
"/home/opt/Xilinx/Vivado/2019.2/data/xilinx_vip/hdl/axi_vip_if.sv" \
"/home/opt/Xilinx/Vivado/2019.2/data/xilinx_vip/hdl/clk_vip_if.sv" \
"/home/opt/Xilinx/Vivado/2019.2/data/xilinx_vip/hdl/rst_vip_if.sv" \

vlog -work xpm -64 -incr -sv -L xilinx_vip "+incdir+../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source" "+incdir+../../../ipstatic/hdl/verilog" "+incdir+/opt/Xilinx/Vivado/2019.2/data/xilinx_vip/include" \
"/opt/Xilinx/Vivado/2019.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/opt/Xilinx/Vivado/2019.2/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv" \
"/opt/Xilinx/Vivado/2019.2/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -64 -93 \
"/home/opt/Xilinx/Vivado/2019.2/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work gtwizard_ultrascale_v1_7_7 -64 -incr "+incdir+../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source" "+incdir+../../../ipstatic/hdl/verilog" "+incdir+/opt/Xilinx/Vivado/2019.2/data/xilinx_vip/include" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_bit_sync.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gte4_drp_arb.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gthe4_delay_powergood.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtye4_delay_powergood.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gthe3_cpll_cal.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gthe3_cal_freqcnt.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gthe4_cpll_cal.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gthe4_cpll_cal_rx.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gthe4_cpll_cal_tx.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gthe4_cal_freqcnt.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtye4_cpll_cal.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtye4_cpll_cal_rx.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtye4_cpll_cal_tx.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtye4_cal_freqcnt.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtwiz_buffbypass_rx.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtwiz_buffbypass_tx.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtwiz_reset.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtwiz_userclk_rx.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtwiz_userclk_tx.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtwiz_userdata_rx.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_gtwiz_userdata_tx.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_reset_sync.v" \
"../../../ipstatic/hdl/gtwizard_ultrascale_v1_7_reset_inv_sync.v" \

vlog -work xil_defaultlib -64 -incr "+incdir+../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source" "+incdir+../../../ipstatic/hdl/verilog" "+incdir+/opt/Xilinx/Vivado/2019.2/data/xilinx_vip/include" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/ip_0/sim/gtwizard_ultrascale_v1_7_gtye4_channel.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/ip_0/sim/xdma_0_pcie4c_ip_gt_gtye4_channel_wrapper.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/ip_0/sim/gtwizard_ultrascale_v1_7_gtye4_common.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/ip_0/sim/xdma_0_pcie4c_ip_gt_gtye4_common_wrapper.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/ip_0/sim/xdma_0_pcie4c_ip_gt_gtwizard_gtye4.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/ip_0/sim/xdma_0_pcie4c_ip_gt_gtwizard_top.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/ip_0/sim/xdma_0_pcie4c_ip_gt.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_cxs_remap.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_bram_16k_int.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_bram_16k.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_bram_32k.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_bram_4k_int.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_bram_msix.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_bram_rep_int.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_bram_rep.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_bram_tph.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_bram.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_gtwizard_top.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_phy_ff_chain.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_phy_pipeline.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_gt_gt_channel.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_gt_gt_common.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_gt_phy_clk.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_gt_phy_rst.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_gt_phy_rxeq.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_gt_phy_txeq.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_sync_cell.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_sync.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_gt_cdr_ctrl_on_eidle.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_gt_receiver_detect_rxterm.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_gt_phy_wrapper.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_phy_top.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_init_ctrl.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_pl_eq.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_vf_decode.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_vf_decode_attr.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_pipe.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_seqnum_fifo.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_sys_clk_gen_ps.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source/xdma_0_pcie4c_ip_pcie4c_uscale_core_top.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/sim/xdma_0_pcie4c_ip.v" \

vlog -work blk_mem_gen_v8_4_4 -64 -incr "+incdir+../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source" "+incdir+../../../ipstatic/hdl/verilog" "+incdir+/opt/Xilinx/Vivado/2019.2/data/xilinx_vip/include" \
"../../../ipstatic/simulation/blk_mem_gen_v8_4.v" \

vlog -work xil_defaultlib -64 -incr "+incdir+../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source" "+incdir+../../../ipstatic/hdl/verilog" "+incdir+/opt/Xilinx/Vivado/2019.2/data/xilinx_vip/include" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_1/sim/xdma_v4_1_4_blk_mem_64_reg_be.v" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_2/sim/xdma_v4_1_4_blk_mem_64_noreg_be.v" \

vlog -work xdma_v4_1_4 -64 -incr -sv -L xilinx_vip "+incdir+../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source" "+incdir+../../../ipstatic/hdl/verilog" "+incdir+/opt/Xilinx/Vivado/2019.2/data/xilinx_vip/include" \
"../../../ipstatic/hdl/xdma_v4_1_vl_rfs.sv" \

vlog -work xil_defaultlib -64 -incr -sv -L xilinx_vip "+incdir+../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/ip_0/source" "+incdir+../../../ipstatic/hdl/verilog" "+incdir+/opt/Xilinx/Vivado/2019.2/data/xilinx_vip/include" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/xdma_v4_1/hdl/verilog/xdma_0_dma_bram_wrap.sv" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/xdma_v4_1/hdl/verilog/xdma_0_dma_bram_wrap_1024.sv" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/xdma_v4_1/hdl/verilog/xdma_0_core_top.sv" \
"../../../../vcu128-scouting-156-25.srcs/sources_1/ip/xdma_0/sim/xdma_0.sv" \

vlog -work xil_defaultlib \
"glbl.v"

