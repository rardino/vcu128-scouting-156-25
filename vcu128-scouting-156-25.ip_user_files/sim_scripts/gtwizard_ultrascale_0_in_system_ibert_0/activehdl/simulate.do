onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+gtwizard_ultrascale_0_in_system_ibert_0 -L xpm -L in_system_ibert_v1_0_10 -L xil_defaultlib -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.gtwizard_ultrascale_0_in_system_ibert_0 xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {gtwizard_ultrascale_0_in_system_ibert_0.udo}

run -all

endsim

quit -force
