vlib work
vlib activehdl

vlib activehdl/xpm
vlib activehdl/in_system_ibert_v1_0_10
vlib activehdl/xil_defaultlib

vmap xpm activehdl/xpm
vmap in_system_ibert_v1_0_10 activehdl/in_system_ibert_v1_0_10
vmap xil_defaultlib activehdl/xil_defaultlib

vlog -work xpm  -sv2k12 "+incdir+../../../../vcu128-scouting.srcs/sources_1/ip/gtwizard_ultrascale_0_in_system_ibert_0/hdl/verilog" \
"/opt/Xilinx/Vivado/2019.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/opt/Xilinx/Vivado/2019.2/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv" \
"/opt/Xilinx/Vivado/2019.2/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -93 \
"/home/opt/Xilinx/Vivado/2019.2/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work in_system_ibert_v1_0_10  -v2k5 "+incdir+../../../../vcu128-scouting.srcs/sources_1/ip/gtwizard_ultrascale_0_in_system_ibert_0/hdl/verilog" \
"../../../ipstatic/hdl/in_system_ibert_v1_0_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../vcu128-scouting.srcs/sources_1/ip/gtwizard_ultrascale_0_in_system_ibert_0/hdl/verilog" \
"../../../../vcu128-scouting.srcs/sources_1/ip/gtwizard_ultrascale_0_in_system_ibert_0/sim/gtwizard_ultrascale_0_in_system_ibert_0.v" \

vlog -work xil_defaultlib \
"glbl.v"

