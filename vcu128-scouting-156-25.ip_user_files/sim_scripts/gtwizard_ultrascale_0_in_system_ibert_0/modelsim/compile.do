vlib modelsim_lib/work
vlib modelsim_lib/msim

vlib modelsim_lib/msim/xpm
vlib modelsim_lib/msim/in_system_ibert_v1_0_10
vlib modelsim_lib/msim/xil_defaultlib

vmap xpm modelsim_lib/msim/xpm
vmap in_system_ibert_v1_0_10 modelsim_lib/msim/in_system_ibert_v1_0_10
vmap xil_defaultlib modelsim_lib/msim/xil_defaultlib

vlog -work xpm -64 -incr -sv "+incdir+../../../../vcu128-scouting.srcs/sources_1/ip/gtwizard_ultrascale_0_in_system_ibert_0/hdl/verilog" \
"/opt/Xilinx/Vivado/2019.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/opt/Xilinx/Vivado/2019.2/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv" \
"/opt/Xilinx/Vivado/2019.2/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -64 -93 \
"/home/opt/Xilinx/Vivado/2019.2/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work in_system_ibert_v1_0_10 -64 -incr "+incdir+../../../../vcu128-scouting.srcs/sources_1/ip/gtwizard_ultrascale_0_in_system_ibert_0/hdl/verilog" \
"../../../ipstatic/hdl/in_system_ibert_v1_0_rfs.v" \

vlog -work xil_defaultlib -64 -incr "+incdir+../../../../vcu128-scouting.srcs/sources_1/ip/gtwizard_ultrascale_0_in_system_ibert_0/hdl/verilog" \
"../../../../vcu128-scouting.srcs/sources_1/ip/gtwizard_ultrascale_0_in_system_ibert_0/sim/gtwizard_ultrascale_0_in_system_ibert_0.v" \

vlog -work xil_defaultlib \
"glbl.v"

