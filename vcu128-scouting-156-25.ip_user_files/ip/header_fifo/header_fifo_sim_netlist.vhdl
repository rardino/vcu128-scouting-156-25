-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Thu Dec  2 11:17:53 2021
-- Host        : daqlab40-skylake16 running 64-bit CentOS Linux release 7.9.2009 (Core)
-- Command     : write_vhdl -force -mode funcsim -rename_top header_fifo -prefix
--               header_fifo_ header_fifo_sim_netlist.vhdl
-- Design      : header_fifo
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcvu37p-fsvh2892-2L-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo_xpm_cdc_async_rst is
  port (
    src_arst : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_arst : out STD_LOGIC
  );
  attribute DEF_VAL : string;
  attribute DEF_VAL of header_fifo_xpm_cdc_async_rst : entity is "1'b0";
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of header_fifo_xpm_cdc_async_rst : entity is 2;
  attribute INIT_SYNC_FF : integer;
  attribute INIT_SYNC_FF of header_fifo_xpm_cdc_async_rst : entity is 0;
  attribute INV_DEF_VAL : string;
  attribute INV_DEF_VAL of header_fifo_xpm_cdc_async_rst : entity is "1'b1";
  attribute RST_ACTIVE_HIGH : integer;
  attribute RST_ACTIVE_HIGH of header_fifo_xpm_cdc_async_rst : entity is 1;
  attribute VERSION : integer;
  attribute VERSION of header_fifo_xpm_cdc_async_rst : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of header_fifo_xpm_cdc_async_rst : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of header_fifo_xpm_cdc_async_rst : entity is "ASYNC_RST";
end header_fifo_xpm_cdc_async_rst;

architecture STRUCTURE of header_fifo_xpm_cdc_async_rst is
  signal arststages_ff : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of arststages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of arststages_ff : signal is "true";
  attribute xpm_cdc of arststages_ff : signal is "ASYNC_RST";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \arststages_ff_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \arststages_ff_reg[0]\ : label is "true";
  attribute XPM_CDC of \arststages_ff_reg[0]\ : label is "ASYNC_RST";
  attribute ASYNC_REG_boolean of \arststages_ff_reg[1]\ : label is std.standard.true;
  attribute KEEP of \arststages_ff_reg[1]\ : label is "true";
  attribute XPM_CDC of \arststages_ff_reg[1]\ : label is "ASYNC_RST";
begin
  dest_arst <= arststages_ff(1);
\arststages_ff_reg[0]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '0'
    )
        port map (
      C => dest_clk,
      CE => '1',
      D => '0',
      PRE => src_arst,
      Q => arststages_ff(0)
    );
\arststages_ff_reg[1]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '0'
    )
        port map (
      C => dest_clk,
      CE => '1',
      D => arststages_ff(0),
      PRE => src_arst,
      Q => arststages_ff(1)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \header_fifo_xpm_cdc_async_rst__1\ is
  port (
    src_arst : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_arst : out STD_LOGIC
  );
  attribute DEF_VAL : string;
  attribute DEF_VAL of \header_fifo_xpm_cdc_async_rst__1\ : entity is "1'b0";
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \header_fifo_xpm_cdc_async_rst__1\ : entity is 2;
  attribute INIT_SYNC_FF : integer;
  attribute INIT_SYNC_FF of \header_fifo_xpm_cdc_async_rst__1\ : entity is 0;
  attribute INV_DEF_VAL : string;
  attribute INV_DEF_VAL of \header_fifo_xpm_cdc_async_rst__1\ : entity is "1'b1";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \header_fifo_xpm_cdc_async_rst__1\ : entity is "xpm_cdc_async_rst";
  attribute RST_ACTIVE_HIGH : integer;
  attribute RST_ACTIVE_HIGH of \header_fifo_xpm_cdc_async_rst__1\ : entity is 1;
  attribute VERSION : integer;
  attribute VERSION of \header_fifo_xpm_cdc_async_rst__1\ : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \header_fifo_xpm_cdc_async_rst__1\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \header_fifo_xpm_cdc_async_rst__1\ : entity is "ASYNC_RST";
end \header_fifo_xpm_cdc_async_rst__1\;

architecture STRUCTURE of \header_fifo_xpm_cdc_async_rst__1\ is
  signal arststages_ff : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of arststages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of arststages_ff : signal is "true";
  attribute xpm_cdc of arststages_ff : signal is "ASYNC_RST";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \arststages_ff_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \arststages_ff_reg[0]\ : label is "true";
  attribute XPM_CDC of \arststages_ff_reg[0]\ : label is "ASYNC_RST";
  attribute ASYNC_REG_boolean of \arststages_ff_reg[1]\ : label is std.standard.true;
  attribute KEEP of \arststages_ff_reg[1]\ : label is "true";
  attribute XPM_CDC of \arststages_ff_reg[1]\ : label is "ASYNC_RST";
begin
  dest_arst <= arststages_ff(1);
\arststages_ff_reg[0]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '0'
    )
        port map (
      C => dest_clk,
      CE => '1',
      D => '0',
      PRE => src_arst,
      Q => arststages_ff(0)
    );
\arststages_ff_reg[1]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '0'
    )
        port map (
      C => dest_clk,
      CE => '1',
      D => arststages_ff(0),
      PRE => src_arst,
      Q => arststages_ff(1)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo_xpm_cdc_gray is
  port (
    src_clk : in STD_LOGIC;
    src_in_bin : in STD_LOGIC_VECTOR ( 5 downto 0 );
    dest_clk : in STD_LOGIC;
    dest_out_bin : out STD_LOGIC_VECTOR ( 5 downto 0 )
  );
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of header_fifo_xpm_cdc_gray : entity is 2;
  attribute INIT_SYNC_FF : integer;
  attribute INIT_SYNC_FF of header_fifo_xpm_cdc_gray : entity is 0;
  attribute REG_OUTPUT : integer;
  attribute REG_OUTPUT of header_fifo_xpm_cdc_gray : entity is 1;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of header_fifo_xpm_cdc_gray : entity is 0;
  attribute SIM_LOSSLESS_GRAY_CHK : integer;
  attribute SIM_LOSSLESS_GRAY_CHK of header_fifo_xpm_cdc_gray : entity is 0;
  attribute VERSION : integer;
  attribute VERSION of header_fifo_xpm_cdc_gray : entity is 0;
  attribute WIDTH : integer;
  attribute WIDTH of header_fifo_xpm_cdc_gray : entity is 6;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of header_fifo_xpm_cdc_gray : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of header_fifo_xpm_cdc_gray : entity is "GRAY";
end header_fifo_xpm_cdc_gray;

architecture STRUCTURE of header_fifo_xpm_cdc_gray is
  signal async_path : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal binval : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \dest_graysync_ff[0]\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of \dest_graysync_ff[0]\ : signal is "true";
  attribute async_reg : string;
  attribute async_reg of \dest_graysync_ff[0]\ : signal is "true";
  attribute xpm_cdc of \dest_graysync_ff[0]\ : signal is "GRAY";
  signal \dest_graysync_ff[1]\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  attribute RTL_KEEP of \dest_graysync_ff[1]\ : signal is "true";
  attribute async_reg of \dest_graysync_ff[1]\ : signal is "true";
  attribute xpm_cdc of \dest_graysync_ff[1]\ : signal is "GRAY";
  signal gray_enc : STD_LOGIC_VECTOR ( 4 downto 0 );
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[0][0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \dest_graysync_ff_reg[0][0]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[0][0]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[0][1]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[0][1]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[0][1]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[0][2]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[0][2]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[0][2]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[0][3]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[0][3]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[0][3]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[0][4]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[0][4]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[0][4]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[0][5]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[0][5]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[0][5]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[1][0]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[1][0]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[1][0]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[1][1]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[1][1]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[1][1]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[1][2]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[1][2]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[1][2]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[1][3]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[1][3]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[1][3]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[1][4]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[1][4]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[1][4]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[1][5]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[1][5]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[1][5]\ : label is "GRAY";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \src_gray_ff[0]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \src_gray_ff[1]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \src_gray_ff[2]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \src_gray_ff[3]_i_1\ : label is "soft_lutpair3";
begin
\dest_graysync_ff_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => async_path(0),
      Q => \dest_graysync_ff[0]\(0),
      R => '0'
    );
\dest_graysync_ff_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => async_path(1),
      Q => \dest_graysync_ff[0]\(1),
      R => '0'
    );
\dest_graysync_ff_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => async_path(2),
      Q => \dest_graysync_ff[0]\(2),
      R => '0'
    );
\dest_graysync_ff_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => async_path(3),
      Q => \dest_graysync_ff[0]\(3),
      R => '0'
    );
\dest_graysync_ff_reg[0][4]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => async_path(4),
      Q => \dest_graysync_ff[0]\(4),
      R => '0'
    );
\dest_graysync_ff_reg[0][5]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => async_path(5),
      Q => \dest_graysync_ff[0]\(5),
      R => '0'
    );
\dest_graysync_ff_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => \dest_graysync_ff[0]\(0),
      Q => \dest_graysync_ff[1]\(0),
      R => '0'
    );
\dest_graysync_ff_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => \dest_graysync_ff[0]\(1),
      Q => \dest_graysync_ff[1]\(1),
      R => '0'
    );
\dest_graysync_ff_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => \dest_graysync_ff[0]\(2),
      Q => \dest_graysync_ff[1]\(2),
      R => '0'
    );
\dest_graysync_ff_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => \dest_graysync_ff[0]\(3),
      Q => \dest_graysync_ff[1]\(3),
      R => '0'
    );
\dest_graysync_ff_reg[1][4]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => \dest_graysync_ff[0]\(4),
      Q => \dest_graysync_ff[1]\(4),
      R => '0'
    );
\dest_graysync_ff_reg[1][5]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => \dest_graysync_ff[0]\(5),
      Q => \dest_graysync_ff[1]\(5),
      R => '0'
    );
\dest_out_bin_ff[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996966996696996"
    )
        port map (
      I0 => \dest_graysync_ff[1]\(0),
      I1 => \dest_graysync_ff[1]\(2),
      I2 => \dest_graysync_ff[1]\(4),
      I3 => \dest_graysync_ff[1]\(5),
      I4 => \dest_graysync_ff[1]\(3),
      I5 => \dest_graysync_ff[1]\(1),
      O => binval(0)
    );
\dest_out_bin_ff[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => \dest_graysync_ff[1]\(1),
      I1 => \dest_graysync_ff[1]\(3),
      I2 => \dest_graysync_ff[1]\(5),
      I3 => \dest_graysync_ff[1]\(4),
      I4 => \dest_graysync_ff[1]\(2),
      O => binval(1)
    );
\dest_out_bin_ff[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \dest_graysync_ff[1]\(2),
      I1 => \dest_graysync_ff[1]\(4),
      I2 => \dest_graysync_ff[1]\(5),
      I3 => \dest_graysync_ff[1]\(3),
      O => binval(2)
    );
\dest_out_bin_ff[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \dest_graysync_ff[1]\(3),
      I1 => \dest_graysync_ff[1]\(5),
      I2 => \dest_graysync_ff[1]\(4),
      O => binval(3)
    );
\dest_out_bin_ff[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \dest_graysync_ff[1]\(4),
      I1 => \dest_graysync_ff[1]\(5),
      O => binval(4)
    );
\dest_out_bin_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => binval(0),
      Q => dest_out_bin(0),
      R => '0'
    );
\dest_out_bin_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => binval(1),
      Q => dest_out_bin(1),
      R => '0'
    );
\dest_out_bin_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => binval(2),
      Q => dest_out_bin(2),
      R => '0'
    );
\dest_out_bin_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => binval(3),
      Q => dest_out_bin(3),
      R => '0'
    );
\dest_out_bin_ff_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => binval(4),
      Q => dest_out_bin(4),
      R => '0'
    );
\dest_out_bin_ff_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => \dest_graysync_ff[1]\(5),
      Q => dest_out_bin(5),
      R => '0'
    );
\src_gray_ff[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => src_in_bin(1),
      I1 => src_in_bin(0),
      O => gray_enc(0)
    );
\src_gray_ff[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => src_in_bin(2),
      I1 => src_in_bin(1),
      O => gray_enc(1)
    );
\src_gray_ff[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => src_in_bin(3),
      I1 => src_in_bin(2),
      O => gray_enc(2)
    );
\src_gray_ff[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => src_in_bin(4),
      I1 => src_in_bin(3),
      O => gray_enc(3)
    );
\src_gray_ff[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => src_in_bin(5),
      I1 => src_in_bin(4),
      O => gray_enc(4)
    );
\src_gray_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => src_clk,
      CE => '1',
      D => gray_enc(0),
      Q => async_path(0),
      R => '0'
    );
\src_gray_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => src_clk,
      CE => '1',
      D => gray_enc(1),
      Q => async_path(1),
      R => '0'
    );
\src_gray_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => src_clk,
      CE => '1',
      D => gray_enc(2),
      Q => async_path(2),
      R => '0'
    );
\src_gray_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => src_clk,
      CE => '1',
      D => gray_enc(3),
      Q => async_path(3),
      R => '0'
    );
\src_gray_ff_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => src_clk,
      CE => '1',
      D => gray_enc(4),
      Q => async_path(4),
      R => '0'
    );
\src_gray_ff_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => src_clk,
      CE => '1',
      D => src_in_bin(5),
      Q => async_path(5),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \header_fifo_xpm_cdc_gray__2\ is
  port (
    src_clk : in STD_LOGIC;
    src_in_bin : in STD_LOGIC_VECTOR ( 5 downto 0 );
    dest_clk : in STD_LOGIC;
    dest_out_bin : out STD_LOGIC_VECTOR ( 5 downto 0 )
  );
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \header_fifo_xpm_cdc_gray__2\ : entity is 2;
  attribute INIT_SYNC_FF : integer;
  attribute INIT_SYNC_FF of \header_fifo_xpm_cdc_gray__2\ : entity is 0;
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \header_fifo_xpm_cdc_gray__2\ : entity is "xpm_cdc_gray";
  attribute REG_OUTPUT : integer;
  attribute REG_OUTPUT of \header_fifo_xpm_cdc_gray__2\ : entity is 1;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of \header_fifo_xpm_cdc_gray__2\ : entity is 0;
  attribute SIM_LOSSLESS_GRAY_CHK : integer;
  attribute SIM_LOSSLESS_GRAY_CHK of \header_fifo_xpm_cdc_gray__2\ : entity is 0;
  attribute VERSION : integer;
  attribute VERSION of \header_fifo_xpm_cdc_gray__2\ : entity is 0;
  attribute WIDTH : integer;
  attribute WIDTH of \header_fifo_xpm_cdc_gray__2\ : entity is 6;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \header_fifo_xpm_cdc_gray__2\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \header_fifo_xpm_cdc_gray__2\ : entity is "GRAY";
end \header_fifo_xpm_cdc_gray__2\;

architecture STRUCTURE of \header_fifo_xpm_cdc_gray__2\ is
  signal async_path : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal binval : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \dest_graysync_ff[0]\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of \dest_graysync_ff[0]\ : signal is "true";
  attribute async_reg : string;
  attribute async_reg of \dest_graysync_ff[0]\ : signal is "true";
  attribute xpm_cdc of \dest_graysync_ff[0]\ : signal is "GRAY";
  signal \dest_graysync_ff[1]\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  attribute RTL_KEEP of \dest_graysync_ff[1]\ : signal is "true";
  attribute async_reg of \dest_graysync_ff[1]\ : signal is "true";
  attribute xpm_cdc of \dest_graysync_ff[1]\ : signal is "GRAY";
  signal gray_enc : STD_LOGIC_VECTOR ( 4 downto 0 );
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[0][0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \dest_graysync_ff_reg[0][0]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[0][0]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[0][1]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[0][1]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[0][1]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[0][2]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[0][2]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[0][2]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[0][3]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[0][3]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[0][3]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[0][4]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[0][4]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[0][4]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[0][5]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[0][5]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[0][5]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[1][0]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[1][0]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[1][0]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[1][1]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[1][1]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[1][1]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[1][2]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[1][2]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[1][2]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[1][3]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[1][3]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[1][3]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[1][4]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[1][4]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[1][4]\ : label is "GRAY";
  attribute ASYNC_REG_boolean of \dest_graysync_ff_reg[1][5]\ : label is std.standard.true;
  attribute KEEP of \dest_graysync_ff_reg[1][5]\ : label is "true";
  attribute XPM_CDC of \dest_graysync_ff_reg[1][5]\ : label is "GRAY";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \src_gray_ff[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \src_gray_ff[1]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \src_gray_ff[2]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \src_gray_ff[3]_i_1\ : label is "soft_lutpair1";
begin
\dest_graysync_ff_reg[0][0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => async_path(0),
      Q => \dest_graysync_ff[0]\(0),
      R => '0'
    );
\dest_graysync_ff_reg[0][1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => async_path(1),
      Q => \dest_graysync_ff[0]\(1),
      R => '0'
    );
\dest_graysync_ff_reg[0][2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => async_path(2),
      Q => \dest_graysync_ff[0]\(2),
      R => '0'
    );
\dest_graysync_ff_reg[0][3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => async_path(3),
      Q => \dest_graysync_ff[0]\(3),
      R => '0'
    );
\dest_graysync_ff_reg[0][4]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => async_path(4),
      Q => \dest_graysync_ff[0]\(4),
      R => '0'
    );
\dest_graysync_ff_reg[0][5]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => async_path(5),
      Q => \dest_graysync_ff[0]\(5),
      R => '0'
    );
\dest_graysync_ff_reg[1][0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => \dest_graysync_ff[0]\(0),
      Q => \dest_graysync_ff[1]\(0),
      R => '0'
    );
\dest_graysync_ff_reg[1][1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => \dest_graysync_ff[0]\(1),
      Q => \dest_graysync_ff[1]\(1),
      R => '0'
    );
\dest_graysync_ff_reg[1][2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => \dest_graysync_ff[0]\(2),
      Q => \dest_graysync_ff[1]\(2),
      R => '0'
    );
\dest_graysync_ff_reg[1][3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => \dest_graysync_ff[0]\(3),
      Q => \dest_graysync_ff[1]\(3),
      R => '0'
    );
\dest_graysync_ff_reg[1][4]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => \dest_graysync_ff[0]\(4),
      Q => \dest_graysync_ff[1]\(4),
      R => '0'
    );
\dest_graysync_ff_reg[1][5]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => \dest_graysync_ff[0]\(5),
      Q => \dest_graysync_ff[1]\(5),
      R => '0'
    );
\dest_out_bin_ff[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996966996696996"
    )
        port map (
      I0 => \dest_graysync_ff[1]\(0),
      I1 => \dest_graysync_ff[1]\(2),
      I2 => \dest_graysync_ff[1]\(4),
      I3 => \dest_graysync_ff[1]\(5),
      I4 => \dest_graysync_ff[1]\(3),
      I5 => \dest_graysync_ff[1]\(1),
      O => binval(0)
    );
\dest_out_bin_ff[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => \dest_graysync_ff[1]\(1),
      I1 => \dest_graysync_ff[1]\(3),
      I2 => \dest_graysync_ff[1]\(5),
      I3 => \dest_graysync_ff[1]\(4),
      I4 => \dest_graysync_ff[1]\(2),
      O => binval(1)
    );
\dest_out_bin_ff[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \dest_graysync_ff[1]\(2),
      I1 => \dest_graysync_ff[1]\(4),
      I2 => \dest_graysync_ff[1]\(5),
      I3 => \dest_graysync_ff[1]\(3),
      O => binval(2)
    );
\dest_out_bin_ff[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \dest_graysync_ff[1]\(3),
      I1 => \dest_graysync_ff[1]\(5),
      I2 => \dest_graysync_ff[1]\(4),
      O => binval(3)
    );
\dest_out_bin_ff[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \dest_graysync_ff[1]\(4),
      I1 => \dest_graysync_ff[1]\(5),
      O => binval(4)
    );
\dest_out_bin_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => binval(0),
      Q => dest_out_bin(0),
      R => '0'
    );
\dest_out_bin_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => binval(1),
      Q => dest_out_bin(1),
      R => '0'
    );
\dest_out_bin_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => binval(2),
      Q => dest_out_bin(2),
      R => '0'
    );
\dest_out_bin_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => binval(3),
      Q => dest_out_bin(3),
      R => '0'
    );
\dest_out_bin_ff_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => binval(4),
      Q => dest_out_bin(4),
      R => '0'
    );
\dest_out_bin_ff_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => \dest_graysync_ff[1]\(5),
      Q => dest_out_bin(5),
      R => '0'
    );
\src_gray_ff[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => src_in_bin(1),
      I1 => src_in_bin(0),
      O => gray_enc(0)
    );
\src_gray_ff[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => src_in_bin(2),
      I1 => src_in_bin(1),
      O => gray_enc(1)
    );
\src_gray_ff[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => src_in_bin(3),
      I1 => src_in_bin(2),
      O => gray_enc(2)
    );
\src_gray_ff[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => src_in_bin(4),
      I1 => src_in_bin(3),
      O => gray_enc(3)
    );
\src_gray_ff[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => src_in_bin(5),
      I1 => src_in_bin(4),
      O => gray_enc(4)
    );
\src_gray_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => src_clk,
      CE => '1',
      D => gray_enc(0),
      Q => async_path(0),
      R => '0'
    );
\src_gray_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => src_clk,
      CE => '1',
      D => gray_enc(1),
      Q => async_path(1),
      R => '0'
    );
\src_gray_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => src_clk,
      CE => '1',
      D => gray_enc(2),
      Q => async_path(2),
      R => '0'
    );
\src_gray_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => src_clk,
      CE => '1',
      D => gray_enc(3),
      Q => async_path(3),
      R => '0'
    );
\src_gray_ff_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => src_clk,
      CE => '1',
      D => gray_enc(4),
      Q => async_path(4),
      R => '0'
    );
\src_gray_ff_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => src_clk,
      CE => '1',
      D => src_in_bin(5),
      Q => async_path(5),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo_xpm_cdc_single is
  port (
    src_clk : in STD_LOGIC;
    src_in : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_out : out STD_LOGIC
  );
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of header_fifo_xpm_cdc_single : entity is 4;
  attribute INIT_SYNC_FF : integer;
  attribute INIT_SYNC_FF of header_fifo_xpm_cdc_single : entity is 0;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of header_fifo_xpm_cdc_single : entity is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of header_fifo_xpm_cdc_single : entity is 0;
  attribute VERSION : integer;
  attribute VERSION of header_fifo_xpm_cdc_single : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of header_fifo_xpm_cdc_single : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of header_fifo_xpm_cdc_single : entity is "SINGLE";
end header_fifo_xpm_cdc_single;

architecture STRUCTURE of header_fifo_xpm_cdc_single is
  signal syncstages_ff : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of syncstages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of syncstages_ff : signal is "true";
  attribute xpm_cdc of syncstages_ff : signal is "SINGLE";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \syncstages_ff_reg[0]\ : label is "true";
  attribute XPM_CDC of \syncstages_ff_reg[0]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[1]\ : label is std.standard.true;
  attribute KEEP of \syncstages_ff_reg[1]\ : label is "true";
  attribute XPM_CDC of \syncstages_ff_reg[1]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[2]\ : label is std.standard.true;
  attribute KEEP of \syncstages_ff_reg[2]\ : label is "true";
  attribute XPM_CDC of \syncstages_ff_reg[2]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[3]\ : label is std.standard.true;
  attribute KEEP of \syncstages_ff_reg[3]\ : label is "true";
  attribute XPM_CDC of \syncstages_ff_reg[3]\ : label is "SINGLE";
begin
  dest_out <= syncstages_ff(3);
\syncstages_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => src_in,
      Q => syncstages_ff(0),
      R => '0'
    );
\syncstages_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(0),
      Q => syncstages_ff(1),
      R => '0'
    );
\syncstages_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(1),
      Q => syncstages_ff(2),
      R => '0'
    );
\syncstages_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(2),
      Q => syncstages_ff(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \header_fifo_xpm_cdc_single__2\ is
  port (
    src_clk : in STD_LOGIC;
    src_in : in STD_LOGIC;
    dest_clk : in STD_LOGIC;
    dest_out : out STD_LOGIC
  );
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \header_fifo_xpm_cdc_single__2\ : entity is 4;
  attribute INIT_SYNC_FF : integer;
  attribute INIT_SYNC_FF of \header_fifo_xpm_cdc_single__2\ : entity is 0;
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \header_fifo_xpm_cdc_single__2\ : entity is "xpm_cdc_single";
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of \header_fifo_xpm_cdc_single__2\ : entity is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of \header_fifo_xpm_cdc_single__2\ : entity is 0;
  attribute VERSION : integer;
  attribute VERSION of \header_fifo_xpm_cdc_single__2\ : entity is 0;
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \header_fifo_xpm_cdc_single__2\ : entity is "TRUE";
  attribute xpm_cdc : string;
  attribute xpm_cdc of \header_fifo_xpm_cdc_single__2\ : entity is "SINGLE";
end \header_fifo_xpm_cdc_single__2\;

architecture STRUCTURE of \header_fifo_xpm_cdc_single__2\ is
  signal syncstages_ff : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of syncstages_ff : signal is "true";
  attribute async_reg : string;
  attribute async_reg of syncstages_ff : signal is "true";
  attribute xpm_cdc of syncstages_ff : signal is "SINGLE";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \syncstages_ff_reg[0]\ : label is "true";
  attribute XPM_CDC of \syncstages_ff_reg[0]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[1]\ : label is std.standard.true;
  attribute KEEP of \syncstages_ff_reg[1]\ : label is "true";
  attribute XPM_CDC of \syncstages_ff_reg[1]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[2]\ : label is std.standard.true;
  attribute KEEP of \syncstages_ff_reg[2]\ : label is "true";
  attribute XPM_CDC of \syncstages_ff_reg[2]\ : label is "SINGLE";
  attribute ASYNC_REG_boolean of \syncstages_ff_reg[3]\ : label is std.standard.true;
  attribute KEEP of \syncstages_ff_reg[3]\ : label is "true";
  attribute XPM_CDC of \syncstages_ff_reg[3]\ : label is "SINGLE";
begin
  dest_out <= syncstages_ff(3);
\syncstages_ff_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => src_in,
      Q => syncstages_ff(0),
      R => '0'
    );
\syncstages_ff_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(0),
      Q => syncstages_ff(1),
      R => '0'
    );
\syncstages_ff_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(1),
      Q => syncstages_ff(2),
      R => '0'
    );
\syncstages_ff_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => dest_clk,
      CE => '1',
      D => syncstages_ff(2),
      Q => syncstages_ff(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo_dmem is
  port (
    \gpr1.dout_i_reg[255]_0\ : out STD_LOGIC_VECTOR ( 255 downto 0 );
    wr_clk : in STD_LOGIC;
    din : in STD_LOGIC_VECTOR ( 255 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 5 downto 0 );
    \gpr1.dout_i_reg[0]_0\ : in STD_LOGIC_VECTOR ( 5 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \gpr1.dout_i_reg[0]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    rd_clk : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end header_fifo_dmem;

architecture STRUCTURE of header_fifo_dmem is
  signal dout_i0 : STD_LOGIC_VECTOR ( 255 downto 0 );
  signal NLW_RAM_reg_0_63_0_6_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_105_111_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_112_118_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_119_125_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_126_132_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_133_139_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_140_146_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_147_153_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_14_20_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_154_160_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_161_167_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_168_174_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_175_181_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_182_188_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_189_195_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_196_202_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_203_209_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_210_216_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_217_223_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_21_27_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_224_230_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_231_237_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_238_244_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_245_251_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_252_255_DOE_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_252_255_DOF_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_252_255_DOG_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_252_255_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_28_34_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_35_41_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_42_48_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_49_55_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_56_62_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_63_69_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_70_76_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_77_83_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_7_13_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_84_90_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_91_97_DOH_UNCONNECTED : STD_LOGIC;
  signal NLW_RAM_reg_0_63_98_104_DOH_UNCONNECTED : STD_LOGIC;
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_0_6 : label is "";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of RAM_reg_0_63_0_6 : label is 16384;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of RAM_reg_0_63_0_6 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of RAM_reg_0_63_0_6 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of RAM_reg_0_63_0_6 : label is 63;
  attribute ram_offset : integer;
  attribute ram_offset of RAM_reg_0_63_0_6 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of RAM_reg_0_63_0_6 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of RAM_reg_0_63_0_6 : label is 6;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_105_111 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_105_111 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_105_111 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_105_111 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_105_111 : label is 63;
  attribute ram_offset of RAM_reg_0_63_105_111 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_105_111 : label is 105;
  attribute ram_slice_end of RAM_reg_0_63_105_111 : label is 111;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_112_118 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_112_118 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_112_118 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_112_118 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_112_118 : label is 63;
  attribute ram_offset of RAM_reg_0_63_112_118 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_112_118 : label is 112;
  attribute ram_slice_end of RAM_reg_0_63_112_118 : label is 118;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_119_125 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_119_125 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_119_125 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_119_125 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_119_125 : label is 63;
  attribute ram_offset of RAM_reg_0_63_119_125 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_119_125 : label is 119;
  attribute ram_slice_end of RAM_reg_0_63_119_125 : label is 125;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_126_132 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_126_132 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_126_132 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_126_132 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_126_132 : label is 63;
  attribute ram_offset of RAM_reg_0_63_126_132 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_126_132 : label is 126;
  attribute ram_slice_end of RAM_reg_0_63_126_132 : label is 132;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_133_139 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_133_139 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_133_139 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_133_139 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_133_139 : label is 63;
  attribute ram_offset of RAM_reg_0_63_133_139 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_133_139 : label is 133;
  attribute ram_slice_end of RAM_reg_0_63_133_139 : label is 139;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_140_146 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_140_146 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_140_146 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_140_146 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_140_146 : label is 63;
  attribute ram_offset of RAM_reg_0_63_140_146 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_140_146 : label is 140;
  attribute ram_slice_end of RAM_reg_0_63_140_146 : label is 146;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_147_153 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_147_153 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_147_153 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_147_153 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_147_153 : label is 63;
  attribute ram_offset of RAM_reg_0_63_147_153 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_147_153 : label is 147;
  attribute ram_slice_end of RAM_reg_0_63_147_153 : label is 153;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_14_20 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_14_20 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_14_20 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_14_20 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_14_20 : label is 63;
  attribute ram_offset of RAM_reg_0_63_14_20 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_14_20 : label is 14;
  attribute ram_slice_end of RAM_reg_0_63_14_20 : label is 20;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_154_160 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_154_160 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_154_160 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_154_160 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_154_160 : label is 63;
  attribute ram_offset of RAM_reg_0_63_154_160 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_154_160 : label is 154;
  attribute ram_slice_end of RAM_reg_0_63_154_160 : label is 160;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_161_167 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_161_167 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_161_167 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_161_167 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_161_167 : label is 63;
  attribute ram_offset of RAM_reg_0_63_161_167 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_161_167 : label is 161;
  attribute ram_slice_end of RAM_reg_0_63_161_167 : label is 167;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_168_174 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_168_174 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_168_174 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_168_174 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_168_174 : label is 63;
  attribute ram_offset of RAM_reg_0_63_168_174 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_168_174 : label is 168;
  attribute ram_slice_end of RAM_reg_0_63_168_174 : label is 174;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_175_181 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_175_181 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_175_181 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_175_181 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_175_181 : label is 63;
  attribute ram_offset of RAM_reg_0_63_175_181 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_175_181 : label is 175;
  attribute ram_slice_end of RAM_reg_0_63_175_181 : label is 181;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_182_188 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_182_188 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_182_188 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_182_188 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_182_188 : label is 63;
  attribute ram_offset of RAM_reg_0_63_182_188 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_182_188 : label is 182;
  attribute ram_slice_end of RAM_reg_0_63_182_188 : label is 188;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_189_195 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_189_195 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_189_195 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_189_195 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_189_195 : label is 63;
  attribute ram_offset of RAM_reg_0_63_189_195 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_189_195 : label is 189;
  attribute ram_slice_end of RAM_reg_0_63_189_195 : label is 195;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_196_202 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_196_202 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_196_202 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_196_202 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_196_202 : label is 63;
  attribute ram_offset of RAM_reg_0_63_196_202 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_196_202 : label is 196;
  attribute ram_slice_end of RAM_reg_0_63_196_202 : label is 202;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_203_209 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_203_209 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_203_209 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_203_209 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_203_209 : label is 63;
  attribute ram_offset of RAM_reg_0_63_203_209 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_203_209 : label is 203;
  attribute ram_slice_end of RAM_reg_0_63_203_209 : label is 209;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_210_216 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_210_216 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_210_216 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_210_216 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_210_216 : label is 63;
  attribute ram_offset of RAM_reg_0_63_210_216 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_210_216 : label is 210;
  attribute ram_slice_end of RAM_reg_0_63_210_216 : label is 216;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_217_223 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_217_223 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_217_223 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_217_223 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_217_223 : label is 63;
  attribute ram_offset of RAM_reg_0_63_217_223 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_217_223 : label is 217;
  attribute ram_slice_end of RAM_reg_0_63_217_223 : label is 223;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_21_27 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_21_27 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_21_27 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_21_27 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_21_27 : label is 63;
  attribute ram_offset of RAM_reg_0_63_21_27 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_21_27 : label is 21;
  attribute ram_slice_end of RAM_reg_0_63_21_27 : label is 27;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_224_230 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_224_230 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_224_230 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_224_230 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_224_230 : label is 63;
  attribute ram_offset of RAM_reg_0_63_224_230 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_224_230 : label is 224;
  attribute ram_slice_end of RAM_reg_0_63_224_230 : label is 230;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_231_237 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_231_237 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_231_237 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_231_237 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_231_237 : label is 63;
  attribute ram_offset of RAM_reg_0_63_231_237 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_231_237 : label is 231;
  attribute ram_slice_end of RAM_reg_0_63_231_237 : label is 237;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_238_244 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_238_244 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_238_244 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_238_244 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_238_244 : label is 63;
  attribute ram_offset of RAM_reg_0_63_238_244 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_238_244 : label is 238;
  attribute ram_slice_end of RAM_reg_0_63_238_244 : label is 244;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_245_251 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_245_251 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_245_251 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_245_251 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_245_251 : label is 63;
  attribute ram_offset of RAM_reg_0_63_245_251 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_245_251 : label is 245;
  attribute ram_slice_end of RAM_reg_0_63_245_251 : label is 251;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_252_255 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_252_255 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_252_255 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_252_255 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_252_255 : label is 63;
  attribute ram_offset of RAM_reg_0_63_252_255 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_252_255 : label is 252;
  attribute ram_slice_end of RAM_reg_0_63_252_255 : label is 255;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_28_34 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_28_34 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_28_34 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_28_34 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_28_34 : label is 63;
  attribute ram_offset of RAM_reg_0_63_28_34 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_28_34 : label is 28;
  attribute ram_slice_end of RAM_reg_0_63_28_34 : label is 34;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_35_41 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_35_41 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_35_41 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_35_41 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_35_41 : label is 63;
  attribute ram_offset of RAM_reg_0_63_35_41 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_35_41 : label is 35;
  attribute ram_slice_end of RAM_reg_0_63_35_41 : label is 41;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_42_48 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_42_48 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_42_48 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_42_48 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_42_48 : label is 63;
  attribute ram_offset of RAM_reg_0_63_42_48 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_42_48 : label is 42;
  attribute ram_slice_end of RAM_reg_0_63_42_48 : label is 48;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_49_55 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_49_55 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_49_55 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_49_55 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_49_55 : label is 63;
  attribute ram_offset of RAM_reg_0_63_49_55 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_49_55 : label is 49;
  attribute ram_slice_end of RAM_reg_0_63_49_55 : label is 55;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_56_62 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_56_62 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_56_62 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_56_62 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_56_62 : label is 63;
  attribute ram_offset of RAM_reg_0_63_56_62 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_56_62 : label is 56;
  attribute ram_slice_end of RAM_reg_0_63_56_62 : label is 62;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_63_69 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_63_69 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_63_69 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_63_69 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_63_69 : label is 63;
  attribute ram_offset of RAM_reg_0_63_63_69 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_63_69 : label is 63;
  attribute ram_slice_end of RAM_reg_0_63_63_69 : label is 69;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_70_76 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_70_76 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_70_76 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_70_76 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_70_76 : label is 63;
  attribute ram_offset of RAM_reg_0_63_70_76 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_70_76 : label is 70;
  attribute ram_slice_end of RAM_reg_0_63_70_76 : label is 76;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_77_83 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_77_83 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_77_83 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_77_83 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_77_83 : label is 63;
  attribute ram_offset of RAM_reg_0_63_77_83 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_77_83 : label is 77;
  attribute ram_slice_end of RAM_reg_0_63_77_83 : label is 83;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_7_13 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_7_13 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_7_13 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_7_13 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_7_13 : label is 63;
  attribute ram_offset of RAM_reg_0_63_7_13 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_7_13 : label is 7;
  attribute ram_slice_end of RAM_reg_0_63_7_13 : label is 13;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_84_90 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_84_90 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_84_90 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_84_90 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_84_90 : label is 63;
  attribute ram_offset of RAM_reg_0_63_84_90 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_84_90 : label is 84;
  attribute ram_slice_end of RAM_reg_0_63_84_90 : label is 90;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_91_97 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_91_97 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_91_97 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_91_97 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_91_97 : label is 63;
  attribute ram_offset of RAM_reg_0_63_91_97 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_91_97 : label is 91;
  attribute ram_slice_end of RAM_reg_0_63_91_97 : label is 97;
  attribute METHODOLOGY_DRC_VIOS of RAM_reg_0_63_98_104 : label is "";
  attribute RTL_RAM_BITS of RAM_reg_0_63_98_104 : label is 16384;
  attribute RTL_RAM_NAME of RAM_reg_0_63_98_104 : label is "inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gdm.dm_gen.dm/RAM";
  attribute ram_addr_begin of RAM_reg_0_63_98_104 : label is 0;
  attribute ram_addr_end of RAM_reg_0_63_98_104 : label is 63;
  attribute ram_offset of RAM_reg_0_63_98_104 : label is 0;
  attribute ram_slice_begin of RAM_reg_0_63_98_104 : label is 98;
  attribute ram_slice_end of RAM_reg_0_63_98_104 : label is 104;
begin
RAM_reg_0_63_0_6: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(0),
      DIB => din(1),
      DIC => din(2),
      DID => din(3),
      DIE => din(4),
      DIF => din(5),
      DIG => din(6),
      DIH => '0',
      DOA => dout_i0(0),
      DOB => dout_i0(1),
      DOC => dout_i0(2),
      DOD => dout_i0(3),
      DOE => dout_i0(4),
      DOF => dout_i0(5),
      DOG => dout_i0(6),
      DOH => NLW_RAM_reg_0_63_0_6_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_105_111: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(105),
      DIB => din(106),
      DIC => din(107),
      DID => din(108),
      DIE => din(109),
      DIF => din(110),
      DIG => din(111),
      DIH => '0',
      DOA => dout_i0(105),
      DOB => dout_i0(106),
      DOC => dout_i0(107),
      DOD => dout_i0(108),
      DOE => dout_i0(109),
      DOF => dout_i0(110),
      DOG => dout_i0(111),
      DOH => NLW_RAM_reg_0_63_105_111_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_112_118: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(112),
      DIB => din(113),
      DIC => din(114),
      DID => din(115),
      DIE => din(116),
      DIF => din(117),
      DIG => din(118),
      DIH => '0',
      DOA => dout_i0(112),
      DOB => dout_i0(113),
      DOC => dout_i0(114),
      DOD => dout_i0(115),
      DOE => dout_i0(116),
      DOF => dout_i0(117),
      DOG => dout_i0(118),
      DOH => NLW_RAM_reg_0_63_112_118_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_119_125: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(119),
      DIB => din(120),
      DIC => din(121),
      DID => din(122),
      DIE => din(123),
      DIF => din(124),
      DIG => din(125),
      DIH => '0',
      DOA => dout_i0(119),
      DOB => dout_i0(120),
      DOC => dout_i0(121),
      DOD => dout_i0(122),
      DOE => dout_i0(123),
      DOF => dout_i0(124),
      DOG => dout_i0(125),
      DOH => NLW_RAM_reg_0_63_119_125_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_126_132: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(126),
      DIB => din(127),
      DIC => din(128),
      DID => din(129),
      DIE => din(130),
      DIF => din(131),
      DIG => din(132),
      DIH => '0',
      DOA => dout_i0(126),
      DOB => dout_i0(127),
      DOC => dout_i0(128),
      DOD => dout_i0(129),
      DOE => dout_i0(130),
      DOF => dout_i0(131),
      DOG => dout_i0(132),
      DOH => NLW_RAM_reg_0_63_126_132_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_133_139: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(133),
      DIB => din(134),
      DIC => din(135),
      DID => din(136),
      DIE => din(137),
      DIF => din(138),
      DIG => din(139),
      DIH => '0',
      DOA => dout_i0(133),
      DOB => dout_i0(134),
      DOC => dout_i0(135),
      DOD => dout_i0(136),
      DOE => dout_i0(137),
      DOF => dout_i0(138),
      DOG => dout_i0(139),
      DOH => NLW_RAM_reg_0_63_133_139_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_140_146: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(140),
      DIB => din(141),
      DIC => din(142),
      DID => din(143),
      DIE => din(144),
      DIF => din(145),
      DIG => din(146),
      DIH => '0',
      DOA => dout_i0(140),
      DOB => dout_i0(141),
      DOC => dout_i0(142),
      DOD => dout_i0(143),
      DOE => dout_i0(144),
      DOF => dout_i0(145),
      DOG => dout_i0(146),
      DOH => NLW_RAM_reg_0_63_140_146_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_147_153: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(147),
      DIB => din(148),
      DIC => din(149),
      DID => din(150),
      DIE => din(151),
      DIF => din(152),
      DIG => din(153),
      DIH => '0',
      DOA => dout_i0(147),
      DOB => dout_i0(148),
      DOC => dout_i0(149),
      DOD => dout_i0(150),
      DOE => dout_i0(151),
      DOF => dout_i0(152),
      DOG => dout_i0(153),
      DOH => NLW_RAM_reg_0_63_147_153_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_14_20: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(14),
      DIB => din(15),
      DIC => din(16),
      DID => din(17),
      DIE => din(18),
      DIF => din(19),
      DIG => din(20),
      DIH => '0',
      DOA => dout_i0(14),
      DOB => dout_i0(15),
      DOC => dout_i0(16),
      DOD => dout_i0(17),
      DOE => dout_i0(18),
      DOF => dout_i0(19),
      DOG => dout_i0(20),
      DOH => NLW_RAM_reg_0_63_14_20_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_154_160: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(154),
      DIB => din(155),
      DIC => din(156),
      DID => din(157),
      DIE => din(158),
      DIF => din(159),
      DIG => din(160),
      DIH => '0',
      DOA => dout_i0(154),
      DOB => dout_i0(155),
      DOC => dout_i0(156),
      DOD => dout_i0(157),
      DOE => dout_i0(158),
      DOF => dout_i0(159),
      DOG => dout_i0(160),
      DOH => NLW_RAM_reg_0_63_154_160_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_161_167: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(161),
      DIB => din(162),
      DIC => din(163),
      DID => din(164),
      DIE => din(165),
      DIF => din(166),
      DIG => din(167),
      DIH => '0',
      DOA => dout_i0(161),
      DOB => dout_i0(162),
      DOC => dout_i0(163),
      DOD => dout_i0(164),
      DOE => dout_i0(165),
      DOF => dout_i0(166),
      DOG => dout_i0(167),
      DOH => NLW_RAM_reg_0_63_161_167_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_168_174: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(168),
      DIB => din(169),
      DIC => din(170),
      DID => din(171),
      DIE => din(172),
      DIF => din(173),
      DIG => din(174),
      DIH => '0',
      DOA => dout_i0(168),
      DOB => dout_i0(169),
      DOC => dout_i0(170),
      DOD => dout_i0(171),
      DOE => dout_i0(172),
      DOF => dout_i0(173),
      DOG => dout_i0(174),
      DOH => NLW_RAM_reg_0_63_168_174_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_175_181: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(175),
      DIB => din(176),
      DIC => din(177),
      DID => din(178),
      DIE => din(179),
      DIF => din(180),
      DIG => din(181),
      DIH => '0',
      DOA => dout_i0(175),
      DOB => dout_i0(176),
      DOC => dout_i0(177),
      DOD => dout_i0(178),
      DOE => dout_i0(179),
      DOF => dout_i0(180),
      DOG => dout_i0(181),
      DOH => NLW_RAM_reg_0_63_175_181_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_182_188: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(182),
      DIB => din(183),
      DIC => din(184),
      DID => din(185),
      DIE => din(186),
      DIF => din(187),
      DIG => din(188),
      DIH => '0',
      DOA => dout_i0(182),
      DOB => dout_i0(183),
      DOC => dout_i0(184),
      DOD => dout_i0(185),
      DOE => dout_i0(186),
      DOF => dout_i0(187),
      DOG => dout_i0(188),
      DOH => NLW_RAM_reg_0_63_182_188_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_189_195: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(189),
      DIB => din(190),
      DIC => din(191),
      DID => din(192),
      DIE => din(193),
      DIF => din(194),
      DIG => din(195),
      DIH => '0',
      DOA => dout_i0(189),
      DOB => dout_i0(190),
      DOC => dout_i0(191),
      DOD => dout_i0(192),
      DOE => dout_i0(193),
      DOF => dout_i0(194),
      DOG => dout_i0(195),
      DOH => NLW_RAM_reg_0_63_189_195_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_196_202: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(196),
      DIB => din(197),
      DIC => din(198),
      DID => din(199),
      DIE => din(200),
      DIF => din(201),
      DIG => din(202),
      DIH => '0',
      DOA => dout_i0(196),
      DOB => dout_i0(197),
      DOC => dout_i0(198),
      DOD => dout_i0(199),
      DOE => dout_i0(200),
      DOF => dout_i0(201),
      DOG => dout_i0(202),
      DOH => NLW_RAM_reg_0_63_196_202_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_203_209: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(203),
      DIB => din(204),
      DIC => din(205),
      DID => din(206),
      DIE => din(207),
      DIF => din(208),
      DIG => din(209),
      DIH => '0',
      DOA => dout_i0(203),
      DOB => dout_i0(204),
      DOC => dout_i0(205),
      DOD => dout_i0(206),
      DOE => dout_i0(207),
      DOF => dout_i0(208),
      DOG => dout_i0(209),
      DOH => NLW_RAM_reg_0_63_203_209_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_210_216: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(210),
      DIB => din(211),
      DIC => din(212),
      DID => din(213),
      DIE => din(214),
      DIF => din(215),
      DIG => din(216),
      DIH => '0',
      DOA => dout_i0(210),
      DOB => dout_i0(211),
      DOC => dout_i0(212),
      DOD => dout_i0(213),
      DOE => dout_i0(214),
      DOF => dout_i0(215),
      DOG => dout_i0(216),
      DOH => NLW_RAM_reg_0_63_210_216_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_217_223: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(217),
      DIB => din(218),
      DIC => din(219),
      DID => din(220),
      DIE => din(221),
      DIF => din(222),
      DIG => din(223),
      DIH => '0',
      DOA => dout_i0(217),
      DOB => dout_i0(218),
      DOC => dout_i0(219),
      DOD => dout_i0(220),
      DOE => dout_i0(221),
      DOF => dout_i0(222),
      DOG => dout_i0(223),
      DOH => NLW_RAM_reg_0_63_217_223_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_21_27: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(21),
      DIB => din(22),
      DIC => din(23),
      DID => din(24),
      DIE => din(25),
      DIF => din(26),
      DIG => din(27),
      DIH => '0',
      DOA => dout_i0(21),
      DOB => dout_i0(22),
      DOC => dout_i0(23),
      DOD => dout_i0(24),
      DOE => dout_i0(25),
      DOF => dout_i0(26),
      DOG => dout_i0(27),
      DOH => NLW_RAM_reg_0_63_21_27_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_224_230: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(224),
      DIB => din(225),
      DIC => din(226),
      DID => din(227),
      DIE => din(228),
      DIF => din(229),
      DIG => din(230),
      DIH => '0',
      DOA => dout_i0(224),
      DOB => dout_i0(225),
      DOC => dout_i0(226),
      DOD => dout_i0(227),
      DOE => dout_i0(228),
      DOF => dout_i0(229),
      DOG => dout_i0(230),
      DOH => NLW_RAM_reg_0_63_224_230_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_231_237: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(231),
      DIB => din(232),
      DIC => din(233),
      DID => din(234),
      DIE => din(235),
      DIF => din(236),
      DIG => din(237),
      DIH => '0',
      DOA => dout_i0(231),
      DOB => dout_i0(232),
      DOC => dout_i0(233),
      DOD => dout_i0(234),
      DOE => dout_i0(235),
      DOF => dout_i0(236),
      DOG => dout_i0(237),
      DOH => NLW_RAM_reg_0_63_231_237_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_238_244: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(238),
      DIB => din(239),
      DIC => din(240),
      DID => din(241),
      DIE => din(242),
      DIF => din(243),
      DIG => din(244),
      DIH => '0',
      DOA => dout_i0(238),
      DOB => dout_i0(239),
      DOC => dout_i0(240),
      DOD => dout_i0(241),
      DOE => dout_i0(242),
      DOF => dout_i0(243),
      DOG => dout_i0(244),
      DOH => NLW_RAM_reg_0_63_238_244_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_245_251: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(245),
      DIB => din(246),
      DIC => din(247),
      DID => din(248),
      DIE => din(249),
      DIF => din(250),
      DIG => din(251),
      DIH => '0',
      DOA => dout_i0(245),
      DOB => dout_i0(246),
      DOC => dout_i0(247),
      DOD => dout_i0(248),
      DOE => dout_i0(249),
      DOF => dout_i0(250),
      DOG => dout_i0(251),
      DOH => NLW_RAM_reg_0_63_245_251_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_252_255: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(252),
      DIB => din(253),
      DIC => din(254),
      DID => din(255),
      DIE => '0',
      DIF => '0',
      DIG => '0',
      DIH => '0',
      DOA => dout_i0(252),
      DOB => dout_i0(253),
      DOC => dout_i0(254),
      DOD => dout_i0(255),
      DOE => NLW_RAM_reg_0_63_252_255_DOE_UNCONNECTED,
      DOF => NLW_RAM_reg_0_63_252_255_DOF_UNCONNECTED,
      DOG => NLW_RAM_reg_0_63_252_255_DOG_UNCONNECTED,
      DOH => NLW_RAM_reg_0_63_252_255_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_28_34: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(28),
      DIB => din(29),
      DIC => din(30),
      DID => din(31),
      DIE => din(32),
      DIF => din(33),
      DIG => din(34),
      DIH => '0',
      DOA => dout_i0(28),
      DOB => dout_i0(29),
      DOC => dout_i0(30),
      DOD => dout_i0(31),
      DOE => dout_i0(32),
      DOF => dout_i0(33),
      DOG => dout_i0(34),
      DOH => NLW_RAM_reg_0_63_28_34_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_35_41: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(35),
      DIB => din(36),
      DIC => din(37),
      DID => din(38),
      DIE => din(39),
      DIF => din(40),
      DIG => din(41),
      DIH => '0',
      DOA => dout_i0(35),
      DOB => dout_i0(36),
      DOC => dout_i0(37),
      DOD => dout_i0(38),
      DOE => dout_i0(39),
      DOF => dout_i0(40),
      DOG => dout_i0(41),
      DOH => NLW_RAM_reg_0_63_35_41_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_42_48: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(42),
      DIB => din(43),
      DIC => din(44),
      DID => din(45),
      DIE => din(46),
      DIF => din(47),
      DIG => din(48),
      DIH => '0',
      DOA => dout_i0(42),
      DOB => dout_i0(43),
      DOC => dout_i0(44),
      DOD => dout_i0(45),
      DOE => dout_i0(46),
      DOF => dout_i0(47),
      DOG => dout_i0(48),
      DOH => NLW_RAM_reg_0_63_42_48_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_49_55: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(49),
      DIB => din(50),
      DIC => din(51),
      DID => din(52),
      DIE => din(53),
      DIF => din(54),
      DIG => din(55),
      DIH => '0',
      DOA => dout_i0(49),
      DOB => dout_i0(50),
      DOC => dout_i0(51),
      DOD => dout_i0(52),
      DOE => dout_i0(53),
      DOF => dout_i0(54),
      DOG => dout_i0(55),
      DOH => NLW_RAM_reg_0_63_49_55_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_56_62: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(56),
      DIB => din(57),
      DIC => din(58),
      DID => din(59),
      DIE => din(60),
      DIF => din(61),
      DIG => din(62),
      DIH => '0',
      DOA => dout_i0(56),
      DOB => dout_i0(57),
      DOC => dout_i0(58),
      DOD => dout_i0(59),
      DOE => dout_i0(60),
      DOF => dout_i0(61),
      DOG => dout_i0(62),
      DOH => NLW_RAM_reg_0_63_56_62_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_63_69: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(63),
      DIB => din(64),
      DIC => din(65),
      DID => din(66),
      DIE => din(67),
      DIF => din(68),
      DIG => din(69),
      DIH => '0',
      DOA => dout_i0(63),
      DOB => dout_i0(64),
      DOC => dout_i0(65),
      DOD => dout_i0(66),
      DOE => dout_i0(67),
      DOF => dout_i0(68),
      DOG => dout_i0(69),
      DOH => NLW_RAM_reg_0_63_63_69_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_70_76: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(70),
      DIB => din(71),
      DIC => din(72),
      DID => din(73),
      DIE => din(74),
      DIF => din(75),
      DIG => din(76),
      DIH => '0',
      DOA => dout_i0(70),
      DOB => dout_i0(71),
      DOC => dout_i0(72),
      DOD => dout_i0(73),
      DOE => dout_i0(74),
      DOF => dout_i0(75),
      DOG => dout_i0(76),
      DOH => NLW_RAM_reg_0_63_70_76_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_77_83: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(77),
      DIB => din(78),
      DIC => din(79),
      DID => din(80),
      DIE => din(81),
      DIF => din(82),
      DIG => din(83),
      DIH => '0',
      DOA => dout_i0(77),
      DOB => dout_i0(78),
      DOC => dout_i0(79),
      DOD => dout_i0(80),
      DOE => dout_i0(81),
      DOF => dout_i0(82),
      DOG => dout_i0(83),
      DOH => NLW_RAM_reg_0_63_77_83_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_7_13: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(7),
      DIB => din(8),
      DIC => din(9),
      DID => din(10),
      DIE => din(11),
      DIF => din(12),
      DIG => din(13),
      DIH => '0',
      DOA => dout_i0(7),
      DOB => dout_i0(8),
      DOC => dout_i0(9),
      DOD => dout_i0(10),
      DOE => dout_i0(11),
      DOF => dout_i0(12),
      DOG => dout_i0(13),
      DOH => NLW_RAM_reg_0_63_7_13_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_84_90: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(84),
      DIB => din(85),
      DIC => din(86),
      DID => din(87),
      DIE => din(88),
      DIF => din(89),
      DIG => din(90),
      DIH => '0',
      DOA => dout_i0(84),
      DOB => dout_i0(85),
      DOC => dout_i0(86),
      DOD => dout_i0(87),
      DOE => dout_i0(88),
      DOF => dout_i0(89),
      DOG => dout_i0(90),
      DOH => NLW_RAM_reg_0_63_84_90_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_91_97: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(91),
      DIB => din(92),
      DIC => din(93),
      DID => din(94),
      DIE => din(95),
      DIF => din(96),
      DIG => din(97),
      DIH => '0',
      DOA => dout_i0(91),
      DOB => dout_i0(92),
      DOC => dout_i0(93),
      DOD => dout_i0(94),
      DOE => dout_i0(95),
      DOF => dout_i0(96),
      DOG => dout_i0(97),
      DOH => NLW_RAM_reg_0_63_91_97_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
RAM_reg_0_63_98_104: unisim.vcomponents.RAM64M8
     port map (
      ADDRA(5 downto 0) => Q(5 downto 0),
      ADDRB(5 downto 0) => Q(5 downto 0),
      ADDRC(5 downto 0) => Q(5 downto 0),
      ADDRD(5 downto 0) => Q(5 downto 0),
      ADDRE(5 downto 0) => Q(5 downto 0),
      ADDRF(5 downto 0) => Q(5 downto 0),
      ADDRG(5 downto 0) => Q(5 downto 0),
      ADDRH(5 downto 0) => \gpr1.dout_i_reg[0]_0\(5 downto 0),
      DIA => din(98),
      DIB => din(99),
      DIC => din(100),
      DID => din(101),
      DIE => din(102),
      DIF => din(103),
      DIG => din(104),
      DIH => '0',
      DOA => dout_i0(98),
      DOB => dout_i0(99),
      DOC => dout_i0(100),
      DOD => dout_i0(101),
      DOE => dout_i0(102),
      DOF => dout_i0(103),
      DOG => dout_i0(104),
      DOH => NLW_RAM_reg_0_63_98_104_DOH_UNCONNECTED,
      WCLK => wr_clk,
      WE => E(0)
    );
\gpr1.dout_i_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(0),
      Q => \gpr1.dout_i_reg[255]_0\(0)
    );
\gpr1.dout_i_reg[100]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(100),
      Q => \gpr1.dout_i_reg[255]_0\(100)
    );
\gpr1.dout_i_reg[101]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(101),
      Q => \gpr1.dout_i_reg[255]_0\(101)
    );
\gpr1.dout_i_reg[102]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(102),
      Q => \gpr1.dout_i_reg[255]_0\(102)
    );
\gpr1.dout_i_reg[103]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(103),
      Q => \gpr1.dout_i_reg[255]_0\(103)
    );
\gpr1.dout_i_reg[104]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(104),
      Q => \gpr1.dout_i_reg[255]_0\(104)
    );
\gpr1.dout_i_reg[105]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(105),
      Q => \gpr1.dout_i_reg[255]_0\(105)
    );
\gpr1.dout_i_reg[106]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(106),
      Q => \gpr1.dout_i_reg[255]_0\(106)
    );
\gpr1.dout_i_reg[107]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(107),
      Q => \gpr1.dout_i_reg[255]_0\(107)
    );
\gpr1.dout_i_reg[108]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(108),
      Q => \gpr1.dout_i_reg[255]_0\(108)
    );
\gpr1.dout_i_reg[109]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(109),
      Q => \gpr1.dout_i_reg[255]_0\(109)
    );
\gpr1.dout_i_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(10),
      Q => \gpr1.dout_i_reg[255]_0\(10)
    );
\gpr1.dout_i_reg[110]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(110),
      Q => \gpr1.dout_i_reg[255]_0\(110)
    );
\gpr1.dout_i_reg[111]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(111),
      Q => \gpr1.dout_i_reg[255]_0\(111)
    );
\gpr1.dout_i_reg[112]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(112),
      Q => \gpr1.dout_i_reg[255]_0\(112)
    );
\gpr1.dout_i_reg[113]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(113),
      Q => \gpr1.dout_i_reg[255]_0\(113)
    );
\gpr1.dout_i_reg[114]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(114),
      Q => \gpr1.dout_i_reg[255]_0\(114)
    );
\gpr1.dout_i_reg[115]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(115),
      Q => \gpr1.dout_i_reg[255]_0\(115)
    );
\gpr1.dout_i_reg[116]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(116),
      Q => \gpr1.dout_i_reg[255]_0\(116)
    );
\gpr1.dout_i_reg[117]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(117),
      Q => \gpr1.dout_i_reg[255]_0\(117)
    );
\gpr1.dout_i_reg[118]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(118),
      Q => \gpr1.dout_i_reg[255]_0\(118)
    );
\gpr1.dout_i_reg[119]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(119),
      Q => \gpr1.dout_i_reg[255]_0\(119)
    );
\gpr1.dout_i_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(11),
      Q => \gpr1.dout_i_reg[255]_0\(11)
    );
\gpr1.dout_i_reg[120]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(120),
      Q => \gpr1.dout_i_reg[255]_0\(120)
    );
\gpr1.dout_i_reg[121]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(121),
      Q => \gpr1.dout_i_reg[255]_0\(121)
    );
\gpr1.dout_i_reg[122]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(122),
      Q => \gpr1.dout_i_reg[255]_0\(122)
    );
\gpr1.dout_i_reg[123]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(123),
      Q => \gpr1.dout_i_reg[255]_0\(123)
    );
\gpr1.dout_i_reg[124]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(124),
      Q => \gpr1.dout_i_reg[255]_0\(124)
    );
\gpr1.dout_i_reg[125]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(125),
      Q => \gpr1.dout_i_reg[255]_0\(125)
    );
\gpr1.dout_i_reg[126]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(126),
      Q => \gpr1.dout_i_reg[255]_0\(126)
    );
\gpr1.dout_i_reg[127]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(127),
      Q => \gpr1.dout_i_reg[255]_0\(127)
    );
\gpr1.dout_i_reg[128]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(128),
      Q => \gpr1.dout_i_reg[255]_0\(128)
    );
\gpr1.dout_i_reg[129]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(129),
      Q => \gpr1.dout_i_reg[255]_0\(129)
    );
\gpr1.dout_i_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(12),
      Q => \gpr1.dout_i_reg[255]_0\(12)
    );
\gpr1.dout_i_reg[130]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(130),
      Q => \gpr1.dout_i_reg[255]_0\(130)
    );
\gpr1.dout_i_reg[131]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(131),
      Q => \gpr1.dout_i_reg[255]_0\(131)
    );
\gpr1.dout_i_reg[132]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(132),
      Q => \gpr1.dout_i_reg[255]_0\(132)
    );
\gpr1.dout_i_reg[133]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(133),
      Q => \gpr1.dout_i_reg[255]_0\(133)
    );
\gpr1.dout_i_reg[134]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(134),
      Q => \gpr1.dout_i_reg[255]_0\(134)
    );
\gpr1.dout_i_reg[135]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(135),
      Q => \gpr1.dout_i_reg[255]_0\(135)
    );
\gpr1.dout_i_reg[136]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(136),
      Q => \gpr1.dout_i_reg[255]_0\(136)
    );
\gpr1.dout_i_reg[137]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(137),
      Q => \gpr1.dout_i_reg[255]_0\(137)
    );
\gpr1.dout_i_reg[138]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(138),
      Q => \gpr1.dout_i_reg[255]_0\(138)
    );
\gpr1.dout_i_reg[139]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(139),
      Q => \gpr1.dout_i_reg[255]_0\(139)
    );
\gpr1.dout_i_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(13),
      Q => \gpr1.dout_i_reg[255]_0\(13)
    );
\gpr1.dout_i_reg[140]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(140),
      Q => \gpr1.dout_i_reg[255]_0\(140)
    );
\gpr1.dout_i_reg[141]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(141),
      Q => \gpr1.dout_i_reg[255]_0\(141)
    );
\gpr1.dout_i_reg[142]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(142),
      Q => \gpr1.dout_i_reg[255]_0\(142)
    );
\gpr1.dout_i_reg[143]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(143),
      Q => \gpr1.dout_i_reg[255]_0\(143)
    );
\gpr1.dout_i_reg[144]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(144),
      Q => \gpr1.dout_i_reg[255]_0\(144)
    );
\gpr1.dout_i_reg[145]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(145),
      Q => \gpr1.dout_i_reg[255]_0\(145)
    );
\gpr1.dout_i_reg[146]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(146),
      Q => \gpr1.dout_i_reg[255]_0\(146)
    );
\gpr1.dout_i_reg[147]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(147),
      Q => \gpr1.dout_i_reg[255]_0\(147)
    );
\gpr1.dout_i_reg[148]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(148),
      Q => \gpr1.dout_i_reg[255]_0\(148)
    );
\gpr1.dout_i_reg[149]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(149),
      Q => \gpr1.dout_i_reg[255]_0\(149)
    );
\gpr1.dout_i_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(14),
      Q => \gpr1.dout_i_reg[255]_0\(14)
    );
\gpr1.dout_i_reg[150]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(150),
      Q => \gpr1.dout_i_reg[255]_0\(150)
    );
\gpr1.dout_i_reg[151]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(151),
      Q => \gpr1.dout_i_reg[255]_0\(151)
    );
\gpr1.dout_i_reg[152]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(152),
      Q => \gpr1.dout_i_reg[255]_0\(152)
    );
\gpr1.dout_i_reg[153]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(153),
      Q => \gpr1.dout_i_reg[255]_0\(153)
    );
\gpr1.dout_i_reg[154]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(154),
      Q => \gpr1.dout_i_reg[255]_0\(154)
    );
\gpr1.dout_i_reg[155]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(155),
      Q => \gpr1.dout_i_reg[255]_0\(155)
    );
\gpr1.dout_i_reg[156]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(156),
      Q => \gpr1.dout_i_reg[255]_0\(156)
    );
\gpr1.dout_i_reg[157]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(157),
      Q => \gpr1.dout_i_reg[255]_0\(157)
    );
\gpr1.dout_i_reg[158]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(158),
      Q => \gpr1.dout_i_reg[255]_0\(158)
    );
\gpr1.dout_i_reg[159]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(159),
      Q => \gpr1.dout_i_reg[255]_0\(159)
    );
\gpr1.dout_i_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(15),
      Q => \gpr1.dout_i_reg[255]_0\(15)
    );
\gpr1.dout_i_reg[160]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(160),
      Q => \gpr1.dout_i_reg[255]_0\(160)
    );
\gpr1.dout_i_reg[161]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(161),
      Q => \gpr1.dout_i_reg[255]_0\(161)
    );
\gpr1.dout_i_reg[162]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(162),
      Q => \gpr1.dout_i_reg[255]_0\(162)
    );
\gpr1.dout_i_reg[163]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(163),
      Q => \gpr1.dout_i_reg[255]_0\(163)
    );
\gpr1.dout_i_reg[164]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(164),
      Q => \gpr1.dout_i_reg[255]_0\(164)
    );
\gpr1.dout_i_reg[165]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(165),
      Q => \gpr1.dout_i_reg[255]_0\(165)
    );
\gpr1.dout_i_reg[166]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(166),
      Q => \gpr1.dout_i_reg[255]_0\(166)
    );
\gpr1.dout_i_reg[167]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(167),
      Q => \gpr1.dout_i_reg[255]_0\(167)
    );
\gpr1.dout_i_reg[168]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(168),
      Q => \gpr1.dout_i_reg[255]_0\(168)
    );
\gpr1.dout_i_reg[169]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(169),
      Q => \gpr1.dout_i_reg[255]_0\(169)
    );
\gpr1.dout_i_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(16),
      Q => \gpr1.dout_i_reg[255]_0\(16)
    );
\gpr1.dout_i_reg[170]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(170),
      Q => \gpr1.dout_i_reg[255]_0\(170)
    );
\gpr1.dout_i_reg[171]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(171),
      Q => \gpr1.dout_i_reg[255]_0\(171)
    );
\gpr1.dout_i_reg[172]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(172),
      Q => \gpr1.dout_i_reg[255]_0\(172)
    );
\gpr1.dout_i_reg[173]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(173),
      Q => \gpr1.dout_i_reg[255]_0\(173)
    );
\gpr1.dout_i_reg[174]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(174),
      Q => \gpr1.dout_i_reg[255]_0\(174)
    );
\gpr1.dout_i_reg[175]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(175),
      Q => \gpr1.dout_i_reg[255]_0\(175)
    );
\gpr1.dout_i_reg[176]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(176),
      Q => \gpr1.dout_i_reg[255]_0\(176)
    );
\gpr1.dout_i_reg[177]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(177),
      Q => \gpr1.dout_i_reg[255]_0\(177)
    );
\gpr1.dout_i_reg[178]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(178),
      Q => \gpr1.dout_i_reg[255]_0\(178)
    );
\gpr1.dout_i_reg[179]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(179),
      Q => \gpr1.dout_i_reg[255]_0\(179)
    );
\gpr1.dout_i_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(17),
      Q => \gpr1.dout_i_reg[255]_0\(17)
    );
\gpr1.dout_i_reg[180]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(180),
      Q => \gpr1.dout_i_reg[255]_0\(180)
    );
\gpr1.dout_i_reg[181]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(181),
      Q => \gpr1.dout_i_reg[255]_0\(181)
    );
\gpr1.dout_i_reg[182]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(182),
      Q => \gpr1.dout_i_reg[255]_0\(182)
    );
\gpr1.dout_i_reg[183]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(183),
      Q => \gpr1.dout_i_reg[255]_0\(183)
    );
\gpr1.dout_i_reg[184]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(184),
      Q => \gpr1.dout_i_reg[255]_0\(184)
    );
\gpr1.dout_i_reg[185]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(185),
      Q => \gpr1.dout_i_reg[255]_0\(185)
    );
\gpr1.dout_i_reg[186]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(186),
      Q => \gpr1.dout_i_reg[255]_0\(186)
    );
\gpr1.dout_i_reg[187]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(187),
      Q => \gpr1.dout_i_reg[255]_0\(187)
    );
\gpr1.dout_i_reg[188]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(188),
      Q => \gpr1.dout_i_reg[255]_0\(188)
    );
\gpr1.dout_i_reg[189]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(189),
      Q => \gpr1.dout_i_reg[255]_0\(189)
    );
\gpr1.dout_i_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(18),
      Q => \gpr1.dout_i_reg[255]_0\(18)
    );
\gpr1.dout_i_reg[190]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(190),
      Q => \gpr1.dout_i_reg[255]_0\(190)
    );
\gpr1.dout_i_reg[191]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(191),
      Q => \gpr1.dout_i_reg[255]_0\(191)
    );
\gpr1.dout_i_reg[192]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(192),
      Q => \gpr1.dout_i_reg[255]_0\(192)
    );
\gpr1.dout_i_reg[193]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(193),
      Q => \gpr1.dout_i_reg[255]_0\(193)
    );
\gpr1.dout_i_reg[194]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(194),
      Q => \gpr1.dout_i_reg[255]_0\(194)
    );
\gpr1.dout_i_reg[195]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(195),
      Q => \gpr1.dout_i_reg[255]_0\(195)
    );
\gpr1.dout_i_reg[196]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(196),
      Q => \gpr1.dout_i_reg[255]_0\(196)
    );
\gpr1.dout_i_reg[197]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(197),
      Q => \gpr1.dout_i_reg[255]_0\(197)
    );
\gpr1.dout_i_reg[198]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(198),
      Q => \gpr1.dout_i_reg[255]_0\(198)
    );
\gpr1.dout_i_reg[199]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(199),
      Q => \gpr1.dout_i_reg[255]_0\(199)
    );
\gpr1.dout_i_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(19),
      Q => \gpr1.dout_i_reg[255]_0\(19)
    );
\gpr1.dout_i_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(1),
      Q => \gpr1.dout_i_reg[255]_0\(1)
    );
\gpr1.dout_i_reg[200]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(200),
      Q => \gpr1.dout_i_reg[255]_0\(200)
    );
\gpr1.dout_i_reg[201]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(201),
      Q => \gpr1.dout_i_reg[255]_0\(201)
    );
\gpr1.dout_i_reg[202]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(202),
      Q => \gpr1.dout_i_reg[255]_0\(202)
    );
\gpr1.dout_i_reg[203]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(203),
      Q => \gpr1.dout_i_reg[255]_0\(203)
    );
\gpr1.dout_i_reg[204]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(204),
      Q => \gpr1.dout_i_reg[255]_0\(204)
    );
\gpr1.dout_i_reg[205]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(205),
      Q => \gpr1.dout_i_reg[255]_0\(205)
    );
\gpr1.dout_i_reg[206]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(206),
      Q => \gpr1.dout_i_reg[255]_0\(206)
    );
\gpr1.dout_i_reg[207]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(207),
      Q => \gpr1.dout_i_reg[255]_0\(207)
    );
\gpr1.dout_i_reg[208]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(208),
      Q => \gpr1.dout_i_reg[255]_0\(208)
    );
\gpr1.dout_i_reg[209]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(209),
      Q => \gpr1.dout_i_reg[255]_0\(209)
    );
\gpr1.dout_i_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(20),
      Q => \gpr1.dout_i_reg[255]_0\(20)
    );
\gpr1.dout_i_reg[210]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(210),
      Q => \gpr1.dout_i_reg[255]_0\(210)
    );
\gpr1.dout_i_reg[211]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(211),
      Q => \gpr1.dout_i_reg[255]_0\(211)
    );
\gpr1.dout_i_reg[212]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(212),
      Q => \gpr1.dout_i_reg[255]_0\(212)
    );
\gpr1.dout_i_reg[213]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(213),
      Q => \gpr1.dout_i_reg[255]_0\(213)
    );
\gpr1.dout_i_reg[214]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(214),
      Q => \gpr1.dout_i_reg[255]_0\(214)
    );
\gpr1.dout_i_reg[215]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(215),
      Q => \gpr1.dout_i_reg[255]_0\(215)
    );
\gpr1.dout_i_reg[216]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(216),
      Q => \gpr1.dout_i_reg[255]_0\(216)
    );
\gpr1.dout_i_reg[217]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(217),
      Q => \gpr1.dout_i_reg[255]_0\(217)
    );
\gpr1.dout_i_reg[218]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(218),
      Q => \gpr1.dout_i_reg[255]_0\(218)
    );
\gpr1.dout_i_reg[219]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(219),
      Q => \gpr1.dout_i_reg[255]_0\(219)
    );
\gpr1.dout_i_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(21),
      Q => \gpr1.dout_i_reg[255]_0\(21)
    );
\gpr1.dout_i_reg[220]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(220),
      Q => \gpr1.dout_i_reg[255]_0\(220)
    );
\gpr1.dout_i_reg[221]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(221),
      Q => \gpr1.dout_i_reg[255]_0\(221)
    );
\gpr1.dout_i_reg[222]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(222),
      Q => \gpr1.dout_i_reg[255]_0\(222)
    );
\gpr1.dout_i_reg[223]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(223),
      Q => \gpr1.dout_i_reg[255]_0\(223)
    );
\gpr1.dout_i_reg[224]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(224),
      Q => \gpr1.dout_i_reg[255]_0\(224)
    );
\gpr1.dout_i_reg[225]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(225),
      Q => \gpr1.dout_i_reg[255]_0\(225)
    );
\gpr1.dout_i_reg[226]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(226),
      Q => \gpr1.dout_i_reg[255]_0\(226)
    );
\gpr1.dout_i_reg[227]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(227),
      Q => \gpr1.dout_i_reg[255]_0\(227)
    );
\gpr1.dout_i_reg[228]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(228),
      Q => \gpr1.dout_i_reg[255]_0\(228)
    );
\gpr1.dout_i_reg[229]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(229),
      Q => \gpr1.dout_i_reg[255]_0\(229)
    );
\gpr1.dout_i_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(22),
      Q => \gpr1.dout_i_reg[255]_0\(22)
    );
\gpr1.dout_i_reg[230]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(230),
      Q => \gpr1.dout_i_reg[255]_0\(230)
    );
\gpr1.dout_i_reg[231]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(231),
      Q => \gpr1.dout_i_reg[255]_0\(231)
    );
\gpr1.dout_i_reg[232]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(232),
      Q => \gpr1.dout_i_reg[255]_0\(232)
    );
\gpr1.dout_i_reg[233]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(233),
      Q => \gpr1.dout_i_reg[255]_0\(233)
    );
\gpr1.dout_i_reg[234]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(234),
      Q => \gpr1.dout_i_reg[255]_0\(234)
    );
\gpr1.dout_i_reg[235]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(235),
      Q => \gpr1.dout_i_reg[255]_0\(235)
    );
\gpr1.dout_i_reg[236]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(236),
      Q => \gpr1.dout_i_reg[255]_0\(236)
    );
\gpr1.dout_i_reg[237]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(237),
      Q => \gpr1.dout_i_reg[255]_0\(237)
    );
\gpr1.dout_i_reg[238]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(238),
      Q => \gpr1.dout_i_reg[255]_0\(238)
    );
\gpr1.dout_i_reg[239]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(239),
      Q => \gpr1.dout_i_reg[255]_0\(239)
    );
\gpr1.dout_i_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(23),
      Q => \gpr1.dout_i_reg[255]_0\(23)
    );
\gpr1.dout_i_reg[240]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(240),
      Q => \gpr1.dout_i_reg[255]_0\(240)
    );
\gpr1.dout_i_reg[241]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(241),
      Q => \gpr1.dout_i_reg[255]_0\(241)
    );
\gpr1.dout_i_reg[242]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(242),
      Q => \gpr1.dout_i_reg[255]_0\(242)
    );
\gpr1.dout_i_reg[243]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(243),
      Q => \gpr1.dout_i_reg[255]_0\(243)
    );
\gpr1.dout_i_reg[244]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(244),
      Q => \gpr1.dout_i_reg[255]_0\(244)
    );
\gpr1.dout_i_reg[245]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(245),
      Q => \gpr1.dout_i_reg[255]_0\(245)
    );
\gpr1.dout_i_reg[246]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(246),
      Q => \gpr1.dout_i_reg[255]_0\(246)
    );
\gpr1.dout_i_reg[247]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(247),
      Q => \gpr1.dout_i_reg[255]_0\(247)
    );
\gpr1.dout_i_reg[248]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(248),
      Q => \gpr1.dout_i_reg[255]_0\(248)
    );
\gpr1.dout_i_reg[249]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(249),
      Q => \gpr1.dout_i_reg[255]_0\(249)
    );
\gpr1.dout_i_reg[24]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(24),
      Q => \gpr1.dout_i_reg[255]_0\(24)
    );
\gpr1.dout_i_reg[250]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(250),
      Q => \gpr1.dout_i_reg[255]_0\(250)
    );
\gpr1.dout_i_reg[251]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(251),
      Q => \gpr1.dout_i_reg[255]_0\(251)
    );
\gpr1.dout_i_reg[252]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(252),
      Q => \gpr1.dout_i_reg[255]_0\(252)
    );
\gpr1.dout_i_reg[253]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(253),
      Q => \gpr1.dout_i_reg[255]_0\(253)
    );
\gpr1.dout_i_reg[254]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(254),
      Q => \gpr1.dout_i_reg[255]_0\(254)
    );
\gpr1.dout_i_reg[255]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(255),
      Q => \gpr1.dout_i_reg[255]_0\(255)
    );
\gpr1.dout_i_reg[25]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(25),
      Q => \gpr1.dout_i_reg[255]_0\(25)
    );
\gpr1.dout_i_reg[26]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(26),
      Q => \gpr1.dout_i_reg[255]_0\(26)
    );
\gpr1.dout_i_reg[27]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(27),
      Q => \gpr1.dout_i_reg[255]_0\(27)
    );
\gpr1.dout_i_reg[28]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(28),
      Q => \gpr1.dout_i_reg[255]_0\(28)
    );
\gpr1.dout_i_reg[29]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(29),
      Q => \gpr1.dout_i_reg[255]_0\(29)
    );
\gpr1.dout_i_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(2),
      Q => \gpr1.dout_i_reg[255]_0\(2)
    );
\gpr1.dout_i_reg[30]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(30),
      Q => \gpr1.dout_i_reg[255]_0\(30)
    );
\gpr1.dout_i_reg[31]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(31),
      Q => \gpr1.dout_i_reg[255]_0\(31)
    );
\gpr1.dout_i_reg[32]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(32),
      Q => \gpr1.dout_i_reg[255]_0\(32)
    );
\gpr1.dout_i_reg[33]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(33),
      Q => \gpr1.dout_i_reg[255]_0\(33)
    );
\gpr1.dout_i_reg[34]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(34),
      Q => \gpr1.dout_i_reg[255]_0\(34)
    );
\gpr1.dout_i_reg[35]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(35),
      Q => \gpr1.dout_i_reg[255]_0\(35)
    );
\gpr1.dout_i_reg[36]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(36),
      Q => \gpr1.dout_i_reg[255]_0\(36)
    );
\gpr1.dout_i_reg[37]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(37),
      Q => \gpr1.dout_i_reg[255]_0\(37)
    );
\gpr1.dout_i_reg[38]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(38),
      Q => \gpr1.dout_i_reg[255]_0\(38)
    );
\gpr1.dout_i_reg[39]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(39),
      Q => \gpr1.dout_i_reg[255]_0\(39)
    );
\gpr1.dout_i_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(3),
      Q => \gpr1.dout_i_reg[255]_0\(3)
    );
\gpr1.dout_i_reg[40]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(40),
      Q => \gpr1.dout_i_reg[255]_0\(40)
    );
\gpr1.dout_i_reg[41]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(41),
      Q => \gpr1.dout_i_reg[255]_0\(41)
    );
\gpr1.dout_i_reg[42]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(42),
      Q => \gpr1.dout_i_reg[255]_0\(42)
    );
\gpr1.dout_i_reg[43]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(43),
      Q => \gpr1.dout_i_reg[255]_0\(43)
    );
\gpr1.dout_i_reg[44]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(44),
      Q => \gpr1.dout_i_reg[255]_0\(44)
    );
\gpr1.dout_i_reg[45]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(45),
      Q => \gpr1.dout_i_reg[255]_0\(45)
    );
\gpr1.dout_i_reg[46]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(46),
      Q => \gpr1.dout_i_reg[255]_0\(46)
    );
\gpr1.dout_i_reg[47]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(47),
      Q => \gpr1.dout_i_reg[255]_0\(47)
    );
\gpr1.dout_i_reg[48]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(48),
      Q => \gpr1.dout_i_reg[255]_0\(48)
    );
\gpr1.dout_i_reg[49]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(49),
      Q => \gpr1.dout_i_reg[255]_0\(49)
    );
\gpr1.dout_i_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(4),
      Q => \gpr1.dout_i_reg[255]_0\(4)
    );
\gpr1.dout_i_reg[50]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(50),
      Q => \gpr1.dout_i_reg[255]_0\(50)
    );
\gpr1.dout_i_reg[51]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(51),
      Q => \gpr1.dout_i_reg[255]_0\(51)
    );
\gpr1.dout_i_reg[52]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(52),
      Q => \gpr1.dout_i_reg[255]_0\(52)
    );
\gpr1.dout_i_reg[53]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(53),
      Q => \gpr1.dout_i_reg[255]_0\(53)
    );
\gpr1.dout_i_reg[54]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(54),
      Q => \gpr1.dout_i_reg[255]_0\(54)
    );
\gpr1.dout_i_reg[55]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(55),
      Q => \gpr1.dout_i_reg[255]_0\(55)
    );
\gpr1.dout_i_reg[56]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(56),
      Q => \gpr1.dout_i_reg[255]_0\(56)
    );
\gpr1.dout_i_reg[57]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(57),
      Q => \gpr1.dout_i_reg[255]_0\(57)
    );
\gpr1.dout_i_reg[58]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(58),
      Q => \gpr1.dout_i_reg[255]_0\(58)
    );
\gpr1.dout_i_reg[59]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(59),
      Q => \gpr1.dout_i_reg[255]_0\(59)
    );
\gpr1.dout_i_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(5),
      Q => \gpr1.dout_i_reg[255]_0\(5)
    );
\gpr1.dout_i_reg[60]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(60),
      Q => \gpr1.dout_i_reg[255]_0\(60)
    );
\gpr1.dout_i_reg[61]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(61),
      Q => \gpr1.dout_i_reg[255]_0\(61)
    );
\gpr1.dout_i_reg[62]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(62),
      Q => \gpr1.dout_i_reg[255]_0\(62)
    );
\gpr1.dout_i_reg[63]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(63),
      Q => \gpr1.dout_i_reg[255]_0\(63)
    );
\gpr1.dout_i_reg[64]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(64),
      Q => \gpr1.dout_i_reg[255]_0\(64)
    );
\gpr1.dout_i_reg[65]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(65),
      Q => \gpr1.dout_i_reg[255]_0\(65)
    );
\gpr1.dout_i_reg[66]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(66),
      Q => \gpr1.dout_i_reg[255]_0\(66)
    );
\gpr1.dout_i_reg[67]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(67),
      Q => \gpr1.dout_i_reg[255]_0\(67)
    );
\gpr1.dout_i_reg[68]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(68),
      Q => \gpr1.dout_i_reg[255]_0\(68)
    );
\gpr1.dout_i_reg[69]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(69),
      Q => \gpr1.dout_i_reg[255]_0\(69)
    );
\gpr1.dout_i_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(6),
      Q => \gpr1.dout_i_reg[255]_0\(6)
    );
\gpr1.dout_i_reg[70]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(70),
      Q => \gpr1.dout_i_reg[255]_0\(70)
    );
\gpr1.dout_i_reg[71]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(71),
      Q => \gpr1.dout_i_reg[255]_0\(71)
    );
\gpr1.dout_i_reg[72]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(72),
      Q => \gpr1.dout_i_reg[255]_0\(72)
    );
\gpr1.dout_i_reg[73]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(73),
      Q => \gpr1.dout_i_reg[255]_0\(73)
    );
\gpr1.dout_i_reg[74]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(74),
      Q => \gpr1.dout_i_reg[255]_0\(74)
    );
\gpr1.dout_i_reg[75]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(75),
      Q => \gpr1.dout_i_reg[255]_0\(75)
    );
\gpr1.dout_i_reg[76]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(76),
      Q => \gpr1.dout_i_reg[255]_0\(76)
    );
\gpr1.dout_i_reg[77]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(77),
      Q => \gpr1.dout_i_reg[255]_0\(77)
    );
\gpr1.dout_i_reg[78]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(78),
      Q => \gpr1.dout_i_reg[255]_0\(78)
    );
\gpr1.dout_i_reg[79]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(79),
      Q => \gpr1.dout_i_reg[255]_0\(79)
    );
\gpr1.dout_i_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(7),
      Q => \gpr1.dout_i_reg[255]_0\(7)
    );
\gpr1.dout_i_reg[80]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(80),
      Q => \gpr1.dout_i_reg[255]_0\(80)
    );
\gpr1.dout_i_reg[81]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(81),
      Q => \gpr1.dout_i_reg[255]_0\(81)
    );
\gpr1.dout_i_reg[82]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(82),
      Q => \gpr1.dout_i_reg[255]_0\(82)
    );
\gpr1.dout_i_reg[83]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(83),
      Q => \gpr1.dout_i_reg[255]_0\(83)
    );
\gpr1.dout_i_reg[84]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(84),
      Q => \gpr1.dout_i_reg[255]_0\(84)
    );
\gpr1.dout_i_reg[85]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(85),
      Q => \gpr1.dout_i_reg[255]_0\(85)
    );
\gpr1.dout_i_reg[86]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(86),
      Q => \gpr1.dout_i_reg[255]_0\(86)
    );
\gpr1.dout_i_reg[87]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(87),
      Q => \gpr1.dout_i_reg[255]_0\(87)
    );
\gpr1.dout_i_reg[88]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(88),
      Q => \gpr1.dout_i_reg[255]_0\(88)
    );
\gpr1.dout_i_reg[89]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(89),
      Q => \gpr1.dout_i_reg[255]_0\(89)
    );
\gpr1.dout_i_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(8),
      Q => \gpr1.dout_i_reg[255]_0\(8)
    );
\gpr1.dout_i_reg[90]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(90),
      Q => \gpr1.dout_i_reg[255]_0\(90)
    );
\gpr1.dout_i_reg[91]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(91),
      Q => \gpr1.dout_i_reg[255]_0\(91)
    );
\gpr1.dout_i_reg[92]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(92),
      Q => \gpr1.dout_i_reg[255]_0\(92)
    );
\gpr1.dout_i_reg[93]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(93),
      Q => \gpr1.dout_i_reg[255]_0\(93)
    );
\gpr1.dout_i_reg[94]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(94),
      Q => \gpr1.dout_i_reg[255]_0\(94)
    );
\gpr1.dout_i_reg[95]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(95),
      Q => \gpr1.dout_i_reg[255]_0\(95)
    );
\gpr1.dout_i_reg[96]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(96),
      Q => \gpr1.dout_i_reg[255]_0\(96)
    );
\gpr1.dout_i_reg[97]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(97),
      Q => \gpr1.dout_i_reg[255]_0\(97)
    );
\gpr1.dout_i_reg[98]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(98),
      Q => \gpr1.dout_i_reg[255]_0\(98)
    );
\gpr1.dout_i_reg[99]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(99),
      Q => \gpr1.dout_i_reg[255]_0\(99)
    );
\gpr1.dout_i_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \gpr1.dout_i_reg[0]_1\(0),
      CLR => AR(0),
      D => dout_i0(9),
      Q => \gpr1.dout_i_reg[255]_0\(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo_rd_bin_cntr is
  port (
    comp1 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 5 downto 0 );
    WR_PNTR_RD : in STD_LOGIC_VECTOR ( 5 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    rd_clk : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end header_fifo_rd_bin_cntr;

architecture STRUCTURE of header_fifo_rd_bin_cntr is
  signal \plusOp__0\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal ram_empty_i_i_6_n_0 : STD_LOGIC;
  signal ram_empty_i_i_7_n_0 : STD_LOGIC;
  signal rd_pntr_plus1 : STD_LOGIC_VECTOR ( 5 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \gc0.count[1]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \gc0.count[2]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \gc0.count[3]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \gc0.count[4]_i_1\ : label is "soft_lutpair4";
begin
\gc0.count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => rd_pntr_plus1(0),
      O => \plusOp__0\(0)
    );
\gc0.count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => rd_pntr_plus1(0),
      I1 => rd_pntr_plus1(1),
      O => \plusOp__0\(1)
    );
\gc0.count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => rd_pntr_plus1(0),
      I1 => rd_pntr_plus1(1),
      I2 => rd_pntr_plus1(2),
      O => \plusOp__0\(2)
    );
\gc0.count[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => rd_pntr_plus1(1),
      I1 => rd_pntr_plus1(0),
      I2 => rd_pntr_plus1(2),
      I3 => rd_pntr_plus1(3),
      O => \plusOp__0\(3)
    );
\gc0.count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => rd_pntr_plus1(2),
      I1 => rd_pntr_plus1(0),
      I2 => rd_pntr_plus1(1),
      I3 => rd_pntr_plus1(3),
      I4 => rd_pntr_plus1(4),
      O => \plusOp__0\(4)
    );
\gc0.count[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => rd_pntr_plus1(3),
      I1 => rd_pntr_plus1(1),
      I2 => rd_pntr_plus1(0),
      I3 => rd_pntr_plus1(2),
      I4 => rd_pntr_plus1(4),
      I5 => rd_pntr_plus1(5),
      O => \plusOp__0\(5)
    );
\gc0.count_d1_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => E(0),
      CLR => AR(0),
      D => rd_pntr_plus1(0),
      Q => Q(0)
    );
\gc0.count_d1_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => E(0),
      CLR => AR(0),
      D => rd_pntr_plus1(1),
      Q => Q(1)
    );
\gc0.count_d1_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => E(0),
      CLR => AR(0),
      D => rd_pntr_plus1(2),
      Q => Q(2)
    );
\gc0.count_d1_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => E(0),
      CLR => AR(0),
      D => rd_pntr_plus1(3),
      Q => Q(3)
    );
\gc0.count_d1_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => E(0),
      CLR => AR(0),
      D => rd_pntr_plus1(4),
      Q => Q(4)
    );
\gc0.count_d1_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => E(0),
      CLR => AR(0),
      D => rd_pntr_plus1(5),
      Q => Q(5)
    );
\gc0.count_reg[0]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => rd_clk,
      CE => E(0),
      D => \plusOp__0\(0),
      PRE => AR(0),
      Q => rd_pntr_plus1(0)
    );
\gc0.count_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => E(0),
      CLR => AR(0),
      D => \plusOp__0\(1),
      Q => rd_pntr_plus1(1)
    );
\gc0.count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => E(0),
      CLR => AR(0),
      D => \plusOp__0\(2),
      Q => rd_pntr_plus1(2)
    );
\gc0.count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => E(0),
      CLR => AR(0),
      D => \plusOp__0\(3),
      Q => rd_pntr_plus1(3)
    );
\gc0.count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => E(0),
      CLR => AR(0),
      D => \plusOp__0\(4),
      Q => rd_pntr_plus1(4)
    );
\gc0.count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => E(0),
      CLR => AR(0),
      D => \plusOp__0\(5),
      Q => rd_pntr_plus1(5)
    );
ram_empty_i_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000000000"
    )
        port map (
      I0 => WR_PNTR_RD(1),
      I1 => rd_pntr_plus1(1),
      I2 => WR_PNTR_RD(0),
      I3 => rd_pntr_plus1(0),
      I4 => ram_empty_i_i_6_n_0,
      I5 => ram_empty_i_i_7_n_0,
      O => comp1
    );
ram_empty_i_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => rd_pntr_plus1(4),
      I1 => WR_PNTR_RD(4),
      I2 => rd_pntr_plus1(5),
      I3 => WR_PNTR_RD(5),
      O => ram_empty_i_i_6_n_0
    );
ram_empty_i_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => rd_pntr_plus1(2),
      I1 => WR_PNTR_RD(2),
      I2 => rd_pntr_plus1(3),
      I3 => WR_PNTR_RD(3),
      O => ram_empty_i_i_7_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo_rd_dc_as is
  port (
    rd_data_count : out STD_LOGIC_VECTOR ( 5 downto 0 );
    WR_PNTR_RD : in STD_LOGIC_VECTOR ( 4 downto 0 );
    S : in STD_LOGIC_VECTOR ( 5 downto 0 );
    rd_clk : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end header_fifo_rd_dc_as;

architecture STRUCTURE of header_fifo_rd_dc_as is
  signal minusOp : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal minusOp_carry_n_3 : STD_LOGIC;
  signal minusOp_carry_n_4 : STD_LOGIC;
  signal minusOp_carry_n_5 : STD_LOGIC;
  signal minusOp_carry_n_6 : STD_LOGIC;
  signal minusOp_carry_n_7 : STD_LOGIC;
  signal NLW_minusOp_carry_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 5 );
  signal NLW_minusOp_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 6 );
begin
minusOp_carry: unisim.vcomponents.CARRY8
     port map (
      CI => '1',
      CI_TOP => '0',
      CO(7 downto 5) => NLW_minusOp_carry_CO_UNCONNECTED(7 downto 5),
      CO(4) => minusOp_carry_n_3,
      CO(3) => minusOp_carry_n_4,
      CO(2) => minusOp_carry_n_5,
      CO(1) => minusOp_carry_n_6,
      CO(0) => minusOp_carry_n_7,
      DI(7 downto 5) => B"000",
      DI(4 downto 0) => WR_PNTR_RD(4 downto 0),
      O(7 downto 6) => NLW_minusOp_carry_O_UNCONNECTED(7 downto 6),
      O(5 downto 0) => minusOp(5 downto 0),
      S(7 downto 6) => B"00",
      S(5 downto 0) => S(5 downto 0)
    );
\rd_dc_i_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => '1',
      CLR => AR(0),
      D => minusOp(0),
      Q => rd_data_count(0)
    );
\rd_dc_i_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => '1',
      CLR => AR(0),
      D => minusOp(1),
      Q => rd_data_count(1)
    );
\rd_dc_i_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => '1',
      CLR => AR(0),
      D => minusOp(2),
      Q => rd_data_count(2)
    );
\rd_dc_i_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => '1',
      CLR => AR(0),
      D => minusOp(3),
      Q => rd_data_count(3)
    );
\rd_dc_i_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => '1',
      CLR => AR(0),
      D => minusOp(4),
      Q => rd_data_count(4)
    );
\rd_dc_i_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => '1',
      CLR => AR(0),
      D => minusOp(5),
      Q => rd_data_count(5)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo_rd_fwft is
  port (
    empty : out STD_LOGIC;
    valid : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    underflow_i0 : out STD_LOGIC;
    \gpregsm1.curr_fwft_state_reg[1]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    ram_empty_fb_i_reg : out STD_LOGIC_VECTOR ( 0 to 0 );
    rd_clk : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    rd_en : in STD_LOGIC;
    \out\ : in STD_LOGIC
  );
end header_fifo_rd_fwft;

architecture STRUCTURE of header_fifo_rd_fwft is
  signal aempty_fwft_fb_i : STD_LOGIC;
  attribute DONT_TOUCH : boolean;
  attribute DONT_TOUCH of aempty_fwft_fb_i : signal is std.standard.true;
  signal aempty_fwft_i : STD_LOGIC;
  attribute DONT_TOUCH of aempty_fwft_i : signal is std.standard.true;
  signal aempty_fwft_i0 : STD_LOGIC;
  signal curr_fwft_state : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute DONT_TOUCH of curr_fwft_state : signal is std.standard.true;
  signal empty_fwft_fb_i : STD_LOGIC;
  attribute DONT_TOUCH of empty_fwft_fb_i : signal is std.standard.true;
  signal empty_fwft_fb_o_i : STD_LOGIC;
  attribute DONT_TOUCH of empty_fwft_fb_o_i : signal is std.standard.true;
  signal empty_fwft_fb_o_i0 : STD_LOGIC;
  signal empty_fwft_i : STD_LOGIC;
  attribute DONT_TOUCH of empty_fwft_i : signal is std.standard.true;
  signal empty_fwft_i0 : STD_LOGIC;
  signal next_fwft_state : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal user_valid : STD_LOGIC;
  attribute DONT_TOUCH of user_valid : signal is std.standard.true;
  attribute DONT_TOUCH of aempty_fwft_fb_i_reg : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of aempty_fwft_fb_i_reg : label is "yes";
  attribute equivalent_register_removal : string;
  attribute equivalent_register_removal of aempty_fwft_fb_i_reg : label is "no";
  attribute DONT_TOUCH of aempty_fwft_i_reg : label is std.standard.true;
  attribute KEEP of aempty_fwft_i_reg : label is "yes";
  attribute equivalent_register_removal of aempty_fwft_i_reg : label is "no";
  attribute DONT_TOUCH of empty_fwft_fb_i_reg : label is std.standard.true;
  attribute KEEP of empty_fwft_fb_i_reg : label is "yes";
  attribute equivalent_register_removal of empty_fwft_fb_i_reg : label is "no";
  attribute DONT_TOUCH of empty_fwft_fb_o_i_reg : label is std.standard.true;
  attribute KEEP of empty_fwft_fb_o_i_reg : label is "yes";
  attribute equivalent_register_removal of empty_fwft_fb_o_i_reg : label is "no";
  attribute DONT_TOUCH of empty_fwft_i_reg : label is std.standard.true;
  attribute KEEP of empty_fwft_i_reg : label is "yes";
  attribute equivalent_register_removal of empty_fwft_i_reg : label is "no";
  attribute DONT_TOUCH of \gpregsm1.curr_fwft_state_reg[0]\ : label is std.standard.true;
  attribute KEEP of \gpregsm1.curr_fwft_state_reg[0]\ : label is "yes";
  attribute equivalent_register_removal of \gpregsm1.curr_fwft_state_reg[0]\ : label is "no";
  attribute DONT_TOUCH of \gpregsm1.curr_fwft_state_reg[1]\ : label is std.standard.true;
  attribute KEEP of \gpregsm1.curr_fwft_state_reg[1]\ : label is "yes";
  attribute equivalent_register_removal of \gpregsm1.curr_fwft_state_reg[1]\ : label is "no";
  attribute DONT_TOUCH of \gpregsm1.user_valid_reg\ : label is std.standard.true;
  attribute KEEP of \gpregsm1.user_valid_reg\ : label is "yes";
  attribute equivalent_register_removal of \gpregsm1.user_valid_reg\ : label is "no";
begin
  empty <= empty_fwft_i;
  valid <= user_valid;
aempty_fwft_fb_i_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFCB8000"
    )
        port map (
      I0 => rd_en,
      I1 => curr_fwft_state(0),
      I2 => curr_fwft_state(1),
      I3 => \out\,
      I4 => aempty_fwft_fb_i,
      O => aempty_fwft_i0
    );
aempty_fwft_fb_i_reg: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => rd_clk,
      CE => '1',
      D => aempty_fwft_i0,
      PRE => AR(0),
      Q => aempty_fwft_fb_i
    );
aempty_fwft_i_reg: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => rd_clk,
      CE => '1',
      D => aempty_fwft_i0,
      PRE => AR(0),
      Q => aempty_fwft_i
    );
empty_fwft_fb_i_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F320"
    )
        port map (
      I0 => rd_en,
      I1 => curr_fwft_state(1),
      I2 => curr_fwft_state(0),
      I3 => empty_fwft_fb_i,
      O => empty_fwft_i0
    );
empty_fwft_fb_i_reg: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => rd_clk,
      CE => '1',
      D => empty_fwft_i0,
      PRE => AR(0),
      Q => empty_fwft_fb_i
    );
empty_fwft_fb_o_i_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F320"
    )
        port map (
      I0 => rd_en,
      I1 => curr_fwft_state(1),
      I2 => curr_fwft_state(0),
      I3 => empty_fwft_fb_o_i,
      O => empty_fwft_fb_o_i0
    );
empty_fwft_fb_o_i_reg: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => rd_clk,
      CE => '1',
      D => empty_fwft_fb_o_i0,
      PRE => AR(0),
      Q => empty_fwft_fb_o_i
    );
empty_fwft_i_reg: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => rd_clk,
      CE => '1',
      D => empty_fwft_i0,
      PRE => AR(0),
      Q => empty_fwft_i
    );
\gc0.count_d1[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4555"
    )
        port map (
      I0 => \out\,
      I1 => rd_en,
      I2 => curr_fwft_state(1),
      I3 => curr_fwft_state(0),
      O => ram_empty_fb_i_reg(0)
    );
\goreg_dm.dout_i[255]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => curr_fwft_state(1),
      I1 => curr_fwft_state(0),
      I2 => rd_en,
      O => \gpregsm1.curr_fwft_state_reg[1]_0\(0)
    );
\gpr1.dout_i[255]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00F7"
    )
        port map (
      I0 => curr_fwft_state(0),
      I1 => curr_fwft_state(1),
      I2 => rd_en,
      I3 => \out\,
      O => E(0)
    );
\gpregsm1.curr_fwft_state[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => curr_fwft_state(1),
      I1 => rd_en,
      I2 => curr_fwft_state(0),
      O => next_fwft_state(0)
    );
\gpregsm1.curr_fwft_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"20FF"
    )
        port map (
      I0 => curr_fwft_state(1),
      I1 => rd_en,
      I2 => curr_fwft_state(0),
      I3 => \out\,
      O => next_fwft_state(1)
    );
\gpregsm1.curr_fwft_state_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => '1',
      CLR => AR(0),
      D => next_fwft_state(0),
      Q => curr_fwft_state(0)
    );
\gpregsm1.curr_fwft_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => '1',
      CLR => AR(0),
      D => next_fwft_state(1),
      Q => curr_fwft_state(1)
    );
\gpregsm1.user_valid_reg\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => '1',
      CLR => AR(0),
      D => next_fwft_state(0),
      Q => user_valid
    );
\guf.guf1.underflow_i_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => empty_fwft_i,
      I1 => AR(0),
      I2 => rd_en,
      O => underflow_i0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo_rd_handshaking_flags is
  port (
    underflow : out STD_LOGIC;
    underflow_i0 : in STD_LOGIC;
    rd_clk : in STD_LOGIC
  );
end header_fifo_rd_handshaking_flags;

architecture STRUCTURE of header_fifo_rd_handshaking_flags is
begin
\guf.guf1.underflow_i_reg\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => '1',
      D => underflow_i0,
      Q => underflow,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo_rd_status_flags_as is
  port (
    \out\ : out STD_LOGIC;
    ram_empty_fb_i_reg_0 : in STD_LOGIC;
    rd_clk : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end header_fifo_rd_status_flags_as;

architecture STRUCTURE of header_fifo_rd_status_flags_as is
  signal ram_empty_fb_i : STD_LOGIC;
  attribute DONT_TOUCH : boolean;
  attribute DONT_TOUCH of ram_empty_fb_i : signal is std.standard.true;
  signal ram_empty_i : STD_LOGIC;
  attribute DONT_TOUCH of ram_empty_i : signal is std.standard.true;
  attribute DONT_TOUCH of ram_empty_fb_i_reg : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of ram_empty_fb_i_reg : label is "yes";
  attribute equivalent_register_removal : string;
  attribute equivalent_register_removal of ram_empty_fb_i_reg : label is "no";
  attribute DONT_TOUCH of ram_empty_i_reg : label is std.standard.true;
  attribute KEEP of ram_empty_i_reg : label is "yes";
  attribute equivalent_register_removal of ram_empty_i_reg : label is "no";
begin
  \out\ <= ram_empty_fb_i;
ram_empty_fb_i_reg: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => rd_clk,
      CE => '1',
      D => ram_empty_fb_i_reg_0,
      PRE => AR(0),
      Q => ram_empty_fb_i
    );
ram_empty_i_reg: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => rd_clk,
      CE => '1',
      D => ram_empty_fb_i_reg_0,
      PRE => AR(0),
      Q => ram_empty_i
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo_wr_bin_cntr is
  port (
    S : out STD_LOGIC_VECTOR ( 5 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 5 downto 0 );
    \gic0.gc0.count_reg[5]_0\ : out STD_LOGIC_VECTOR ( 5 downto 0 );
    \gic0.gc0.count_d1_reg[5]_0\ : out STD_LOGIC_VECTOR ( 5 downto 0 );
    RD_PNTR_WR : in STD_LOGIC_VECTOR ( 5 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    wr_clk : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end header_fifo_wr_bin_cntr;

architecture STRUCTURE of header_fifo_wr_bin_cntr is
  signal \^q\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal \^gic0.gc0.count_d1_reg[5]_0\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal \^gic0.gc0.count_reg[5]_0\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal plusOp : STD_LOGIC_VECTOR ( 5 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \gic0.gc0.count[0]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \gic0.gc0.count[2]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \gic0.gc0.count[3]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \gic0.gc0.count[4]_i_1\ : label is "soft_lutpair6";
begin
  Q(5 downto 0) <= \^q\(5 downto 0);
  \gic0.gc0.count_d1_reg[5]_0\(5 downto 0) <= \^gic0.gc0.count_d1_reg[5]_0\(5 downto 0);
  \gic0.gc0.count_reg[5]_0\(5 downto 0) <= \^gic0.gc0.count_reg[5]_0\(5 downto 0);
\gic0.gc0.count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^gic0.gc0.count_reg[5]_0\(0),
      O => plusOp(0)
    );
\gic0.gc0.count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^gic0.gc0.count_reg[5]_0\(0),
      I1 => \^gic0.gc0.count_reg[5]_0\(1),
      O => plusOp(1)
    );
\gic0.gc0.count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^gic0.gc0.count_reg[5]_0\(0),
      I1 => \^gic0.gc0.count_reg[5]_0\(1),
      I2 => \^gic0.gc0.count_reg[5]_0\(2),
      O => plusOp(2)
    );
\gic0.gc0.count[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \^gic0.gc0.count_reg[5]_0\(1),
      I1 => \^gic0.gc0.count_reg[5]_0\(0),
      I2 => \^gic0.gc0.count_reg[5]_0\(2),
      I3 => \^gic0.gc0.count_reg[5]_0\(3),
      O => plusOp(3)
    );
\gic0.gc0.count[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \^gic0.gc0.count_reg[5]_0\(2),
      I1 => \^gic0.gc0.count_reg[5]_0\(0),
      I2 => \^gic0.gc0.count_reg[5]_0\(1),
      I3 => \^gic0.gc0.count_reg[5]_0\(3),
      I4 => \^gic0.gc0.count_reg[5]_0\(4),
      O => plusOp(4)
    );
\gic0.gc0.count[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \^gic0.gc0.count_reg[5]_0\(3),
      I1 => \^gic0.gc0.count_reg[5]_0\(1),
      I2 => \^gic0.gc0.count_reg[5]_0\(0),
      I3 => \^gic0.gc0.count_reg[5]_0\(2),
      I4 => \^gic0.gc0.count_reg[5]_0\(4),
      I5 => \^gic0.gc0.count_reg[5]_0\(5),
      O => plusOp(5)
    );
\gic0.gc0.count_d1_reg[0]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => wr_clk,
      CE => E(0),
      D => \^gic0.gc0.count_reg[5]_0\(0),
      PRE => AR(0),
      Q => \^gic0.gc0.count_d1_reg[5]_0\(0)
    );
\gic0.gc0.count_d1_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => E(0),
      CLR => AR(0),
      D => \^gic0.gc0.count_reg[5]_0\(1),
      Q => \^gic0.gc0.count_d1_reg[5]_0\(1)
    );
\gic0.gc0.count_d1_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => E(0),
      CLR => AR(0),
      D => \^gic0.gc0.count_reg[5]_0\(2),
      Q => \^gic0.gc0.count_d1_reg[5]_0\(2)
    );
\gic0.gc0.count_d1_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => E(0),
      CLR => AR(0),
      D => \^gic0.gc0.count_reg[5]_0\(3),
      Q => \^gic0.gc0.count_d1_reg[5]_0\(3)
    );
\gic0.gc0.count_d1_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => E(0),
      CLR => AR(0),
      D => \^gic0.gc0.count_reg[5]_0\(4),
      Q => \^gic0.gc0.count_d1_reg[5]_0\(4)
    );
\gic0.gc0.count_d1_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => E(0),
      CLR => AR(0),
      D => \^gic0.gc0.count_reg[5]_0\(5),
      Q => \^gic0.gc0.count_d1_reg[5]_0\(5)
    );
\gic0.gc0.count_d2_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => E(0),
      CLR => AR(0),
      D => \^gic0.gc0.count_d1_reg[5]_0\(0),
      Q => \^q\(0)
    );
\gic0.gc0.count_d2_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => E(0),
      CLR => AR(0),
      D => \^gic0.gc0.count_d1_reg[5]_0\(1),
      Q => \^q\(1)
    );
\gic0.gc0.count_d2_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => E(0),
      CLR => AR(0),
      D => \^gic0.gc0.count_d1_reg[5]_0\(2),
      Q => \^q\(2)
    );
\gic0.gc0.count_d2_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => E(0),
      CLR => AR(0),
      D => \^gic0.gc0.count_d1_reg[5]_0\(3),
      Q => \^q\(3)
    );
\gic0.gc0.count_d2_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => E(0),
      CLR => AR(0),
      D => \^gic0.gc0.count_d1_reg[5]_0\(4),
      Q => \^q\(4)
    );
\gic0.gc0.count_d2_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => E(0),
      CLR => AR(0),
      D => \^gic0.gc0.count_d1_reg[5]_0\(5),
      Q => \^q\(5)
    );
\gic0.gc0.count_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => E(0),
      CLR => AR(0),
      D => plusOp(0),
      Q => \^gic0.gc0.count_reg[5]_0\(0)
    );
\gic0.gc0.count_reg[1]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => wr_clk,
      CE => E(0),
      D => plusOp(1),
      PRE => AR(0),
      Q => \^gic0.gc0.count_reg[5]_0\(1)
    );
\gic0.gc0.count_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => E(0),
      CLR => AR(0),
      D => plusOp(2),
      Q => \^gic0.gc0.count_reg[5]_0\(2)
    );
\gic0.gc0.count_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => E(0),
      CLR => AR(0),
      D => plusOp(3),
      Q => \^gic0.gc0.count_reg[5]_0\(3)
    );
\gic0.gc0.count_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => E(0),
      CLR => AR(0),
      D => plusOp(4),
      Q => \^gic0.gc0.count_reg[5]_0\(4)
    );
\gic0.gc0.count_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => E(0),
      CLR => AR(0),
      D => plusOp(5),
      Q => \^gic0.gc0.count_reg[5]_0\(5)
    );
minusOp_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(5),
      I1 => RD_PNTR_WR(5),
      O => S(5)
    );
minusOp_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(4),
      I1 => RD_PNTR_WR(4),
      O => S(4)
    );
minusOp_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(3),
      I1 => RD_PNTR_WR(3),
      O => S(3)
    );
minusOp_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(2),
      I1 => RD_PNTR_WR(2),
      O => S(2)
    );
minusOp_carry_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(1),
      I1 => RD_PNTR_WR(1),
      O => S(1)
    );
minusOp_carry_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(0),
      I1 => RD_PNTR_WR(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo_wr_dc_as is
  port (
    wr_data_count : out STD_LOGIC_VECTOR ( 5 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 4 downto 0 );
    S : in STD_LOGIC_VECTOR ( 5 downto 0 );
    wr_clk : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end header_fifo_wr_dc_as;

architecture STRUCTURE of header_fifo_wr_dc_as is
  signal minusOp_carry_n_10 : STD_LOGIC;
  signal minusOp_carry_n_11 : STD_LOGIC;
  signal minusOp_carry_n_12 : STD_LOGIC;
  signal minusOp_carry_n_13 : STD_LOGIC;
  signal minusOp_carry_n_14 : STD_LOGIC;
  signal minusOp_carry_n_15 : STD_LOGIC;
  signal minusOp_carry_n_3 : STD_LOGIC;
  signal minusOp_carry_n_4 : STD_LOGIC;
  signal minusOp_carry_n_5 : STD_LOGIC;
  signal minusOp_carry_n_6 : STD_LOGIC;
  signal minusOp_carry_n_7 : STD_LOGIC;
  signal NLW_minusOp_carry_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 5 );
  signal NLW_minusOp_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 6 );
begin
minusOp_carry: unisim.vcomponents.CARRY8
     port map (
      CI => '1',
      CI_TOP => '0',
      CO(7 downto 5) => NLW_minusOp_carry_CO_UNCONNECTED(7 downto 5),
      CO(4) => minusOp_carry_n_3,
      CO(3) => minusOp_carry_n_4,
      CO(2) => minusOp_carry_n_5,
      CO(1) => minusOp_carry_n_6,
      CO(0) => minusOp_carry_n_7,
      DI(7 downto 5) => B"000",
      DI(4 downto 0) => Q(4 downto 0),
      O(7 downto 6) => NLW_minusOp_carry_O_UNCONNECTED(7 downto 6),
      O(5) => minusOp_carry_n_10,
      O(4) => minusOp_carry_n_11,
      O(3) => minusOp_carry_n_12,
      O(2) => minusOp_carry_n_13,
      O(1) => minusOp_carry_n_14,
      O(0) => minusOp_carry_n_15,
      S(7 downto 6) => B"00",
      S(5 downto 0) => S(5 downto 0)
    );
\wr_data_count_i_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => '1',
      CLR => AR(0),
      D => minusOp_carry_n_15,
      Q => wr_data_count(0)
    );
\wr_data_count_i_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => '1',
      CLR => AR(0),
      D => minusOp_carry_n_14,
      Q => wr_data_count(1)
    );
\wr_data_count_i_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => '1',
      CLR => AR(0),
      D => minusOp_carry_n_13,
      Q => wr_data_count(2)
    );
\wr_data_count_i_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => '1',
      CLR => AR(0),
      D => minusOp_carry_n_12,
      Q => wr_data_count(3)
    );
\wr_data_count_i_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => '1',
      CLR => AR(0),
      D => minusOp_carry_n_11,
      Q => wr_data_count(4)
    );
\wr_data_count_i_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => '1',
      CLR => AR(0),
      D => minusOp_carry_n_10,
      Q => wr_data_count(5)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo_wr_status_flags_as is
  port (
    full : out STD_LOGIC;
    \out\ : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    ram_full_fb_i_reg_0 : in STD_LOGIC;
    wr_clk : in STD_LOGIC;
    ram_full_fb_i_reg_1 : in STD_LOGIC;
    wr_en : in STD_LOGIC
  );
end header_fifo_wr_status_flags_as;

architecture STRUCTURE of header_fifo_wr_status_flags_as is
  signal ram_full_fb_i : STD_LOGIC;
  attribute DONT_TOUCH : boolean;
  attribute DONT_TOUCH of ram_full_fb_i : signal is std.standard.true;
  signal ram_full_i : STD_LOGIC;
  attribute DONT_TOUCH of ram_full_i : signal is std.standard.true;
  attribute DONT_TOUCH of ram_full_fb_i_reg : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of ram_full_fb_i_reg : label is "yes";
  attribute equivalent_register_removal : string;
  attribute equivalent_register_removal of ram_full_fb_i_reg : label is "no";
  attribute DONT_TOUCH of ram_full_i_reg : label is std.standard.true;
  attribute KEEP of ram_full_i_reg : label is "yes";
  attribute equivalent_register_removal of ram_full_i_reg : label is "no";
begin
  full <= ram_full_i;
  \out\ <= ram_full_fb_i;
\gic0.gc0.count_d1[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => wr_en,
      I1 => ram_full_fb_i,
      O => E(0)
    );
ram_full_fb_i_reg: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => wr_clk,
      CE => '1',
      D => ram_full_fb_i_reg_0,
      PRE => ram_full_fb_i_reg_1,
      Q => ram_full_fb_i
    );
ram_full_i_reg: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => wr_clk,
      CE => '1',
      D => ram_full_fb_i_reg_0,
      PRE => ram_full_fb_i_reg_1,
      Q => ram_full_i
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo_clk_x_pntrs is
  port (
    S : out STD_LOGIC_VECTOR ( 5 downto 0 );
    WR_PNTR_RD : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ram_full_fb_i_reg : out STD_LOGIC;
    RD_PNTR_WR : out STD_LOGIC_VECTOR ( 5 downto 0 );
    \dest_out_bin_ff_reg[0]\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 5 downto 0 );
    \out\ : in STD_LOGIC;
    wr_en : in STD_LOGIC;
    ram_full_fb_i_reg_0 : in STD_LOGIC;
    ram_full_i_i_3_0 : in STD_LOGIC_VECTOR ( 5 downto 0 );
    ram_full_i_i_2_0 : in STD_LOGIC_VECTOR ( 5 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    comp1 : in STD_LOGIC;
    wr_clk : in STD_LOGIC;
    \src_gray_ff_reg[5]\ : in STD_LOGIC_VECTOR ( 5 downto 0 );
    rd_clk : in STD_LOGIC
  );
end header_fifo_clk_x_pntrs;

architecture STRUCTURE of header_fifo_clk_x_pntrs is
  signal \^rd_pntr_wr\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal \^wr_pntr_rd\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal \gntv_or_sync_fifo.gl0.wr/gwas.wsts/comp1\ : STD_LOGIC;
  signal \gntv_or_sync_fifo.gl0.wr/gwas.wsts/comp2\ : STD_LOGIC;
  signal ram_empty_i_i_2_n_0 : STD_LOGIC;
  signal ram_empty_i_i_3_n_0 : STD_LOGIC;
  signal ram_empty_i_i_5_n_0 : STD_LOGIC;
  signal ram_full_i_i_4_n_0 : STD_LOGIC;
  signal ram_full_i_i_5_n_0 : STD_LOGIC;
  signal ram_full_i_i_6_n_0 : STD_LOGIC;
  signal ram_full_i_i_7_n_0 : STD_LOGIC;
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of rd_pntr_cdc_inst : label is 2;
  attribute INIT_SYNC_FF : integer;
  attribute INIT_SYNC_FF of rd_pntr_cdc_inst : label is 0;
  attribute REG_OUTPUT : integer;
  attribute REG_OUTPUT of rd_pntr_cdc_inst : label is 1;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of rd_pntr_cdc_inst : label is 0;
  attribute SIM_LOSSLESS_GRAY_CHK : integer;
  attribute SIM_LOSSLESS_GRAY_CHK of rd_pntr_cdc_inst : label is 0;
  attribute VERSION : integer;
  attribute VERSION of rd_pntr_cdc_inst : label is 0;
  attribute WIDTH : integer;
  attribute WIDTH of rd_pntr_cdc_inst : label is 6;
  attribute XPM_CDC : string;
  attribute XPM_CDC of rd_pntr_cdc_inst : label is "GRAY";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of rd_pntr_cdc_inst : label is "TRUE";
  attribute DEST_SYNC_FF of wr_pntr_cdc_inst : label is 2;
  attribute INIT_SYNC_FF of wr_pntr_cdc_inst : label is 0;
  attribute REG_OUTPUT of wr_pntr_cdc_inst : label is 1;
  attribute SIM_ASSERT_CHK of wr_pntr_cdc_inst : label is 0;
  attribute SIM_LOSSLESS_GRAY_CHK of wr_pntr_cdc_inst : label is 0;
  attribute VERSION of wr_pntr_cdc_inst : label is 0;
  attribute WIDTH of wr_pntr_cdc_inst : label is 6;
  attribute XPM_CDC of wr_pntr_cdc_inst : label is "GRAY";
  attribute XPM_MODULE of wr_pntr_cdc_inst : label is "TRUE";
begin
  RD_PNTR_WR(5 downto 0) <= \^rd_pntr_wr\(5 downto 0);
  WR_PNTR_RD(5 downto 0) <= \^wr_pntr_rd\(5 downto 0);
\minusOp_carry_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^wr_pntr_rd\(5),
      I1 => Q(5),
      O => S(5)
    );
\minusOp_carry_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^wr_pntr_rd\(4),
      I1 => Q(4),
      O => S(4)
    );
\minusOp_carry_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^wr_pntr_rd\(3),
      I1 => Q(3),
      O => S(3)
    );
\minusOp_carry_i_4__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^wr_pntr_rd\(2),
      I1 => Q(2),
      O => S(2)
    );
\minusOp_carry_i_5__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^wr_pntr_rd\(1),
      I1 => Q(1),
      O => S(1)
    );
\minusOp_carry_i_6__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^wr_pntr_rd\(0),
      I1 => Q(0),
      O => S(0)
    );
ram_empty_i_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF820082008200"
    )
        port map (
      I0 => ram_empty_i_i_2_n_0,
      I1 => \^wr_pntr_rd\(0),
      I2 => Q(0),
      I3 => ram_empty_i_i_3_n_0,
      I4 => E(0),
      I5 => comp1,
      O => \dest_out_bin_ff_reg[0]\
    );
ram_empty_i_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"90090000"
    )
        port map (
      I0 => Q(3),
      I1 => \^wr_pntr_rd\(3),
      I2 => Q(2),
      I3 => \^wr_pntr_rd\(2),
      I4 => ram_empty_i_i_5_n_0,
      O => ram_empty_i_i_2_n_0
    );
ram_empty_i_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^wr_pntr_rd\(1),
      I1 => Q(1),
      O => ram_empty_i_i_3_n_0
    );
ram_empty_i_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^wr_pntr_rd\(4),
      I1 => Q(4),
      I2 => \^wr_pntr_rd\(5),
      I3 => Q(5),
      O => ram_empty_i_i_5_n_0
    );
ram_full_i_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000FF20"
    )
        port map (
      I0 => \gntv_or_sync_fifo.gl0.wr/gwas.wsts/comp2\,
      I1 => \out\,
      I2 => wr_en,
      I3 => \gntv_or_sync_fifo.gl0.wr/gwas.wsts/comp1\,
      I4 => ram_full_fb_i_reg_0,
      O => ram_full_fb_i_reg
    );
ram_full_i_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000000000"
    )
        port map (
      I0 => ram_full_i_i_2_0(1),
      I1 => \^rd_pntr_wr\(1),
      I2 => ram_full_i_i_2_0(0),
      I3 => \^rd_pntr_wr\(0),
      I4 => ram_full_i_i_4_n_0,
      I5 => ram_full_i_i_5_n_0,
      O => \gntv_or_sync_fifo.gl0.wr/gwas.wsts/comp2\
    );
ram_full_i_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000000000"
    )
        port map (
      I0 => ram_full_i_i_3_0(1),
      I1 => \^rd_pntr_wr\(1),
      I2 => ram_full_i_i_3_0(0),
      I3 => \^rd_pntr_wr\(0),
      I4 => ram_full_i_i_6_n_0,
      I5 => ram_full_i_i_7_n_0,
      O => \gntv_or_sync_fifo.gl0.wr/gwas.wsts/comp1\
    );
ram_full_i_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^rd_pntr_wr\(4),
      I1 => ram_full_i_i_2_0(4),
      I2 => \^rd_pntr_wr\(5),
      I3 => ram_full_i_i_2_0(5),
      O => ram_full_i_i_4_n_0
    );
ram_full_i_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^rd_pntr_wr\(2),
      I1 => ram_full_i_i_2_0(2),
      I2 => \^rd_pntr_wr\(3),
      I3 => ram_full_i_i_2_0(3),
      O => ram_full_i_i_5_n_0
    );
ram_full_i_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^rd_pntr_wr\(4),
      I1 => ram_full_i_i_3_0(4),
      I2 => \^rd_pntr_wr\(5),
      I3 => ram_full_i_i_3_0(5),
      O => ram_full_i_i_6_n_0
    );
ram_full_i_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \^rd_pntr_wr\(2),
      I1 => ram_full_i_i_3_0(2),
      I2 => \^rd_pntr_wr\(3),
      I3 => ram_full_i_i_3_0(3),
      O => ram_full_i_i_7_n_0
    );
rd_pntr_cdc_inst: entity work.header_fifo_xpm_cdc_gray
     port map (
      dest_clk => wr_clk,
      dest_out_bin(5 downto 0) => \^rd_pntr_wr\(5 downto 0),
      src_clk => rd_clk,
      src_in_bin(5 downto 0) => Q(5 downto 0)
    );
wr_pntr_cdc_inst: entity work.\header_fifo_xpm_cdc_gray__2\
     port map (
      dest_clk => rd_clk,
      dest_out_bin(5 downto 0) => \^wr_pntr_rd\(5 downto 0),
      src_clk => wr_clk,
      src_in_bin(5 downto 0) => \src_gray_ff_reg[5]\(5 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo_memory is
  port (
    dout : out STD_LOGIC_VECTOR ( 255 downto 0 );
    wr_clk : in STD_LOGIC;
    din : in STD_LOGIC_VECTOR ( 255 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 5 downto 0 );
    \gpr1.dout_i_reg[0]\ : in STD_LOGIC_VECTOR ( 5 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \gpr1.dout_i_reg[0]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    rd_clk : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    \goreg_dm.dout_i_reg[255]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end header_fifo_memory;

architecture STRUCTURE of header_fifo_memory is
  signal dout_i : STD_LOGIC_VECTOR ( 255 downto 0 );
begin
\gdm.dm_gen.dm\: entity work.header_fifo_dmem
     port map (
      AR(0) => AR(0),
      E(0) => E(0),
      Q(5 downto 0) => Q(5 downto 0),
      din(255 downto 0) => din(255 downto 0),
      \gpr1.dout_i_reg[0]_0\(5 downto 0) => \gpr1.dout_i_reg[0]\(5 downto 0),
      \gpr1.dout_i_reg[0]_1\(0) => \gpr1.dout_i_reg[0]_0\(0),
      \gpr1.dout_i_reg[255]_0\(255 downto 0) => dout_i(255 downto 0),
      rd_clk => rd_clk,
      wr_clk => wr_clk
    );
\goreg_dm.dout_i_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(0),
      Q => dout(0)
    );
\goreg_dm.dout_i_reg[100]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(100),
      Q => dout(100)
    );
\goreg_dm.dout_i_reg[101]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(101),
      Q => dout(101)
    );
\goreg_dm.dout_i_reg[102]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(102),
      Q => dout(102)
    );
\goreg_dm.dout_i_reg[103]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(103),
      Q => dout(103)
    );
\goreg_dm.dout_i_reg[104]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(104),
      Q => dout(104)
    );
\goreg_dm.dout_i_reg[105]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(105),
      Q => dout(105)
    );
\goreg_dm.dout_i_reg[106]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(106),
      Q => dout(106)
    );
\goreg_dm.dout_i_reg[107]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(107),
      Q => dout(107)
    );
\goreg_dm.dout_i_reg[108]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(108),
      Q => dout(108)
    );
\goreg_dm.dout_i_reg[109]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(109),
      Q => dout(109)
    );
\goreg_dm.dout_i_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(10),
      Q => dout(10)
    );
\goreg_dm.dout_i_reg[110]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(110),
      Q => dout(110)
    );
\goreg_dm.dout_i_reg[111]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(111),
      Q => dout(111)
    );
\goreg_dm.dout_i_reg[112]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(112),
      Q => dout(112)
    );
\goreg_dm.dout_i_reg[113]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(113),
      Q => dout(113)
    );
\goreg_dm.dout_i_reg[114]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(114),
      Q => dout(114)
    );
\goreg_dm.dout_i_reg[115]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(115),
      Q => dout(115)
    );
\goreg_dm.dout_i_reg[116]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(116),
      Q => dout(116)
    );
\goreg_dm.dout_i_reg[117]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(117),
      Q => dout(117)
    );
\goreg_dm.dout_i_reg[118]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(118),
      Q => dout(118)
    );
\goreg_dm.dout_i_reg[119]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(119),
      Q => dout(119)
    );
\goreg_dm.dout_i_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(11),
      Q => dout(11)
    );
\goreg_dm.dout_i_reg[120]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(120),
      Q => dout(120)
    );
\goreg_dm.dout_i_reg[121]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(121),
      Q => dout(121)
    );
\goreg_dm.dout_i_reg[122]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(122),
      Q => dout(122)
    );
\goreg_dm.dout_i_reg[123]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(123),
      Q => dout(123)
    );
\goreg_dm.dout_i_reg[124]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(124),
      Q => dout(124)
    );
\goreg_dm.dout_i_reg[125]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(125),
      Q => dout(125)
    );
\goreg_dm.dout_i_reg[126]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(126),
      Q => dout(126)
    );
\goreg_dm.dout_i_reg[127]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(127),
      Q => dout(127)
    );
\goreg_dm.dout_i_reg[128]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(128),
      Q => dout(128)
    );
\goreg_dm.dout_i_reg[129]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(129),
      Q => dout(129)
    );
\goreg_dm.dout_i_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(12),
      Q => dout(12)
    );
\goreg_dm.dout_i_reg[130]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(130),
      Q => dout(130)
    );
\goreg_dm.dout_i_reg[131]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(131),
      Q => dout(131)
    );
\goreg_dm.dout_i_reg[132]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(132),
      Q => dout(132)
    );
\goreg_dm.dout_i_reg[133]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(133),
      Q => dout(133)
    );
\goreg_dm.dout_i_reg[134]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(134),
      Q => dout(134)
    );
\goreg_dm.dout_i_reg[135]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(135),
      Q => dout(135)
    );
\goreg_dm.dout_i_reg[136]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(136),
      Q => dout(136)
    );
\goreg_dm.dout_i_reg[137]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(137),
      Q => dout(137)
    );
\goreg_dm.dout_i_reg[138]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(138),
      Q => dout(138)
    );
\goreg_dm.dout_i_reg[139]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(139),
      Q => dout(139)
    );
\goreg_dm.dout_i_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(13),
      Q => dout(13)
    );
\goreg_dm.dout_i_reg[140]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(140),
      Q => dout(140)
    );
\goreg_dm.dout_i_reg[141]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(141),
      Q => dout(141)
    );
\goreg_dm.dout_i_reg[142]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(142),
      Q => dout(142)
    );
\goreg_dm.dout_i_reg[143]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(143),
      Q => dout(143)
    );
\goreg_dm.dout_i_reg[144]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(144),
      Q => dout(144)
    );
\goreg_dm.dout_i_reg[145]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(145),
      Q => dout(145)
    );
\goreg_dm.dout_i_reg[146]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(146),
      Q => dout(146)
    );
\goreg_dm.dout_i_reg[147]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(147),
      Q => dout(147)
    );
\goreg_dm.dout_i_reg[148]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(148),
      Q => dout(148)
    );
\goreg_dm.dout_i_reg[149]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(149),
      Q => dout(149)
    );
\goreg_dm.dout_i_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(14),
      Q => dout(14)
    );
\goreg_dm.dout_i_reg[150]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(150),
      Q => dout(150)
    );
\goreg_dm.dout_i_reg[151]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(151),
      Q => dout(151)
    );
\goreg_dm.dout_i_reg[152]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(152),
      Q => dout(152)
    );
\goreg_dm.dout_i_reg[153]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(153),
      Q => dout(153)
    );
\goreg_dm.dout_i_reg[154]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(154),
      Q => dout(154)
    );
\goreg_dm.dout_i_reg[155]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(155),
      Q => dout(155)
    );
\goreg_dm.dout_i_reg[156]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(156),
      Q => dout(156)
    );
\goreg_dm.dout_i_reg[157]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(157),
      Q => dout(157)
    );
\goreg_dm.dout_i_reg[158]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(158),
      Q => dout(158)
    );
\goreg_dm.dout_i_reg[159]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(159),
      Q => dout(159)
    );
\goreg_dm.dout_i_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(15),
      Q => dout(15)
    );
\goreg_dm.dout_i_reg[160]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(160),
      Q => dout(160)
    );
\goreg_dm.dout_i_reg[161]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(161),
      Q => dout(161)
    );
\goreg_dm.dout_i_reg[162]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(162),
      Q => dout(162)
    );
\goreg_dm.dout_i_reg[163]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(163),
      Q => dout(163)
    );
\goreg_dm.dout_i_reg[164]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(164),
      Q => dout(164)
    );
\goreg_dm.dout_i_reg[165]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(165),
      Q => dout(165)
    );
\goreg_dm.dout_i_reg[166]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(166),
      Q => dout(166)
    );
\goreg_dm.dout_i_reg[167]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(167),
      Q => dout(167)
    );
\goreg_dm.dout_i_reg[168]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(168),
      Q => dout(168)
    );
\goreg_dm.dout_i_reg[169]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(169),
      Q => dout(169)
    );
\goreg_dm.dout_i_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(16),
      Q => dout(16)
    );
\goreg_dm.dout_i_reg[170]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(170),
      Q => dout(170)
    );
\goreg_dm.dout_i_reg[171]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(171),
      Q => dout(171)
    );
\goreg_dm.dout_i_reg[172]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(172),
      Q => dout(172)
    );
\goreg_dm.dout_i_reg[173]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(173),
      Q => dout(173)
    );
\goreg_dm.dout_i_reg[174]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(174),
      Q => dout(174)
    );
\goreg_dm.dout_i_reg[175]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(175),
      Q => dout(175)
    );
\goreg_dm.dout_i_reg[176]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(176),
      Q => dout(176)
    );
\goreg_dm.dout_i_reg[177]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(177),
      Q => dout(177)
    );
\goreg_dm.dout_i_reg[178]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(178),
      Q => dout(178)
    );
\goreg_dm.dout_i_reg[179]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(179),
      Q => dout(179)
    );
\goreg_dm.dout_i_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(17),
      Q => dout(17)
    );
\goreg_dm.dout_i_reg[180]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(180),
      Q => dout(180)
    );
\goreg_dm.dout_i_reg[181]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(181),
      Q => dout(181)
    );
\goreg_dm.dout_i_reg[182]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(182),
      Q => dout(182)
    );
\goreg_dm.dout_i_reg[183]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(183),
      Q => dout(183)
    );
\goreg_dm.dout_i_reg[184]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(184),
      Q => dout(184)
    );
\goreg_dm.dout_i_reg[185]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(185),
      Q => dout(185)
    );
\goreg_dm.dout_i_reg[186]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(186),
      Q => dout(186)
    );
\goreg_dm.dout_i_reg[187]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(187),
      Q => dout(187)
    );
\goreg_dm.dout_i_reg[188]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(188),
      Q => dout(188)
    );
\goreg_dm.dout_i_reg[189]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(189),
      Q => dout(189)
    );
\goreg_dm.dout_i_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(18),
      Q => dout(18)
    );
\goreg_dm.dout_i_reg[190]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(190),
      Q => dout(190)
    );
\goreg_dm.dout_i_reg[191]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(191),
      Q => dout(191)
    );
\goreg_dm.dout_i_reg[192]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(192),
      Q => dout(192)
    );
\goreg_dm.dout_i_reg[193]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(193),
      Q => dout(193)
    );
\goreg_dm.dout_i_reg[194]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(194),
      Q => dout(194)
    );
\goreg_dm.dout_i_reg[195]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(195),
      Q => dout(195)
    );
\goreg_dm.dout_i_reg[196]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(196),
      Q => dout(196)
    );
\goreg_dm.dout_i_reg[197]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(197),
      Q => dout(197)
    );
\goreg_dm.dout_i_reg[198]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(198),
      Q => dout(198)
    );
\goreg_dm.dout_i_reg[199]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(199),
      Q => dout(199)
    );
\goreg_dm.dout_i_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(19),
      Q => dout(19)
    );
\goreg_dm.dout_i_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(1),
      Q => dout(1)
    );
\goreg_dm.dout_i_reg[200]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(200),
      Q => dout(200)
    );
\goreg_dm.dout_i_reg[201]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(201),
      Q => dout(201)
    );
\goreg_dm.dout_i_reg[202]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(202),
      Q => dout(202)
    );
\goreg_dm.dout_i_reg[203]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(203),
      Q => dout(203)
    );
\goreg_dm.dout_i_reg[204]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(204),
      Q => dout(204)
    );
\goreg_dm.dout_i_reg[205]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(205),
      Q => dout(205)
    );
\goreg_dm.dout_i_reg[206]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(206),
      Q => dout(206)
    );
\goreg_dm.dout_i_reg[207]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(207),
      Q => dout(207)
    );
\goreg_dm.dout_i_reg[208]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(208),
      Q => dout(208)
    );
\goreg_dm.dout_i_reg[209]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(209),
      Q => dout(209)
    );
\goreg_dm.dout_i_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(20),
      Q => dout(20)
    );
\goreg_dm.dout_i_reg[210]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(210),
      Q => dout(210)
    );
\goreg_dm.dout_i_reg[211]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(211),
      Q => dout(211)
    );
\goreg_dm.dout_i_reg[212]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(212),
      Q => dout(212)
    );
\goreg_dm.dout_i_reg[213]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(213),
      Q => dout(213)
    );
\goreg_dm.dout_i_reg[214]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(214),
      Q => dout(214)
    );
\goreg_dm.dout_i_reg[215]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(215),
      Q => dout(215)
    );
\goreg_dm.dout_i_reg[216]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(216),
      Q => dout(216)
    );
\goreg_dm.dout_i_reg[217]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(217),
      Q => dout(217)
    );
\goreg_dm.dout_i_reg[218]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(218),
      Q => dout(218)
    );
\goreg_dm.dout_i_reg[219]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(219),
      Q => dout(219)
    );
\goreg_dm.dout_i_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(21),
      Q => dout(21)
    );
\goreg_dm.dout_i_reg[220]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(220),
      Q => dout(220)
    );
\goreg_dm.dout_i_reg[221]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(221),
      Q => dout(221)
    );
\goreg_dm.dout_i_reg[222]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(222),
      Q => dout(222)
    );
\goreg_dm.dout_i_reg[223]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(223),
      Q => dout(223)
    );
\goreg_dm.dout_i_reg[224]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(224),
      Q => dout(224)
    );
\goreg_dm.dout_i_reg[225]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(225),
      Q => dout(225)
    );
\goreg_dm.dout_i_reg[226]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(226),
      Q => dout(226)
    );
\goreg_dm.dout_i_reg[227]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(227),
      Q => dout(227)
    );
\goreg_dm.dout_i_reg[228]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(228),
      Q => dout(228)
    );
\goreg_dm.dout_i_reg[229]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(229),
      Q => dout(229)
    );
\goreg_dm.dout_i_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(22),
      Q => dout(22)
    );
\goreg_dm.dout_i_reg[230]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(230),
      Q => dout(230)
    );
\goreg_dm.dout_i_reg[231]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(231),
      Q => dout(231)
    );
\goreg_dm.dout_i_reg[232]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(232),
      Q => dout(232)
    );
\goreg_dm.dout_i_reg[233]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(233),
      Q => dout(233)
    );
\goreg_dm.dout_i_reg[234]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(234),
      Q => dout(234)
    );
\goreg_dm.dout_i_reg[235]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(235),
      Q => dout(235)
    );
\goreg_dm.dout_i_reg[236]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(236),
      Q => dout(236)
    );
\goreg_dm.dout_i_reg[237]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(237),
      Q => dout(237)
    );
\goreg_dm.dout_i_reg[238]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(238),
      Q => dout(238)
    );
\goreg_dm.dout_i_reg[239]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(239),
      Q => dout(239)
    );
\goreg_dm.dout_i_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(23),
      Q => dout(23)
    );
\goreg_dm.dout_i_reg[240]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(240),
      Q => dout(240)
    );
\goreg_dm.dout_i_reg[241]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(241),
      Q => dout(241)
    );
\goreg_dm.dout_i_reg[242]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(242),
      Q => dout(242)
    );
\goreg_dm.dout_i_reg[243]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(243),
      Q => dout(243)
    );
\goreg_dm.dout_i_reg[244]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(244),
      Q => dout(244)
    );
\goreg_dm.dout_i_reg[245]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(245),
      Q => dout(245)
    );
\goreg_dm.dout_i_reg[246]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(246),
      Q => dout(246)
    );
\goreg_dm.dout_i_reg[247]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(247),
      Q => dout(247)
    );
\goreg_dm.dout_i_reg[248]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(248),
      Q => dout(248)
    );
\goreg_dm.dout_i_reg[249]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(249),
      Q => dout(249)
    );
\goreg_dm.dout_i_reg[24]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(24),
      Q => dout(24)
    );
\goreg_dm.dout_i_reg[250]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(250),
      Q => dout(250)
    );
\goreg_dm.dout_i_reg[251]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(251),
      Q => dout(251)
    );
\goreg_dm.dout_i_reg[252]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(252),
      Q => dout(252)
    );
\goreg_dm.dout_i_reg[253]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(253),
      Q => dout(253)
    );
\goreg_dm.dout_i_reg[254]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(254),
      Q => dout(254)
    );
\goreg_dm.dout_i_reg[255]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(255),
      Q => dout(255)
    );
\goreg_dm.dout_i_reg[25]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(25),
      Q => dout(25)
    );
\goreg_dm.dout_i_reg[26]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(26),
      Q => dout(26)
    );
\goreg_dm.dout_i_reg[27]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(27),
      Q => dout(27)
    );
\goreg_dm.dout_i_reg[28]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(28),
      Q => dout(28)
    );
\goreg_dm.dout_i_reg[29]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(29),
      Q => dout(29)
    );
\goreg_dm.dout_i_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(2),
      Q => dout(2)
    );
\goreg_dm.dout_i_reg[30]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(30),
      Q => dout(30)
    );
\goreg_dm.dout_i_reg[31]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(31),
      Q => dout(31)
    );
\goreg_dm.dout_i_reg[32]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(32),
      Q => dout(32)
    );
\goreg_dm.dout_i_reg[33]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(33),
      Q => dout(33)
    );
\goreg_dm.dout_i_reg[34]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(34),
      Q => dout(34)
    );
\goreg_dm.dout_i_reg[35]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(35),
      Q => dout(35)
    );
\goreg_dm.dout_i_reg[36]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(36),
      Q => dout(36)
    );
\goreg_dm.dout_i_reg[37]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(37),
      Q => dout(37)
    );
\goreg_dm.dout_i_reg[38]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(38),
      Q => dout(38)
    );
\goreg_dm.dout_i_reg[39]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(39),
      Q => dout(39)
    );
\goreg_dm.dout_i_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(3),
      Q => dout(3)
    );
\goreg_dm.dout_i_reg[40]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(40),
      Q => dout(40)
    );
\goreg_dm.dout_i_reg[41]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(41),
      Q => dout(41)
    );
\goreg_dm.dout_i_reg[42]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(42),
      Q => dout(42)
    );
\goreg_dm.dout_i_reg[43]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(43),
      Q => dout(43)
    );
\goreg_dm.dout_i_reg[44]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(44),
      Q => dout(44)
    );
\goreg_dm.dout_i_reg[45]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(45),
      Q => dout(45)
    );
\goreg_dm.dout_i_reg[46]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(46),
      Q => dout(46)
    );
\goreg_dm.dout_i_reg[47]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(47),
      Q => dout(47)
    );
\goreg_dm.dout_i_reg[48]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(48),
      Q => dout(48)
    );
\goreg_dm.dout_i_reg[49]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(49),
      Q => dout(49)
    );
\goreg_dm.dout_i_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(4),
      Q => dout(4)
    );
\goreg_dm.dout_i_reg[50]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(50),
      Q => dout(50)
    );
\goreg_dm.dout_i_reg[51]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(51),
      Q => dout(51)
    );
\goreg_dm.dout_i_reg[52]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(52),
      Q => dout(52)
    );
\goreg_dm.dout_i_reg[53]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(53),
      Q => dout(53)
    );
\goreg_dm.dout_i_reg[54]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(54),
      Q => dout(54)
    );
\goreg_dm.dout_i_reg[55]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(55),
      Q => dout(55)
    );
\goreg_dm.dout_i_reg[56]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(56),
      Q => dout(56)
    );
\goreg_dm.dout_i_reg[57]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(57),
      Q => dout(57)
    );
\goreg_dm.dout_i_reg[58]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(58),
      Q => dout(58)
    );
\goreg_dm.dout_i_reg[59]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(59),
      Q => dout(59)
    );
\goreg_dm.dout_i_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(5),
      Q => dout(5)
    );
\goreg_dm.dout_i_reg[60]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(60),
      Q => dout(60)
    );
\goreg_dm.dout_i_reg[61]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(61),
      Q => dout(61)
    );
\goreg_dm.dout_i_reg[62]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(62),
      Q => dout(62)
    );
\goreg_dm.dout_i_reg[63]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(63),
      Q => dout(63)
    );
\goreg_dm.dout_i_reg[64]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(64),
      Q => dout(64)
    );
\goreg_dm.dout_i_reg[65]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(65),
      Q => dout(65)
    );
\goreg_dm.dout_i_reg[66]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(66),
      Q => dout(66)
    );
\goreg_dm.dout_i_reg[67]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(67),
      Q => dout(67)
    );
\goreg_dm.dout_i_reg[68]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(68),
      Q => dout(68)
    );
\goreg_dm.dout_i_reg[69]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(69),
      Q => dout(69)
    );
\goreg_dm.dout_i_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(6),
      Q => dout(6)
    );
\goreg_dm.dout_i_reg[70]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(70),
      Q => dout(70)
    );
\goreg_dm.dout_i_reg[71]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(71),
      Q => dout(71)
    );
\goreg_dm.dout_i_reg[72]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(72),
      Q => dout(72)
    );
\goreg_dm.dout_i_reg[73]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(73),
      Q => dout(73)
    );
\goreg_dm.dout_i_reg[74]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(74),
      Q => dout(74)
    );
\goreg_dm.dout_i_reg[75]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(75),
      Q => dout(75)
    );
\goreg_dm.dout_i_reg[76]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(76),
      Q => dout(76)
    );
\goreg_dm.dout_i_reg[77]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(77),
      Q => dout(77)
    );
\goreg_dm.dout_i_reg[78]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(78),
      Q => dout(78)
    );
\goreg_dm.dout_i_reg[79]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(79),
      Q => dout(79)
    );
\goreg_dm.dout_i_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(7),
      Q => dout(7)
    );
\goreg_dm.dout_i_reg[80]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(80),
      Q => dout(80)
    );
\goreg_dm.dout_i_reg[81]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(81),
      Q => dout(81)
    );
\goreg_dm.dout_i_reg[82]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(82),
      Q => dout(82)
    );
\goreg_dm.dout_i_reg[83]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(83),
      Q => dout(83)
    );
\goreg_dm.dout_i_reg[84]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(84),
      Q => dout(84)
    );
\goreg_dm.dout_i_reg[85]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(85),
      Q => dout(85)
    );
\goreg_dm.dout_i_reg[86]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(86),
      Q => dout(86)
    );
\goreg_dm.dout_i_reg[87]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(87),
      Q => dout(87)
    );
\goreg_dm.dout_i_reg[88]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(88),
      Q => dout(88)
    );
\goreg_dm.dout_i_reg[89]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(89),
      Q => dout(89)
    );
\goreg_dm.dout_i_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(8),
      Q => dout(8)
    );
\goreg_dm.dout_i_reg[90]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(90),
      Q => dout(90)
    );
\goreg_dm.dout_i_reg[91]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(91),
      Q => dout(91)
    );
\goreg_dm.dout_i_reg[92]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(92),
      Q => dout(92)
    );
\goreg_dm.dout_i_reg[93]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(93),
      Q => dout(93)
    );
\goreg_dm.dout_i_reg[94]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(94),
      Q => dout(94)
    );
\goreg_dm.dout_i_reg[95]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(95),
      Q => dout(95)
    );
\goreg_dm.dout_i_reg[96]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(96),
      Q => dout(96)
    );
\goreg_dm.dout_i_reg[97]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(97),
      Q => dout(97)
    );
\goreg_dm.dout_i_reg[98]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(98),
      Q => dout(98)
    );
\goreg_dm.dout_i_reg[99]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(99),
      Q => dout(99)
    );
\goreg_dm.dout_i_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => \goreg_dm.dout_i_reg[255]_0\(0),
      CLR => AR(0),
      D => dout_i(9),
      Q => dout(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo_rd_logic is
  port (
    empty : out STD_LOGIC;
    valid : out STD_LOGIC;
    underflow : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    comp1 : out STD_LOGIC;
    \gpregsm1.curr_fwft_state_reg[1]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    ram_empty_fb_i_reg : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : out STD_LOGIC_VECTOR ( 5 downto 0 );
    rd_data_count : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ram_empty_fb_i_reg_0 : in STD_LOGIC;
    rd_clk : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    WR_PNTR_RD : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S : in STD_LOGIC_VECTOR ( 5 downto 0 );
    rd_en : in STD_LOGIC
  );
end header_fifo_rd_logic;

architecture STRUCTURE of header_fifo_rd_logic is
  signal empty_fb_i : STD_LOGIC;
  signal \^ram_empty_fb_i_reg\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal underflow_i0 : STD_LOGIC;
begin
  ram_empty_fb_i_reg(0) <= \^ram_empty_fb_i_reg\(0);
\gr1.gr1_int.rfwft\: entity work.header_fifo_rd_fwft
     port map (
      AR(0) => AR(0),
      E(0) => E(0),
      empty => empty,
      \gpregsm1.curr_fwft_state_reg[1]_0\(0) => \gpregsm1.curr_fwft_state_reg[1]\(0),
      \out\ => empty_fb_i,
      ram_empty_fb_i_reg(0) => \^ram_empty_fb_i_reg\(0),
      rd_clk => rd_clk,
      rd_en => rd_en,
      underflow_i0 => underflow_i0,
      valid => valid
    );
\gras.grdc1.rdc\: entity work.header_fifo_rd_dc_as
     port map (
      AR(0) => AR(0),
      S(5 downto 0) => S(5 downto 0),
      WR_PNTR_RD(4 downto 0) => WR_PNTR_RD(4 downto 0),
      rd_clk => rd_clk,
      rd_data_count(5 downto 0) => rd_data_count(5 downto 0)
    );
\gras.rsts\: entity work.header_fifo_rd_status_flags_as
     port map (
      AR(0) => AR(0),
      \out\ => empty_fb_i,
      ram_empty_fb_i_reg_0 => ram_empty_fb_i_reg_0,
      rd_clk => rd_clk
    );
\grhf.rhf\: entity work.header_fifo_rd_handshaking_flags
     port map (
      rd_clk => rd_clk,
      underflow => underflow,
      underflow_i0 => underflow_i0
    );
rpntr: entity work.header_fifo_rd_bin_cntr
     port map (
      AR(0) => AR(0),
      E(0) => \^ram_empty_fb_i_reg\(0),
      Q(5 downto 0) => Q(5 downto 0),
      WR_PNTR_RD(5 downto 0) => WR_PNTR_RD(5 downto 0),
      comp1 => comp1,
      rd_clk => rd_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo_reset_blk_ramfifo is
  port (
    AR : out STD_LOGIC_VECTOR ( 0 to 0 );
    \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.sckt_rd_rst_ic_reg_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \out\ : out STD_LOGIC;
    \grstd1.grst_full.grst_f.rst_d3_reg_0\ : out STD_LOGIC;
    wr_rst_busy : out STD_LOGIC;
    rst : in STD_LOGIC;
    wr_clk : in STD_LOGIC;
    rd_clk : in STD_LOGIC
  );
end header_fifo_reset_blk_ramfifo;

architecture STRUCTURE of header_fifo_reset_blk_ramfifo is
  signal \^ar\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal dest_out : STD_LOGIC;
  signal \grstd1.grst_full.grst_f.rst_d3_i_1_n_0\ : STD_LOGIC;
  signal \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.sckt_rd_rst_ic_i_1_n_0\ : STD_LOGIC;
  signal \^ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.sckt_rd_rst_ic_reg_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.sckt_wr_rst_ic_i_1_n_0\ : STD_LOGIC;
  signal \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.wr_rst_busy_i_i_1_n_0\ : STD_LOGIC;
  signal rd_rst_reg : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute DONT_TOUCH : boolean;
  attribute DONT_TOUCH of rd_rst_reg : signal is std.standard.true;
  signal rd_rst_wr_ext : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rst_d1 : STD_LOGIC;
  attribute async_reg : string;
  attribute async_reg of rst_d1 : signal is "true";
  attribute msgon : string;
  attribute msgon of rst_d1 : signal is "true";
  signal rst_d2 : STD_LOGIC;
  attribute async_reg of rst_d2 : signal is "true";
  attribute msgon of rst_d2 : signal is "true";
  signal rst_d3 : STD_LOGIC;
  attribute async_reg of rst_d3 : signal is "true";
  attribute msgon of rst_d3 : signal is "true";
  signal rst_d4 : STD_LOGIC;
  attribute async_reg of rst_d4 : signal is "true";
  attribute msgon of rst_d4 : signal is "true";
  signal rst_d5 : STD_LOGIC;
  attribute async_reg of rst_d5 : signal is "true";
  attribute msgon of rst_d5 : signal is "true";
  signal rst_d6 : STD_LOGIC;
  attribute async_reg of rst_d6 : signal is "true";
  attribute msgon of rst_d6 : signal is "true";
  signal rst_d7 : STD_LOGIC;
  attribute async_reg of rst_d7 : signal is "true";
  attribute msgon of rst_d7 : signal is "true";
  signal rst_rd_reg1 : STD_LOGIC;
  attribute async_reg of rst_rd_reg1 : signal is "true";
  attribute msgon of rst_rd_reg1 : signal is "true";
  signal rst_rd_reg2 : STD_LOGIC;
  attribute async_reg of rst_rd_reg2 : signal is "true";
  attribute msgon of rst_rd_reg2 : signal is "true";
  signal rst_wr_reg1 : STD_LOGIC;
  attribute async_reg of rst_wr_reg1 : signal is "true";
  attribute msgon of rst_wr_reg1 : signal is "true";
  signal rst_wr_reg2 : STD_LOGIC;
  attribute async_reg of rst_wr_reg2 : signal is "true";
  attribute msgon of rst_wr_reg2 : signal is "true";
  signal sckt_rd_rst_wr : STD_LOGIC;
  signal \^wr_rst_busy\ : STD_LOGIC;
  signal wr_rst_rd_ext : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal wr_rst_reg : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute DONT_TOUCH of wr_rst_reg : signal is std.standard.true;
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \grstd1.grst_full.grst_f.rst_d1_reg\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \grstd1.grst_full.grst_f.rst_d1_reg\ : label is "yes";
  attribute msgon of \grstd1.grst_full.grst_f.rst_d1_reg\ : label is "true";
  attribute ASYNC_REG_boolean of \grstd1.grst_full.grst_f.rst_d2_reg\ : label is std.standard.true;
  attribute KEEP of \grstd1.grst_full.grst_f.rst_d2_reg\ : label is "yes";
  attribute msgon of \grstd1.grst_full.grst_f.rst_d2_reg\ : label is "true";
  attribute ASYNC_REG_boolean of \grstd1.grst_full.grst_f.rst_d3_reg\ : label is std.standard.true;
  attribute KEEP of \grstd1.grst_full.grst_f.rst_d3_reg\ : label is "yes";
  attribute msgon of \grstd1.grst_full.grst_f.rst_d3_reg\ : label is "true";
  attribute ASYNC_REG_boolean of \grstd1.grst_full.grst_f.rst_d4_reg\ : label is std.standard.true;
  attribute KEEP of \grstd1.grst_full.grst_f.rst_d4_reg\ : label is "yes";
  attribute msgon of \grstd1.grst_full.grst_f.rst_d4_reg\ : label is "true";
  attribute DEF_VAL : string;
  attribute DEF_VAL of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.rst_rd_reg2_inst\ : label is "1'b0";
  attribute DEST_SYNC_FF : integer;
  attribute DEST_SYNC_FF of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.rst_rd_reg2_inst\ : label is 2;
  attribute INIT_SYNC_FF : integer;
  attribute INIT_SYNC_FF of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.rst_rd_reg2_inst\ : label is 0;
  attribute INV_DEF_VAL : string;
  attribute INV_DEF_VAL of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.rst_rd_reg2_inst\ : label is "1'b1";
  attribute RST_ACTIVE_HIGH : integer;
  attribute RST_ACTIVE_HIGH of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.rst_rd_reg2_inst\ : label is 1;
  attribute VERSION : integer;
  attribute VERSION of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.rst_rd_reg2_inst\ : label is 0;
  attribute XPM_CDC : string;
  attribute XPM_CDC of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.rst_rd_reg2_inst\ : label is "ASYNC_RST";
  attribute XPM_MODULE : string;
  attribute XPM_MODULE of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.rst_rd_reg2_inst\ : label is "TRUE";
  attribute DEST_SYNC_FF of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.xpm_cdc_single_inst_rrst_wr\ : label is 4;
  attribute INIT_SYNC_FF of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.xpm_cdc_single_inst_rrst_wr\ : label is 0;
  attribute SIM_ASSERT_CHK : integer;
  attribute SIM_ASSERT_CHK of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.xpm_cdc_single_inst_rrst_wr\ : label is 0;
  attribute SRC_INPUT_REG : integer;
  attribute SRC_INPUT_REG of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.xpm_cdc_single_inst_rrst_wr\ : label is 0;
  attribute VERSION of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.xpm_cdc_single_inst_rrst_wr\ : label is 0;
  attribute XPM_CDC of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.xpm_cdc_single_inst_rrst_wr\ : label is "SINGLE";
  attribute XPM_MODULE of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.xpm_cdc_single_inst_rrst_wr\ : label is "TRUE";
  attribute DEST_SYNC_FF of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.xpm_cdc_single_inst_wrst_rd\ : label is 4;
  attribute INIT_SYNC_FF of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.xpm_cdc_single_inst_wrst_rd\ : label is 0;
  attribute SIM_ASSERT_CHK of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.xpm_cdc_single_inst_wrst_rd\ : label is 0;
  attribute SRC_INPUT_REG of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.xpm_cdc_single_inst_wrst_rd\ : label is 0;
  attribute VERSION of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.xpm_cdc_single_inst_wrst_rd\ : label is 0;
  attribute XPM_CDC of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.xpm_cdc_single_inst_wrst_rd\ : label is "SINGLE";
  attribute XPM_MODULE of \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.xpm_cdc_single_inst_wrst_rd\ : label is "TRUE";
  attribute DEF_VAL of \ngwrdrst.grst.g7serrst.gnsckt_wrst.rst_wr_reg2_inst\ : label is "1'b0";
  attribute DEST_SYNC_FF of \ngwrdrst.grst.g7serrst.gnsckt_wrst.rst_wr_reg2_inst\ : label is 2;
  attribute INIT_SYNC_FF of \ngwrdrst.grst.g7serrst.gnsckt_wrst.rst_wr_reg2_inst\ : label is 0;
  attribute INV_DEF_VAL of \ngwrdrst.grst.g7serrst.gnsckt_wrst.rst_wr_reg2_inst\ : label is "1'b1";
  attribute RST_ACTIVE_HIGH of \ngwrdrst.grst.g7serrst.gnsckt_wrst.rst_wr_reg2_inst\ : label is 1;
  attribute VERSION of \ngwrdrst.grst.g7serrst.gnsckt_wrst.rst_wr_reg2_inst\ : label is 0;
  attribute XPM_CDC of \ngwrdrst.grst.g7serrst.gnsckt_wrst.rst_wr_reg2_inst\ : label is "ASYNC_RST";
  attribute XPM_MODULE of \ngwrdrst.grst.g7serrst.gnsckt_wrst.rst_wr_reg2_inst\ : label is "TRUE";
begin
  AR(0) <= \^ar\(0);
  \grstd1.grst_full.grst_f.rst_d3_reg_0\ <= rst_d3;
  \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.sckt_rd_rst_ic_reg_0\(0) <= \^ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.sckt_rd_rst_ic_reg_0\(0);
  \out\ <= rst_d2;
  wr_rst_busy <= \^wr_rst_busy\;
\grstd1.grst_full.grst_f.rst_d1_reg\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => wr_clk,
      CE => '1',
      D => \^wr_rst_busy\,
      PRE => rst_wr_reg2,
      Q => rst_d1
    );
\grstd1.grst_full.grst_f.rst_d2_reg\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => wr_clk,
      CE => '1',
      D => rst_d1,
      PRE => rst_wr_reg2,
      Q => rst_d2
    );
\grstd1.grst_full.grst_f.rst_d3_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => rst_d2,
      I1 => \^ar\(0),
      O => \grstd1.grst_full.grst_f.rst_d3_i_1_n_0\
    );
\grstd1.grst_full.grst_f.rst_d3_reg\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => wr_clk,
      CE => '1',
      D => \grstd1.grst_full.grst_f.rst_d3_i_1_n_0\,
      PRE => rst_wr_reg2,
      Q => rst_d3
    );
\grstd1.grst_full.grst_f.rst_d4_reg\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => wr_clk,
      CE => '1',
      D => rst_d3,
      PRE => rst_wr_reg2,
      Q => rst_d4
    );
i_0: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '1',
      O => wr_rst_reg(2)
    );
i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '1',
      O => wr_rst_reg(1)
    );
i_10: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '1',
      O => rst_d7
    );
i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '1',
      O => wr_rst_reg(0)
    );
i_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '1',
      O => rd_rst_reg(2)
    );
i_4: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '1',
      O => rd_rst_reg(1)
    );
i_5: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '1',
      O => rd_rst_reg(0)
    );
i_6: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => rst_wr_reg1
    );
i_7: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '0',
      O => rst_rd_reg1
    );
i_8: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '1',
      O => rst_d5
    );
i_9: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => '1',
      O => rst_d6
    );
\ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.rd_rst_wr_ext_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => '1',
      CLR => rst_wr_reg2,
      D => sckt_rd_rst_wr,
      Q => rd_rst_wr_ext(0)
    );
\ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.rd_rst_wr_ext_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => '1',
      CLR => rst_wr_reg2,
      D => rd_rst_wr_ext(0),
      Q => rd_rst_wr_ext(1)
    );
\ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.rd_rst_wr_ext_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => '1',
      CLR => rst_wr_reg2,
      D => rd_rst_wr_ext(1),
      Q => rd_rst_wr_ext(2)
    );
\ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.rd_rst_wr_ext_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => '1',
      CLR => rst_wr_reg2,
      D => rd_rst_wr_ext(2),
      Q => rd_rst_wr_ext(3)
    );
\ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.rst_rd_reg2_inst\: entity work.header_fifo_xpm_cdc_async_rst
     port map (
      dest_arst => rst_rd_reg2,
      dest_clk => rd_clk,
      src_arst => rst
    );
\ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.sckt_rd_rst_ic_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.sckt_rd_rst_ic_reg_0\(0),
      I1 => wr_rst_rd_ext(1),
      O => \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.sckt_rd_rst_ic_i_1_n_0\
    );
\ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.sckt_rd_rst_ic_reg\: unisim.vcomponents.FDPE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => '1',
      D => \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.sckt_rd_rst_ic_i_1_n_0\,
      PRE => rst_rd_reg2,
      Q => \^ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.sckt_rd_rst_ic_reg_0\(0)
    );
\ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.sckt_wr_rst_ic_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \^ar\(0),
      I1 => rd_rst_wr_ext(0),
      I2 => rd_rst_wr_ext(1),
      O => \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.sckt_wr_rst_ic_i_1_n_0\
    );
\ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.sckt_wr_rst_ic_reg\: unisim.vcomponents.FDPE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => '1',
      D => \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.sckt_wr_rst_ic_i_1_n_0\,
      PRE => rst_wr_reg2,
      Q => \^ar\(0)
    );
\ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.wr_rst_busy_i_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA08AA"
    )
        port map (
      I0 => \^wr_rst_busy\,
      I1 => rd_rst_wr_ext(1),
      I2 => rd_rst_wr_ext(0),
      I3 => rd_rst_wr_ext(3),
      I4 => rd_rst_wr_ext(2),
      O => \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.wr_rst_busy_i_i_1_n_0\
    );
\ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.wr_rst_busy_i_reg\: unisim.vcomponents.FDPE
    generic map(
      INIT => '0'
    )
        port map (
      C => wr_clk,
      CE => '1',
      D => \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.wr_rst_busy_i_i_1_n_0\,
      PRE => rst_wr_reg2,
      Q => \^wr_rst_busy\
    );
\ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.wr_rst_rd_ext_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => '1',
      CLR => rst_rd_reg2,
      D => dest_out,
      Q => wr_rst_rd_ext(0)
    );
\ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.wr_rst_rd_ext_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => rd_clk,
      CE => '1',
      CLR => rst_rd_reg2,
      D => wr_rst_rd_ext(0),
      Q => wr_rst_rd_ext(1)
    );
\ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.xpm_cdc_single_inst_rrst_wr\: entity work.header_fifo_xpm_cdc_single
     port map (
      dest_clk => wr_clk,
      dest_out => sckt_rd_rst_wr,
      src_clk => rd_clk,
      src_in => \^ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.sckt_rd_rst_ic_reg_0\(0)
    );
\ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.xpm_cdc_single_inst_wrst_rd\: entity work.\header_fifo_xpm_cdc_single__2\
     port map (
      dest_clk => rd_clk,
      dest_out => dest_out,
      src_clk => wr_clk,
      src_in => \^ar\(0)
    );
\ngwrdrst.grst.g7serrst.gnsckt_wrst.rst_wr_reg2_inst\: entity work.\header_fifo_xpm_cdc_async_rst__1\
     port map (
      dest_arst => rst_wr_reg2,
      dest_clk => wr_clk,
      src_arst => rst
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo_wr_logic is
  port (
    full : out STD_LOGIC;
    \out\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 5 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \gic0.gc0.count_reg[5]\ : out STD_LOGIC_VECTOR ( 5 downto 0 );
    \gic0.gc0.count_d1_reg[5]\ : out STD_LOGIC_VECTOR ( 5 downto 0 );
    wr_data_count : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ram_full_fb_i_reg : in STD_LOGIC;
    wr_clk : in STD_LOGIC;
    ram_full_fb_i_reg_0 : in STD_LOGIC;
    RD_PNTR_WR : in STD_LOGIC_VECTOR ( 5 downto 0 );
    wr_en : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end header_fifo_wr_logic;

architecture STRUCTURE of header_fifo_wr_logic is
  signal \^e\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^q\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal wpntr_n_0 : STD_LOGIC;
  signal wpntr_n_1 : STD_LOGIC;
  signal wpntr_n_2 : STD_LOGIC;
  signal wpntr_n_3 : STD_LOGIC;
  signal wpntr_n_4 : STD_LOGIC;
  signal wpntr_n_5 : STD_LOGIC;
begin
  E(0) <= \^e\(0);
  Q(5 downto 0) <= \^q\(5 downto 0);
\gwas.gwdc0.wdc\: entity work.header_fifo_wr_dc_as
     port map (
      AR(0) => AR(0),
      Q(4 downto 0) => \^q\(4 downto 0),
      S(5) => wpntr_n_0,
      S(4) => wpntr_n_1,
      S(3) => wpntr_n_2,
      S(2) => wpntr_n_3,
      S(1) => wpntr_n_4,
      S(0) => wpntr_n_5,
      wr_clk => wr_clk,
      wr_data_count(5 downto 0) => wr_data_count(5 downto 0)
    );
\gwas.wsts\: entity work.header_fifo_wr_status_flags_as
     port map (
      E(0) => \^e\(0),
      full => full,
      \out\ => \out\,
      ram_full_fb_i_reg_0 => ram_full_fb_i_reg,
      ram_full_fb_i_reg_1 => ram_full_fb_i_reg_0,
      wr_clk => wr_clk,
      wr_en => wr_en
    );
wpntr: entity work.header_fifo_wr_bin_cntr
     port map (
      AR(0) => AR(0),
      E(0) => \^e\(0),
      Q(5 downto 0) => \^q\(5 downto 0),
      RD_PNTR_WR(5 downto 0) => RD_PNTR_WR(5 downto 0),
      S(5) => wpntr_n_0,
      S(4) => wpntr_n_1,
      S(3) => wpntr_n_2,
      S(2) => wpntr_n_3,
      S(1) => wpntr_n_4,
      S(0) => wpntr_n_5,
      \gic0.gc0.count_d1_reg[5]_0\(5 downto 0) => \gic0.gc0.count_d1_reg[5]\(5 downto 0),
      \gic0.gc0.count_reg[5]_0\(5 downto 0) => \gic0.gc0.count_reg[5]\(5 downto 0),
      wr_clk => wr_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo_fifo_generator_ramfifo is
  port (
    wr_rst_busy : out STD_LOGIC;
    AR : out STD_LOGIC_VECTOR ( 0 to 0 );
    empty : out STD_LOGIC;
    valid : out STD_LOGIC;
    full : out STD_LOGIC;
    dout : out STD_LOGIC_VECTOR ( 255 downto 0 );
    underflow : out STD_LOGIC;
    rd_data_count : out STD_LOGIC_VECTOR ( 5 downto 0 );
    wr_data_count : out STD_LOGIC_VECTOR ( 5 downto 0 );
    rst : in STD_LOGIC;
    wr_clk : in STD_LOGIC;
    rd_clk : in STD_LOGIC;
    din : in STD_LOGIC_VECTOR ( 255 downto 0 );
    wr_en : in STD_LOGIC;
    rd_en : in STD_LOGIC
  );
end header_fifo_fifo_generator_ramfifo;

architecture STRUCTURE of header_fifo_fifo_generator_ramfifo is
  signal \^ar\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \gntv_or_sync_fifo.gcx.clkx_n_0\ : STD_LOGIC;
  signal \gntv_or_sync_fifo.gcx.clkx_n_1\ : STD_LOGIC;
  signal \gntv_or_sync_fifo.gcx.clkx_n_12\ : STD_LOGIC;
  signal \gntv_or_sync_fifo.gcx.clkx_n_19\ : STD_LOGIC;
  signal \gntv_or_sync_fifo.gcx.clkx_n_2\ : STD_LOGIC;
  signal \gntv_or_sync_fifo.gcx.clkx_n_3\ : STD_LOGIC;
  signal \gntv_or_sync_fifo.gcx.clkx_n_4\ : STD_LOGIC;
  signal \gntv_or_sync_fifo.gcx.clkx_n_5\ : STD_LOGIC;
  signal \gntv_or_sync_fifo.gl0.wr_n_1\ : STD_LOGIC;
  signal \gras.rsts/comp1\ : STD_LOGIC;
  signal ram_rd_en : STD_LOGIC;
  signal ram_rd_en_i : STD_LOGIC;
  signal ram_regout_en : STD_LOGIC;
  signal ram_wr_en : STD_LOGIC;
  signal rd_pntr : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal rd_pntr_wr : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal rst_full_ff_i : STD_LOGIC;
  signal rst_full_gen_i : STD_LOGIC;
  signal rstblk_n_0 : STD_LOGIC;
  signal wr_pntr : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal wr_pntr_plus1 : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal wr_pntr_plus2 : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal wr_pntr_rd : STD_LOGIC_VECTOR ( 5 downto 0 );
begin
  AR(0) <= \^ar\(0);
\gntv_or_sync_fifo.gcx.clkx\: entity work.header_fifo_clk_x_pntrs
     port map (
      E(0) => ram_rd_en,
      Q(5 downto 0) => rd_pntr(5 downto 0),
      RD_PNTR_WR(5 downto 0) => rd_pntr_wr(5 downto 0),
      S(5) => \gntv_or_sync_fifo.gcx.clkx_n_0\,
      S(4) => \gntv_or_sync_fifo.gcx.clkx_n_1\,
      S(3) => \gntv_or_sync_fifo.gcx.clkx_n_2\,
      S(2) => \gntv_or_sync_fifo.gcx.clkx_n_3\,
      S(1) => \gntv_or_sync_fifo.gcx.clkx_n_4\,
      S(0) => \gntv_or_sync_fifo.gcx.clkx_n_5\,
      WR_PNTR_RD(5 downto 0) => wr_pntr_rd(5 downto 0),
      comp1 => \gras.rsts/comp1\,
      \dest_out_bin_ff_reg[0]\ => \gntv_or_sync_fifo.gcx.clkx_n_19\,
      \out\ => \gntv_or_sync_fifo.gl0.wr_n_1\,
      ram_full_fb_i_reg => \gntv_or_sync_fifo.gcx.clkx_n_12\,
      ram_full_fb_i_reg_0 => rst_full_gen_i,
      ram_full_i_i_2_0(5 downto 0) => wr_pntr_plus2(5 downto 0),
      ram_full_i_i_3_0(5 downto 0) => wr_pntr_plus1(5 downto 0),
      rd_clk => rd_clk,
      \src_gray_ff_reg[5]\(5 downto 0) => wr_pntr(5 downto 0),
      wr_clk => wr_clk,
      wr_en => wr_en
    );
\gntv_or_sync_fifo.gl0.rd\: entity work.header_fifo_rd_logic
     port map (
      AR(0) => \^ar\(0),
      E(0) => ram_rd_en_i,
      Q(5 downto 0) => rd_pntr(5 downto 0),
      S(5) => \gntv_or_sync_fifo.gcx.clkx_n_0\,
      S(4) => \gntv_or_sync_fifo.gcx.clkx_n_1\,
      S(3) => \gntv_or_sync_fifo.gcx.clkx_n_2\,
      S(2) => \gntv_or_sync_fifo.gcx.clkx_n_3\,
      S(1) => \gntv_or_sync_fifo.gcx.clkx_n_4\,
      S(0) => \gntv_or_sync_fifo.gcx.clkx_n_5\,
      WR_PNTR_RD(5 downto 0) => wr_pntr_rd(5 downto 0),
      comp1 => \gras.rsts/comp1\,
      empty => empty,
      \gpregsm1.curr_fwft_state_reg[1]\(0) => ram_regout_en,
      ram_empty_fb_i_reg(0) => ram_rd_en,
      ram_empty_fb_i_reg_0 => \gntv_or_sync_fifo.gcx.clkx_n_19\,
      rd_clk => rd_clk,
      rd_data_count(5 downto 0) => rd_data_count(5 downto 0),
      rd_en => rd_en,
      underflow => underflow,
      valid => valid
    );
\gntv_or_sync_fifo.gl0.wr\: entity work.header_fifo_wr_logic
     port map (
      AR(0) => rstblk_n_0,
      E(0) => ram_wr_en,
      Q(5 downto 0) => wr_pntr(5 downto 0),
      RD_PNTR_WR(5 downto 0) => rd_pntr_wr(5 downto 0),
      full => full,
      \gic0.gc0.count_d1_reg[5]\(5 downto 0) => wr_pntr_plus1(5 downto 0),
      \gic0.gc0.count_reg[5]\(5 downto 0) => wr_pntr_plus2(5 downto 0),
      \out\ => \gntv_or_sync_fifo.gl0.wr_n_1\,
      ram_full_fb_i_reg => \gntv_or_sync_fifo.gcx.clkx_n_12\,
      ram_full_fb_i_reg_0 => rst_full_ff_i,
      wr_clk => wr_clk,
      wr_data_count(5 downto 0) => wr_data_count(5 downto 0),
      wr_en => wr_en
    );
\gntv_or_sync_fifo.mem\: entity work.header_fifo_memory
     port map (
      AR(0) => \^ar\(0),
      E(0) => ram_wr_en,
      Q(5 downto 0) => rd_pntr(5 downto 0),
      din(255 downto 0) => din(255 downto 0),
      dout(255 downto 0) => dout(255 downto 0),
      \goreg_dm.dout_i_reg[255]_0\(0) => ram_regout_en,
      \gpr1.dout_i_reg[0]\(5 downto 0) => wr_pntr(5 downto 0),
      \gpr1.dout_i_reg[0]_0\(0) => ram_rd_en_i,
      rd_clk => rd_clk,
      wr_clk => wr_clk
    );
rstblk: entity work.header_fifo_reset_blk_ramfifo
     port map (
      AR(0) => rstblk_n_0,
      \grstd1.grst_full.grst_f.rst_d3_reg_0\ => rst_full_gen_i,
      \ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.sckt_rd_rst_ic_reg_0\(0) => \^ar\(0),
      \out\ => rst_full_ff_i,
      rd_clk => rd_clk,
      rst => rst,
      wr_clk => wr_clk,
      wr_rst_busy => wr_rst_busy
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo_fifo_generator_top is
  port (
    wr_rst_busy : out STD_LOGIC;
    AR : out STD_LOGIC_VECTOR ( 0 to 0 );
    empty : out STD_LOGIC;
    valid : out STD_LOGIC;
    full : out STD_LOGIC;
    dout : out STD_LOGIC_VECTOR ( 255 downto 0 );
    underflow : out STD_LOGIC;
    rd_data_count : out STD_LOGIC_VECTOR ( 5 downto 0 );
    wr_data_count : out STD_LOGIC_VECTOR ( 5 downto 0 );
    rst : in STD_LOGIC;
    wr_clk : in STD_LOGIC;
    rd_clk : in STD_LOGIC;
    din : in STD_LOGIC_VECTOR ( 255 downto 0 );
    wr_en : in STD_LOGIC;
    rd_en : in STD_LOGIC
  );
end header_fifo_fifo_generator_top;

architecture STRUCTURE of header_fifo_fifo_generator_top is
begin
\grf.rf\: entity work.header_fifo_fifo_generator_ramfifo
     port map (
      AR(0) => AR(0),
      din(255 downto 0) => din(255 downto 0),
      dout(255 downto 0) => dout(255 downto 0),
      empty => empty,
      full => full,
      rd_clk => rd_clk,
      rd_data_count(5 downto 0) => rd_data_count(5 downto 0),
      rd_en => rd_en,
      rst => rst,
      underflow => underflow,
      valid => valid,
      wr_clk => wr_clk,
      wr_data_count(5 downto 0) => wr_data_count(5 downto 0),
      wr_en => wr_en,
      wr_rst_busy => wr_rst_busy
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo_fifo_generator_v13_2_5_synth is
  port (
    wr_rst_busy : out STD_LOGIC;
    AR : out STD_LOGIC_VECTOR ( 0 to 0 );
    empty : out STD_LOGIC;
    valid : out STD_LOGIC;
    full : out STD_LOGIC;
    dout : out STD_LOGIC_VECTOR ( 255 downto 0 );
    underflow : out STD_LOGIC;
    rd_data_count : out STD_LOGIC_VECTOR ( 5 downto 0 );
    wr_data_count : out STD_LOGIC_VECTOR ( 5 downto 0 );
    rst : in STD_LOGIC;
    wr_clk : in STD_LOGIC;
    rd_clk : in STD_LOGIC;
    din : in STD_LOGIC_VECTOR ( 255 downto 0 );
    wr_en : in STD_LOGIC;
    rd_en : in STD_LOGIC
  );
end header_fifo_fifo_generator_v13_2_5_synth;

architecture STRUCTURE of header_fifo_fifo_generator_v13_2_5_synth is
begin
\gconvfifo.rf\: entity work.header_fifo_fifo_generator_top
     port map (
      AR(0) => AR(0),
      din(255 downto 0) => din(255 downto 0),
      dout(255 downto 0) => dout(255 downto 0),
      empty => empty,
      full => full,
      rd_clk => rd_clk,
      rd_data_count(5 downto 0) => rd_data_count(5 downto 0),
      rd_en => rd_en,
      rst => rst,
      underflow => underflow,
      valid => valid,
      wr_clk => wr_clk,
      wr_data_count(5 downto 0) => wr_data_count(5 downto 0),
      wr_en => wr_en,
      wr_rst_busy => wr_rst_busy
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo_fifo_generator_v13_2_5 is
  port (
    backup : in STD_LOGIC;
    backup_marker : in STD_LOGIC;
    clk : in STD_LOGIC;
    rst : in STD_LOGIC;
    srst : in STD_LOGIC;
    wr_clk : in STD_LOGIC;
    wr_rst : in STD_LOGIC;
    rd_clk : in STD_LOGIC;
    rd_rst : in STD_LOGIC;
    din : in STD_LOGIC_VECTOR ( 255 downto 0 );
    wr_en : in STD_LOGIC;
    rd_en : in STD_LOGIC;
    prog_empty_thresh : in STD_LOGIC_VECTOR ( 5 downto 0 );
    prog_empty_thresh_assert : in STD_LOGIC_VECTOR ( 5 downto 0 );
    prog_empty_thresh_negate : in STD_LOGIC_VECTOR ( 5 downto 0 );
    prog_full_thresh : in STD_LOGIC_VECTOR ( 5 downto 0 );
    prog_full_thresh_assert : in STD_LOGIC_VECTOR ( 5 downto 0 );
    prog_full_thresh_negate : in STD_LOGIC_VECTOR ( 5 downto 0 );
    int_clk : in STD_LOGIC;
    injectdbiterr : in STD_LOGIC;
    injectsbiterr : in STD_LOGIC;
    sleep : in STD_LOGIC;
    dout : out STD_LOGIC_VECTOR ( 255 downto 0 );
    full : out STD_LOGIC;
    almost_full : out STD_LOGIC;
    wr_ack : out STD_LOGIC;
    overflow : out STD_LOGIC;
    empty : out STD_LOGIC;
    almost_empty : out STD_LOGIC;
    valid : out STD_LOGIC;
    underflow : out STD_LOGIC;
    data_count : out STD_LOGIC_VECTOR ( 5 downto 0 );
    rd_data_count : out STD_LOGIC_VECTOR ( 5 downto 0 );
    wr_data_count : out STD_LOGIC_VECTOR ( 5 downto 0 );
    prog_full : out STD_LOGIC;
    prog_empty : out STD_LOGIC;
    sbiterr : out STD_LOGIC;
    dbiterr : out STD_LOGIC;
    wr_rst_busy : out STD_LOGIC;
    rd_rst_busy : out STD_LOGIC;
    m_aclk : in STD_LOGIC;
    s_aclk : in STD_LOGIC;
    s_aresetn : in STD_LOGIC;
    m_aclk_en : in STD_LOGIC;
    s_aclk_en : in STD_LOGIC;
    s_axi_awid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_awlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_wdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_wlast : in STD_LOGIC;
    s_axi_wuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bid : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_buser : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    m_axi_awid : out STD_LOGIC_VECTOR ( 0 to 0 );
    m_axi_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_awlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axi_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_awlock : out STD_LOGIC_VECTOR ( 0 to 0 );
    m_axi_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_awqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_awregion : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_awuser : out STD_LOGIC_VECTOR ( 0 to 0 );
    m_axi_awvalid : out STD_LOGIC;
    m_axi_awready : in STD_LOGIC;
    m_axi_wid : out STD_LOGIC_VECTOR ( 0 to 0 );
    m_axi_wdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    m_axi_wstrb : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axi_wlast : out STD_LOGIC;
    m_axi_wuser : out STD_LOGIC_VECTOR ( 0 to 0 );
    m_axi_wvalid : out STD_LOGIC;
    m_axi_wready : in STD_LOGIC;
    m_axi_bid : in STD_LOGIC_VECTOR ( 0 to 0 );
    m_axi_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_buser : in STD_LOGIC_VECTOR ( 0 to 0 );
    m_axi_bvalid : in STD_LOGIC;
    m_axi_bready : out STD_LOGIC;
    s_axi_arid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_arlock : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_arregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_aruser : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rid : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rlast : out STD_LOGIC;
    s_axi_ruser : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    m_axi_arid : out STD_LOGIC_VECTOR ( 0 to 0 );
    m_axi_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_arlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axi_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_arlock : out STD_LOGIC_VECTOR ( 0 to 0 );
    m_axi_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_arregion : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_aruser : out STD_LOGIC_VECTOR ( 0 to 0 );
    m_axi_arvalid : out STD_LOGIC;
    m_axi_arready : in STD_LOGIC;
    m_axi_rid : in STD_LOGIC_VECTOR ( 0 to 0 );
    m_axi_rdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    m_axi_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_rlast : in STD_LOGIC;
    m_axi_ruser : in STD_LOGIC_VECTOR ( 0 to 0 );
    m_axi_rvalid : in STD_LOGIC;
    m_axi_rready : out STD_LOGIC;
    s_axis_tvalid : in STD_LOGIC;
    s_axis_tready : out STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axis_tstrb : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_tkeep : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_tlast : in STD_LOGIC;
    s_axis_tid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_tdest : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_tuser : in STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axis_tvalid : out STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    m_axis_tdata : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axis_tstrb : out STD_LOGIC_VECTOR ( 0 to 0 );
    m_axis_tkeep : out STD_LOGIC_VECTOR ( 0 to 0 );
    m_axis_tlast : out STD_LOGIC;
    m_axis_tid : out STD_LOGIC_VECTOR ( 0 to 0 );
    m_axis_tdest : out STD_LOGIC_VECTOR ( 0 to 0 );
    m_axis_tuser : out STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_aw_injectsbiterr : in STD_LOGIC;
    axi_aw_injectdbiterr : in STD_LOGIC;
    axi_aw_prog_full_thresh : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_aw_prog_empty_thresh : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_aw_data_count : out STD_LOGIC_VECTOR ( 4 downto 0 );
    axi_aw_wr_data_count : out STD_LOGIC_VECTOR ( 4 downto 0 );
    axi_aw_rd_data_count : out STD_LOGIC_VECTOR ( 4 downto 0 );
    axi_aw_sbiterr : out STD_LOGIC;
    axi_aw_dbiterr : out STD_LOGIC;
    axi_aw_overflow : out STD_LOGIC;
    axi_aw_underflow : out STD_LOGIC;
    axi_aw_prog_full : out STD_LOGIC;
    axi_aw_prog_empty : out STD_LOGIC;
    axi_w_injectsbiterr : in STD_LOGIC;
    axi_w_injectdbiterr : in STD_LOGIC;
    axi_w_prog_full_thresh : in STD_LOGIC_VECTOR ( 9 downto 0 );
    axi_w_prog_empty_thresh : in STD_LOGIC_VECTOR ( 9 downto 0 );
    axi_w_data_count : out STD_LOGIC_VECTOR ( 10 downto 0 );
    axi_w_wr_data_count : out STD_LOGIC_VECTOR ( 10 downto 0 );
    axi_w_rd_data_count : out STD_LOGIC_VECTOR ( 10 downto 0 );
    axi_w_sbiterr : out STD_LOGIC;
    axi_w_dbiterr : out STD_LOGIC;
    axi_w_overflow : out STD_LOGIC;
    axi_w_underflow : out STD_LOGIC;
    axi_w_prog_full : out STD_LOGIC;
    axi_w_prog_empty : out STD_LOGIC;
    axi_b_injectsbiterr : in STD_LOGIC;
    axi_b_injectdbiterr : in STD_LOGIC;
    axi_b_prog_full_thresh : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_b_prog_empty_thresh : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_b_data_count : out STD_LOGIC_VECTOR ( 4 downto 0 );
    axi_b_wr_data_count : out STD_LOGIC_VECTOR ( 4 downto 0 );
    axi_b_rd_data_count : out STD_LOGIC_VECTOR ( 4 downto 0 );
    axi_b_sbiterr : out STD_LOGIC;
    axi_b_dbiterr : out STD_LOGIC;
    axi_b_overflow : out STD_LOGIC;
    axi_b_underflow : out STD_LOGIC;
    axi_b_prog_full : out STD_LOGIC;
    axi_b_prog_empty : out STD_LOGIC;
    axi_ar_injectsbiterr : in STD_LOGIC;
    axi_ar_injectdbiterr : in STD_LOGIC;
    axi_ar_prog_full_thresh : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_ar_prog_empty_thresh : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_ar_data_count : out STD_LOGIC_VECTOR ( 4 downto 0 );
    axi_ar_wr_data_count : out STD_LOGIC_VECTOR ( 4 downto 0 );
    axi_ar_rd_data_count : out STD_LOGIC_VECTOR ( 4 downto 0 );
    axi_ar_sbiterr : out STD_LOGIC;
    axi_ar_dbiterr : out STD_LOGIC;
    axi_ar_overflow : out STD_LOGIC;
    axi_ar_underflow : out STD_LOGIC;
    axi_ar_prog_full : out STD_LOGIC;
    axi_ar_prog_empty : out STD_LOGIC;
    axi_r_injectsbiterr : in STD_LOGIC;
    axi_r_injectdbiterr : in STD_LOGIC;
    axi_r_prog_full_thresh : in STD_LOGIC_VECTOR ( 9 downto 0 );
    axi_r_prog_empty_thresh : in STD_LOGIC_VECTOR ( 9 downto 0 );
    axi_r_data_count : out STD_LOGIC_VECTOR ( 10 downto 0 );
    axi_r_wr_data_count : out STD_LOGIC_VECTOR ( 10 downto 0 );
    axi_r_rd_data_count : out STD_LOGIC_VECTOR ( 10 downto 0 );
    axi_r_sbiterr : out STD_LOGIC;
    axi_r_dbiterr : out STD_LOGIC;
    axi_r_overflow : out STD_LOGIC;
    axi_r_underflow : out STD_LOGIC;
    axi_r_prog_full : out STD_LOGIC;
    axi_r_prog_empty : out STD_LOGIC;
    axis_injectsbiterr : in STD_LOGIC;
    axis_injectdbiterr : in STD_LOGIC;
    axis_prog_full_thresh : in STD_LOGIC_VECTOR ( 9 downto 0 );
    axis_prog_empty_thresh : in STD_LOGIC_VECTOR ( 9 downto 0 );
    axis_data_count : out STD_LOGIC_VECTOR ( 10 downto 0 );
    axis_wr_data_count : out STD_LOGIC_VECTOR ( 10 downto 0 );
    axis_rd_data_count : out STD_LOGIC_VECTOR ( 10 downto 0 );
    axis_sbiterr : out STD_LOGIC;
    axis_dbiterr : out STD_LOGIC;
    axis_overflow : out STD_LOGIC;
    axis_underflow : out STD_LOGIC;
    axis_prog_full : out STD_LOGIC;
    axis_prog_empty : out STD_LOGIC
  );
  attribute C_ADD_NGC_CONSTRAINT : integer;
  attribute C_ADD_NGC_CONSTRAINT of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_APPLICATION_TYPE_AXIS : integer;
  attribute C_APPLICATION_TYPE_AXIS of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_APPLICATION_TYPE_RACH : integer;
  attribute C_APPLICATION_TYPE_RACH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_APPLICATION_TYPE_RDCH : integer;
  attribute C_APPLICATION_TYPE_RDCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_APPLICATION_TYPE_WACH : integer;
  attribute C_APPLICATION_TYPE_WACH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_APPLICATION_TYPE_WDCH : integer;
  attribute C_APPLICATION_TYPE_WDCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_APPLICATION_TYPE_WRCH : integer;
  attribute C_APPLICATION_TYPE_WRCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_AXIS_TDATA_WIDTH : integer;
  attribute C_AXIS_TDATA_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 8;
  attribute C_AXIS_TDEST_WIDTH : integer;
  attribute C_AXIS_TDEST_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_AXIS_TID_WIDTH : integer;
  attribute C_AXIS_TID_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_AXIS_TKEEP_WIDTH : integer;
  attribute C_AXIS_TKEEP_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_AXIS_TSTRB_WIDTH : integer;
  attribute C_AXIS_TSTRB_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_AXIS_TUSER_WIDTH : integer;
  attribute C_AXIS_TUSER_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 4;
  attribute C_AXIS_TYPE : integer;
  attribute C_AXIS_TYPE of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_AXI_ADDR_WIDTH : integer;
  attribute C_AXI_ADDR_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 32;
  attribute C_AXI_ARUSER_WIDTH : integer;
  attribute C_AXI_ARUSER_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_AXI_AWUSER_WIDTH : integer;
  attribute C_AXI_AWUSER_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_AXI_BUSER_WIDTH : integer;
  attribute C_AXI_BUSER_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_AXI_DATA_WIDTH : integer;
  attribute C_AXI_DATA_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 64;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_AXI_LEN_WIDTH : integer;
  attribute C_AXI_LEN_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 8;
  attribute C_AXI_LOCK_WIDTH : integer;
  attribute C_AXI_LOCK_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_AXI_RUSER_WIDTH : integer;
  attribute C_AXI_RUSER_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_AXI_WUSER_WIDTH : integer;
  attribute C_AXI_WUSER_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_COMMON_CLOCK : integer;
  attribute C_COMMON_CLOCK of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_COUNT_TYPE : integer;
  attribute C_COUNT_TYPE of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_DATA_COUNT_WIDTH : integer;
  attribute C_DATA_COUNT_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 6;
  attribute C_DEFAULT_VALUE : string;
  attribute C_DEFAULT_VALUE of header_fifo_fifo_generator_v13_2_5 : entity is "BlankString";
  attribute C_DIN_WIDTH : integer;
  attribute C_DIN_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 256;
  attribute C_DIN_WIDTH_AXIS : integer;
  attribute C_DIN_WIDTH_AXIS of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_DIN_WIDTH_RACH : integer;
  attribute C_DIN_WIDTH_RACH of header_fifo_fifo_generator_v13_2_5 : entity is 32;
  attribute C_DIN_WIDTH_RDCH : integer;
  attribute C_DIN_WIDTH_RDCH of header_fifo_fifo_generator_v13_2_5 : entity is 64;
  attribute C_DIN_WIDTH_WACH : integer;
  attribute C_DIN_WIDTH_WACH of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_DIN_WIDTH_WDCH : integer;
  attribute C_DIN_WIDTH_WDCH of header_fifo_fifo_generator_v13_2_5 : entity is 64;
  attribute C_DIN_WIDTH_WRCH : integer;
  attribute C_DIN_WIDTH_WRCH of header_fifo_fifo_generator_v13_2_5 : entity is 2;
  attribute C_DOUT_RST_VAL : string;
  attribute C_DOUT_RST_VAL of header_fifo_fifo_generator_v13_2_5 : entity is "0";
  attribute C_DOUT_WIDTH : integer;
  attribute C_DOUT_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 256;
  attribute C_ENABLE_RLOCS : integer;
  attribute C_ENABLE_RLOCS of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_ENABLE_RST_SYNC : integer;
  attribute C_ENABLE_RST_SYNC of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_ERROR_INJECTION_TYPE : integer;
  attribute C_ERROR_INJECTION_TYPE of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_ERROR_INJECTION_TYPE_AXIS : integer;
  attribute C_ERROR_INJECTION_TYPE_AXIS of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_ERROR_INJECTION_TYPE_RACH : integer;
  attribute C_ERROR_INJECTION_TYPE_RACH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_ERROR_INJECTION_TYPE_RDCH : integer;
  attribute C_ERROR_INJECTION_TYPE_RDCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_ERROR_INJECTION_TYPE_WACH : integer;
  attribute C_ERROR_INJECTION_TYPE_WACH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_ERROR_INJECTION_TYPE_WDCH : integer;
  attribute C_ERROR_INJECTION_TYPE_WDCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_ERROR_INJECTION_TYPE_WRCH : integer;
  attribute C_ERROR_INJECTION_TYPE_WRCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_FAMILY : string;
  attribute C_FAMILY of header_fifo_fifo_generator_v13_2_5 : entity is "virtexuplusHBM";
  attribute C_FULL_FLAGS_RST_VAL : integer;
  attribute C_FULL_FLAGS_RST_VAL of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_HAS_ALMOST_EMPTY : integer;
  attribute C_HAS_ALMOST_EMPTY of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_ALMOST_FULL : integer;
  attribute C_HAS_ALMOST_FULL of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_AXIS_TDATA : integer;
  attribute C_HAS_AXIS_TDATA of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_HAS_AXIS_TDEST : integer;
  attribute C_HAS_AXIS_TDEST of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_AXIS_TID : integer;
  attribute C_HAS_AXIS_TID of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_AXIS_TKEEP : integer;
  attribute C_HAS_AXIS_TKEEP of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_AXIS_TLAST : integer;
  attribute C_HAS_AXIS_TLAST of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_AXIS_TREADY : integer;
  attribute C_HAS_AXIS_TREADY of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_HAS_AXIS_TSTRB : integer;
  attribute C_HAS_AXIS_TSTRB of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_AXIS_TUSER : integer;
  attribute C_HAS_AXIS_TUSER of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_HAS_AXI_ARUSER : integer;
  attribute C_HAS_AXI_ARUSER of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_AXI_AWUSER : integer;
  attribute C_HAS_AXI_AWUSER of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_AXI_BUSER : integer;
  attribute C_HAS_AXI_BUSER of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_AXI_RD_CHANNEL : integer;
  attribute C_HAS_AXI_RD_CHANNEL of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_HAS_AXI_RUSER : integer;
  attribute C_HAS_AXI_RUSER of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_AXI_WR_CHANNEL : integer;
  attribute C_HAS_AXI_WR_CHANNEL of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_HAS_AXI_WUSER : integer;
  attribute C_HAS_AXI_WUSER of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_BACKUP : integer;
  attribute C_HAS_BACKUP of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_DATA_COUNT : integer;
  attribute C_HAS_DATA_COUNT of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_DATA_COUNTS_AXIS : integer;
  attribute C_HAS_DATA_COUNTS_AXIS of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_DATA_COUNTS_RACH : integer;
  attribute C_HAS_DATA_COUNTS_RACH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_DATA_COUNTS_RDCH : integer;
  attribute C_HAS_DATA_COUNTS_RDCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_DATA_COUNTS_WACH : integer;
  attribute C_HAS_DATA_COUNTS_WACH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_DATA_COUNTS_WDCH : integer;
  attribute C_HAS_DATA_COUNTS_WDCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_DATA_COUNTS_WRCH : integer;
  attribute C_HAS_DATA_COUNTS_WRCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_INT_CLK : integer;
  attribute C_HAS_INT_CLK of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_MASTER_CE : integer;
  attribute C_HAS_MASTER_CE of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_MEMINIT_FILE : integer;
  attribute C_HAS_MEMINIT_FILE of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_OVERFLOW : integer;
  attribute C_HAS_OVERFLOW of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_PROG_FLAGS_AXIS : integer;
  attribute C_HAS_PROG_FLAGS_AXIS of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_PROG_FLAGS_RACH : integer;
  attribute C_HAS_PROG_FLAGS_RACH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_PROG_FLAGS_RDCH : integer;
  attribute C_HAS_PROG_FLAGS_RDCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_PROG_FLAGS_WACH : integer;
  attribute C_HAS_PROG_FLAGS_WACH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_PROG_FLAGS_WDCH : integer;
  attribute C_HAS_PROG_FLAGS_WDCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_PROG_FLAGS_WRCH : integer;
  attribute C_HAS_PROG_FLAGS_WRCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_RD_DATA_COUNT : integer;
  attribute C_HAS_RD_DATA_COUNT of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_HAS_RD_RST : integer;
  attribute C_HAS_RD_RST of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_RST : integer;
  attribute C_HAS_RST of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_HAS_SLAVE_CE : integer;
  attribute C_HAS_SLAVE_CE of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_SRST : integer;
  attribute C_HAS_SRST of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_UNDERFLOW : integer;
  attribute C_HAS_UNDERFLOW of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_HAS_VALID : integer;
  attribute C_HAS_VALID of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_HAS_WR_ACK : integer;
  attribute C_HAS_WR_ACK of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_HAS_WR_DATA_COUNT : integer;
  attribute C_HAS_WR_DATA_COUNT of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_HAS_WR_RST : integer;
  attribute C_HAS_WR_RST of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_IMPLEMENTATION_TYPE : integer;
  attribute C_IMPLEMENTATION_TYPE of header_fifo_fifo_generator_v13_2_5 : entity is 2;
  attribute C_IMPLEMENTATION_TYPE_AXIS : integer;
  attribute C_IMPLEMENTATION_TYPE_AXIS of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_IMPLEMENTATION_TYPE_RACH : integer;
  attribute C_IMPLEMENTATION_TYPE_RACH of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_IMPLEMENTATION_TYPE_RDCH : integer;
  attribute C_IMPLEMENTATION_TYPE_RDCH of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_IMPLEMENTATION_TYPE_WACH : integer;
  attribute C_IMPLEMENTATION_TYPE_WACH of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_IMPLEMENTATION_TYPE_WDCH : integer;
  attribute C_IMPLEMENTATION_TYPE_WDCH of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_IMPLEMENTATION_TYPE_WRCH : integer;
  attribute C_IMPLEMENTATION_TYPE_WRCH of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_INIT_WR_PNTR_VAL : integer;
  attribute C_INIT_WR_PNTR_VAL of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_MEMORY_TYPE : integer;
  attribute C_MEMORY_TYPE of header_fifo_fifo_generator_v13_2_5 : entity is 2;
  attribute C_MIF_FILE_NAME : string;
  attribute C_MIF_FILE_NAME of header_fifo_fifo_generator_v13_2_5 : entity is "BlankString";
  attribute C_MSGON_VAL : integer;
  attribute C_MSGON_VAL of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_OPTIMIZATION_MODE : integer;
  attribute C_OPTIMIZATION_MODE of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_OVERFLOW_LOW : integer;
  attribute C_OVERFLOW_LOW of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_POWER_SAVING_MODE : integer;
  attribute C_POWER_SAVING_MODE of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_PRELOAD_LATENCY : integer;
  attribute C_PRELOAD_LATENCY of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_PRELOAD_REGS : integer;
  attribute C_PRELOAD_REGS of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_PRIM_FIFO_TYPE : string;
  attribute C_PRIM_FIFO_TYPE of header_fifo_fifo_generator_v13_2_5 : entity is "512x72";
  attribute C_PRIM_FIFO_TYPE_AXIS : string;
  attribute C_PRIM_FIFO_TYPE_AXIS of header_fifo_fifo_generator_v13_2_5 : entity is "1kx18";
  attribute C_PRIM_FIFO_TYPE_RACH : string;
  attribute C_PRIM_FIFO_TYPE_RACH of header_fifo_fifo_generator_v13_2_5 : entity is "512x36";
  attribute C_PRIM_FIFO_TYPE_RDCH : string;
  attribute C_PRIM_FIFO_TYPE_RDCH of header_fifo_fifo_generator_v13_2_5 : entity is "512x72";
  attribute C_PRIM_FIFO_TYPE_WACH : string;
  attribute C_PRIM_FIFO_TYPE_WACH of header_fifo_fifo_generator_v13_2_5 : entity is "512x36";
  attribute C_PRIM_FIFO_TYPE_WDCH : string;
  attribute C_PRIM_FIFO_TYPE_WDCH of header_fifo_fifo_generator_v13_2_5 : entity is "512x72";
  attribute C_PRIM_FIFO_TYPE_WRCH : string;
  attribute C_PRIM_FIFO_TYPE_WRCH of header_fifo_fifo_generator_v13_2_5 : entity is "512x36";
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL : integer;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL of header_fifo_fifo_generator_v13_2_5 : entity is 4;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_AXIS : integer;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_AXIS of header_fifo_fifo_generator_v13_2_5 : entity is 1022;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_RACH : integer;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_RACH of header_fifo_fifo_generator_v13_2_5 : entity is 1022;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_RDCH : integer;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_RDCH of header_fifo_fifo_generator_v13_2_5 : entity is 1022;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_WACH : integer;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_WACH of header_fifo_fifo_generator_v13_2_5 : entity is 1022;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_WDCH : integer;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_WDCH of header_fifo_fifo_generator_v13_2_5 : entity is 1022;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_WRCH : integer;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_WRCH of header_fifo_fifo_generator_v13_2_5 : entity is 1022;
  attribute C_PROG_EMPTY_THRESH_NEGATE_VAL : integer;
  attribute C_PROG_EMPTY_THRESH_NEGATE_VAL of header_fifo_fifo_generator_v13_2_5 : entity is 5;
  attribute C_PROG_EMPTY_TYPE : integer;
  attribute C_PROG_EMPTY_TYPE of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_PROG_EMPTY_TYPE_AXIS : integer;
  attribute C_PROG_EMPTY_TYPE_AXIS of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_PROG_EMPTY_TYPE_RACH : integer;
  attribute C_PROG_EMPTY_TYPE_RACH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_PROG_EMPTY_TYPE_RDCH : integer;
  attribute C_PROG_EMPTY_TYPE_RDCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_PROG_EMPTY_TYPE_WACH : integer;
  attribute C_PROG_EMPTY_TYPE_WACH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_PROG_EMPTY_TYPE_WDCH : integer;
  attribute C_PROG_EMPTY_TYPE_WDCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_PROG_EMPTY_TYPE_WRCH : integer;
  attribute C_PROG_EMPTY_TYPE_WRCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL : integer;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL of header_fifo_fifo_generator_v13_2_5 : entity is 63;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_AXIS : integer;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_AXIS of header_fifo_fifo_generator_v13_2_5 : entity is 1023;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_RACH : integer;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_RACH of header_fifo_fifo_generator_v13_2_5 : entity is 1023;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_RDCH : integer;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_RDCH of header_fifo_fifo_generator_v13_2_5 : entity is 1023;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_WACH : integer;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_WACH of header_fifo_fifo_generator_v13_2_5 : entity is 1023;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_WDCH : integer;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_WDCH of header_fifo_fifo_generator_v13_2_5 : entity is 1023;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_WRCH : integer;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_WRCH of header_fifo_fifo_generator_v13_2_5 : entity is 1023;
  attribute C_PROG_FULL_THRESH_NEGATE_VAL : integer;
  attribute C_PROG_FULL_THRESH_NEGATE_VAL of header_fifo_fifo_generator_v13_2_5 : entity is 62;
  attribute C_PROG_FULL_TYPE : integer;
  attribute C_PROG_FULL_TYPE of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_PROG_FULL_TYPE_AXIS : integer;
  attribute C_PROG_FULL_TYPE_AXIS of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_PROG_FULL_TYPE_RACH : integer;
  attribute C_PROG_FULL_TYPE_RACH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_PROG_FULL_TYPE_RDCH : integer;
  attribute C_PROG_FULL_TYPE_RDCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_PROG_FULL_TYPE_WACH : integer;
  attribute C_PROG_FULL_TYPE_WACH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_PROG_FULL_TYPE_WDCH : integer;
  attribute C_PROG_FULL_TYPE_WDCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_PROG_FULL_TYPE_WRCH : integer;
  attribute C_PROG_FULL_TYPE_WRCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_RACH_TYPE : integer;
  attribute C_RACH_TYPE of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_RDCH_TYPE : integer;
  attribute C_RDCH_TYPE of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_RD_DATA_COUNT_WIDTH : integer;
  attribute C_RD_DATA_COUNT_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 6;
  attribute C_RD_DEPTH : integer;
  attribute C_RD_DEPTH of header_fifo_fifo_generator_v13_2_5 : entity is 64;
  attribute C_RD_FREQ : integer;
  attribute C_RD_FREQ of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_RD_PNTR_WIDTH : integer;
  attribute C_RD_PNTR_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 6;
  attribute C_REG_SLICE_MODE_AXIS : integer;
  attribute C_REG_SLICE_MODE_AXIS of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_REG_SLICE_MODE_RACH : integer;
  attribute C_REG_SLICE_MODE_RACH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_REG_SLICE_MODE_RDCH : integer;
  attribute C_REG_SLICE_MODE_RDCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_REG_SLICE_MODE_WACH : integer;
  attribute C_REG_SLICE_MODE_WACH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_REG_SLICE_MODE_WDCH : integer;
  attribute C_REG_SLICE_MODE_WDCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_REG_SLICE_MODE_WRCH : integer;
  attribute C_REG_SLICE_MODE_WRCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_SELECT_XPM : integer;
  attribute C_SELECT_XPM of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_SYNCHRONIZER_STAGE : integer;
  attribute C_SYNCHRONIZER_STAGE of header_fifo_fifo_generator_v13_2_5 : entity is 2;
  attribute C_UNDERFLOW_LOW : integer;
  attribute C_UNDERFLOW_LOW of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_USE_COMMON_OVERFLOW : integer;
  attribute C_USE_COMMON_OVERFLOW of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_USE_COMMON_UNDERFLOW : integer;
  attribute C_USE_COMMON_UNDERFLOW of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_USE_DEFAULT_SETTINGS : integer;
  attribute C_USE_DEFAULT_SETTINGS of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_USE_DOUT_RST : integer;
  attribute C_USE_DOUT_RST of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_USE_ECC_AXIS : integer;
  attribute C_USE_ECC_AXIS of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_USE_ECC_RACH : integer;
  attribute C_USE_ECC_RACH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_USE_ECC_RDCH : integer;
  attribute C_USE_ECC_RDCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_USE_ECC_WACH : integer;
  attribute C_USE_ECC_WACH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_USE_ECC_WDCH : integer;
  attribute C_USE_ECC_WDCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_USE_ECC_WRCH : integer;
  attribute C_USE_ECC_WRCH of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_USE_EMBEDDED_REG : integer;
  attribute C_USE_EMBEDDED_REG of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_USE_FIFO16_FLAGS : integer;
  attribute C_USE_FIFO16_FLAGS of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_USE_FWFT_DATA_COUNT : integer;
  attribute C_USE_FWFT_DATA_COUNT of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_USE_PIPELINE_REG : integer;
  attribute C_USE_PIPELINE_REG of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_VALID_LOW : integer;
  attribute C_VALID_LOW of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_WACH_TYPE : integer;
  attribute C_WACH_TYPE of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_WDCH_TYPE : integer;
  attribute C_WDCH_TYPE of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_WRCH_TYPE : integer;
  attribute C_WRCH_TYPE of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_WR_ACK_LOW : integer;
  attribute C_WR_ACK_LOW of header_fifo_fifo_generator_v13_2_5 : entity is 0;
  attribute C_WR_DATA_COUNT_WIDTH : integer;
  attribute C_WR_DATA_COUNT_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 6;
  attribute C_WR_DEPTH : integer;
  attribute C_WR_DEPTH of header_fifo_fifo_generator_v13_2_5 : entity is 64;
  attribute C_WR_DEPTH_AXIS : integer;
  attribute C_WR_DEPTH_AXIS of header_fifo_fifo_generator_v13_2_5 : entity is 1024;
  attribute C_WR_DEPTH_RACH : integer;
  attribute C_WR_DEPTH_RACH of header_fifo_fifo_generator_v13_2_5 : entity is 16;
  attribute C_WR_DEPTH_RDCH : integer;
  attribute C_WR_DEPTH_RDCH of header_fifo_fifo_generator_v13_2_5 : entity is 1024;
  attribute C_WR_DEPTH_WACH : integer;
  attribute C_WR_DEPTH_WACH of header_fifo_fifo_generator_v13_2_5 : entity is 16;
  attribute C_WR_DEPTH_WDCH : integer;
  attribute C_WR_DEPTH_WDCH of header_fifo_fifo_generator_v13_2_5 : entity is 1024;
  attribute C_WR_DEPTH_WRCH : integer;
  attribute C_WR_DEPTH_WRCH of header_fifo_fifo_generator_v13_2_5 : entity is 16;
  attribute C_WR_FREQ : integer;
  attribute C_WR_FREQ of header_fifo_fifo_generator_v13_2_5 : entity is 1;
  attribute C_WR_PNTR_WIDTH : integer;
  attribute C_WR_PNTR_WIDTH of header_fifo_fifo_generator_v13_2_5 : entity is 6;
  attribute C_WR_PNTR_WIDTH_AXIS : integer;
  attribute C_WR_PNTR_WIDTH_AXIS of header_fifo_fifo_generator_v13_2_5 : entity is 10;
  attribute C_WR_PNTR_WIDTH_RACH : integer;
  attribute C_WR_PNTR_WIDTH_RACH of header_fifo_fifo_generator_v13_2_5 : entity is 4;
  attribute C_WR_PNTR_WIDTH_RDCH : integer;
  attribute C_WR_PNTR_WIDTH_RDCH of header_fifo_fifo_generator_v13_2_5 : entity is 10;
  attribute C_WR_PNTR_WIDTH_WACH : integer;
  attribute C_WR_PNTR_WIDTH_WACH of header_fifo_fifo_generator_v13_2_5 : entity is 4;
  attribute C_WR_PNTR_WIDTH_WDCH : integer;
  attribute C_WR_PNTR_WIDTH_WDCH of header_fifo_fifo_generator_v13_2_5 : entity is 10;
  attribute C_WR_PNTR_WIDTH_WRCH : integer;
  attribute C_WR_PNTR_WIDTH_WRCH of header_fifo_fifo_generator_v13_2_5 : entity is 4;
  attribute C_WR_RESPONSE_LATENCY : integer;
  attribute C_WR_RESPONSE_LATENCY of header_fifo_fifo_generator_v13_2_5 : entity is 1;
end header_fifo_fifo_generator_v13_2_5;

architecture STRUCTURE of header_fifo_fifo_generator_v13_2_5 is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
begin
  almost_empty <= \<const0>\;
  almost_full <= \<const0>\;
  axi_ar_data_count(4) <= \<const0>\;
  axi_ar_data_count(3) <= \<const0>\;
  axi_ar_data_count(2) <= \<const0>\;
  axi_ar_data_count(1) <= \<const0>\;
  axi_ar_data_count(0) <= \<const0>\;
  axi_ar_dbiterr <= \<const0>\;
  axi_ar_overflow <= \<const0>\;
  axi_ar_prog_empty <= \<const1>\;
  axi_ar_prog_full <= \<const0>\;
  axi_ar_rd_data_count(4) <= \<const0>\;
  axi_ar_rd_data_count(3) <= \<const0>\;
  axi_ar_rd_data_count(2) <= \<const0>\;
  axi_ar_rd_data_count(1) <= \<const0>\;
  axi_ar_rd_data_count(0) <= \<const0>\;
  axi_ar_sbiterr <= \<const0>\;
  axi_ar_underflow <= \<const0>\;
  axi_ar_wr_data_count(4) <= \<const0>\;
  axi_ar_wr_data_count(3) <= \<const0>\;
  axi_ar_wr_data_count(2) <= \<const0>\;
  axi_ar_wr_data_count(1) <= \<const0>\;
  axi_ar_wr_data_count(0) <= \<const0>\;
  axi_aw_data_count(4) <= \<const0>\;
  axi_aw_data_count(3) <= \<const0>\;
  axi_aw_data_count(2) <= \<const0>\;
  axi_aw_data_count(1) <= \<const0>\;
  axi_aw_data_count(0) <= \<const0>\;
  axi_aw_dbiterr <= \<const0>\;
  axi_aw_overflow <= \<const0>\;
  axi_aw_prog_empty <= \<const1>\;
  axi_aw_prog_full <= \<const0>\;
  axi_aw_rd_data_count(4) <= \<const0>\;
  axi_aw_rd_data_count(3) <= \<const0>\;
  axi_aw_rd_data_count(2) <= \<const0>\;
  axi_aw_rd_data_count(1) <= \<const0>\;
  axi_aw_rd_data_count(0) <= \<const0>\;
  axi_aw_sbiterr <= \<const0>\;
  axi_aw_underflow <= \<const0>\;
  axi_aw_wr_data_count(4) <= \<const0>\;
  axi_aw_wr_data_count(3) <= \<const0>\;
  axi_aw_wr_data_count(2) <= \<const0>\;
  axi_aw_wr_data_count(1) <= \<const0>\;
  axi_aw_wr_data_count(0) <= \<const0>\;
  axi_b_data_count(4) <= \<const0>\;
  axi_b_data_count(3) <= \<const0>\;
  axi_b_data_count(2) <= \<const0>\;
  axi_b_data_count(1) <= \<const0>\;
  axi_b_data_count(0) <= \<const0>\;
  axi_b_dbiterr <= \<const0>\;
  axi_b_overflow <= \<const0>\;
  axi_b_prog_empty <= \<const1>\;
  axi_b_prog_full <= \<const0>\;
  axi_b_rd_data_count(4) <= \<const0>\;
  axi_b_rd_data_count(3) <= \<const0>\;
  axi_b_rd_data_count(2) <= \<const0>\;
  axi_b_rd_data_count(1) <= \<const0>\;
  axi_b_rd_data_count(0) <= \<const0>\;
  axi_b_sbiterr <= \<const0>\;
  axi_b_underflow <= \<const0>\;
  axi_b_wr_data_count(4) <= \<const0>\;
  axi_b_wr_data_count(3) <= \<const0>\;
  axi_b_wr_data_count(2) <= \<const0>\;
  axi_b_wr_data_count(1) <= \<const0>\;
  axi_b_wr_data_count(0) <= \<const0>\;
  axi_r_data_count(10) <= \<const0>\;
  axi_r_data_count(9) <= \<const0>\;
  axi_r_data_count(8) <= \<const0>\;
  axi_r_data_count(7) <= \<const0>\;
  axi_r_data_count(6) <= \<const0>\;
  axi_r_data_count(5) <= \<const0>\;
  axi_r_data_count(4) <= \<const0>\;
  axi_r_data_count(3) <= \<const0>\;
  axi_r_data_count(2) <= \<const0>\;
  axi_r_data_count(1) <= \<const0>\;
  axi_r_data_count(0) <= \<const0>\;
  axi_r_dbiterr <= \<const0>\;
  axi_r_overflow <= \<const0>\;
  axi_r_prog_empty <= \<const1>\;
  axi_r_prog_full <= \<const0>\;
  axi_r_rd_data_count(10) <= \<const0>\;
  axi_r_rd_data_count(9) <= \<const0>\;
  axi_r_rd_data_count(8) <= \<const0>\;
  axi_r_rd_data_count(7) <= \<const0>\;
  axi_r_rd_data_count(6) <= \<const0>\;
  axi_r_rd_data_count(5) <= \<const0>\;
  axi_r_rd_data_count(4) <= \<const0>\;
  axi_r_rd_data_count(3) <= \<const0>\;
  axi_r_rd_data_count(2) <= \<const0>\;
  axi_r_rd_data_count(1) <= \<const0>\;
  axi_r_rd_data_count(0) <= \<const0>\;
  axi_r_sbiterr <= \<const0>\;
  axi_r_underflow <= \<const0>\;
  axi_r_wr_data_count(10) <= \<const0>\;
  axi_r_wr_data_count(9) <= \<const0>\;
  axi_r_wr_data_count(8) <= \<const0>\;
  axi_r_wr_data_count(7) <= \<const0>\;
  axi_r_wr_data_count(6) <= \<const0>\;
  axi_r_wr_data_count(5) <= \<const0>\;
  axi_r_wr_data_count(4) <= \<const0>\;
  axi_r_wr_data_count(3) <= \<const0>\;
  axi_r_wr_data_count(2) <= \<const0>\;
  axi_r_wr_data_count(1) <= \<const0>\;
  axi_r_wr_data_count(0) <= \<const0>\;
  axi_w_data_count(10) <= \<const0>\;
  axi_w_data_count(9) <= \<const0>\;
  axi_w_data_count(8) <= \<const0>\;
  axi_w_data_count(7) <= \<const0>\;
  axi_w_data_count(6) <= \<const0>\;
  axi_w_data_count(5) <= \<const0>\;
  axi_w_data_count(4) <= \<const0>\;
  axi_w_data_count(3) <= \<const0>\;
  axi_w_data_count(2) <= \<const0>\;
  axi_w_data_count(1) <= \<const0>\;
  axi_w_data_count(0) <= \<const0>\;
  axi_w_dbiterr <= \<const0>\;
  axi_w_overflow <= \<const0>\;
  axi_w_prog_empty <= \<const1>\;
  axi_w_prog_full <= \<const0>\;
  axi_w_rd_data_count(10) <= \<const0>\;
  axi_w_rd_data_count(9) <= \<const0>\;
  axi_w_rd_data_count(8) <= \<const0>\;
  axi_w_rd_data_count(7) <= \<const0>\;
  axi_w_rd_data_count(6) <= \<const0>\;
  axi_w_rd_data_count(5) <= \<const0>\;
  axi_w_rd_data_count(4) <= \<const0>\;
  axi_w_rd_data_count(3) <= \<const0>\;
  axi_w_rd_data_count(2) <= \<const0>\;
  axi_w_rd_data_count(1) <= \<const0>\;
  axi_w_rd_data_count(0) <= \<const0>\;
  axi_w_sbiterr <= \<const0>\;
  axi_w_underflow <= \<const0>\;
  axi_w_wr_data_count(10) <= \<const0>\;
  axi_w_wr_data_count(9) <= \<const0>\;
  axi_w_wr_data_count(8) <= \<const0>\;
  axi_w_wr_data_count(7) <= \<const0>\;
  axi_w_wr_data_count(6) <= \<const0>\;
  axi_w_wr_data_count(5) <= \<const0>\;
  axi_w_wr_data_count(4) <= \<const0>\;
  axi_w_wr_data_count(3) <= \<const0>\;
  axi_w_wr_data_count(2) <= \<const0>\;
  axi_w_wr_data_count(1) <= \<const0>\;
  axi_w_wr_data_count(0) <= \<const0>\;
  axis_data_count(10) <= \<const0>\;
  axis_data_count(9) <= \<const0>\;
  axis_data_count(8) <= \<const0>\;
  axis_data_count(7) <= \<const0>\;
  axis_data_count(6) <= \<const0>\;
  axis_data_count(5) <= \<const0>\;
  axis_data_count(4) <= \<const0>\;
  axis_data_count(3) <= \<const0>\;
  axis_data_count(2) <= \<const0>\;
  axis_data_count(1) <= \<const0>\;
  axis_data_count(0) <= \<const0>\;
  axis_dbiterr <= \<const0>\;
  axis_overflow <= \<const0>\;
  axis_prog_empty <= \<const1>\;
  axis_prog_full <= \<const0>\;
  axis_rd_data_count(10) <= \<const0>\;
  axis_rd_data_count(9) <= \<const0>\;
  axis_rd_data_count(8) <= \<const0>\;
  axis_rd_data_count(7) <= \<const0>\;
  axis_rd_data_count(6) <= \<const0>\;
  axis_rd_data_count(5) <= \<const0>\;
  axis_rd_data_count(4) <= \<const0>\;
  axis_rd_data_count(3) <= \<const0>\;
  axis_rd_data_count(2) <= \<const0>\;
  axis_rd_data_count(1) <= \<const0>\;
  axis_rd_data_count(0) <= \<const0>\;
  axis_sbiterr <= \<const0>\;
  axis_underflow <= \<const0>\;
  axis_wr_data_count(10) <= \<const0>\;
  axis_wr_data_count(9) <= \<const0>\;
  axis_wr_data_count(8) <= \<const0>\;
  axis_wr_data_count(7) <= \<const0>\;
  axis_wr_data_count(6) <= \<const0>\;
  axis_wr_data_count(5) <= \<const0>\;
  axis_wr_data_count(4) <= \<const0>\;
  axis_wr_data_count(3) <= \<const0>\;
  axis_wr_data_count(2) <= \<const0>\;
  axis_wr_data_count(1) <= \<const0>\;
  axis_wr_data_count(0) <= \<const0>\;
  data_count(5) <= \<const0>\;
  data_count(4) <= \<const0>\;
  data_count(3) <= \<const0>\;
  data_count(2) <= \<const0>\;
  data_count(1) <= \<const0>\;
  data_count(0) <= \<const0>\;
  dbiterr <= \<const0>\;
  m_axi_araddr(31) <= \<const0>\;
  m_axi_araddr(30) <= \<const0>\;
  m_axi_araddr(29) <= \<const0>\;
  m_axi_araddr(28) <= \<const0>\;
  m_axi_araddr(27) <= \<const0>\;
  m_axi_araddr(26) <= \<const0>\;
  m_axi_araddr(25) <= \<const0>\;
  m_axi_araddr(24) <= \<const0>\;
  m_axi_araddr(23) <= \<const0>\;
  m_axi_araddr(22) <= \<const0>\;
  m_axi_araddr(21) <= \<const0>\;
  m_axi_araddr(20) <= \<const0>\;
  m_axi_araddr(19) <= \<const0>\;
  m_axi_araddr(18) <= \<const0>\;
  m_axi_araddr(17) <= \<const0>\;
  m_axi_araddr(16) <= \<const0>\;
  m_axi_araddr(15) <= \<const0>\;
  m_axi_araddr(14) <= \<const0>\;
  m_axi_araddr(13) <= \<const0>\;
  m_axi_araddr(12) <= \<const0>\;
  m_axi_araddr(11) <= \<const0>\;
  m_axi_araddr(10) <= \<const0>\;
  m_axi_araddr(9) <= \<const0>\;
  m_axi_araddr(8) <= \<const0>\;
  m_axi_araddr(7) <= \<const0>\;
  m_axi_araddr(6) <= \<const0>\;
  m_axi_araddr(5) <= \<const0>\;
  m_axi_araddr(4) <= \<const0>\;
  m_axi_araddr(3) <= \<const0>\;
  m_axi_araddr(2) <= \<const0>\;
  m_axi_araddr(1) <= \<const0>\;
  m_axi_araddr(0) <= \<const0>\;
  m_axi_arburst(1) <= \<const0>\;
  m_axi_arburst(0) <= \<const0>\;
  m_axi_arcache(3) <= \<const0>\;
  m_axi_arcache(2) <= \<const0>\;
  m_axi_arcache(1) <= \<const0>\;
  m_axi_arcache(0) <= \<const0>\;
  m_axi_arid(0) <= \<const0>\;
  m_axi_arlen(7) <= \<const0>\;
  m_axi_arlen(6) <= \<const0>\;
  m_axi_arlen(5) <= \<const0>\;
  m_axi_arlen(4) <= \<const0>\;
  m_axi_arlen(3) <= \<const0>\;
  m_axi_arlen(2) <= \<const0>\;
  m_axi_arlen(1) <= \<const0>\;
  m_axi_arlen(0) <= \<const0>\;
  m_axi_arlock(0) <= \<const0>\;
  m_axi_arprot(2) <= \<const0>\;
  m_axi_arprot(1) <= \<const0>\;
  m_axi_arprot(0) <= \<const0>\;
  m_axi_arqos(3) <= \<const0>\;
  m_axi_arqos(2) <= \<const0>\;
  m_axi_arqos(1) <= \<const0>\;
  m_axi_arqos(0) <= \<const0>\;
  m_axi_arregion(3) <= \<const0>\;
  m_axi_arregion(2) <= \<const0>\;
  m_axi_arregion(1) <= \<const0>\;
  m_axi_arregion(0) <= \<const0>\;
  m_axi_arsize(2) <= \<const0>\;
  m_axi_arsize(1) <= \<const0>\;
  m_axi_arsize(0) <= \<const0>\;
  m_axi_aruser(0) <= \<const0>\;
  m_axi_arvalid <= \<const0>\;
  m_axi_awaddr(31) <= \<const0>\;
  m_axi_awaddr(30) <= \<const0>\;
  m_axi_awaddr(29) <= \<const0>\;
  m_axi_awaddr(28) <= \<const0>\;
  m_axi_awaddr(27) <= \<const0>\;
  m_axi_awaddr(26) <= \<const0>\;
  m_axi_awaddr(25) <= \<const0>\;
  m_axi_awaddr(24) <= \<const0>\;
  m_axi_awaddr(23) <= \<const0>\;
  m_axi_awaddr(22) <= \<const0>\;
  m_axi_awaddr(21) <= \<const0>\;
  m_axi_awaddr(20) <= \<const0>\;
  m_axi_awaddr(19) <= \<const0>\;
  m_axi_awaddr(18) <= \<const0>\;
  m_axi_awaddr(17) <= \<const0>\;
  m_axi_awaddr(16) <= \<const0>\;
  m_axi_awaddr(15) <= \<const0>\;
  m_axi_awaddr(14) <= \<const0>\;
  m_axi_awaddr(13) <= \<const0>\;
  m_axi_awaddr(12) <= \<const0>\;
  m_axi_awaddr(11) <= \<const0>\;
  m_axi_awaddr(10) <= \<const0>\;
  m_axi_awaddr(9) <= \<const0>\;
  m_axi_awaddr(8) <= \<const0>\;
  m_axi_awaddr(7) <= \<const0>\;
  m_axi_awaddr(6) <= \<const0>\;
  m_axi_awaddr(5) <= \<const0>\;
  m_axi_awaddr(4) <= \<const0>\;
  m_axi_awaddr(3) <= \<const0>\;
  m_axi_awaddr(2) <= \<const0>\;
  m_axi_awaddr(1) <= \<const0>\;
  m_axi_awaddr(0) <= \<const0>\;
  m_axi_awburst(1) <= \<const0>\;
  m_axi_awburst(0) <= \<const0>\;
  m_axi_awcache(3) <= \<const0>\;
  m_axi_awcache(2) <= \<const0>\;
  m_axi_awcache(1) <= \<const0>\;
  m_axi_awcache(0) <= \<const0>\;
  m_axi_awid(0) <= \<const0>\;
  m_axi_awlen(7) <= \<const0>\;
  m_axi_awlen(6) <= \<const0>\;
  m_axi_awlen(5) <= \<const0>\;
  m_axi_awlen(4) <= \<const0>\;
  m_axi_awlen(3) <= \<const0>\;
  m_axi_awlen(2) <= \<const0>\;
  m_axi_awlen(1) <= \<const0>\;
  m_axi_awlen(0) <= \<const0>\;
  m_axi_awlock(0) <= \<const0>\;
  m_axi_awprot(2) <= \<const0>\;
  m_axi_awprot(1) <= \<const0>\;
  m_axi_awprot(0) <= \<const0>\;
  m_axi_awqos(3) <= \<const0>\;
  m_axi_awqos(2) <= \<const0>\;
  m_axi_awqos(1) <= \<const0>\;
  m_axi_awqos(0) <= \<const0>\;
  m_axi_awregion(3) <= \<const0>\;
  m_axi_awregion(2) <= \<const0>\;
  m_axi_awregion(1) <= \<const0>\;
  m_axi_awregion(0) <= \<const0>\;
  m_axi_awsize(2) <= \<const0>\;
  m_axi_awsize(1) <= \<const0>\;
  m_axi_awsize(0) <= \<const0>\;
  m_axi_awuser(0) <= \<const0>\;
  m_axi_awvalid <= \<const0>\;
  m_axi_bready <= \<const0>\;
  m_axi_rready <= \<const0>\;
  m_axi_wdata(63) <= \<const0>\;
  m_axi_wdata(62) <= \<const0>\;
  m_axi_wdata(61) <= \<const0>\;
  m_axi_wdata(60) <= \<const0>\;
  m_axi_wdata(59) <= \<const0>\;
  m_axi_wdata(58) <= \<const0>\;
  m_axi_wdata(57) <= \<const0>\;
  m_axi_wdata(56) <= \<const0>\;
  m_axi_wdata(55) <= \<const0>\;
  m_axi_wdata(54) <= \<const0>\;
  m_axi_wdata(53) <= \<const0>\;
  m_axi_wdata(52) <= \<const0>\;
  m_axi_wdata(51) <= \<const0>\;
  m_axi_wdata(50) <= \<const0>\;
  m_axi_wdata(49) <= \<const0>\;
  m_axi_wdata(48) <= \<const0>\;
  m_axi_wdata(47) <= \<const0>\;
  m_axi_wdata(46) <= \<const0>\;
  m_axi_wdata(45) <= \<const0>\;
  m_axi_wdata(44) <= \<const0>\;
  m_axi_wdata(43) <= \<const0>\;
  m_axi_wdata(42) <= \<const0>\;
  m_axi_wdata(41) <= \<const0>\;
  m_axi_wdata(40) <= \<const0>\;
  m_axi_wdata(39) <= \<const0>\;
  m_axi_wdata(38) <= \<const0>\;
  m_axi_wdata(37) <= \<const0>\;
  m_axi_wdata(36) <= \<const0>\;
  m_axi_wdata(35) <= \<const0>\;
  m_axi_wdata(34) <= \<const0>\;
  m_axi_wdata(33) <= \<const0>\;
  m_axi_wdata(32) <= \<const0>\;
  m_axi_wdata(31) <= \<const0>\;
  m_axi_wdata(30) <= \<const0>\;
  m_axi_wdata(29) <= \<const0>\;
  m_axi_wdata(28) <= \<const0>\;
  m_axi_wdata(27) <= \<const0>\;
  m_axi_wdata(26) <= \<const0>\;
  m_axi_wdata(25) <= \<const0>\;
  m_axi_wdata(24) <= \<const0>\;
  m_axi_wdata(23) <= \<const0>\;
  m_axi_wdata(22) <= \<const0>\;
  m_axi_wdata(21) <= \<const0>\;
  m_axi_wdata(20) <= \<const0>\;
  m_axi_wdata(19) <= \<const0>\;
  m_axi_wdata(18) <= \<const0>\;
  m_axi_wdata(17) <= \<const0>\;
  m_axi_wdata(16) <= \<const0>\;
  m_axi_wdata(15) <= \<const0>\;
  m_axi_wdata(14) <= \<const0>\;
  m_axi_wdata(13) <= \<const0>\;
  m_axi_wdata(12) <= \<const0>\;
  m_axi_wdata(11) <= \<const0>\;
  m_axi_wdata(10) <= \<const0>\;
  m_axi_wdata(9) <= \<const0>\;
  m_axi_wdata(8) <= \<const0>\;
  m_axi_wdata(7) <= \<const0>\;
  m_axi_wdata(6) <= \<const0>\;
  m_axi_wdata(5) <= \<const0>\;
  m_axi_wdata(4) <= \<const0>\;
  m_axi_wdata(3) <= \<const0>\;
  m_axi_wdata(2) <= \<const0>\;
  m_axi_wdata(1) <= \<const0>\;
  m_axi_wdata(0) <= \<const0>\;
  m_axi_wid(0) <= \<const0>\;
  m_axi_wlast <= \<const0>\;
  m_axi_wstrb(7) <= \<const0>\;
  m_axi_wstrb(6) <= \<const0>\;
  m_axi_wstrb(5) <= \<const0>\;
  m_axi_wstrb(4) <= \<const0>\;
  m_axi_wstrb(3) <= \<const0>\;
  m_axi_wstrb(2) <= \<const0>\;
  m_axi_wstrb(1) <= \<const0>\;
  m_axi_wstrb(0) <= \<const0>\;
  m_axi_wuser(0) <= \<const0>\;
  m_axi_wvalid <= \<const0>\;
  m_axis_tdata(7) <= \<const0>\;
  m_axis_tdata(6) <= \<const0>\;
  m_axis_tdata(5) <= \<const0>\;
  m_axis_tdata(4) <= \<const0>\;
  m_axis_tdata(3) <= \<const0>\;
  m_axis_tdata(2) <= \<const0>\;
  m_axis_tdata(1) <= \<const0>\;
  m_axis_tdata(0) <= \<const0>\;
  m_axis_tdest(0) <= \<const0>\;
  m_axis_tid(0) <= \<const0>\;
  m_axis_tkeep(0) <= \<const0>\;
  m_axis_tlast <= \<const0>\;
  m_axis_tstrb(0) <= \<const0>\;
  m_axis_tuser(3) <= \<const0>\;
  m_axis_tuser(2) <= \<const0>\;
  m_axis_tuser(1) <= \<const0>\;
  m_axis_tuser(0) <= \<const0>\;
  m_axis_tvalid <= \<const0>\;
  overflow <= \<const0>\;
  prog_empty <= \<const0>\;
  prog_full <= \<const0>\;
  s_axi_arready <= \<const0>\;
  s_axi_awready <= \<const0>\;
  s_axi_bid(0) <= \<const0>\;
  s_axi_bresp(1) <= \<const0>\;
  s_axi_bresp(0) <= \<const0>\;
  s_axi_buser(0) <= \<const0>\;
  s_axi_bvalid <= \<const0>\;
  s_axi_rdata(63) <= \<const0>\;
  s_axi_rdata(62) <= \<const0>\;
  s_axi_rdata(61) <= \<const0>\;
  s_axi_rdata(60) <= \<const0>\;
  s_axi_rdata(59) <= \<const0>\;
  s_axi_rdata(58) <= \<const0>\;
  s_axi_rdata(57) <= \<const0>\;
  s_axi_rdata(56) <= \<const0>\;
  s_axi_rdata(55) <= \<const0>\;
  s_axi_rdata(54) <= \<const0>\;
  s_axi_rdata(53) <= \<const0>\;
  s_axi_rdata(52) <= \<const0>\;
  s_axi_rdata(51) <= \<const0>\;
  s_axi_rdata(50) <= \<const0>\;
  s_axi_rdata(49) <= \<const0>\;
  s_axi_rdata(48) <= \<const0>\;
  s_axi_rdata(47) <= \<const0>\;
  s_axi_rdata(46) <= \<const0>\;
  s_axi_rdata(45) <= \<const0>\;
  s_axi_rdata(44) <= \<const0>\;
  s_axi_rdata(43) <= \<const0>\;
  s_axi_rdata(42) <= \<const0>\;
  s_axi_rdata(41) <= \<const0>\;
  s_axi_rdata(40) <= \<const0>\;
  s_axi_rdata(39) <= \<const0>\;
  s_axi_rdata(38) <= \<const0>\;
  s_axi_rdata(37) <= \<const0>\;
  s_axi_rdata(36) <= \<const0>\;
  s_axi_rdata(35) <= \<const0>\;
  s_axi_rdata(34) <= \<const0>\;
  s_axi_rdata(33) <= \<const0>\;
  s_axi_rdata(32) <= \<const0>\;
  s_axi_rdata(31) <= \<const0>\;
  s_axi_rdata(30) <= \<const0>\;
  s_axi_rdata(29) <= \<const0>\;
  s_axi_rdata(28) <= \<const0>\;
  s_axi_rdata(27) <= \<const0>\;
  s_axi_rdata(26) <= \<const0>\;
  s_axi_rdata(25) <= \<const0>\;
  s_axi_rdata(24) <= \<const0>\;
  s_axi_rdata(23) <= \<const0>\;
  s_axi_rdata(22) <= \<const0>\;
  s_axi_rdata(21) <= \<const0>\;
  s_axi_rdata(20) <= \<const0>\;
  s_axi_rdata(19) <= \<const0>\;
  s_axi_rdata(18) <= \<const0>\;
  s_axi_rdata(17) <= \<const0>\;
  s_axi_rdata(16) <= \<const0>\;
  s_axi_rdata(15) <= \<const0>\;
  s_axi_rdata(14) <= \<const0>\;
  s_axi_rdata(13) <= \<const0>\;
  s_axi_rdata(12) <= \<const0>\;
  s_axi_rdata(11) <= \<const0>\;
  s_axi_rdata(10) <= \<const0>\;
  s_axi_rdata(9) <= \<const0>\;
  s_axi_rdata(8) <= \<const0>\;
  s_axi_rdata(7) <= \<const0>\;
  s_axi_rdata(6) <= \<const0>\;
  s_axi_rdata(5) <= \<const0>\;
  s_axi_rdata(4) <= \<const0>\;
  s_axi_rdata(3) <= \<const0>\;
  s_axi_rdata(2) <= \<const0>\;
  s_axi_rdata(1) <= \<const0>\;
  s_axi_rdata(0) <= \<const0>\;
  s_axi_rid(0) <= \<const0>\;
  s_axi_rlast <= \<const0>\;
  s_axi_rresp(1) <= \<const0>\;
  s_axi_rresp(0) <= \<const0>\;
  s_axi_ruser(0) <= \<const0>\;
  s_axi_rvalid <= \<const0>\;
  s_axi_wready <= \<const0>\;
  s_axis_tready <= \<const0>\;
  sbiterr <= \<const0>\;
  wr_ack <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst_fifo_gen: entity work.header_fifo_fifo_generator_v13_2_5_synth
     port map (
      AR(0) => rd_rst_busy,
      din(255 downto 0) => din(255 downto 0),
      dout(255 downto 0) => dout(255 downto 0),
      empty => empty,
      full => full,
      rd_clk => rd_clk,
      rd_data_count(5 downto 0) => rd_data_count(5 downto 0),
      rd_en => rd_en,
      rst => rst,
      underflow => underflow,
      valid => valid,
      wr_clk => wr_clk,
      wr_data_count(5 downto 0) => wr_data_count(5 downto 0),
      wr_en => wr_en,
      wr_rst_busy => wr_rst_busy
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity header_fifo is
  port (
    rst : in STD_LOGIC;
    wr_clk : in STD_LOGIC;
    rd_clk : in STD_LOGIC;
    din : in STD_LOGIC_VECTOR ( 255 downto 0 );
    wr_en : in STD_LOGIC;
    rd_en : in STD_LOGIC;
    dout : out STD_LOGIC_VECTOR ( 255 downto 0 );
    full : out STD_LOGIC;
    empty : out STD_LOGIC;
    valid : out STD_LOGIC;
    underflow : out STD_LOGIC;
    rd_data_count : out STD_LOGIC_VECTOR ( 5 downto 0 );
    wr_data_count : out STD_LOGIC_VECTOR ( 5 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of header_fifo : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of header_fifo : entity is "header_fifo,fifo_generator_v13_2_5,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of header_fifo : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of header_fifo : entity is "fifo_generator_v13_2_5,Vivado 2019.2";
end header_fifo;

architecture STRUCTURE of header_fifo is
  signal NLW_U0_almost_empty_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_almost_full_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_ar_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_ar_overflow_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_ar_prog_empty_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_ar_prog_full_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_ar_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_ar_underflow_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_aw_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_aw_overflow_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_aw_prog_empty_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_aw_prog_full_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_aw_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_aw_underflow_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_b_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_b_overflow_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_b_prog_empty_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_b_prog_full_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_b_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_b_underflow_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_r_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_r_overflow_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_r_prog_empty_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_r_prog_full_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_r_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_r_underflow_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_w_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_w_overflow_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_w_prog_empty_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_w_prog_full_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_w_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_w_underflow_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axis_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axis_overflow_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axis_prog_empty_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axis_prog_full_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axis_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axis_underflow_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_m_axi_arvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_m_axi_awvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_m_axi_bready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_m_axi_rready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_m_axi_wlast_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_m_axi_wvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_m_axis_tlast_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_m_axis_tvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_overflow_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_prog_empty_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_prog_full_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rd_rst_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rlast_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axis_tready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_wr_ack_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_wr_rst_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_axi_ar_data_count_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_U0_axi_ar_rd_data_count_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_U0_axi_ar_wr_data_count_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_U0_axi_aw_data_count_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_U0_axi_aw_rd_data_count_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_U0_axi_aw_wr_data_count_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_U0_axi_b_data_count_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_U0_axi_b_rd_data_count_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_U0_axi_b_wr_data_count_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_U0_axi_r_data_count_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_axi_r_rd_data_count_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_axi_r_wr_data_count_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_axi_w_data_count_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_axi_w_rd_data_count_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_axi_w_wr_data_count_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_axis_data_count_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_axis_rd_data_count_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_axis_wr_data_count_UNCONNECTED : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal NLW_U0_data_count_UNCONNECTED : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal NLW_U0_m_axi_araddr_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_U0_m_axi_arburst_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_U0_m_axi_arcache_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_m_axi_arid_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_U0_m_axi_arlen_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_U0_m_axi_arlock_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_U0_m_axi_arprot_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_U0_m_axi_arqos_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_m_axi_arregion_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_m_axi_arsize_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_U0_m_axi_aruser_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_U0_m_axi_awaddr_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_U0_m_axi_awburst_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_U0_m_axi_awcache_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_m_axi_awid_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_U0_m_axi_awlen_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_U0_m_axi_awlock_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_U0_m_axi_awprot_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_U0_m_axi_awqos_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_m_axi_awregion_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_m_axi_awsize_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_U0_m_axi_awuser_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_U0_m_axi_wdata_UNCONNECTED : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal NLW_U0_m_axi_wid_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_U0_m_axi_wstrb_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_U0_m_axi_wuser_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_U0_m_axis_tdata_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_U0_m_axis_tdest_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_U0_m_axis_tid_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_U0_m_axis_tkeep_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_U0_m_axis_tstrb_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_U0_m_axis_tuser_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_bid_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_U0_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_U0_s_axi_buser_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_U0_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal NLW_U0_s_axi_rid_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_U0_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_U0_s_axi_ruser_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute C_ADD_NGC_CONSTRAINT : integer;
  attribute C_ADD_NGC_CONSTRAINT of U0 : label is 0;
  attribute C_APPLICATION_TYPE_AXIS : integer;
  attribute C_APPLICATION_TYPE_AXIS of U0 : label is 0;
  attribute C_APPLICATION_TYPE_RACH : integer;
  attribute C_APPLICATION_TYPE_RACH of U0 : label is 0;
  attribute C_APPLICATION_TYPE_RDCH : integer;
  attribute C_APPLICATION_TYPE_RDCH of U0 : label is 0;
  attribute C_APPLICATION_TYPE_WACH : integer;
  attribute C_APPLICATION_TYPE_WACH of U0 : label is 0;
  attribute C_APPLICATION_TYPE_WDCH : integer;
  attribute C_APPLICATION_TYPE_WDCH of U0 : label is 0;
  attribute C_APPLICATION_TYPE_WRCH : integer;
  attribute C_APPLICATION_TYPE_WRCH of U0 : label is 0;
  attribute C_AXIS_TDATA_WIDTH : integer;
  attribute C_AXIS_TDATA_WIDTH of U0 : label is 8;
  attribute C_AXIS_TDEST_WIDTH : integer;
  attribute C_AXIS_TDEST_WIDTH of U0 : label is 1;
  attribute C_AXIS_TID_WIDTH : integer;
  attribute C_AXIS_TID_WIDTH of U0 : label is 1;
  attribute C_AXIS_TKEEP_WIDTH : integer;
  attribute C_AXIS_TKEEP_WIDTH of U0 : label is 1;
  attribute C_AXIS_TSTRB_WIDTH : integer;
  attribute C_AXIS_TSTRB_WIDTH of U0 : label is 1;
  attribute C_AXIS_TUSER_WIDTH : integer;
  attribute C_AXIS_TUSER_WIDTH of U0 : label is 4;
  attribute C_AXIS_TYPE : integer;
  attribute C_AXIS_TYPE of U0 : label is 0;
  attribute C_AXI_ADDR_WIDTH : integer;
  attribute C_AXI_ADDR_WIDTH of U0 : label is 32;
  attribute C_AXI_ARUSER_WIDTH : integer;
  attribute C_AXI_ARUSER_WIDTH of U0 : label is 1;
  attribute C_AXI_AWUSER_WIDTH : integer;
  attribute C_AXI_AWUSER_WIDTH of U0 : label is 1;
  attribute C_AXI_BUSER_WIDTH : integer;
  attribute C_AXI_BUSER_WIDTH of U0 : label is 1;
  attribute C_AXI_DATA_WIDTH : integer;
  attribute C_AXI_DATA_WIDTH of U0 : label is 64;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of U0 : label is 1;
  attribute C_AXI_LEN_WIDTH : integer;
  attribute C_AXI_LEN_WIDTH of U0 : label is 8;
  attribute C_AXI_LOCK_WIDTH : integer;
  attribute C_AXI_LOCK_WIDTH of U0 : label is 1;
  attribute C_AXI_RUSER_WIDTH : integer;
  attribute C_AXI_RUSER_WIDTH of U0 : label is 1;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of U0 : label is 1;
  attribute C_AXI_WUSER_WIDTH : integer;
  attribute C_AXI_WUSER_WIDTH of U0 : label is 1;
  attribute C_COMMON_CLOCK : integer;
  attribute C_COMMON_CLOCK of U0 : label is 0;
  attribute C_COUNT_TYPE : integer;
  attribute C_COUNT_TYPE of U0 : label is 0;
  attribute C_DATA_COUNT_WIDTH : integer;
  attribute C_DATA_COUNT_WIDTH of U0 : label is 6;
  attribute C_DEFAULT_VALUE : string;
  attribute C_DEFAULT_VALUE of U0 : label is "BlankString";
  attribute C_DIN_WIDTH : integer;
  attribute C_DIN_WIDTH of U0 : label is 256;
  attribute C_DIN_WIDTH_AXIS : integer;
  attribute C_DIN_WIDTH_AXIS of U0 : label is 1;
  attribute C_DIN_WIDTH_RACH : integer;
  attribute C_DIN_WIDTH_RACH of U0 : label is 32;
  attribute C_DIN_WIDTH_RDCH : integer;
  attribute C_DIN_WIDTH_RDCH of U0 : label is 64;
  attribute C_DIN_WIDTH_WACH : integer;
  attribute C_DIN_WIDTH_WACH of U0 : label is 1;
  attribute C_DIN_WIDTH_WDCH : integer;
  attribute C_DIN_WIDTH_WDCH of U0 : label is 64;
  attribute C_DIN_WIDTH_WRCH : integer;
  attribute C_DIN_WIDTH_WRCH of U0 : label is 2;
  attribute C_DOUT_RST_VAL : string;
  attribute C_DOUT_RST_VAL of U0 : label is "0";
  attribute C_DOUT_WIDTH : integer;
  attribute C_DOUT_WIDTH of U0 : label is 256;
  attribute C_ENABLE_RLOCS : integer;
  attribute C_ENABLE_RLOCS of U0 : label is 0;
  attribute C_ENABLE_RST_SYNC : integer;
  attribute C_ENABLE_RST_SYNC of U0 : label is 1;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of U0 : label is 0;
  attribute C_ERROR_INJECTION_TYPE : integer;
  attribute C_ERROR_INJECTION_TYPE of U0 : label is 0;
  attribute C_ERROR_INJECTION_TYPE_AXIS : integer;
  attribute C_ERROR_INJECTION_TYPE_AXIS of U0 : label is 0;
  attribute C_ERROR_INJECTION_TYPE_RACH : integer;
  attribute C_ERROR_INJECTION_TYPE_RACH of U0 : label is 0;
  attribute C_ERROR_INJECTION_TYPE_RDCH : integer;
  attribute C_ERROR_INJECTION_TYPE_RDCH of U0 : label is 0;
  attribute C_ERROR_INJECTION_TYPE_WACH : integer;
  attribute C_ERROR_INJECTION_TYPE_WACH of U0 : label is 0;
  attribute C_ERROR_INJECTION_TYPE_WDCH : integer;
  attribute C_ERROR_INJECTION_TYPE_WDCH of U0 : label is 0;
  attribute C_ERROR_INJECTION_TYPE_WRCH : integer;
  attribute C_ERROR_INJECTION_TYPE_WRCH of U0 : label is 0;
  attribute C_FAMILY : string;
  attribute C_FAMILY of U0 : label is "virtexuplusHBM";
  attribute C_FULL_FLAGS_RST_VAL : integer;
  attribute C_FULL_FLAGS_RST_VAL of U0 : label is 1;
  attribute C_HAS_ALMOST_EMPTY : integer;
  attribute C_HAS_ALMOST_EMPTY of U0 : label is 0;
  attribute C_HAS_ALMOST_FULL : integer;
  attribute C_HAS_ALMOST_FULL of U0 : label is 0;
  attribute C_HAS_AXIS_TDATA : integer;
  attribute C_HAS_AXIS_TDATA of U0 : label is 1;
  attribute C_HAS_AXIS_TDEST : integer;
  attribute C_HAS_AXIS_TDEST of U0 : label is 0;
  attribute C_HAS_AXIS_TID : integer;
  attribute C_HAS_AXIS_TID of U0 : label is 0;
  attribute C_HAS_AXIS_TKEEP : integer;
  attribute C_HAS_AXIS_TKEEP of U0 : label is 0;
  attribute C_HAS_AXIS_TLAST : integer;
  attribute C_HAS_AXIS_TLAST of U0 : label is 0;
  attribute C_HAS_AXIS_TREADY : integer;
  attribute C_HAS_AXIS_TREADY of U0 : label is 1;
  attribute C_HAS_AXIS_TSTRB : integer;
  attribute C_HAS_AXIS_TSTRB of U0 : label is 0;
  attribute C_HAS_AXIS_TUSER : integer;
  attribute C_HAS_AXIS_TUSER of U0 : label is 1;
  attribute C_HAS_AXI_ARUSER : integer;
  attribute C_HAS_AXI_ARUSER of U0 : label is 0;
  attribute C_HAS_AXI_AWUSER : integer;
  attribute C_HAS_AXI_AWUSER of U0 : label is 0;
  attribute C_HAS_AXI_BUSER : integer;
  attribute C_HAS_AXI_BUSER of U0 : label is 0;
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of U0 : label is 0;
  attribute C_HAS_AXI_RD_CHANNEL : integer;
  attribute C_HAS_AXI_RD_CHANNEL of U0 : label is 1;
  attribute C_HAS_AXI_RUSER : integer;
  attribute C_HAS_AXI_RUSER of U0 : label is 0;
  attribute C_HAS_AXI_WR_CHANNEL : integer;
  attribute C_HAS_AXI_WR_CHANNEL of U0 : label is 1;
  attribute C_HAS_AXI_WUSER : integer;
  attribute C_HAS_AXI_WUSER of U0 : label is 0;
  attribute C_HAS_BACKUP : integer;
  attribute C_HAS_BACKUP of U0 : label is 0;
  attribute C_HAS_DATA_COUNT : integer;
  attribute C_HAS_DATA_COUNT of U0 : label is 0;
  attribute C_HAS_DATA_COUNTS_AXIS : integer;
  attribute C_HAS_DATA_COUNTS_AXIS of U0 : label is 0;
  attribute C_HAS_DATA_COUNTS_RACH : integer;
  attribute C_HAS_DATA_COUNTS_RACH of U0 : label is 0;
  attribute C_HAS_DATA_COUNTS_RDCH : integer;
  attribute C_HAS_DATA_COUNTS_RDCH of U0 : label is 0;
  attribute C_HAS_DATA_COUNTS_WACH : integer;
  attribute C_HAS_DATA_COUNTS_WACH of U0 : label is 0;
  attribute C_HAS_DATA_COUNTS_WDCH : integer;
  attribute C_HAS_DATA_COUNTS_WDCH of U0 : label is 0;
  attribute C_HAS_DATA_COUNTS_WRCH : integer;
  attribute C_HAS_DATA_COUNTS_WRCH of U0 : label is 0;
  attribute C_HAS_INT_CLK : integer;
  attribute C_HAS_INT_CLK of U0 : label is 0;
  attribute C_HAS_MASTER_CE : integer;
  attribute C_HAS_MASTER_CE of U0 : label is 0;
  attribute C_HAS_MEMINIT_FILE : integer;
  attribute C_HAS_MEMINIT_FILE of U0 : label is 0;
  attribute C_HAS_OVERFLOW : integer;
  attribute C_HAS_OVERFLOW of U0 : label is 0;
  attribute C_HAS_PROG_FLAGS_AXIS : integer;
  attribute C_HAS_PROG_FLAGS_AXIS of U0 : label is 0;
  attribute C_HAS_PROG_FLAGS_RACH : integer;
  attribute C_HAS_PROG_FLAGS_RACH of U0 : label is 0;
  attribute C_HAS_PROG_FLAGS_RDCH : integer;
  attribute C_HAS_PROG_FLAGS_RDCH of U0 : label is 0;
  attribute C_HAS_PROG_FLAGS_WACH : integer;
  attribute C_HAS_PROG_FLAGS_WACH of U0 : label is 0;
  attribute C_HAS_PROG_FLAGS_WDCH : integer;
  attribute C_HAS_PROG_FLAGS_WDCH of U0 : label is 0;
  attribute C_HAS_PROG_FLAGS_WRCH : integer;
  attribute C_HAS_PROG_FLAGS_WRCH of U0 : label is 0;
  attribute C_HAS_RD_DATA_COUNT : integer;
  attribute C_HAS_RD_DATA_COUNT of U0 : label is 1;
  attribute C_HAS_RD_RST : integer;
  attribute C_HAS_RD_RST of U0 : label is 0;
  attribute C_HAS_RST : integer;
  attribute C_HAS_RST of U0 : label is 1;
  attribute C_HAS_SLAVE_CE : integer;
  attribute C_HAS_SLAVE_CE of U0 : label is 0;
  attribute C_HAS_SRST : integer;
  attribute C_HAS_SRST of U0 : label is 0;
  attribute C_HAS_UNDERFLOW : integer;
  attribute C_HAS_UNDERFLOW of U0 : label is 1;
  attribute C_HAS_VALID : integer;
  attribute C_HAS_VALID of U0 : label is 1;
  attribute C_HAS_WR_ACK : integer;
  attribute C_HAS_WR_ACK of U0 : label is 0;
  attribute C_HAS_WR_DATA_COUNT : integer;
  attribute C_HAS_WR_DATA_COUNT of U0 : label is 1;
  attribute C_HAS_WR_RST : integer;
  attribute C_HAS_WR_RST of U0 : label is 0;
  attribute C_IMPLEMENTATION_TYPE : integer;
  attribute C_IMPLEMENTATION_TYPE of U0 : label is 2;
  attribute C_IMPLEMENTATION_TYPE_AXIS : integer;
  attribute C_IMPLEMENTATION_TYPE_AXIS of U0 : label is 1;
  attribute C_IMPLEMENTATION_TYPE_RACH : integer;
  attribute C_IMPLEMENTATION_TYPE_RACH of U0 : label is 1;
  attribute C_IMPLEMENTATION_TYPE_RDCH : integer;
  attribute C_IMPLEMENTATION_TYPE_RDCH of U0 : label is 1;
  attribute C_IMPLEMENTATION_TYPE_WACH : integer;
  attribute C_IMPLEMENTATION_TYPE_WACH of U0 : label is 1;
  attribute C_IMPLEMENTATION_TYPE_WDCH : integer;
  attribute C_IMPLEMENTATION_TYPE_WDCH of U0 : label is 1;
  attribute C_IMPLEMENTATION_TYPE_WRCH : integer;
  attribute C_IMPLEMENTATION_TYPE_WRCH of U0 : label is 1;
  attribute C_INIT_WR_PNTR_VAL : integer;
  attribute C_INIT_WR_PNTR_VAL of U0 : label is 0;
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of U0 : label is 0;
  attribute C_MEMORY_TYPE : integer;
  attribute C_MEMORY_TYPE of U0 : label is 2;
  attribute C_MIF_FILE_NAME : string;
  attribute C_MIF_FILE_NAME of U0 : label is "BlankString";
  attribute C_MSGON_VAL : integer;
  attribute C_MSGON_VAL of U0 : label is 1;
  attribute C_OPTIMIZATION_MODE : integer;
  attribute C_OPTIMIZATION_MODE of U0 : label is 0;
  attribute C_OVERFLOW_LOW : integer;
  attribute C_OVERFLOW_LOW of U0 : label is 0;
  attribute C_POWER_SAVING_MODE : integer;
  attribute C_POWER_SAVING_MODE of U0 : label is 0;
  attribute C_PRELOAD_LATENCY : integer;
  attribute C_PRELOAD_LATENCY of U0 : label is 0;
  attribute C_PRELOAD_REGS : integer;
  attribute C_PRELOAD_REGS of U0 : label is 1;
  attribute C_PRIM_FIFO_TYPE : string;
  attribute C_PRIM_FIFO_TYPE of U0 : label is "512x72";
  attribute C_PRIM_FIFO_TYPE_AXIS : string;
  attribute C_PRIM_FIFO_TYPE_AXIS of U0 : label is "1kx18";
  attribute C_PRIM_FIFO_TYPE_RACH : string;
  attribute C_PRIM_FIFO_TYPE_RACH of U0 : label is "512x36";
  attribute C_PRIM_FIFO_TYPE_RDCH : string;
  attribute C_PRIM_FIFO_TYPE_RDCH of U0 : label is "512x72";
  attribute C_PRIM_FIFO_TYPE_WACH : string;
  attribute C_PRIM_FIFO_TYPE_WACH of U0 : label is "512x36";
  attribute C_PRIM_FIFO_TYPE_WDCH : string;
  attribute C_PRIM_FIFO_TYPE_WDCH of U0 : label is "512x72";
  attribute C_PRIM_FIFO_TYPE_WRCH : string;
  attribute C_PRIM_FIFO_TYPE_WRCH of U0 : label is "512x36";
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL : integer;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL of U0 : label is 4;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_AXIS : integer;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_AXIS of U0 : label is 1022;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_RACH : integer;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_RACH of U0 : label is 1022;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_RDCH : integer;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_RDCH of U0 : label is 1022;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_WACH : integer;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_WACH of U0 : label is 1022;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_WDCH : integer;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_WDCH of U0 : label is 1022;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_WRCH : integer;
  attribute C_PROG_EMPTY_THRESH_ASSERT_VAL_WRCH of U0 : label is 1022;
  attribute C_PROG_EMPTY_THRESH_NEGATE_VAL : integer;
  attribute C_PROG_EMPTY_THRESH_NEGATE_VAL of U0 : label is 5;
  attribute C_PROG_EMPTY_TYPE : integer;
  attribute C_PROG_EMPTY_TYPE of U0 : label is 0;
  attribute C_PROG_EMPTY_TYPE_AXIS : integer;
  attribute C_PROG_EMPTY_TYPE_AXIS of U0 : label is 0;
  attribute C_PROG_EMPTY_TYPE_RACH : integer;
  attribute C_PROG_EMPTY_TYPE_RACH of U0 : label is 0;
  attribute C_PROG_EMPTY_TYPE_RDCH : integer;
  attribute C_PROG_EMPTY_TYPE_RDCH of U0 : label is 0;
  attribute C_PROG_EMPTY_TYPE_WACH : integer;
  attribute C_PROG_EMPTY_TYPE_WACH of U0 : label is 0;
  attribute C_PROG_EMPTY_TYPE_WDCH : integer;
  attribute C_PROG_EMPTY_TYPE_WDCH of U0 : label is 0;
  attribute C_PROG_EMPTY_TYPE_WRCH : integer;
  attribute C_PROG_EMPTY_TYPE_WRCH of U0 : label is 0;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL : integer;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL of U0 : label is 63;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_AXIS : integer;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_AXIS of U0 : label is 1023;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_RACH : integer;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_RACH of U0 : label is 1023;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_RDCH : integer;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_RDCH of U0 : label is 1023;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_WACH : integer;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_WACH of U0 : label is 1023;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_WDCH : integer;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_WDCH of U0 : label is 1023;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_WRCH : integer;
  attribute C_PROG_FULL_THRESH_ASSERT_VAL_WRCH of U0 : label is 1023;
  attribute C_PROG_FULL_THRESH_NEGATE_VAL : integer;
  attribute C_PROG_FULL_THRESH_NEGATE_VAL of U0 : label is 62;
  attribute C_PROG_FULL_TYPE : integer;
  attribute C_PROG_FULL_TYPE of U0 : label is 0;
  attribute C_PROG_FULL_TYPE_AXIS : integer;
  attribute C_PROG_FULL_TYPE_AXIS of U0 : label is 0;
  attribute C_PROG_FULL_TYPE_RACH : integer;
  attribute C_PROG_FULL_TYPE_RACH of U0 : label is 0;
  attribute C_PROG_FULL_TYPE_RDCH : integer;
  attribute C_PROG_FULL_TYPE_RDCH of U0 : label is 0;
  attribute C_PROG_FULL_TYPE_WACH : integer;
  attribute C_PROG_FULL_TYPE_WACH of U0 : label is 0;
  attribute C_PROG_FULL_TYPE_WDCH : integer;
  attribute C_PROG_FULL_TYPE_WDCH of U0 : label is 0;
  attribute C_PROG_FULL_TYPE_WRCH : integer;
  attribute C_PROG_FULL_TYPE_WRCH of U0 : label is 0;
  attribute C_RACH_TYPE : integer;
  attribute C_RACH_TYPE of U0 : label is 0;
  attribute C_RDCH_TYPE : integer;
  attribute C_RDCH_TYPE of U0 : label is 0;
  attribute C_RD_DATA_COUNT_WIDTH : integer;
  attribute C_RD_DATA_COUNT_WIDTH of U0 : label is 6;
  attribute C_RD_DEPTH : integer;
  attribute C_RD_DEPTH of U0 : label is 64;
  attribute C_RD_FREQ : integer;
  attribute C_RD_FREQ of U0 : label is 1;
  attribute C_RD_PNTR_WIDTH : integer;
  attribute C_RD_PNTR_WIDTH of U0 : label is 6;
  attribute C_REG_SLICE_MODE_AXIS : integer;
  attribute C_REG_SLICE_MODE_AXIS of U0 : label is 0;
  attribute C_REG_SLICE_MODE_RACH : integer;
  attribute C_REG_SLICE_MODE_RACH of U0 : label is 0;
  attribute C_REG_SLICE_MODE_RDCH : integer;
  attribute C_REG_SLICE_MODE_RDCH of U0 : label is 0;
  attribute C_REG_SLICE_MODE_WACH : integer;
  attribute C_REG_SLICE_MODE_WACH of U0 : label is 0;
  attribute C_REG_SLICE_MODE_WDCH : integer;
  attribute C_REG_SLICE_MODE_WDCH of U0 : label is 0;
  attribute C_REG_SLICE_MODE_WRCH : integer;
  attribute C_REG_SLICE_MODE_WRCH of U0 : label is 0;
  attribute C_SELECT_XPM : integer;
  attribute C_SELECT_XPM of U0 : label is 0;
  attribute C_SYNCHRONIZER_STAGE : integer;
  attribute C_SYNCHRONIZER_STAGE of U0 : label is 2;
  attribute C_UNDERFLOW_LOW : integer;
  attribute C_UNDERFLOW_LOW of U0 : label is 0;
  attribute C_USE_COMMON_OVERFLOW : integer;
  attribute C_USE_COMMON_OVERFLOW of U0 : label is 0;
  attribute C_USE_COMMON_UNDERFLOW : integer;
  attribute C_USE_COMMON_UNDERFLOW of U0 : label is 0;
  attribute C_USE_DEFAULT_SETTINGS : integer;
  attribute C_USE_DEFAULT_SETTINGS of U0 : label is 0;
  attribute C_USE_DOUT_RST : integer;
  attribute C_USE_DOUT_RST of U0 : label is 1;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of U0 : label is 0;
  attribute C_USE_ECC_AXIS : integer;
  attribute C_USE_ECC_AXIS of U0 : label is 0;
  attribute C_USE_ECC_RACH : integer;
  attribute C_USE_ECC_RACH of U0 : label is 0;
  attribute C_USE_ECC_RDCH : integer;
  attribute C_USE_ECC_RDCH of U0 : label is 0;
  attribute C_USE_ECC_WACH : integer;
  attribute C_USE_ECC_WACH of U0 : label is 0;
  attribute C_USE_ECC_WDCH : integer;
  attribute C_USE_ECC_WDCH of U0 : label is 0;
  attribute C_USE_ECC_WRCH : integer;
  attribute C_USE_ECC_WRCH of U0 : label is 0;
  attribute C_USE_EMBEDDED_REG : integer;
  attribute C_USE_EMBEDDED_REG of U0 : label is 0;
  attribute C_USE_FIFO16_FLAGS : integer;
  attribute C_USE_FIFO16_FLAGS of U0 : label is 0;
  attribute C_USE_FWFT_DATA_COUNT : integer;
  attribute C_USE_FWFT_DATA_COUNT of U0 : label is 0;
  attribute C_USE_PIPELINE_REG : integer;
  attribute C_USE_PIPELINE_REG of U0 : label is 0;
  attribute C_VALID_LOW : integer;
  attribute C_VALID_LOW of U0 : label is 0;
  attribute C_WACH_TYPE : integer;
  attribute C_WACH_TYPE of U0 : label is 0;
  attribute C_WDCH_TYPE : integer;
  attribute C_WDCH_TYPE of U0 : label is 0;
  attribute C_WRCH_TYPE : integer;
  attribute C_WRCH_TYPE of U0 : label is 0;
  attribute C_WR_ACK_LOW : integer;
  attribute C_WR_ACK_LOW of U0 : label is 0;
  attribute C_WR_DATA_COUNT_WIDTH : integer;
  attribute C_WR_DATA_COUNT_WIDTH of U0 : label is 6;
  attribute C_WR_DEPTH : integer;
  attribute C_WR_DEPTH of U0 : label is 64;
  attribute C_WR_DEPTH_AXIS : integer;
  attribute C_WR_DEPTH_AXIS of U0 : label is 1024;
  attribute C_WR_DEPTH_RACH : integer;
  attribute C_WR_DEPTH_RACH of U0 : label is 16;
  attribute C_WR_DEPTH_RDCH : integer;
  attribute C_WR_DEPTH_RDCH of U0 : label is 1024;
  attribute C_WR_DEPTH_WACH : integer;
  attribute C_WR_DEPTH_WACH of U0 : label is 16;
  attribute C_WR_DEPTH_WDCH : integer;
  attribute C_WR_DEPTH_WDCH of U0 : label is 1024;
  attribute C_WR_DEPTH_WRCH : integer;
  attribute C_WR_DEPTH_WRCH of U0 : label is 16;
  attribute C_WR_FREQ : integer;
  attribute C_WR_FREQ of U0 : label is 1;
  attribute C_WR_PNTR_WIDTH : integer;
  attribute C_WR_PNTR_WIDTH of U0 : label is 6;
  attribute C_WR_PNTR_WIDTH_AXIS : integer;
  attribute C_WR_PNTR_WIDTH_AXIS of U0 : label is 10;
  attribute C_WR_PNTR_WIDTH_RACH : integer;
  attribute C_WR_PNTR_WIDTH_RACH of U0 : label is 4;
  attribute C_WR_PNTR_WIDTH_RDCH : integer;
  attribute C_WR_PNTR_WIDTH_RDCH of U0 : label is 10;
  attribute C_WR_PNTR_WIDTH_WACH : integer;
  attribute C_WR_PNTR_WIDTH_WACH of U0 : label is 4;
  attribute C_WR_PNTR_WIDTH_WDCH : integer;
  attribute C_WR_PNTR_WIDTH_WDCH of U0 : label is 10;
  attribute C_WR_PNTR_WIDTH_WRCH : integer;
  attribute C_WR_PNTR_WIDTH_WRCH of U0 : label is 4;
  attribute C_WR_RESPONSE_LATENCY : integer;
  attribute C_WR_RESPONSE_LATENCY of U0 : label is 1;
  attribute x_interface_info : string;
  attribute x_interface_info of empty : signal is "xilinx.com:interface:fifo_read:1.0 FIFO_READ EMPTY";
  attribute x_interface_info of full : signal is "xilinx.com:interface:fifo_write:1.0 FIFO_WRITE FULL";
  attribute x_interface_info of rd_clk : signal is "xilinx.com:signal:clock:1.0 read_clk CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of rd_clk : signal is "XIL_INTERFACENAME read_clk, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  attribute x_interface_info of rd_en : signal is "xilinx.com:interface:fifo_read:1.0 FIFO_READ RD_EN";
  attribute x_interface_info of wr_clk : signal is "xilinx.com:signal:clock:1.0 write_clk CLK";
  attribute x_interface_parameter of wr_clk : signal is "XIL_INTERFACENAME write_clk, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  attribute x_interface_info of wr_en : signal is "xilinx.com:interface:fifo_write:1.0 FIFO_WRITE WR_EN";
  attribute x_interface_info of din : signal is "xilinx.com:interface:fifo_write:1.0 FIFO_WRITE WR_DATA";
  attribute x_interface_info of dout : signal is "xilinx.com:interface:fifo_read:1.0 FIFO_READ RD_DATA";
begin
U0: entity work.header_fifo_fifo_generator_v13_2_5
     port map (
      almost_empty => NLW_U0_almost_empty_UNCONNECTED,
      almost_full => NLW_U0_almost_full_UNCONNECTED,
      axi_ar_data_count(4 downto 0) => NLW_U0_axi_ar_data_count_UNCONNECTED(4 downto 0),
      axi_ar_dbiterr => NLW_U0_axi_ar_dbiterr_UNCONNECTED,
      axi_ar_injectdbiterr => '0',
      axi_ar_injectsbiterr => '0',
      axi_ar_overflow => NLW_U0_axi_ar_overflow_UNCONNECTED,
      axi_ar_prog_empty => NLW_U0_axi_ar_prog_empty_UNCONNECTED,
      axi_ar_prog_empty_thresh(3 downto 0) => B"0000",
      axi_ar_prog_full => NLW_U0_axi_ar_prog_full_UNCONNECTED,
      axi_ar_prog_full_thresh(3 downto 0) => B"0000",
      axi_ar_rd_data_count(4 downto 0) => NLW_U0_axi_ar_rd_data_count_UNCONNECTED(4 downto 0),
      axi_ar_sbiterr => NLW_U0_axi_ar_sbiterr_UNCONNECTED,
      axi_ar_underflow => NLW_U0_axi_ar_underflow_UNCONNECTED,
      axi_ar_wr_data_count(4 downto 0) => NLW_U0_axi_ar_wr_data_count_UNCONNECTED(4 downto 0),
      axi_aw_data_count(4 downto 0) => NLW_U0_axi_aw_data_count_UNCONNECTED(4 downto 0),
      axi_aw_dbiterr => NLW_U0_axi_aw_dbiterr_UNCONNECTED,
      axi_aw_injectdbiterr => '0',
      axi_aw_injectsbiterr => '0',
      axi_aw_overflow => NLW_U0_axi_aw_overflow_UNCONNECTED,
      axi_aw_prog_empty => NLW_U0_axi_aw_prog_empty_UNCONNECTED,
      axi_aw_prog_empty_thresh(3 downto 0) => B"0000",
      axi_aw_prog_full => NLW_U0_axi_aw_prog_full_UNCONNECTED,
      axi_aw_prog_full_thresh(3 downto 0) => B"0000",
      axi_aw_rd_data_count(4 downto 0) => NLW_U0_axi_aw_rd_data_count_UNCONNECTED(4 downto 0),
      axi_aw_sbiterr => NLW_U0_axi_aw_sbiterr_UNCONNECTED,
      axi_aw_underflow => NLW_U0_axi_aw_underflow_UNCONNECTED,
      axi_aw_wr_data_count(4 downto 0) => NLW_U0_axi_aw_wr_data_count_UNCONNECTED(4 downto 0),
      axi_b_data_count(4 downto 0) => NLW_U0_axi_b_data_count_UNCONNECTED(4 downto 0),
      axi_b_dbiterr => NLW_U0_axi_b_dbiterr_UNCONNECTED,
      axi_b_injectdbiterr => '0',
      axi_b_injectsbiterr => '0',
      axi_b_overflow => NLW_U0_axi_b_overflow_UNCONNECTED,
      axi_b_prog_empty => NLW_U0_axi_b_prog_empty_UNCONNECTED,
      axi_b_prog_empty_thresh(3 downto 0) => B"0000",
      axi_b_prog_full => NLW_U0_axi_b_prog_full_UNCONNECTED,
      axi_b_prog_full_thresh(3 downto 0) => B"0000",
      axi_b_rd_data_count(4 downto 0) => NLW_U0_axi_b_rd_data_count_UNCONNECTED(4 downto 0),
      axi_b_sbiterr => NLW_U0_axi_b_sbiterr_UNCONNECTED,
      axi_b_underflow => NLW_U0_axi_b_underflow_UNCONNECTED,
      axi_b_wr_data_count(4 downto 0) => NLW_U0_axi_b_wr_data_count_UNCONNECTED(4 downto 0),
      axi_r_data_count(10 downto 0) => NLW_U0_axi_r_data_count_UNCONNECTED(10 downto 0),
      axi_r_dbiterr => NLW_U0_axi_r_dbiterr_UNCONNECTED,
      axi_r_injectdbiterr => '0',
      axi_r_injectsbiterr => '0',
      axi_r_overflow => NLW_U0_axi_r_overflow_UNCONNECTED,
      axi_r_prog_empty => NLW_U0_axi_r_prog_empty_UNCONNECTED,
      axi_r_prog_empty_thresh(9 downto 0) => B"0000000000",
      axi_r_prog_full => NLW_U0_axi_r_prog_full_UNCONNECTED,
      axi_r_prog_full_thresh(9 downto 0) => B"0000000000",
      axi_r_rd_data_count(10 downto 0) => NLW_U0_axi_r_rd_data_count_UNCONNECTED(10 downto 0),
      axi_r_sbiterr => NLW_U0_axi_r_sbiterr_UNCONNECTED,
      axi_r_underflow => NLW_U0_axi_r_underflow_UNCONNECTED,
      axi_r_wr_data_count(10 downto 0) => NLW_U0_axi_r_wr_data_count_UNCONNECTED(10 downto 0),
      axi_w_data_count(10 downto 0) => NLW_U0_axi_w_data_count_UNCONNECTED(10 downto 0),
      axi_w_dbiterr => NLW_U0_axi_w_dbiterr_UNCONNECTED,
      axi_w_injectdbiterr => '0',
      axi_w_injectsbiterr => '0',
      axi_w_overflow => NLW_U0_axi_w_overflow_UNCONNECTED,
      axi_w_prog_empty => NLW_U0_axi_w_prog_empty_UNCONNECTED,
      axi_w_prog_empty_thresh(9 downto 0) => B"0000000000",
      axi_w_prog_full => NLW_U0_axi_w_prog_full_UNCONNECTED,
      axi_w_prog_full_thresh(9 downto 0) => B"0000000000",
      axi_w_rd_data_count(10 downto 0) => NLW_U0_axi_w_rd_data_count_UNCONNECTED(10 downto 0),
      axi_w_sbiterr => NLW_U0_axi_w_sbiterr_UNCONNECTED,
      axi_w_underflow => NLW_U0_axi_w_underflow_UNCONNECTED,
      axi_w_wr_data_count(10 downto 0) => NLW_U0_axi_w_wr_data_count_UNCONNECTED(10 downto 0),
      axis_data_count(10 downto 0) => NLW_U0_axis_data_count_UNCONNECTED(10 downto 0),
      axis_dbiterr => NLW_U0_axis_dbiterr_UNCONNECTED,
      axis_injectdbiterr => '0',
      axis_injectsbiterr => '0',
      axis_overflow => NLW_U0_axis_overflow_UNCONNECTED,
      axis_prog_empty => NLW_U0_axis_prog_empty_UNCONNECTED,
      axis_prog_empty_thresh(9 downto 0) => B"0000000000",
      axis_prog_full => NLW_U0_axis_prog_full_UNCONNECTED,
      axis_prog_full_thresh(9 downto 0) => B"0000000000",
      axis_rd_data_count(10 downto 0) => NLW_U0_axis_rd_data_count_UNCONNECTED(10 downto 0),
      axis_sbiterr => NLW_U0_axis_sbiterr_UNCONNECTED,
      axis_underflow => NLW_U0_axis_underflow_UNCONNECTED,
      axis_wr_data_count(10 downto 0) => NLW_U0_axis_wr_data_count_UNCONNECTED(10 downto 0),
      backup => '0',
      backup_marker => '0',
      clk => '0',
      data_count(5 downto 0) => NLW_U0_data_count_UNCONNECTED(5 downto 0),
      dbiterr => NLW_U0_dbiterr_UNCONNECTED,
      din(255 downto 0) => din(255 downto 0),
      dout(255 downto 0) => dout(255 downto 0),
      empty => empty,
      full => full,
      injectdbiterr => '0',
      injectsbiterr => '0',
      int_clk => '0',
      m_aclk => '0',
      m_aclk_en => '0',
      m_axi_araddr(31 downto 0) => NLW_U0_m_axi_araddr_UNCONNECTED(31 downto 0),
      m_axi_arburst(1 downto 0) => NLW_U0_m_axi_arburst_UNCONNECTED(1 downto 0),
      m_axi_arcache(3 downto 0) => NLW_U0_m_axi_arcache_UNCONNECTED(3 downto 0),
      m_axi_arid(0) => NLW_U0_m_axi_arid_UNCONNECTED(0),
      m_axi_arlen(7 downto 0) => NLW_U0_m_axi_arlen_UNCONNECTED(7 downto 0),
      m_axi_arlock(0) => NLW_U0_m_axi_arlock_UNCONNECTED(0),
      m_axi_arprot(2 downto 0) => NLW_U0_m_axi_arprot_UNCONNECTED(2 downto 0),
      m_axi_arqos(3 downto 0) => NLW_U0_m_axi_arqos_UNCONNECTED(3 downto 0),
      m_axi_arready => '0',
      m_axi_arregion(3 downto 0) => NLW_U0_m_axi_arregion_UNCONNECTED(3 downto 0),
      m_axi_arsize(2 downto 0) => NLW_U0_m_axi_arsize_UNCONNECTED(2 downto 0),
      m_axi_aruser(0) => NLW_U0_m_axi_aruser_UNCONNECTED(0),
      m_axi_arvalid => NLW_U0_m_axi_arvalid_UNCONNECTED,
      m_axi_awaddr(31 downto 0) => NLW_U0_m_axi_awaddr_UNCONNECTED(31 downto 0),
      m_axi_awburst(1 downto 0) => NLW_U0_m_axi_awburst_UNCONNECTED(1 downto 0),
      m_axi_awcache(3 downto 0) => NLW_U0_m_axi_awcache_UNCONNECTED(3 downto 0),
      m_axi_awid(0) => NLW_U0_m_axi_awid_UNCONNECTED(0),
      m_axi_awlen(7 downto 0) => NLW_U0_m_axi_awlen_UNCONNECTED(7 downto 0),
      m_axi_awlock(0) => NLW_U0_m_axi_awlock_UNCONNECTED(0),
      m_axi_awprot(2 downto 0) => NLW_U0_m_axi_awprot_UNCONNECTED(2 downto 0),
      m_axi_awqos(3 downto 0) => NLW_U0_m_axi_awqos_UNCONNECTED(3 downto 0),
      m_axi_awready => '0',
      m_axi_awregion(3 downto 0) => NLW_U0_m_axi_awregion_UNCONNECTED(3 downto 0),
      m_axi_awsize(2 downto 0) => NLW_U0_m_axi_awsize_UNCONNECTED(2 downto 0),
      m_axi_awuser(0) => NLW_U0_m_axi_awuser_UNCONNECTED(0),
      m_axi_awvalid => NLW_U0_m_axi_awvalid_UNCONNECTED,
      m_axi_bid(0) => '0',
      m_axi_bready => NLW_U0_m_axi_bready_UNCONNECTED,
      m_axi_bresp(1 downto 0) => B"00",
      m_axi_buser(0) => '0',
      m_axi_bvalid => '0',
      m_axi_rdata(63 downto 0) => B"0000000000000000000000000000000000000000000000000000000000000000",
      m_axi_rid(0) => '0',
      m_axi_rlast => '0',
      m_axi_rready => NLW_U0_m_axi_rready_UNCONNECTED,
      m_axi_rresp(1 downto 0) => B"00",
      m_axi_ruser(0) => '0',
      m_axi_rvalid => '0',
      m_axi_wdata(63 downto 0) => NLW_U0_m_axi_wdata_UNCONNECTED(63 downto 0),
      m_axi_wid(0) => NLW_U0_m_axi_wid_UNCONNECTED(0),
      m_axi_wlast => NLW_U0_m_axi_wlast_UNCONNECTED,
      m_axi_wready => '0',
      m_axi_wstrb(7 downto 0) => NLW_U0_m_axi_wstrb_UNCONNECTED(7 downto 0),
      m_axi_wuser(0) => NLW_U0_m_axi_wuser_UNCONNECTED(0),
      m_axi_wvalid => NLW_U0_m_axi_wvalid_UNCONNECTED,
      m_axis_tdata(7 downto 0) => NLW_U0_m_axis_tdata_UNCONNECTED(7 downto 0),
      m_axis_tdest(0) => NLW_U0_m_axis_tdest_UNCONNECTED(0),
      m_axis_tid(0) => NLW_U0_m_axis_tid_UNCONNECTED(0),
      m_axis_tkeep(0) => NLW_U0_m_axis_tkeep_UNCONNECTED(0),
      m_axis_tlast => NLW_U0_m_axis_tlast_UNCONNECTED,
      m_axis_tready => '0',
      m_axis_tstrb(0) => NLW_U0_m_axis_tstrb_UNCONNECTED(0),
      m_axis_tuser(3 downto 0) => NLW_U0_m_axis_tuser_UNCONNECTED(3 downto 0),
      m_axis_tvalid => NLW_U0_m_axis_tvalid_UNCONNECTED,
      overflow => NLW_U0_overflow_UNCONNECTED,
      prog_empty => NLW_U0_prog_empty_UNCONNECTED,
      prog_empty_thresh(5 downto 0) => B"000000",
      prog_empty_thresh_assert(5 downto 0) => B"000000",
      prog_empty_thresh_negate(5 downto 0) => B"000000",
      prog_full => NLW_U0_prog_full_UNCONNECTED,
      prog_full_thresh(5 downto 0) => B"000000",
      prog_full_thresh_assert(5 downto 0) => B"000000",
      prog_full_thresh_negate(5 downto 0) => B"000000",
      rd_clk => rd_clk,
      rd_data_count(5 downto 0) => rd_data_count(5 downto 0),
      rd_en => rd_en,
      rd_rst => '0',
      rd_rst_busy => NLW_U0_rd_rst_busy_UNCONNECTED,
      rst => rst,
      s_aclk => '0',
      s_aclk_en => '0',
      s_aresetn => '0',
      s_axi_araddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_arburst(1 downto 0) => B"00",
      s_axi_arcache(3 downto 0) => B"0000",
      s_axi_arid(0) => '0',
      s_axi_arlen(7 downto 0) => B"00000000",
      s_axi_arlock(0) => '0',
      s_axi_arprot(2 downto 0) => B"000",
      s_axi_arqos(3 downto 0) => B"0000",
      s_axi_arready => NLW_U0_s_axi_arready_UNCONNECTED,
      s_axi_arregion(3 downto 0) => B"0000",
      s_axi_arsize(2 downto 0) => B"000",
      s_axi_aruser(0) => '0',
      s_axi_arvalid => '0',
      s_axi_awaddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_awburst(1 downto 0) => B"00",
      s_axi_awcache(3 downto 0) => B"0000",
      s_axi_awid(0) => '0',
      s_axi_awlen(7 downto 0) => B"00000000",
      s_axi_awlock(0) => '0',
      s_axi_awprot(2 downto 0) => B"000",
      s_axi_awqos(3 downto 0) => B"0000",
      s_axi_awready => NLW_U0_s_axi_awready_UNCONNECTED,
      s_axi_awregion(3 downto 0) => B"0000",
      s_axi_awsize(2 downto 0) => B"000",
      s_axi_awuser(0) => '0',
      s_axi_awvalid => '0',
      s_axi_bid(0) => NLW_U0_s_axi_bid_UNCONNECTED(0),
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_U0_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_buser(0) => NLW_U0_s_axi_buser_UNCONNECTED(0),
      s_axi_bvalid => NLW_U0_s_axi_bvalid_UNCONNECTED,
      s_axi_rdata(63 downto 0) => NLW_U0_s_axi_rdata_UNCONNECTED(63 downto 0),
      s_axi_rid(0) => NLW_U0_s_axi_rid_UNCONNECTED(0),
      s_axi_rlast => NLW_U0_s_axi_rlast_UNCONNECTED,
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_U0_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_ruser(0) => NLW_U0_s_axi_ruser_UNCONNECTED(0),
      s_axi_rvalid => NLW_U0_s_axi_rvalid_UNCONNECTED,
      s_axi_wdata(63 downto 0) => B"0000000000000000000000000000000000000000000000000000000000000000",
      s_axi_wid(0) => '0',
      s_axi_wlast => '0',
      s_axi_wready => NLW_U0_s_axi_wready_UNCONNECTED,
      s_axi_wstrb(7 downto 0) => B"00000000",
      s_axi_wuser(0) => '0',
      s_axi_wvalid => '0',
      s_axis_tdata(7 downto 0) => B"00000000",
      s_axis_tdest(0) => '0',
      s_axis_tid(0) => '0',
      s_axis_tkeep(0) => '0',
      s_axis_tlast => '0',
      s_axis_tready => NLW_U0_s_axis_tready_UNCONNECTED,
      s_axis_tstrb(0) => '0',
      s_axis_tuser(3 downto 0) => B"0000",
      s_axis_tvalid => '0',
      sbiterr => NLW_U0_sbiterr_UNCONNECTED,
      sleep => '0',
      srst => '0',
      underflow => underflow,
      valid => valid,
      wr_ack => NLW_U0_wr_ack_UNCONNECTED,
      wr_clk => wr_clk,
      wr_data_count(5 downto 0) => wr_data_count(5 downto 0),
      wr_en => wr_en,
      wr_rst => '0',
      wr_rst_busy => NLW_U0_wr_rst_busy_UNCONNECTED
    );
end STRUCTURE;
