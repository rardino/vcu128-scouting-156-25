-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Tue Oct 19 12:50:00 2021
-- Host        : daqlab40-skylake16 running 64-bit CentOS Linux release 7.9.2009 (Core)
-- Command     : write_vhdl -force -mode synth_stub
--               /home/rardino/vcu128-scouting/vcu128-scouting.srcs/sources_1/ip/gtwizard_ultrascale_0_in_system_ibert_0/gtwizard_ultrascale_0_in_system_ibert_0_stub.vhdl
-- Design      : gtwizard_ultrascale_0_in_system_ibert_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcvu37p-fsvh2892-2L-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity gtwizard_ultrascale_0_in_system_ibert_0 is
  Port ( 
    drpclk_o : out STD_LOGIC_VECTOR ( 7 downto 0 );
    gt0_drpen_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_drpwe_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_drpaddr_o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    gt0_drpdi_o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gt0_drprdy_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt0_drpdo_i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gt1_drpen_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt1_drpwe_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt1_drpaddr_o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    gt1_drpdi_o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gt1_drprdy_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt1_drpdo_i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gt2_drpen_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt2_drpwe_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt2_drpaddr_o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    gt2_drpdi_o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gt2_drprdy_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt2_drpdo_i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gt3_drpen_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt3_drpwe_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt3_drpaddr_o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    gt3_drpdi_o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gt3_drprdy_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt3_drpdo_i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gt4_drpen_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt4_drpwe_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt4_drpaddr_o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    gt4_drpdi_o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gt4_drprdy_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt4_drpdo_i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gt5_drpen_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt5_drpwe_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt5_drpaddr_o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    gt5_drpdi_o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gt5_drprdy_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt5_drpdo_i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gt6_drpen_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt6_drpwe_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt6_drpaddr_o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    gt6_drpdi_o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gt6_drprdy_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt6_drpdo_i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    gt7_drpen_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt7_drpwe_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    gt7_drpaddr_o : out STD_LOGIC_VECTOR ( 9 downto 0 );
    gt7_drpdi_o : out STD_LOGIC_VECTOR ( 15 downto 0 );
    gt7_drprdy_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    gt7_drpdo_i : in STD_LOGIC_VECTOR ( 15 downto 0 );
    eyescanreset_o : out STD_LOGIC_VECTOR ( 7 downto 0 );
    rxrate_o : out STD_LOGIC_VECTOR ( 23 downto 0 );
    txdiffctrl_o : out STD_LOGIC_VECTOR ( 39 downto 0 );
    txprecursor_o : out STD_LOGIC_VECTOR ( 39 downto 0 );
    txpostcursor_o : out STD_LOGIC_VECTOR ( 39 downto 0 );
    rxlpmen_o : out STD_LOGIC_VECTOR ( 7 downto 0 );
    rxrate_i : in STD_LOGIC_VECTOR ( 23 downto 0 );
    txdiffctrl_i : in STD_LOGIC_VECTOR ( 39 downto 0 );
    txprecursor_i : in STD_LOGIC_VECTOR ( 39 downto 0 );
    txpostcursor_i : in STD_LOGIC_VECTOR ( 39 downto 0 );
    rxlpmen_i : in STD_LOGIC_VECTOR ( 7 downto 0 );
    drpclk_i : in STD_LOGIC_VECTOR ( 7 downto 0 );
    rxoutclk_i : in STD_LOGIC_VECTOR ( 7 downto 0 );
    clk : in STD_LOGIC
  );

end gtwizard_ultrascale_0_in_system_ibert_0;

architecture stub of gtwizard_ultrascale_0_in_system_ibert_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "drpclk_o[7:0],gt0_drpen_o[0:0],gt0_drpwe_o[0:0],gt0_drpaddr_o[9:0],gt0_drpdi_o[15:0],gt0_drprdy_i[0:0],gt0_drpdo_i[15:0],gt1_drpen_o[0:0],gt1_drpwe_o[0:0],gt1_drpaddr_o[9:0],gt1_drpdi_o[15:0],gt1_drprdy_i[0:0],gt1_drpdo_i[15:0],gt2_drpen_o[0:0],gt2_drpwe_o[0:0],gt2_drpaddr_o[9:0],gt2_drpdi_o[15:0],gt2_drprdy_i[0:0],gt2_drpdo_i[15:0],gt3_drpen_o[0:0],gt3_drpwe_o[0:0],gt3_drpaddr_o[9:0],gt3_drpdi_o[15:0],gt3_drprdy_i[0:0],gt3_drpdo_i[15:0],gt4_drpen_o[0:0],gt4_drpwe_o[0:0],gt4_drpaddr_o[9:0],gt4_drpdi_o[15:0],gt4_drprdy_i[0:0],gt4_drpdo_i[15:0],gt5_drpen_o[0:0],gt5_drpwe_o[0:0],gt5_drpaddr_o[9:0],gt5_drpdi_o[15:0],gt5_drprdy_i[0:0],gt5_drpdo_i[15:0],gt6_drpen_o[0:0],gt6_drpwe_o[0:0],gt6_drpaddr_o[9:0],gt6_drpdi_o[15:0],gt6_drprdy_i[0:0],gt6_drpdo_i[15:0],gt7_drpen_o[0:0],gt7_drpwe_o[0:0],gt7_drpaddr_o[9:0],gt7_drpdi_o[15:0],gt7_drprdy_i[0:0],gt7_drpdo_i[15:0],eyescanreset_o[7:0],rxrate_o[23:0],txdiffctrl_o[39:0],txprecursor_o[39:0],txpostcursor_o[39:0],rxlpmen_o[7:0],rxrate_i[23:0],txdiffctrl_i[39:0],txprecursor_i[39:0],txpostcursor_i[39:0],rxlpmen_i[7:0],drpclk_i[7:0],rxoutclk_i[7:0],clk";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "in_system_ibert,Vivado 2019.2";
begin
end;
