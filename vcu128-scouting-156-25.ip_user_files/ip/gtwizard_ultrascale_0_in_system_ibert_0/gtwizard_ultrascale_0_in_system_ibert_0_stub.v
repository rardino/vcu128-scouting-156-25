// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Tue Oct 19 12:50:00 2021
// Host        : daqlab40-skylake16 running 64-bit CentOS Linux release 7.9.2009 (Core)
// Command     : write_verilog -force -mode synth_stub
//               /home/rardino/vcu128-scouting/vcu128-scouting.srcs/sources_1/ip/gtwizard_ultrascale_0_in_system_ibert_0/gtwizard_ultrascale_0_in_system_ibert_0_stub.v
// Design      : gtwizard_ultrascale_0_in_system_ibert_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xcvu37p-fsvh2892-2L-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "in_system_ibert,Vivado 2019.2" *)
module gtwizard_ultrascale_0_in_system_ibert_0(drpclk_o, gt0_drpen_o, gt0_drpwe_o, 
  gt0_drpaddr_o, gt0_drpdi_o, gt0_drprdy_i, gt0_drpdo_i, gt1_drpen_o, gt1_drpwe_o, 
  gt1_drpaddr_o, gt1_drpdi_o, gt1_drprdy_i, gt1_drpdo_i, gt2_drpen_o, gt2_drpwe_o, 
  gt2_drpaddr_o, gt2_drpdi_o, gt2_drprdy_i, gt2_drpdo_i, gt3_drpen_o, gt3_drpwe_o, 
  gt3_drpaddr_o, gt3_drpdi_o, gt3_drprdy_i, gt3_drpdo_i, gt4_drpen_o, gt4_drpwe_o, 
  gt4_drpaddr_o, gt4_drpdi_o, gt4_drprdy_i, gt4_drpdo_i, gt5_drpen_o, gt5_drpwe_o, 
  gt5_drpaddr_o, gt5_drpdi_o, gt5_drprdy_i, gt5_drpdo_i, gt6_drpen_o, gt6_drpwe_o, 
  gt6_drpaddr_o, gt6_drpdi_o, gt6_drprdy_i, gt6_drpdo_i, gt7_drpen_o, gt7_drpwe_o, 
  gt7_drpaddr_o, gt7_drpdi_o, gt7_drprdy_i, gt7_drpdo_i, eyescanreset_o, rxrate_o, 
  txdiffctrl_o, txprecursor_o, txpostcursor_o, rxlpmen_o, rxrate_i, txdiffctrl_i, 
  txprecursor_i, txpostcursor_i, rxlpmen_i, drpclk_i, rxoutclk_i, clk)
/* synthesis syn_black_box black_box_pad_pin="drpclk_o[7:0],gt0_drpen_o[0:0],gt0_drpwe_o[0:0],gt0_drpaddr_o[9:0],gt0_drpdi_o[15:0],gt0_drprdy_i[0:0],gt0_drpdo_i[15:0],gt1_drpen_o[0:0],gt1_drpwe_o[0:0],gt1_drpaddr_o[9:0],gt1_drpdi_o[15:0],gt1_drprdy_i[0:0],gt1_drpdo_i[15:0],gt2_drpen_o[0:0],gt2_drpwe_o[0:0],gt2_drpaddr_o[9:0],gt2_drpdi_o[15:0],gt2_drprdy_i[0:0],gt2_drpdo_i[15:0],gt3_drpen_o[0:0],gt3_drpwe_o[0:0],gt3_drpaddr_o[9:0],gt3_drpdi_o[15:0],gt3_drprdy_i[0:0],gt3_drpdo_i[15:0],gt4_drpen_o[0:0],gt4_drpwe_o[0:0],gt4_drpaddr_o[9:0],gt4_drpdi_o[15:0],gt4_drprdy_i[0:0],gt4_drpdo_i[15:0],gt5_drpen_o[0:0],gt5_drpwe_o[0:0],gt5_drpaddr_o[9:0],gt5_drpdi_o[15:0],gt5_drprdy_i[0:0],gt5_drpdo_i[15:0],gt6_drpen_o[0:0],gt6_drpwe_o[0:0],gt6_drpaddr_o[9:0],gt6_drpdi_o[15:0],gt6_drprdy_i[0:0],gt6_drpdo_i[15:0],gt7_drpen_o[0:0],gt7_drpwe_o[0:0],gt7_drpaddr_o[9:0],gt7_drpdi_o[15:0],gt7_drprdy_i[0:0],gt7_drpdo_i[15:0],eyescanreset_o[7:0],rxrate_o[23:0],txdiffctrl_o[39:0],txprecursor_o[39:0],txpostcursor_o[39:0],rxlpmen_o[7:0],rxrate_i[23:0],txdiffctrl_i[39:0],txprecursor_i[39:0],txpostcursor_i[39:0],rxlpmen_i[7:0],drpclk_i[7:0],rxoutclk_i[7:0],clk" */;
  output [7:0]drpclk_o;
  output [0:0]gt0_drpen_o;
  output [0:0]gt0_drpwe_o;
  output [9:0]gt0_drpaddr_o;
  output [15:0]gt0_drpdi_o;
  input [0:0]gt0_drprdy_i;
  input [15:0]gt0_drpdo_i;
  output [0:0]gt1_drpen_o;
  output [0:0]gt1_drpwe_o;
  output [9:0]gt1_drpaddr_o;
  output [15:0]gt1_drpdi_o;
  input [0:0]gt1_drprdy_i;
  input [15:0]gt1_drpdo_i;
  output [0:0]gt2_drpen_o;
  output [0:0]gt2_drpwe_o;
  output [9:0]gt2_drpaddr_o;
  output [15:0]gt2_drpdi_o;
  input [0:0]gt2_drprdy_i;
  input [15:0]gt2_drpdo_i;
  output [0:0]gt3_drpen_o;
  output [0:0]gt3_drpwe_o;
  output [9:0]gt3_drpaddr_o;
  output [15:0]gt3_drpdi_o;
  input [0:0]gt3_drprdy_i;
  input [15:0]gt3_drpdo_i;
  output [0:0]gt4_drpen_o;
  output [0:0]gt4_drpwe_o;
  output [9:0]gt4_drpaddr_o;
  output [15:0]gt4_drpdi_o;
  input [0:0]gt4_drprdy_i;
  input [15:0]gt4_drpdo_i;
  output [0:0]gt5_drpen_o;
  output [0:0]gt5_drpwe_o;
  output [9:0]gt5_drpaddr_o;
  output [15:0]gt5_drpdi_o;
  input [0:0]gt5_drprdy_i;
  input [15:0]gt5_drpdo_i;
  output [0:0]gt6_drpen_o;
  output [0:0]gt6_drpwe_o;
  output [9:0]gt6_drpaddr_o;
  output [15:0]gt6_drpdi_o;
  input [0:0]gt6_drprdy_i;
  input [15:0]gt6_drpdo_i;
  output [0:0]gt7_drpen_o;
  output [0:0]gt7_drpwe_o;
  output [9:0]gt7_drpaddr_o;
  output [15:0]gt7_drpdi_o;
  input [0:0]gt7_drprdy_i;
  input [15:0]gt7_drpdo_i;
  output [7:0]eyescanreset_o;
  output [23:0]rxrate_o;
  output [39:0]txdiffctrl_o;
  output [39:0]txprecursor_o;
  output [39:0]txpostcursor_o;
  output [7:0]rxlpmen_o;
  input [23:0]rxrate_i;
  input [39:0]txdiffctrl_i;
  input [39:0]txprecursor_i;
  input [39:0]txpostcursor_i;
  input [7:0]rxlpmen_i;
  input [7:0]drpclk_i;
  input [7:0]rxoutclk_i;
  input clk;
endmodule
