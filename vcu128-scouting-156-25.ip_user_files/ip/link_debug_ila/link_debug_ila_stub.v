// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Wed Nov 17 12:50:38 2021
// Host        : daqlab40-skylake16 running 64-bit CentOS Linux release 7.9.2009 (Core)
// Command     : write_verilog -force -mode synth_stub
//               /home/rardino/vcu128-scouting/vcu128-scouting.srcs/sources_1/ip/link_debug_ila/link_debug_ila_stub.v
// Design      : link_debug_ila
// Purpose     : Stub declaration of top-level module interface
// Device      : xcvu37p-fsvh2892-2L-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "ila,Vivado 2019.2" *)
module link_debug_ila(clk, trig_in, trig_in_ack, probe0, probe1, probe2, 
  probe3, probe4, probe5, probe6, probe7, probe8, probe9, probe10, probe11, probe12, probe13, probe14, 
  probe15, probe16, probe17, probe18, probe19, probe20, probe21, probe22)
/* synthesis syn_black_box black_box_pad_pin="clk,trig_in,trig_in_ack,probe0[31:0],probe1[31:0],probe2[31:0],probe3[31:0],probe4[31:0],probe5[31:0],probe6[31:0],probe7[31:0],probe8[7:0],probe9[7:0],probe10[7:0],probe11[7:0],probe12[0:0],probe13[0:0],probe14[7:0],probe15[7:0],probe16[7:0],probe17[7:0],probe18[7:0],probe19[7:0],probe20[31:0],probe21[7:0],probe22[7:0]" */;
  input clk;
  input trig_in;
  output trig_in_ack;
  input [31:0]probe0;
  input [31:0]probe1;
  input [31:0]probe2;
  input [31:0]probe3;
  input [31:0]probe4;
  input [31:0]probe5;
  input [31:0]probe6;
  input [31:0]probe7;
  input [7:0]probe8;
  input [7:0]probe9;
  input [7:0]probe10;
  input [7:0]probe11;
  input [0:0]probe12;
  input [0:0]probe13;
  input [7:0]probe14;
  input [7:0]probe15;
  input [7:0]probe16;
  input [7:0]probe17;
  input [7:0]probe18;
  input [7:0]probe19;
  input [31:0]probe20;
  input [7:0]probe21;
  input [7:0]probe22;
endmodule
