// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Sat Dec  4 11:57:20 2021
// Host        : daqlab40-skylake16 running 64-bit CentOS Linux release 7.9.2009 (Core)
// Command     : write_verilog -force -mode synth_stub
//               /home/rardino/vcu128-scouting-156-25/vcu128-scouting-156-25.runs/xdma_debug_ila_synth_1/xdma_debug_ila_stub.v
// Design      : xdma_debug_ila
// Purpose     : Stub declaration of top-level module interface
// Device      : xcvu37p-fsvh2892-2L-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "ila,Vivado 2019.2" *)
module xdma_debug_ila(clk, trig_in, trig_in_ack, probe0, probe1, probe2, 
  probe3, probe4, probe5, probe6, probe7, probe8, probe9, probe10, probe11, probe12)
/* synthesis syn_black_box black_box_pad_pin="clk,trig_in,trig_in_ack,probe0[31:0],probe1[31:0],probe2[31:0],probe3[31:0],probe4[3:0],probe5[31:0],probe6[0:0],probe7[255:0],probe8[0:0],probe9[0:0],probe10[0:0],probe11[255:0],probe12[0:0]" */;
  input clk;
  input trig_in;
  output trig_in_ack;
  input [31:0]probe0;
  input [31:0]probe1;
  input [31:0]probe2;
  input [31:0]probe3;
  input [3:0]probe4;
  input [31:0]probe5;
  input [0:0]probe6;
  input [255:0]probe7;
  input [0:0]probe8;
  input [0:0]probe9;
  input [0:0]probe10;
  input [255:0]probe11;
  input [0:0]probe12;
endmodule
