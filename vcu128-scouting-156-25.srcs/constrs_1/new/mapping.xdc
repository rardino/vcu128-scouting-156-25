# PCIE CLOCK
#set_property PACKAGE_PIN AL14 [get_ports pci_exp_clk_n]
#set_property PACKAGE_PIN AL15 [get_ports pci_exp_clk_p]

#create_clock -period 10.000 -name pci_exp_clk_p [get_ports pci_exp_clk_p]



# PCIE RESET
#set_property PACKAGE_PIN BF41 [get_ports pci_exp_rst_n]

#set_property IOSTANDARD LVCMOS12 [get_ports pci_exp_rst_n]



# PCIE TX/RX
set_property PACKAGE_PIN AL11 [get_ports {pci_exp_txp[0]}]
set_property PACKAGE_PIN AM9  [get_ports {pci_exp_txp[1]}]
set_property PACKAGE_PIN AN11 [get_ports {pci_exp_txp[2]}]
set_property PACKAGE_PIN AP9  [get_ports {pci_exp_txp[3]}]
set_property PACKAGE_PIN AR11 [get_ports {pci_exp_txp[4]}]
set_property PACKAGE_PIN AR7  [get_ports {pci_exp_txp[5]}]
set_property PACKAGE_PIN AT9  [get_ports {pci_exp_txp[6]}]
set_property PACKAGE_PIN AU11 [get_ports {pci_exp_txp[7]}]

set_property PACKAGE_PIN AL10 [get_ports {pci_exp_txn[0]}]
set_property PACKAGE_PIN AM8  [get_ports {pci_exp_txn[1]}]
set_property PACKAGE_PIN AN10 [get_ports {pci_exp_txn[2]}]
set_property PACKAGE_PIN AP8  [get_ports {pci_exp_txn[3]}]
set_property PACKAGE_PIN AR10 [get_ports {pci_exp_txn[4]}]
set_property PACKAGE_PIN AR6  [get_ports {pci_exp_txn[5]}]
set_property PACKAGE_PIN AT8  [get_ports {pci_exp_txn[6]}]
set_property PACKAGE_PIN AU10 [get_ports {pci_exp_txn[7]}]

set_property PACKAGE_PIN AL2  [get_ports {pci_exp_rxp[0]}]
set_property PACKAGE_PIN AM4  [get_ports {pci_exp_rxp[1]}]
set_property PACKAGE_PIN AN6  [get_ports {pci_exp_rxp[2]}]
set_property PACKAGE_PIN AN2  [get_ports {pci_exp_rxp[3]}]
set_property PACKAGE_PIN AP4  [get_ports {pci_exp_rxp[4]}]
set_property PACKAGE_PIN AR2  [get_ports {pci_exp_rxp[5]}]
set_property PACKAGE_PIN AT4  [get_ports {pci_exp_rxp[6]}]
set_property PACKAGE_PIN AU2  [get_ports {pci_exp_rxp[7]}]

set_property PACKAGE_PIN AL1  [get_ports {pci_exp_rxn[0]}]
set_property PACKAGE_PIN AM3  [get_ports {pci_exp_rxn[1]}]
set_property PACKAGE_PIN AN5  [get_ports {pci_exp_rxn[2]}]
set_property PACKAGE_PIN AN1  [get_ports {pci_exp_rxn[3]}]
set_property PACKAGE_PIN AP3  [get_ports {pci_exp_rxn[4]}]
set_property PACKAGE_PIN AR1  [get_ports {pci_exp_rxn[5]}]
set_property PACKAGE_PIN AT3  [get_ports {pci_exp_rxn[6]}]
set_property PACKAGE_PIN AU1  [get_ports {pci_exp_rxn[7]}]



# DDR4 CLK
set_property PACKAGE_PIN BH51 [get_ports clk_ddr4_p] 
set_property PACKAGE_PIN BJ51 [get_ports clk_ddr4_n]
set_property IOSTANDARD LVDS  [get_ports clk_ddr4_p]
set_property IOSTANDARD LVDS  [get_ports clk_ddr4_n]



# LEDS
set_property PACKAGE_PIN BH24 [get_ports {led_enable[0]}]
set_property PACKAGE_PIN BG24 [get_ports {led_enable[1]}]
set_property PACKAGE_PIN BG25 [get_ports {led_enable[2]}]
set_property PACKAGE_PIN BF25 [get_ports {led_enable[3]}]
set_property PACKAGE_PIN BF26 [get_ports {led_enable[4]}]
set_property PACKAGE_PIN BF27 [get_ports {led_enable[5]}]
set_property PACKAGE_PIN BG27 [get_ports {led_enable[6]}]
set_property PACKAGE_PIN BG28 [get_ports {led_enable[7]}]

set_property IOSTANDARD LVCMOS18 [get_ports {led_enable[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led_enable[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led_enable[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led_enable[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led_enable[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led_enable[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led_enable[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led_enable[7]}]



# I2C
set_property PACKAGE_PIN BM27 [get_ports scl]
set_property IOSTANDARD LVCMOS18 [get_ports scl]
set_property PACKAGE_PIN BL28 [get_ports sda]
set_property IOSTANDARD LVCMOS18 [get_ports sda]


# FALSE PATHS
set_false_path -from [get_clocks -of_objects [get_pins dma_engine_i/xdma_axi/inst/pcie4c_ip_i/inst/gt_top_i/diablo_gt.diablo_gt_phy_wrapper/phy_clk_i/bufg_gt_userclk/O]] -to [get_clocks -of_objects [get_pins {inputs/gty_wrapper/example_wrapper_inst/gtwizard_ultrascale_0_inst/inst/gen_gtwizard_gtye4_top.gtwizard_ultrascale_0_gtwizard_gtye4_inst/gen_gtwizard_gtye4.gen_channel_container[11].gen_enabled_channel.gtye4_channel_wrapper_inst/channel_inst/gtye4_channel_gen.gen_gtye4_channel_inst[3].GTYE4_CHANNEL_PRIM_INST/RXOUTCLK}]]
set_false_path -from [get_clocks -of_objects [get_pins {inputs/gty_wrapper/example_wrapper_inst/gtwizard_ultrascale_0_inst/inst/gen_gtwizard_gtye4_top.gtwizard_ultrascale_0_gtwizard_gtye4_inst/gen_gtwizard_gtye4.gen_channel_container[11].gen_enabled_channel.gtye4_channel_wrapper_inst/channel_inst/gtye4_channel_gen.gen_gtye4_channel_inst[3].GTYE4_CHANNEL_PRIM_INST/RXOUTCLK}]] -to [get_clocks -of_objects [get_pins dma_engine_i/xdma_axi/inst/pcie4c_ip_i/inst/gt_top_i/diablo_gt.diablo_gt_phy_wrapper/phy_clk_i/bufg_gt_userclk/O]]

set_false_path -from [get_clocks clk_freerun] -to [get_clocks -of_objects [get_pins dma_engine_i/xdma_axi/inst/pcie4c_ip_i/inst/gt_top_i/diablo_gt.diablo_gt_phy_wrapper/phy_clk_i/bufg_gt_userclk/O]]
set_false_path -from [get_clocks -of_objects [get_pins dma_engine_i/xdma_axi/inst/pcie4c_ip_i/inst/gt_top_i/diablo_gt.diablo_gt_phy_wrapper/phy_clk_i/bufg_gt_userclk/O]] -to [get_clocks clk_freerun]

set_false_path -from [get_clocks clk_freerun] -to [get_clocks -of_objects [get_pins {inputs/gty_wrapper/example_wrapper_inst/gtwizard_ultrascale_0_inst/inst/gen_gtwizard_gtye4_top.gtwizard_ultrascale_0_gtwizard_gtye4_inst/gen_gtwizard_gtye4.gen_channel_container[11].gen_enabled_channel.gtye4_channel_wrapper_inst/channel_inst/gtye4_channel_gen.gen_gtye4_channel_inst[3].GTYE4_CHANNEL_PRIM_INST/RXOUTCLK}]]
set_false_path -from [get_clocks -of_objects [get_pins {inputs/gty_wrapper/example_wrapper_inst/gtwizard_ultrascale_0_inst/inst/gen_gtwizard_gtye4_top.gtwizard_ultrascale_0_gtwizard_gtye4_inst/gen_gtwizard_gtye4.gen_channel_container[11].gen_enabled_channel.gtye4_channel_wrapper_inst/channel_inst/gtye4_channel_gen.gen_gtye4_channel_inst[3].GTYE4_CHANNEL_PRIM_INST/RXOUTCLK}]] -to [get_clocks clk_freerun]

set_false_path -from [get_clocks -of_objects [get_pins dma_engine_i/xdma_axi/inst/pcie4c_ip_i/inst/gt_top_i/diablo_gt.diablo_gt_phy_wrapper/phy_clk_i/bufg_gt_userclk/O]] -to [get_clocks clk_mgtrefclk0_x0y11_p]
set_false_path -from [get_clocks clk_mgtrefclk0_x0y11_p] -to [get_clocks -of_objects [get_pins dma_engine_i/xdma_axi/inst/pcie4c_ip_i/inst/gt_top_i/diablo_gt.diablo_gt_phy_wrapper/phy_clk_i/bufg_gt_userclk/O]]

set_false_path -from [get_clocks -of_objects [get_pins dma_engine_i/xdma_axi/inst/pcie4c_ip_i/inst/gt_top_i/diablo_gt.diablo_gt_phy_wrapper/phy_clk_i/bufg_gt_userclk/O]] -to [get_clocks clk_mgtrefclk0_x0y10_p]
set_false_path -from [get_clocks clk_mgtrefclk0_x0y10_p] -to [get_clocks -of_objects [get_pins dma_engine_i/xdma_axi/inst/pcie4c_ip_i/inst/gt_top_i/diablo_gt.diablo_gt_phy_wrapper/phy_clk_i/bufg_gt_userclk/O]]

# SYNCHRONIZER
set_false_path -from [get_pins auto_realign_controller_1/rst_aligner_int_reg/C] -to [get_pins auto_realign_controller_1/sync_aligner_rst_to_clk/fd1/D]
set_false_path -from [get_pins align/reset_read_sm_reg/C] -to [get_pins align/sync_reset_read_sm_to_clk_axi/fd1/D]

set_false_path -from [get_pins -hierarchical -filter {NAME =~ xpm_cdc_array_single_*/syncstages_*}]

set_false_path -from [get_pins freq_meas_clk_axi/store_freq_reg/C] -to [get_pins {freq_meas_clk_axi/store_freq_d_reg[0]/D}]
set_false_path -from [get_pins freq_meas_clk_x0y11/store_freq_reg/C] -to [get_pins {freq_meas_clk_x0y11/store_freq_d_reg[0]/D}]
set_false_path -from [get_pins freq_meas_clk_link/store_freq_reg/C] -to [get_pins {freq_meas_clk_link/store_freq_d_reg[0]/D}]

set_false_path -from [get_pins {central_vio/inst/PROBE_OUT_ALL_INST/G_PROBE_OUT[0].PROBE_OUT0_INST/Probe_out_reg[0]/C}] -to [get_pins global_reset/sync_rst_to_clk_algo/fd1/D]

set_false_path -from [get_pins auto_realign_controller_1/sync_aligner_rst_to_clk/fd2/C] -to [get_pins {central_vio/inst/PROBE_IN_INST/probe_in_reg_reg[9]/D}]

set_false_path -from [get_pins {freq_meas_clk_link/freq_store_reg[*]/C}] -to [get_pins {central_vio/inst/PROBE_IN_INST/probe_in_reg_reg[*]/D}]
set_false_path -from [get_pins {freq_meas_clk_axi/freq_store_reg[*]/C}] -to [get_pins {central_vio/inst/PROBE_IN_INST/probe_in_reg_reg[*]/D}]
set_false_path -from [get_pins {freq_meas_clk_x0y11/freq_store_reg[*]/C}] -to [get_pins {central_vio/inst/PROBE_IN_INST/probe_in_reg_reg[*]/D}]

set_false_path -from [get_pins global_reset/rst_algo_reg/C] -to [get_pins {central_vio/inst/PROBE_IN_INST/probe_in_reg_reg[13]/D}]

set_false_path -from [get_pins auto_realign_controller_1/sync_aligner_rst_to_clk/fd2/C] -to [get_pins {xdma_debug_ila_0/U0/PROBE_PIPE.shift_probes_reg*/D}]

set_false_path -from [get_pins {central_vio/inst/PROBE_OUT_ALL_INST/G_PROBE_OUT[0].PROBE_OUT0_INST/Probe_out_reg[0]/C}] -to [get_pins global_reset/sync_rst_to_clk_free/fd1/D]
# set_false_path -from [get_pins freq_meas_clk_axi/store_freq_reg/C] -to [get_pins {freq_meas_clk_axi/store_freq_d_reg[0]/D}]
# set_false_path -from [get_pins -hierarchical -filter {NAME =~ freq_meas_clk_axi/store_freq_reg/C}] -to [get_pins -hierarchical -filter {NAME =~ {freq_meas_clk_axi/store_freq_d_reg[0]/D}]
# set_false_path -from [get_pins -hierarchical -filter {NAME =~ freq_meas_*/C}] -to [get_pins -hierarchical -filter {NAME =~ central_vio/inst/PROBE_IN_INST/probe_in*/D}]
# set_false_path -from [get_pins -hierarchical -filter {NAME =~ global_reset/rst_algo_reg/C}] -to [get_pins -hierarchical -filter {NAME =~ central_vio/inst/PROBE_IN_INST/probe_in*/D}]
# set_false_path -from [get_pins -hierarchical -filter {NAME =~ central_vio/inst/PROBE_OUT_ALL_INST/G_PROBE_OUT[0].PROBE_OUT0_INST/Probe_out*/C}] -to [get_pins -hierarchical -filter {NAME =~ global_reset/sync_rst_to_clk_algo/*}]
# set_false_path -from [get_pins -hierarchical -filter {NAME =~ auto_realign_controller_1/sync_aligner_rst_to_clk/*}] -to [get_pins -hierarchical -filter {NAME =~ central_vio/inst/PROBE_IN_INST/probe_in*/D}]

# AXI REGISTER INTERFACE FALSE PATHS
# set_false_path -from [get_pins {inputs/gty_wrapper/gtwizard_ultrascale_0_vio_0_inst/inst/PROBE_OUT_ALL_INST/G_PROBE_OUT[1].PROBE_OUT0_INST/Probe_out_reg[0]/C}] -to [get_pins {axi_register_interface/axi_register_interface/axi_rdata_reg[*]/D}]
set_false_path -from [get_pins {inputs/gty_wrapper/gtwizard_ultrascale_0_vio_0_inst/inst/PROBE_OUT_ALL_INST/G_PROBE_OUT[*].PROBE_OUT0_INST/Probe_out_reg[0]/C}] -to [get_pins {axi_register_interface/axi_register_interface/axi_rdata_reg[*]/D}]
set_false_path -from [get_pins inputs/gty_wrapper/example_wrapper_inst/gtwizard_ultrascale_0_inst/inst/gen_gtwizard_gtye4_top.gtwizard_ultrascale_0_gtwizard_gtye4_inst/gen_gtwizard_gtye4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_inst/bit_synchronizer_rxcdrlock_inst/i_in_out_reg/C] -to [get_pins {axi_register_interface/axi_register_interface/axi_rdata_reg[*]/D}]
set_false_path -from [get_pins {inputs/gty_wrapper/example_wrapper_inst/gtwizard_ultrascale_0_inst/inst/gen_gtwizard_gtye4_top.gtwizard_ultrascale_0_gtwizard_gtye4_inst/gen_gtwizard_gtye4.gen_channel_container[*].gen_enabled_channel.gtye4_channel_wrapper_inst/channel_inst/gtye4_channel_gen.gen_gtye4_channel_inst[*].GTYE4_CHANNEL_PRIM_INST/RXUSRCLK2}] -to [get_pins {axi_register_interface/axi_register_interface/axi_rdata_reg[*]/D}]
set_false_path -from [get_pins {freq_meas_clk_link/freq_store_reg[*]/C}] -to [get_pins {axi_register_interface/axi_register_interface/axi_rdata_reg[*]/D}]
set_false_path -from [get_pins inputs/gty_wrapper/example_wrapper_inst/gtwizard_ultrascale_0_inst/inst/gen_gtwizard_gtye4_top.gtwizard_ultrascale_0_gtwizard_gtye4_inst/gen_gtwizard_gtye4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_inst/reset_synchronizer_tx_done_inst/rst_in_out_reg/C] -to [get_pins {axi_register_interface/axi_register_interface/axi_rdata_reg[*]/D}]
set_false_path -from [get_pins inputs/gty_wrapper/example_wrapper_inst/gtwizard_ultrascale_0_inst/inst/gen_gtwizard_gtye4_top.gtwizard_ultrascale_0_gtwizard_gtye4_inst/gen_gtwizard_gtye4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_inst/reset_synchronizer_rx_done_inst/rst_in_out_reg/C] -to [get_pins {axi_register_interface/axi_register_interface/axi_rdata_reg[*]/D}]
set_false_path -from [get_pins {fifo_filler/s_orbit_counter_reg[*]/C}] -to [get_pins {axi_register_interface/axi_register_interface/axi_rdata_reg[*]/D}]
set_false_path -from [get_pins align/waiting_orbit_end_reg/C] -to [get_pins {axi_register_interface/axi_register_interface/axi_rdata_reg[*]/D}]
