----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/11/2022 10:33:39 AM
-- Design Name: 
-- Module Name: top_vcu128_scout_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.VComponents.all;

use work.vcu128_top_decl.all;
-- use work.constants.all;
use work.datatypes.all;

entity top_vcu128_scout_sim is
--  Port ( );
end top_vcu128_scout_sim;

architecture Behavioral of top_vcu128_scout_sim is

    ----------------------------------------------------------------------------
    --
    -- Components
    --
    ----------------------------------------------------------------------------
    ---- scouting preprocessor
    component vcu128_scout_sim
        generic (
            GEN_ORBIT_FULL_LENGTH : natural := ORBIT_LENGTH;
            GEN_ORBIT_DATA_LENGTH : natural := 162;
            N_STREAMS             : natural := 8
        );
        port (
            clk_link            : in  std_logic;
            clk_axi             : in  std_logic;
            rst_global          : in  std_logic;
            rst_packager        : in  std_logic;
            do_zs               : in  std_logic;
            orbits_per_packet   : in  unsigned(15 downto 0);
            q_inputs_o          : out ldata(N_STREAMS-1 downto 0);
            q_cgc_o             : out ldata(N_STREAMS-1 downto 0);
            q_align_o           : out adata(N_STREAMS-1 downto 0);
            q_zs_o              : out adata(N_STREAMS-1 downto 0);
            q_align_ctrl_o      : out acontrol;
            q_zs_ctrl_o         : out acontrol;
            s_axis_c2h_tready_0 : in  std_logic;
            s_axis_c2h_tvalid_0 : out std_logic;
            s_axis_c2h_tdata_0  : out std_logic_vector(32*N_STREAMS-1 downto 0);
            s_axis_c2h_tkeep_0  : out std_logic_vector(31 downto 0);
            s_axis_c2h_tlast_0  : out std_logic
    --        q                 : out adata(N_STREAMS-1 downto 0);
    --        q_ctrl            : out acontrol
        );
    end component;
    ----------------------------------------------------------------------------
    
    
    
    
    
    ----------------------------------------------------------------------------
    --
    -- Signal declaration
    --
    ----------------------------------------------------------------------------
    ---- clock period constants
    constant t_clk_link        : time := 4 ns;
    constant t_clk_axi         : time := 4 ns;
    constant t_half_clk_link   : time := t_clk_link / 2;
    constant t_half_clk_axi    : time := t_clk_axi  / 2;
  
    ---- other constants
    constant BUFFER_SIZE           : integer := 64;
    constant QUADS                 : integer := 2;
    constant N_STREAMS             : integer := 4*QUADS;
    constant GEN_ORBIT_DATA_LENGTH : integer := 162;
    constant GEN_ORBIT_FULL_LENGTH : integer := 500;
    
    ---- clock signals
    signal clk_link : std_logic := '1';
    signal clk_axi  : std_logic := '1';
    
    ---- reset signals
    signal rst_global   : std_logic := '1';
    signal rst_packager : std_logic := '0';
    
    ---- axis signals
    signal s_axis_c2h_tready_0 : std_logic := '0';
    signal s_axis_c2h_tvalid_0 : std_logic;
    signal s_axis_c2h_tdata_0  : std_logic_vector(255 downto 0);
    signal s_axis_c2h_tkeep_0  : std_logic_vector(031 downto 0);
    signal s_axis_c2h_tlast_0  : std_logic;
    
    ---- data and control signals
    signal q_inputs_o     : ldata(N_STREAMS-1 downto 0);
    signal q_cgc_o        : ldata(N_STREAMS-1 downto 0);
    signal q_align_o      : adata(N_STREAMS-1 downto 0);
    signal q_zs_o         : adata(N_STREAMS-1 downto 0);
    signal q_align_ctrl_o : acontrol;
    signal q_zs_ctrl_o    : acontrol;
    ----------------------------------------------------------------------------

begin

    ----------------------------------------------------------------------------
    --
    -- Simulate clocks, resets and ready
    --
    ----------------------------------------------------------------------------
    ---- clocks
    clk_link  <= not clk_link after t_half_clk_link;
    clk_axi   <= not clk_axi  after t_half_clk_axi;
    
    ---- resets
    rst_global <= '0' after 50 us;
    
    ---- ready
    ready : process
    begin
        wait for 1000 us;
        s_axis_c2h_tready_0 <= '1';
        
        wait for 10 us;
        s_axis_c2h_tready_0 <= '0';
        for i in 0 to 10000 loop
            wait for 100 ns;
            s_axis_c2h_tready_0 <= '1';
            wait for 1000 ns;
            s_axis_c2h_tready_0 <= '0';
        end loop;
        wait for 100 ns;
        s_axis_c2h_tready_0 <= '1';
    end process;
    ----------------------------------------------------------------------------




    
    ----------------------------------------------------------------------------
    --
    -- Instantiate scout stream and preprocessing unit
    --
    ----------------------------------------------------------------------------
    top_scout : vcu128_scout_sim
        generic map (
            GEN_ORBIT_FULL_LENGTH => GEN_ORBIT_FULL_LENGTH,
            GEN_ORBIT_DATA_LENGTH => GEN_ORBIT_DATA_LENGTH,
            N_STREAMS             => N_STREAMS
        )
        port map (
            clk_link            => clk_link,
            clk_axi             => clk_axi,
            rst_global          => rst_global,
            rst_packager        => rst_packager,
            do_zs               => '1',
            orbits_per_packet   => x"0004",
            q_inputs_o          => q_inputs_o,
            q_cgc_o             => q_cgc_o,
            q_align_o           => q_align_o,
            q_zs_o              => q_zs_o,
            q_align_ctrl_o      => q_align_ctrl_o,
            q_zs_ctrl_o         => q_zs_ctrl_o,
            s_axis_c2h_tready_0 => s_axis_c2h_tready_0,
            s_axis_c2h_tvalid_0 => s_axis_c2h_tvalid_0,
            s_axis_c2h_tdata_0  => s_axis_c2h_tdata_0,
            s_axis_c2h_tkeep_0  => s_axis_c2h_tkeep_0,
            s_axis_c2h_tlast_0  => s_axis_c2h_tlast_0
        );
    ----------------------------------------------------------------------------

end Behavioral;
