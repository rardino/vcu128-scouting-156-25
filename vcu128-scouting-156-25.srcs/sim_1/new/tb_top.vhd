----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/25/2021 11:29:43 AM
-- Design Name: 
-- Module Name: tb_top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.VComponents.all;

use work.vcu128_top_decl.all;
use work.datatypes.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_top is
--  Port ( );
end tb_top;

architecture Behavioral of tb_top is

    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    --| COMPONENTS declaration                                                  |
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    -- 1 MB data fifo with prog full
    component fifo_generator_0
        port (
            m_aclk         : in  std_logic;
            s_aclk         : in  std_logic;
            s_aresetn      : in  std_logic;
            s_axis_tvalid  : in  std_logic;
            s_axis_tready  : out std_logic;
            s_axis_tdata   : in  std_logic_vector(255 downto 0);
            s_axis_tkeep   : in  std_logic_vector( 31 downto 0);
            s_axis_tlast   : in  std_logic;
            s_axis_tuser   : in  std_logic_vector(  1 downto 0);
            m_axis_tvalid  : out std_logic;
            m_axis_tready  : in  std_logic;
            m_axis_tdata   : out std_logic_vector(255 downto 0);
            m_axis_tkeep   : out std_logic_vector( 31 downto 0);
            m_axis_tlast   : out std_logic;
            m_axis_tuser   : out std_logic_vector(  1 downto 0);
            axis_prog_full : out std_logic
        );
    end component;

    -- 1 MB packet FIFO same clock  
    component fifo_generator_1
        port (
            s_aclk        : in  std_logic;
            s_aresetn     : in  std_logic;
            s_axis_tvalid : in  std_logic;
            s_axis_tready : out std_logic;
            s_axis_tdata  : in  std_logic_vector(255 downto 0);
            s_axis_tkeep  : in  std_logic_vector( 31 downto 0);
            s_axis_tlast  : in  std_logic;
            s_axis_tuser  : in  std_logic_vector(  1 downto 0);
            m_axis_tvalid : out std_logic;
            m_axis_tready : in  std_logic;
            m_axis_tdata  : out std_logic_vector(255 downto 0);
            m_axis_tkeep  : out std_logic_vector( 31 downto 0);
            m_axis_tlast  : out std_logic;
            m_axis_tuser  : out std_logic_vector(  1 downto 0)
        );
    end component;

    component fifo_generator_2
        port (
            s_aclk        : in  std_logic;
            s_aresetn     : in  std_logic;
            s_axis_tvalid : in  std_logic;
            s_axis_tready : out std_logic;
            s_axis_tdata  : in  std_logic_vector(255 downto 0);
            s_axis_tkeep  : in  std_logic_vector(31 downto 0);
            s_axis_tlast  : in  std_logic;
            s_axis_tuser  : in  std_logic_vector(  1 downto 0);
            m_axis_tvalid : out std_logic;
            m_axis_tready : in  std_logic;
            m_axis_tdata  : out std_logic_vector(255 downto 0);
            m_axis_tkeep  : out std_logic_vector(31 downto 0);
            m_axis_tlast  : out std_logic;
            m_axis_tuser  : out std_logic_vector(  1 downto 0)
        );
    end component;
    
    -- fifo to store headers
    component header_fifo
        port (
            rst    : in  std_logic                      := '0';
            wr_clk : in  std_logic                      := '0';
            rd_clk : in  std_logic                      := '0';
            din    : in  std_logic_vector(255 downto 0) := (others => '0');
            wr_en  : in  std_logic                      := '0';
            rd_en  : in  std_logic                      := '0';
            dout   : out std_logic_vector(255 downto 0) := (others => '0');
            full   : out std_logic                      := '0';
            empty  : out std_logic                      := '1';
            underflow : OUT STD_LOGIC;
            rd_data_count : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
            wr_data_count : OUT STD_LOGIC_VECTOR(5 DOWNTO 0)
       );
    end component;
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+


    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    --| SIGNALS declaration                                                     |
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    -- clock period constants
    constant t_link_period     : time := 25 ns;
    constant t_link_halfperiod : time := t_link_period / 2;
    constant t_link_hold       : time := t_link_period / 5;
  
    -- other constants
    constant BUFFER_SIZE       : integer := 64;
    constant QUADS             : integer := 2;
    constant NSTREAMS          : integer := 4*QUADS;
    constant DATA_ORBIT_LENGTH : integer := 162;
    constant FULL_ORBIT_LENGTH : integer := 500;
    
    -- generate testdata signals
    type TBCounterVec is array(natural range <>) of natural range 0 to 3564;
    type TOCounterVec is array(natural range <>) of natural;
    type TEvtWordsVec is array(natural range <>) of natural range 0 to 5;
    signal ocounter : TOCounterVec(7 downto 0) := (others => 65280);
    signal bcounter : TBCounterVec(7 downto 0) := (0, 0, 0, 0, 0, 0, 0, 0);
    signal evt_word : TEvtWordsVec(7 downto 0) := (others => 0);

    -- clock signals
    signal clk_link      : std_logic := '1';
    
    -- reset signals
    signal rst_global   : std_logic := '1';
    signal rst_aligner  : std_logic := '0';
    signal rst_packager : std_logic := '0';
    
    -- comma-gap-cleaner signals
    type TLinkDataBuffer is array (natural range <>) of ldata(7 downto 0);            -- N buffer x M links
    type TLinkFlagBuffer is array (natural range <>) of std_logic_vector(5 downto 0); -- N links  x M buffer
    signal q_padding_buf   : TLinkDataBuffer(23 downto 0);
    signal q_buffer        : TLinkDataBuffer( 5 downto 0);
    signal q_buffer_valid  : TLinkFlagBuffer( 7 downto 0);
    signal q_buffer_strobe : TLinkFlagBuffer( 7 downto 0);

    -- aligner/zs/fifo_filler data signals
    signal q         : ldata(NSTREAMS-1 downto 0);
    signal d_align   : ldata(NSTREAMS-1 downto 0);
    signal d_zs      : adata(NSTREAMS-1 downto 0);
    signal q_zs      : adata(NSTREAMS-1 downto 0);
    signal d_package : adata(NSTREAMS-1 downto 0);
    
    -- aligner/zs/fifo_filler control signals
    signal d_ctrl_zs      : acontrol;
    signal q_ctrl_zs      : acontrol;
    signal d_ctrl_package : acontrol;
    
    -- enable zero suppression
    signal do_zs : std_logic := '0';
    
    -- fifo filler signals
    signal s_axis_tvalid           : std_logic;
    signal s_axis_tready           : std_logic;
    signal s_axis_tlast            : std_logic;
    signal s_axis_tdata            : std_logic_vector(255 downto 0);
    signal s_axis_tuser            : std_logic_vector(  1 downto 0);
    signal s_axis_tvalid_reg       : std_logic;
    signal s_axis_tlast_reg        : std_logic;
    signal s_axis_tdata_reg        : std_logic_vector(255 downto 0);
    signal s_axis_tuser_reg        : std_logic_vector(  1 downto 0);
    signal axi_prog_full           : std_logic;

    type t_FillerState is (FILLING, DROPPING);
    signal fillerState             : t_FillerState;

    signal d1                      : adata(NSTREAMS -1 downto 0);
    signal d_ctrl_d1, d_ctrl_d2    : acontrol;

    signal s_dropped_orbit_counter : unsigned(63 downto 0);
    signal s_orbit_counter         : unsigned(63 downto 0);

    signal orbits_in_packet        : unsigned (15 downto 0);

    signal s_axi_backpressure_seen : std_logic;
    signal orbit_length_int        : unsigned(63 downto 0);
    signal s_orbit_length_cnt      : unsigned(63 downto 0); -- new
    signal packet_length_int       : unsigned(63 downto 0); -- new
    signal packet_length           : unsigned(63 downto 0); -- new

    signal rst_n : std_logic;

    -- from first big data FIFO to small distr RAM FIFO
    signal f1tofd1_axis_tvalid : std_logic;
    signal f1tofd1_axis_tready : std_logic;
    signal f1tofd1_axis_tlast  : std_logic;
    signal f1tofd1_axis_tdata  : std_logic_vector(255 downto 0);
    signal f1tofd1_axis_tkeep  : std_logic_vector( 31 downto 0);
    signal f1tofd1_axis_tuser  : std_logic_vector(  1 downto 0);
    -- from smal distr RAM FIFO to second big (packet) FIFO
    signal fd1tof2_axis_tvalid : std_logic;
    signal fd1tof2_axis_tready : std_logic;
    signal fd1tof2_axis_tlast  : std_logic;
    signal fd1tof2_axis_tdata  : std_logic_vector(255 downto 0);
    signal fd1tof2_axis_tkeep  : std_logic_vector( 31 downto 0);
    signal fd1tof2_axis_tuser  : std_logic_vector(  1 downto 0);
    -- from second big (packet) FIFO to second small distr FIFO
    signal f2tofd2_axis_tvalid : std_logic;
    signal f2tofd2_axis_tready : std_logic;
    signal f2tofd2_axis_tlast  : std_logic;
    signal f2tofd2_axis_tdata  : std_logic_vector(255 downto 0);
    signal f2tofd2_axis_tkeep  : std_logic_vector( 31 downto 0);
    signal f2tofd2_axis_tuser  : std_logic_vector(  1 downto 0);
    -- signal f2tofd2_axis_tvalid_d1 : std_logic;
    -- signal f2tofd2_axis_tready_d1 : std_logic;
    -- signal f2tofd2_axis_tlast_d1  : std_logic;
    -- signal f2tofd2_axis_tdata_d1  : std_logic_vector(255 downto 0);
    -- signal f2tofd2_axis_tkeep_d1  : std_logic_vector( 31 downto 0);
    -- signal f2tofd2_axis_tuser_d1  : std_logic_vector(  1 downto 0);
    -- output
    signal m_axis_tvalid : std_logic;
    signal m_axis_tdata  : std_logic_vector(255 downto 0);
    signal m_axis_tkeep  : std_logic_vector( 31 downto 0);
    signal m_axis_tlast  : std_logic;
    signal m_axis_tready : std_logic := '0';
    signal m_axis_tready_d : std_logic := '0';
    signal m_axis_tuser  : std_logic_vector(  1 downto 0);
    -- register output
    signal m_axis_tvalid_d1 : std_logic;
    signal m_axis_tdata_d1  : std_logic_vector(255 downto 0);
    signal m_axis_tkeep_d1  : std_logic_vector( 31 downto 0);
    signal m_axis_tlast_d1  : std_logic;
    signal m_axis_tready_d1 : std_logic := '0';
    signal m_axis_tuser_d1  : std_logic_vector(  1 downto 0);
    -- register output
    signal m_axis_tvalid_d2 : std_logic;
    signal m_axis_tdata_d2  : std_logic_vector(255 downto 0);
    signal m_axis_tkeep_d2  : std_logic_vector( 31 downto 0);
    signal m_axis_tlast_d2  : std_logic;
    signal m_axis_tready_d2 : std_logic := '0';
    signal m_axis_tuser_d2  : std_logic_vector(  1 downto 0);
    -- header fifo
    signal header_din   : std_logic_vector(255 downto 0) := (others => '0');
    signal header_wen   : std_logic                      := '0';
    signal header_ren   : std_logic                      := '0';
    signal header_dout  : std_logic_vector(255 downto 0);
    signal header_full  : std_logic;
    signal header_empty : std_logic;
    signal header_underflow : std_logic;
    signal header_rd_data_count : std_logic_vector(5 downto 0);
    signal header_wr_data_count : std_logic_vector(5 downto 0);
    
    -- register output
    -- signal s_axis_c2h_tvalid_d1 : std_logic;
    -- signal s_axis_c2h_tdata_d1  : std_logic_vector(255 downto 0);
    -- signal s_axis_c2h_tkeep_d1  : std_logic_vector( 31 downto 0);
    -- signal s_axis_c2h_tlast_d1  : std_logic;
    -- signal s_axis_c2h_tready_d1 : std_logic;
    -- signal s_axis_c2h_tvalid_d2 : std_logic;
    -- signal s_axis_c2h_tdata_d2  : std_logic_vector(255 downto 0);
    -- signal s_axis_c2h_tkeep_d2  : std_logic_vector( 31 downto 0);
    -- signal s_axis_c2h_tlast_d2  : std_logic;
    -- signal s_axis_c2h_tready_d2 : std_logic;
    
    signal packet_end   : std_logic;
    signal packet_end_d : std_logic_vector(22 downto 0);
    
    signal filler_orbit_counter              : unsigned(63 downto 0);
    signal filler_dropped_orbit_counter      : unsigned(63 downto 0);
    signal axi_backpressure_seen             : std_logic;
    signal orbits_per_packet                 : unsigned(15 downto 0) := x"0006";
    signal waiting_orbit_end                 : std_logic;

    signal orbit_length                      : unsigned(63 downto 0);
    signal orbit_length_cnt                  : unsigned(63 downto 0);
    signal orbit_exceeds_size                : std_logic;
    signal enable_autorealign                : std_logic := '1';
    signal autorealign_counter               : unsigned(63 downto 0);
    
    signal filler_state : std_logic;
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    
begin

    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    --| CLOCKS, RESETS and other FLAGS                                          |
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    -- clock
    clk_link  <= not clk_link after t_link_halfperiod;
    
    -- reset
    rst_global <= '0' after 50 us;
    
    -- ready
    ready : process
        
    begin
        wait for 5000 us;
        m_axis_tready <= '1';
        
        wait for 10 us;
        m_axis_tready <= '0';
        for i in 0 to 10000 loop
            wait for 100 ns;
            m_axis_tready <= '1';
            wait for 1000 ns;
            m_axis_tready <= '0';
        end loop;
        wait for 100 ns;
        m_axis_tready <= '1';
    end process;
    -- m_axis_tready <= '1' after 5000 us;
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    
    
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    --| GENERATE TEST DATA                                                      |
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    create_testdata : process(clk_link)
        variable send_padding  : std_logic_vector(QUADS*4-1 downto 0);
    begin
        if clk_link'event and clk_link = '1' then
            -- Create test data now
            for i in QUADS*4-1 downto 0 loop  -- Number of channels
                if ((bcounter(i) +2*i) mod 4 = 0) and (evt_word(i) = 0) and (send_padding(i) = '0') then
                    send_padding(i) := '1';
                elsif evt_word(i) < 5 then
                    send_padding(i) := '0';
                    evt_word(i)     <= evt_word(i)+1;
                elsif evt_word(i) = 5 then
                    send_padding(i) := '0';
                    evt_word(i)     <= 0;
                    if bcounter(i) < FULL_ORBIT_LENGTH then
                        bcounter(i) <= bcounter(i)+1;
                    elsif bcounter(i) = FULL_ORBIT_LENGTH then
                        bcounter(i) <= 1;
                        ocounter(i) <= ocounter(i)+1;
                    else
                        bcounter(i) <= 1;
                    end if;
                else
                    send_padding(i) := '0';
                    evt_word(i)     <= 0;
                end if;

                if send_padding(i) = '1' then
                    q(i).data                  <= x"F7F7F7F7";  -- Padding
                    q(i).valid                 <= '1';
                    q(i).strobe                <= '0';
                    q(i).done                  <= '0';
                
                elsif bcounter(i) > 0 and bcounter(i) < DATA_ORBIT_LENGTH then  -- Arbitrary obit gap
                    if evt_word(i) = 0 then
                        q(i).data <= std_logic_vector(to_unsigned(ocounter(i), 32));
                    elsif evt_word(i) = 1 then
                        q(i).data <= std_logic_vector(to_unsigned(bcounter(i), 32));
                    elsif (evt_word(i) = 2) or (evt_word(i) = 4) then
                        q(i).data <= "000000001" & "1100" & "100000000" & "0000000000";
                    elsif (evt_word(i) = 3) or (evt_word(i) = 5) then
                        q(i).data <= (others => '1');
                    else
                        q(i).data <= (others => '1');
                    end if;

                    q(i).done   <= '0';
                    q(i).valid  <= '1';
                    q(i).strobe <= '1';
                    
                elsif bcounter(i) = DATA_ORBIT_LENGTH and (evt_word(i) = 1 or evt_word(i) = 2) then
                    q(i).done   <= '0';
                    q(i).strobe <= '1';
                    q(i).valid  <= '1';
                    q(i).data   <= "10101010101010101010101010101010";  -- If I see this pattern I'm transmitting "CRCs"
                
                else
                    q(i).done   <= '0';
                    q(i).strobe <= '1';
                    q(i).valid  <= '0';
                    q(i).data   <= x"505050BC";  -- COMMA
                end if;
            end loop;

        end if;
    end process;
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    
    
    
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    --| COMMA_GAP_CLEANER                                                       |
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    q_padding_buf(0)(4*QUADS - 1 downto 0) <= q;

    invalidate_padding_in_comma_gap : process(clk_link)
        -- variable crcs_read : std_logic_vector(4*QUADS - 1 downto 0) := (others => '0');
    begin
        if clk_link'event and clk_link = '1' then
            q_padding_buf(q_padding_buf'high downto 1) <= q_padding_buf(q_padding_buf'high-1 downto 0);
            
            for i in q'range loop
                if  (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 2)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 3)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 4)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 5)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 6)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 7)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 8)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 9)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-10)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-11)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-12)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-13)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-14)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-15)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-16)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-17)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-18)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-19)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-20)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-21)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-22)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-23)(i).valid = '0')
                then
                    q_buffer(0)(i).data   <= q_padding_buf(q_padding_buf'high-1)(i).data;
                    q_buffer(0)(i).strobe <= q_padding_buf(q_padding_buf'high-1)(i).strobe;
                    q_buffer(0)(i).done   <= q_padding_buf(q_padding_buf'high-1)(i).done;
                    q_buffer(0)(i).valid  <= '0';
                    
                    q_buffer_strobe(i)(0) <= q_padding_buf(q_padding_buf'high-1)(i).strobe;
                    q_buffer_valid(i)(0)  <= '0';
                    
                else
                    q_buffer(0)(i) <= q_padding_buf(q_padding_buf'high-1)(i);
                    
                    q_buffer_strobe(i)(0) <= q_padding_buf(q_padding_buf'high-1)(i).strobe;
                    q_buffer_valid(i)(0)  <= q_padding_buf(q_padding_buf'high-1)(i).valid;
                end if;
            end loop;

            q_buffer(q_buffer'high downto 1) <= q_buffer(q_buffer'high-1 downto 0);
            
            for i in q'range loop
                q_buffer_strobe(i)(q_buffer_strobe(i)'high downto 1) <= q_buffer_strobe(i)(q_buffer_strobe(i)'high-1 downto 0);
                q_buffer_valid(i)(q_buffer_valid(i)'high downto 1)   <= q_buffer_valid(i)(q_buffer_valid(i)'high-1 downto 0);
                
                -- Invalidate padding word befor COMMA -> CRC -> LID -> COMMA -> GAP
                if (q_buffer_valid(i) = "110000" and q_buffer_strobe(i) = "101111")
                then
                    d_align(i).data   <= q_buffer(4)(i).data;
                    d_align(i).strobe <= q_buffer(4)(i).strobe;
                    d_align(i).done   <= q_buffer(4)(i).done;
                    d_align(i).valid  <= '0';
                else
                    d_align(i) <= q_buffer(4)(i);
                end if;
            end loop;
            
        end if;
    end process;
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

    
--    ---------------------------------------------------------------------------
--    -- COMMA GAP CLEANER
--    ---------------------------------------------------------------------------
--    gap_cleaner : entity work.comma_gap_cleaner
--        port map (
--            clk => clk_link,
--            rst => rst_global,
--            d   => q,
--            q   => d_align
--        );
--    ---------------------------------------------------------------------------
    
    
    
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    --| ALIGNER                                                                 |
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    align : entity work.bx_aware_aligner
        generic map (
            NSTREAMS => 4* N_REGION
        )
        port map (
            clk_wr            => clk_link,
            clk_rd            => clk_link, -- clk_axi,
            rst               => rst_global, -- rst_aligner,
            enable            => (others => '1'),
            waiting_orbit_end => waiting_orbit_end,
            d                 => d_align,
            q                 => d_zs,
            q_ctrl            => d_ctrl_zs
        );
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    
    
    
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    --| AUTOREALIGN CONTROLLER (if misalignment is found, trigger a reset)      |
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    auto_realign_controller_1 : entity work.auto_realign_controller
        port map (
            axi_clk           => clk_link,
            rst_in            => rst_global,
            enabled           => enable_autorealign,
            clk_aligner       => clk_link,
            rst_aligner_out   => rst_aligner,
            misalignment_seen => orbit_exceeds_size,
            autoreset_count   => autorealign_counter
        );
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    
    
    
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    --| ZERO SUPPRESSION                                                        |
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    zs : entity work.zs
        generic map (
            NSTREAMS => 4* N_REGION
        )
        port map (
            clk    => clk_link, -- clk_axi
            rst    => rst_global, -- rst_packager,
            d      => d_zs,
            d_ctrl => d_ctrl_zs,
            q      => q_zs,
            q_ctrl => q_ctrl_zs
        );
    
    d_package      <= q_zs      when do_zs = '1' else d_zs;
    d_ctrl_package <= q_ctrl_zs when do_zs = '1' else d_ctrl_zs;
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    --| FIFO FILLER                                                             |
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    filler : process (clk_link) is

    begin
        if (clk_link'event and clk_link = '1') then
            if (rst_global = '1') then
                fillerState             <= FILLING;
                -- orbit counters
                s_orbit_counter         <= to_unsigned(0, 64);
                s_dropped_orbit_counter <= to_unsigned(0, 64);
                orbits_in_packet        <= to_unsigned(0, 16);
                -- backpressure
                s_axi_backpressure_seen <= '0';
                -- orbit length
                orbit_exceeds_size      <= '0';
                orbit_length_int        <= to_unsigned(0, 64);
                s_orbit_length_cnt      <= to_unsigned(0, 64);
                -- packet length and end
                packet_length_int       <= to_unsigned(0, 64); -- new
                packet_end              <= '0';                -- new
                packet_end_d            <= (others => '0');    -- new
                -- data
                s_axis_tdata            <= (others => '0');
                s_axis_tuser            <= (others => '0');
                s_axis_tlast            <= '0';
                s_axis_tvalid           <= '0';
                -- header fifo
                header_din              <= (others => '0');
                -- header_dout             <= (others => '0');
                header_wen              <= '0';
                header_ren              <= '0';
                
            else

                d1        <= d_package;
                d_ctrl_d1 <= d_ctrl_package;
                d_ctrl_d2 <= d_ctrl_d1;

                -- register FIFO inputs so that timing is easier to meet 
                s_axis_tvalid_reg <= s_axis_tvalid;
                s_axis_tlast_reg  <= s_axis_tlast;
                s_axis_tdata_reg  <= s_axis_tdata;
                s_axis_tuser_reg  <= s_axis_tuser;
                
                -- disable write to header fifo
                if header_wen = '1' then
                    header_wen <= '0';
                end if;

                -- start of orbit
                if d_ctrl_d1.valid = '1' and d_ctrl_d2.valid = '0' then
                    orbit_length_int   <= to_unsigned(1, 64);
                    s_orbit_length_cnt <= to_unsigned(1, 64); -- new
                end if;

                -- orbit on-going
                if d_ctrl_d1.valid = '1' and d_ctrl_d2.valid = '1' and d_ctrl_d1.strobe = '1' then
                    orbit_length_int   <= orbit_length_int   + 1;
                    s_orbit_length_cnt <= s_orbit_length_cnt + 1;

                    -- check if orbit length exceeds size
                    if orbit_length_int > 21600 then
                        orbit_exceeds_size <= '1';
                    else
                        orbit_exceeds_size <= '0';
                    end if;
                end if;

                -- end of orbit
                if d_ctrl_d1.valid = '0' and d_ctrl_d2.valid = '1' then
                    orbit_length       <= orbit_length_int;
                    orbit_length_int   <= to_unsigned(0, 64);
                    s_orbit_length_cnt <= to_unsigned(0, 64); -- new
                    orbit_exceeds_size <= '0';
                end if;

                --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                --| moved here from FILLER state (new)                          |
                --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                packet_end_d(0) <= packet_end;
                packet_end_d(packet_end_d'high downto 1) <= packet_end_d(packet_end_d'high-1 downto 0);
                
                if packet_end = '1' then
                    packet_end <= '0';
                end if;
                --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-

                case fillerState is
                    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                    --| FSM in FILLER state                                     |
                    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                    when FILLING =>
                        if s_axis_tvalid_reg = '1' and s_axis_tready = '0' then
                            s_axi_backpressure_seen <= '1';
                        end if;
                        
                        if d_ctrl_package.valid = '1' and d_ctrl_d1.valid = '0' and std_logic_vector(orbits_in_packet) = x"0000" then
                            s_axis_tvalid <= '1';
                            s_axis_tlast  <= '0';  -- d_ctrl.last; --- mhmmm
                            s_axis_tuser  <= "10"; -- header
                            s_axis_tdata  <= (others => '0');
                        else
                            s_axis_tvalid <= d_ctrl_d1.valid and d_ctrl_d1.strobe;
                            s_axis_tlast  <= '0';  -- d_ctrl.last; --- mhmmm
                            s_axis_tuser  <= "11"; -- data
                            s_axis_tdata  <= (others => '0');
                            for i in d_package'range loop
                                s_axis_tdata(i*32 + 31 downto i*32) <= d1(i);
                            end loop;
                        end if;

                        --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                        --| packet length in frames and packet end              |
                        --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                        -- start of orbit
                        if d_ctrl_d1.valid = '1' and d_ctrl_d2.valid = '0' then
                        
                            -- start of packet
                            if std_logic_vector(orbits_in_packet) = x"0000" then
                                packet_length_int <= to_unsigned(1, 64);
                            
                            -- packet on-going
                            else
                                packet_length_int <= packet_length_int + 1;
                            end if;
                        
                        -- middle of packet and orbit
                        elsif d_ctrl_d1.valid = '1' and d_ctrl_d2.valid = '1' and d_ctrl_d1.strobe = '1' then
                            packet_length_int <= packet_length_int + 1;

                        -- end of orbit
                        elsif d_ctrl_d1.valid = '0' and d_ctrl_d2.valid = '1' then
                            s_orbit_counter <= s_orbit_counter + 1;
                            if (orbits_in_packet < orbits_per_packet-1) then
                                orbits_in_packet <= orbits_in_packet + 1;
                                packet_end       <= '0'; -- new
                             
                            -- end of packet
                            else
                                packet_end        <= '1'; -- new
                                s_axis_tvalid     <= '1';
                                s_axis_tlast      <= '1';
                                s_axis_tuser      <= "01"; -- trailer
                                s_axis_tdata      <= std_logic_vector(s_orbit_counter + 1)     &
                                                     std_logic_vector(s_dropped_orbit_counter) &
                                                     std_logic_vector(autorealign_counter)     &
                                                     x"deadbeefdeadbeef";                           -- end of packet marker
                                orbits_in_packet  <= to_unsigned(0, 16);
                                packet_length     <= packet_length_int;
                                packet_length_int <= to_unsigned(0, 64);
                                
                                header_din        <= std_logic_vector(s_orbit_counter + 1)     &
                                                     std_logic_vector(s_dropped_orbit_counter) &
                                                     std_logic_vector(packet_length_int)       &
                                                     x"feedbeeffeedbeef";
                                header_wen        <= '1';
                            end if;
                            
                            -- check if fifo is full
                            if axi_prog_full = '1' then
                                fillerState <= DROPPING;
                            end if;
                        end if;
                        --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                        
                    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                    --| FSM in DROPPING state                                   |
                    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                    when DROPPING =>
                        -- send null and invalid data
                        s_axis_tvalid <= '0';
                        s_axis_tlast  <= '0';
                        s_axis_tdata  <= (others => '0');
                        
                        -- end of orbit
                        if d_ctrl_d1.valid = '0' and d_ctrl_d2.valid = '1' then
                            s_orbit_counter         <= s_orbit_counter + 1;
                            s_dropped_orbit_counter <= s_dropped_orbit_counter + 1;
                            if axi_prog_full = '1' then
                                fillerState <= DROPPING;
                            end if;
                        end if;
                        
                        -- start of orbit
                        if d_ctrl_package.valid = '1' and d_ctrl_d1.valid = '0' then
                            if axi_prog_full = '0' then
                                fillerState <= FILLING;
                                
                                if std_logic_vector(orbits_in_packet) = x"0000" then
                                    s_axis_tvalid <= '1';
                                    s_axis_tlast  <= '0';  -- d_ctrl.last; --- mhmmm
                                    s_axis_tuser  <= "10";
                                    s_axis_tdata  <= (others => '0');
                                end if;
                            end if;
                        end if;
                    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

                end case fillerState;
                
                
                if header_ren = '1' then
                    header_ren <= '0';
                end if;

                if m_axis_tready = '1' then
                    if m_axis_tuser_d1 = "10" then 
                        header_ren           <= '1';
                        m_axis_tdata  <= header_dout;
                        m_axis_tvalid <= '1';
                        m_axis_tlast  <= '0';
                        m_axis_tkeep  <= (others => '1');
                    else
                        header_ren           <= '0';
                        m_axis_tdata  <= m_axis_tdata_d1;
                        m_axis_tvalid <= m_axis_tvalid_d1;
                        m_axis_tlast  <= m_axis_tlast_d1;
                        m_axis_tkeep  <= m_axis_tkeep_d1;
                    end if;
                end if;
                
--                if m_axis_tuser = "10" and m_axis_tready = '1' then 
--                    header_ren <= '1';
--                else
--                    header_ren <= '0';
--                end if;
                
--                -- m_axis_tready_d  <= m_axis_tready;
--                m_axis_tready_d1 <= m_axis_tready;
--                m_axis_tready_d2 <= m_axis_tready_d1;
                
--                m_axis_tvalid_d1 <= m_axis_tvalid;
--                m_axis_tdata_d1  <= m_axis_tdata;
--                m_axis_tuser_d1  <= m_axis_tuser;
--                m_axis_tlast_d1  <= m_axis_tlast;
--                m_axis_tkeep_d1  <= m_axis_tkeep;
                
                
--                if header_ren = '0' then
--                    m_axis_tvalid_d2 <= m_axis_tvalid_d1;
--                    m_axis_tdata_d2  <= m_axis_tdata_d1;
--                    m_axis_tuser_d2  <= m_axis_tuser_d1;
--                    m_axis_tlast_d2  <= m_axis_tlast_d1;
--                    m_axis_tkeep_d2  <= m_axis_tkeep_d1;
--                else
--                    m_axis_tvalid_d2 <= '1';
--                    m_axis_tdata_d2  <= header_dout;
--                    m_axis_tuser_d2  <= "10";
--                    m_axis_tlast_d2  <= '0';
--                    m_axis_tkeep_d2  <= (others => '1');
--                end if;
                
                
--                if m_axis_tuser = "10" and m_axis_tready_d1 = '1' then 
--                    header_ren <= '1';
--                else
--                    header_ren <= '0';
--                end if;
                
--                if header_ren = '0' then
--                    m_axis_tvalid_d1 <= m_axis_tvalid;
--                    m_axis_tdata_d1  <= m_axis_tdata;
--                    m_axis_tuser_d1  <= m_axis_tuser;
--                    m_axis_tlast_d1  <= m_axis_tlast;
--                    m_axis_tkeep_d1  <= m_axis_tkeep;
--                    m_axis_tready_d1 <= m_axis_tready;
--                else
--                    m_axis_tvalid_d1 <= '1';
--                    m_axis_tdata_d1  <= header_dout;
--                    m_axis_tuser_d1  <= "10";
--                    m_axis_tlast_d1  <= '0';
--                    m_axis_tkeep_d1  <= (others => '1');
--                    m_axis_tready_d1 <= m_axis_tready;
--                end if;
                
                
--                if header_ren = '0' then
--                    s_axis_c2h_tdata  <= s_axis_c2h_tdata_d2;
--                    s_axis_c2h_tvalid <= s_axis_c2h_tvalid_d2;
--                    s_axis_c2h_tlast  <= s_axis_c2h_tlast_d2;
--                    s_axis_c2h_tkeep  <= s_axis_c2h_tkeep_d2;
--                else
--                    s_axis_c2h_tdata  <= header_dout;
--                    s_axis_c2h_tvalid <= '1';
--                    s_axis_c2h_tlast  <= '0';
--                    s_axis_c2h_tkeep  <= (others => '1');
--                end if;
                
--                s_axis_c2h_tvalid_d2 <= s_axis_c2h_tvalid_d1;
--                s_axis_c2h_tdata_d2  <= s_axis_c2h_tdata_d1;
--                s_axis_c2h_tlast_d2  <= s_axis_c2h_tlast_d1;
--                s_axis_c2h_tkeep_d2  <= s_axis_c2h_tkeep_d1;
                
--                if s_axis_c2h_tdata_d2(63 downto 0) = x"feedbeeffeedbeef" and s_axis_c2h_tdata_d1 /= x"feedbeeffeedbeef" then 
--                    header_ren           <= '1';
--                else
--                    header_ren           <= '0';
--                end if;
                
--                s_axis_c2h_tvalid_d3 <= s_axis_c2h_tvalid_d2;
--                s_axis_c2h_tdata_d3  <= s_axis_c2h_tdata_d2;
--                s_axis_c2h_tlast_d3  <= s_axis_c2h_tlast_d2;
--                s_axis_c2h_tkeep_d3  <= s_axis_c2h_tkeep_d2;
                
                
--                if header_ren = '0' then
--                    s_axis_c2h_tdata  <= s_axis_c2h_tdata_d3;
--                    s_axis_c2h_tvalid <= s_axis_c2h_tvalid_d3;
--                    s_axis_c2h_tlast  <= s_axis_c2h_tlast_d3;
--                    s_axis_c2h_tkeep  <= s_axis_c2h_tkeep_d3;
--                else
--                    s_axis_c2h_tdata  <= header_dout;
--                    s_axis_c2h_tvalid <= '1';
--                    s_axis_c2h_tlast  <= '0';
--                    s_axis_c2h_tkeep  <= (others => '1');
--                end if;
            end if;
            
--            if m_axis_tuser = "10" and m_axis_tready_d2 = '1' then 
--                header_ren <= '1';
--            else
--                header_ren <= '0';
--            end if;
            
--            m_axis_tvalid_d1 <= m_axis_tvalid;
--            m_axis_tdata_d1  <= m_axis_tdata;
--            m_axis_tuser_d1  <= m_axis_tuser;
--            m_axis_tlast_d1  <= m_axis_tlast;
--            m_axis_tkeep_d1  <= m_axis_tkeep;
--            m_axis_tready_d2 <= m_axis_tready_d1;
            
            
--            if header_ren = '0' then
--                m_axis_tvalid_d2 <= m_axis_tvalid_d1;
--                m_axis_tdata_d2  <= m_axis_tdata_d1;
--                m_axis_tuser_d2  <= m_axis_tuser_d1;
--                m_axis_tlast_d2  <= m_axis_tlast_d1;
--                m_axis_tkeep_d2  <= m_axis_tkeep_d1;
--                m_axis_tready_d1 <= m_axis_tready;
--            else
--                m_axis_tvalid_d2 <= '1';
--                m_axis_tdata_d2  <= header_dout;
--                m_axis_tuser_d2  <= "10";
--                m_axis_tlast_d2  <= '0';
--                m_axis_tkeep_d2  <= (others => '1');
--                m_axis_tready_d1 <= m_axis_tready;
--            end if;
        end if;
        
--        if s_axis_c2h_tdata_d1(63 downto 0) = x"feedbeeffeedbeef" and s_axis_c2h_tready = '1' then 
--            header_ren           <= '1';
--        else
--            header_ren           <= '0';
--        end if;
--        s_axis_c2h_tvalid_d2 <= s_axis_c2h_tvalid_d1;
--        s_axis_c2h_tdata_d2  <= s_axis_c2h_tdata_d1;
--        s_axis_c2h_tlast_d2  <= s_axis_c2h_tlast_d1;
--        s_axis_c2h_tkeep_d2  <= s_axis_c2h_tkeep_d1;
        
        
--        if header_ren = '0' then
--            s_axis_c2h_tdata  <= s_axis_c2h_tdata_d2;
--            s_axis_c2h_tvalid <= s_axis_c2h_tvalid_d2;
--            s_axis_c2h_tlast  <= s_axis_c2h_tlast_d2;
--            s_axis_c2h_tkeep  <= s_axis_c2h_tkeep_d2;
--        else
--            s_axis_c2h_tdata  <= header_dout;
--            s_axis_c2h_tvalid <= '1';
--            s_axis_c2h_tlast  <= '0';
--            s_axis_c2h_tkeep  <= (others => '1');
--        end if;
    end process filler;
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    --| OUTPUT signals                                                          |
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--    header : process(clk_link)
--    begin
--        if (clk_link'event and clk_link = '1') then
--            if m_axis_tuser = "10" and m_axis_tready_d = '1' then 
--                header_ren <= '1';
--            else
--                header_ren <= '0';
--            end if;
            
--            m_axis_tready_d  <= m_axis_tready;
--            m_axis_tready_d1 <= m_axis_tready_d;
--            m_axis_tready_d2 <= m_axis_tready_d1;
            
--            m_axis_tvalid_d1 <= m_axis_tvalid;
--            m_axis_tdata_d1  <= m_axis_tdata;
--            m_axis_tuser_d1  <= m_axis_tuser;
--            m_axis_tlast_d1  <= m_axis_tlast;
--            m_axis_tkeep_d1  <= m_axis_tkeep;
            
            
--            if header_ren = '0' then
--                m_axis_tvalid_d2 <= m_axis_tvalid_d1;
--                m_axis_tdata_d2  <= m_axis_tdata_d1;
--                m_axis_tuser_d2  <= m_axis_tuser_d1;
--                m_axis_tlast_d2  <= m_axis_tlast_d1;
--                m_axis_tkeep_d2  <= m_axis_tkeep_d1;
--            else
--                m_axis_tvalid_d2 <= '1';
--                m_axis_tdata_d2  <= header_dout;
--                m_axis_tuser_d2  <= "10";
--                m_axis_tlast_d2  <= '0';
--                m_axis_tkeep_d2  <= (others => '1');
--            end if;
--        end if;
--    end process header;
    
--    header_ren           <= '1' when s_axis_c2h_tdata_d1(63 downto 0) = x"feedbeeffeedbeef" and s_axis_c2h_tready = '1' else '0';
--    s_axis_c2h_tvalid_d2 <= s_axis_c2h_tvalid_d1;
--    s_axis_c2h_tdata_d2  <= s_axis_c2h_tdata_d1;
--    s_axis_c2h_tlast_d2  <= s_axis_c2h_tlast_d1;
--    s_axis_c2h_tkeep_d2  <= s_axis_c2h_tkeep_d1;
    
    
--    s_axis_c2h_tdata  <= s_axis_c2h_tdata_d2  when header_ren = '0' else header_dout;
--    s_axis_c2h_tvalid <= s_axis_c2h_tvalid_d2 when header_ren = '0' else '1';
--    s_axis_c2h_tlast  <= s_axis_c2h_tlast_d2  when header_ren = '0' else '0';
--    s_axis_c2h_tkeep  <= s_axis_c2h_tkeep_d2  when header_ren = '0' else (others => '1');

                                           --                              std_logic_vector(s_dropped_orbit_counter) &
                                           --                              std_logic_vector(packet_length)           &
                                           --                              x"feedbeeffeedbeef";
    -- s_axis_c2h_tkeep  <= m_axis_tkeep_reg ; -- when packet_end_d(22) = '0' else (others => '1');
    -- s_axis_c2h_tlast  <= m_axis_tlast_reg ; -- when packet_end_d(22) = '0' else '0';
--    s_axis_c2h_tvalid <= m_axis_tvalid_reg; -- when packet_end_d(22) = '0' else '1';
--    s_axis_c2h_tdata  <= m_axis_tdata_reg ; -- when packet_end_d(22) = '0' else std_logic_vector(s_orbit_counter)     &
--                                           --                              std_logic_vector(s_dropped_orbit_counter) &
--                                           --                              std_logic_vector(packet_length)           &
--                                           --                              x"feedbeeffeedbeef";
--    s_axis_c2h_tkeep  <= m_axis_tkeep_reg ; -- when packet_end_d(22) = '0' else (others => '1');
--    s_axis_c2h_tlast  <= m_axis_tlast_reg ; -- when packet_end_d(22) = '0' else '0';
    
    -- orbit_length_cnt      <= s_orbit_length_cnt;
    -- orbit_counter         <= s_orbit_counter;
    -- dropped_orbit_counter <= s_dropped_orbit_counter;
    -- axi_backpressure_seen <= s_axi_backpressure_seen;
    -- packet_end_o          <= packet_end_d;
    -- filler_state_o        <= '0' when fillerState = DROPPING else '1';
    -- axi_prog_full_o       <= axi_prog_full;
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+



    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    --| FIFOs instantiations                                                    |
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    rst_n <= not rst_global;

    -- 1 MB data fifo with prog full
    fifo : fifo_generator_0
        port map(
            s_aclk         => clk_link,
            m_aclk         => clk_link,
            s_aresetn      => rst_n,
            s_axis_tvalid  => s_axis_tvalid_reg,
            s_axis_tready  => s_axis_tready,
            s_axis_tdata   => s_axis_tdata_reg,
            s_axis_tkeep   => (others => '1'),
            s_axis_tlast   => s_axis_tlast_reg,
            s_axis_tuser   => s_axis_tuser_reg,
            axis_prog_full => axi_prog_full,

            m_axis_tvalid => f1tofd1_axis_tvalid,
            m_axis_tready => f1tofd1_axis_tready,
            m_axis_tdata  => f1tofd1_axis_tdata,
            m_axis_tkeep  => f1tofd1_axis_tkeep,
            m_axis_tlast  => f1tofd1_axis_tlast,
            m_axis_tuser  => f1tofd1_axis_tuser
        );

    -- small FIFO to achcive timing between two big FIFOs
    distrRAM_fifo12 : fifo_generator_2
        port map (
            s_aclk        => clk_link,
            s_aresetn     => rst_n,
            s_axis_tvalid => f1tofd1_axis_tvalid,
            s_axis_tready => f1tofd1_axis_tready,
            s_axis_tdata  => f1tofd1_axis_tdata,
            s_axis_tkeep  => f1tofd1_axis_tkeep,
            s_axis_tlast  => f1tofd1_axis_tlast,
            s_axis_tuser  => f1tofd1_axis_tuser,
            
            m_axis_tvalid => fd1tof2_axis_tvalid,
            m_axis_tready => fd1tof2_axis_tready,
            m_axis_tdata  => fd1tof2_axis_tdata,
            m_axis_tkeep  => fd1tof2_axis_tkeep,
            m_axis_tlast  => fd1tof2_axis_tlast,
            m_axis_tuser  => fd1tof2_axis_tuser
        );

    -- 1 MB packet FIFO same clock
    fifo2 : fifo_generator_1
        port map (
            s_aclk        => clk_link,
            s_aresetn     => rst_n,
            s_axis_tvalid => fd1tof2_axis_tvalid,
            s_axis_tready => fd1tof2_axis_tready,
            s_axis_tdata  => fd1tof2_axis_tdata,
            s_axis_tkeep  => fd1tof2_axis_tkeep,
            s_axis_tlast  => fd1tof2_axis_tlast,
            s_axis_tuser  => fd1tof2_axis_tuser,

            m_axis_tvalid => f2tofd2_axis_tvalid,
            m_axis_tready => f2tofd2_axis_tready,
            m_axis_tdata  => f2tofd2_axis_tdata,
            m_axis_tkeep  => f2tofd2_axis_tkeep,
            m_axis_tlast  => f2tofd2_axis_tlast,
            m_axis_tuser  => f2tofd2_axis_tuser
        );

    -- small FIFO to achcive timing between two big FIFOs
    distrRAM_fifo2dma : fifo_generator_2
        port map (
            s_aclk        => clk_link,
            s_aresetn     => rst_n,
            s_axis_tvalid => f2tofd2_axis_tvalid,
            s_axis_tready => f2tofd2_axis_tready,
            s_axis_tdata  => f2tofd2_axis_tdata,
            s_axis_tkeep  => f2tofd2_axis_tkeep,
            s_axis_tlast  => f2tofd2_axis_tlast,
            s_axis_tuser  => f2tofd2_axis_tuser,
            
            m_axis_tvalid => m_axis_tvalid_d1,
            m_axis_tready => m_axis_tready,
            m_axis_tdata  => m_axis_tdata_d1,
            m_axis_tkeep  => m_axis_tkeep_d1,
            m_axis_tlast  => m_axis_tlast_d1,
            m_axis_tuser  => m_axis_tuser_d1
        );
    
    distRAM_header_fifo : header_fifo
        port map (
            rst    => rst_global,
            wr_clk => clk_link,
            rd_clk => clk_link,
            din    => header_din,
            wr_en  => header_wen,
            rd_en  => header_ren,
            dout   => header_dout,
            full   => header_full,
            empty  => header_empty,
            underflow     => header_underflow,
            rd_data_count => header_rd_data_count,
            wr_data_count => header_wr_data_count
        );
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    
    
    
--    ---------------------------------------------------------------------------
--    -- FIFO FILLER (and check if zs is enabled)
--    ---------------------------------------------------------------------------
--    d_package      <= q_zs      when do_zs = '1' else d_zs;
--    d_ctrl_package <= q_ctrl_zs when do_zs = '1' else d_ctrl_zs;
    
--    fifo_filler : entity work.fifo_filler
--        generic map (
--            NSTREAMS => 4* N_REGION
--        )
--        port map (
--            d_clk                  => clk_link, -- clk_axi,
--            rst                    => rst_global, -- rst_packager,
--            orbits_per_packet      => unsigned(orbits_per_packet),
--            d                      => d_package,
--            d_ctrl                 => d_ctrl_package,
--            m_aclk                 => clk_link, -- clk_axi,
--            m_axis_tvalid          => s_axis_c2h_tvalid_0,
--            m_axis_tready          => s_axis_c2h_tready_0,
--            m_axis_tdata           => s_axis_c2h_tdata_0,
--            m_axis_tkeep           => s_axis_c2h_tkeep_0,
--            m_axis_tlast           => s_axis_c2h_tlast_0,
--            dropped_orbit_counter  => filler_dropped_orbit_counter,
--            orbit_counter          => filler_orbit_counter,
--            axi_backpressure_seen  => axi_backpressure_seen,
--            orbit_length           => orbit_length,
--            orbit_length_cnt       => orbit_length_cnt,
--            orbit_exceeds_size     => orbit_exceeds_size,
--            in_autorealign_counter => autorealign_counter,
--            filler_state_o         => filler_state
--        );
--    ---------------------------------------------------------------------------




end Behavioral;
