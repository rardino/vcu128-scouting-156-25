library ieee;
use ieee.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use work.datatypes.all;
use work.constants.all;



entity freq_meas_tb is
--  Port ( );
end freq_meas_tb;

architecture Behavioral of freq_meas_tb is

    signal clk            : std_logic := '0';
    signal clk_meas       : std_logic := '0';
    signal rst            : std_logic := '0';
    
    constant t_clk        : time := 10000 ns;
    constant t_clk_meas   : time :=  4000 ns;
    constant t_h_clk      : time := t_clk / 2;
    constant t_h_clk_meas : time := t_clk_meas / 2;
    
    signal freq_clk_meas : std_logic_vector(31 downto 0) := x"00000000";
    
    shared variable cnt : integer range 0 to 1048576 := 0;
    
    signal delay_cnt : std_logic_vector(31 downto 0) := (others => '0');
    signal freq_cnt : std_logic_vector(31 downto 0) := (others => '0');
    signal freq_store : std_logic_Vector(31 downto 0) := (others => '0');
    signal store_freq: std_logic := '0';
    signal store_freq_d: std_logic_vector(2 downto 0) := (others => '0');
    signal store_freq_flag : std_logic := '0';
    
    constant DELAY_THRS_1 : std_logic_Vector(31 downto 0) := x"000186A0";
    constant DELAY_THRS_2 : std_logic_Vector(31 downto 0) := x"000186A5";

begin

    clk_proc :
        clk <=  '1' after t_h_clk when clk = '0' else
                '0' after t_h_clk when clk = '1';
    
    clk_meas_proc :
        clk_meas <=  '1' after t_h_clk_meas when clk_meas = '0' else
                     '0' after t_h_clk_meas when clk_meas = '1';
    
    rst_proc:
        rst <=  '0' after 10000 ms when  rst = '1' else
                '1' after 10000 ms when  rst = '0';
      
     --m_axis_h2c_tready_0 <= '0'; 
--     m_axis_h2c_tready_0 <= '1' after 200 ns; 
     

--    freq_meas_clk_algo : entity work.freq_meas
--        generic map (
--            DELAY_THRS_1 => x"00002710", -- x"0EE6B280",
--            DELAY_THRS_2 => x"00002715" -- x"0EE6B280",
--        )
--        port map (
--            clk      => clk,
--            rst      => rst,
--            clk_meas => clk_meas,
--            freq     => freq_clk_meas
--        );



--    run_tb : process
--        variable i : integer := 0;
--        variable j : integer := 0;
--    begin

--        cnt := 0;
--        while (cnt /= 1000000) loop
        
--            cnt := cnt + 1;
--        end loop;


--    end process run_tb;

    delay_cnt_p: process(rst, clk)
	begin
--		if rst = '1' then
--			delay_cnt <= (others => '0');
--		els
		if rising_edge(clk) then
			store_freq <= '0';
			-- if unsigned(delay_cnt) >= x"0EE6B280" then -- 250 000 000
			if unsigned(delay_cnt) >= unsigned(DELAY_THRS_1) then -- 100 000 000
 				store_freq <= '1';
 		    end if;
 		    -- if unsigned(delay_cnt) >= x"0EE6B285" then
 		    if unsigned(delay_cnt) >= unsigned(DELAY_THRS_2) then
				delay_cnt <= (others => '0');
			else
				delay_cnt <= std_logic_vector( unsigned(delay_cnt) + 1 );
			end if;
		end if;
	end process;

	sync: process(clk_meas)
	begin
		if rising_edge(clk_meas) then
			store_freq_d <= store_freq_d(1 downto 0) & store_freq;
		end if;
	end process;
	
	store_freq_flag <= (not store_freq_d(2)) and store_freq_d(1);
	
	freq_cnt_p: process(clk_meas)
	begin
		if rising_edge(clk_meas) then
			if store_freq_flag = '1' then
				freq_store <= freq_cnt;
				freq_cnt <= (others => '0');
			else
				freq_cnt <= std_logic_vector( unsigned(freq_cnt) + 1 );	
			end if;
		end if;
	end process;
	
	freq_clk_meas <= freq_store;


--    process(clk)
--    begin
--        if rst = '1' then
--            cnt := 0;
--        elsif rising_edge(clk) then
--            if cnt = 1048576 then
--                cnt := 0;
--            end if;

--            -- if m_axil_bvalid = '1' then
--            cnt := cnt + 1;
--            -- end if;
--        end if;
--    end process;

end Behavioral;