----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/09/2022 11:49:36 AM
-- Design Name: 
-- Module Name: vcu128_scout_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.VComponents.all;

use work.vcu128_top_decl.all;
use work.datatypes.all;

entity vcu128_scout_sim is
    generic (
        GEN_ORBIT_FULL_LENGTH : natural := ORBIT_LENGTH;
        GEN_ORBIT_DATA_LENGTH : natural := 162;
        N_STREAMS             : natural := 8
    );
    port (
        clk_link            : in  std_logic;
        clk_axi             : in  std_logic;
        rst_global          : in  std_logic;
        rst_packager        : in  std_logic;
        do_zs               : in  std_logic;
        orbits_per_packet   : in  unsigned(15 downto 0);
        q_inputs_o          : out ldata(N_STREAMS-1 downto 0);
        q_cgc_o             : out ldata(N_STREAMS-1 downto 0);
        q_align_o           : out adata(N_STREAMS-1 downto 0);
        q_zs_o              : out adata(N_STREAMS-1 downto 0);
        q_align_ctrl_o      : out acontrol;
        q_zs_ctrl_o         : out acontrol;
        s_axis_c2h_tready_0 : in  std_logic;
        s_axis_c2h_tvalid_0 : out std_logic;
        s_axis_c2h_tdata_0  : out std_logic_vector(32*N_STREAMS-1 downto 0);
        s_axis_c2h_tkeep_0  : out std_logic_vector(31 downto 0);
        s_axis_c2h_tlast_0  : out std_logic
--        q                 : out adata(N_STREAMS-1 downto 0);
--        q_ctrl            : out acontrol
    );
end vcu128_scout_sim;

architecture Behavioral of vcu128_scout_sim is

    ----------------------------------------------------------------------------
    --
    -- Components
    --
    ----------------------------------------------------------------------------
    ---- 1 MB data fifo with prog full
    component fifo_generator_0
        port (
            m_aclk         : in  std_logic;
            s_aclk         : in  std_logic;
            s_aresetn      : in  std_logic;
            s_axis_tvalid  : in  std_logic;
            s_axis_tready  : out std_logic;
            s_axis_tdata   : in  std_logic_vector(255 downto 0);
            s_axis_tkeep   : in  std_logic_vector( 31 downto 0);
            s_axis_tlast   : in  std_logic;
            s_axis_tuser   : in  std_logic_vector(  1 downto 0);
            m_axis_tvalid  : out std_logic;
            m_axis_tready  : in  std_logic;
            m_axis_tdata   : out std_logic_vector(255 downto 0);
            m_axis_tkeep   : out std_logic_vector( 31 downto 0);
            m_axis_tlast   : out std_logic;
            m_axis_tuser   : out std_logic_vector(  1 downto 0);
            axis_prog_full : out std_logic
        );
    end component;

    ---- 1 MB packet FIFO same clock  
    component fifo_generator_1
        port (
            s_aclk        : in  std_logic;
            s_aresetn     : in  std_logic;
            s_axis_tvalid : in  std_logic;
            s_axis_tready : out std_logic;
            s_axis_tdata  : in  std_logic_vector(255 downto 0);
            s_axis_tkeep  : in  std_logic_vector( 31 downto 0);
            s_axis_tlast  : in  std_logic;
            s_axis_tuser  : in  std_logic_vector(  1 downto 0);
            m_axis_tvalid : out std_logic;
            m_axis_tready : in  std_logic;
            m_axis_tdata  : out std_logic_vector(255 downto 0);
            m_axis_tkeep  : out std_logic_vector( 31 downto 0);
            m_axis_tlast  : out std_logic;
            m_axis_tuser  : out std_logic_vector(  1 downto 0)
        );
    end component;

    component fifo_generator_2
        port (
            s_aclk        : in  std_logic;
            s_aresetn     : in  std_logic;
            s_axis_tvalid : in  std_logic;
            s_axis_tready : out std_logic;
            s_axis_tdata  : in  std_logic_vector(255 downto 0);
            s_axis_tkeep  : in  std_logic_vector(31 downto 0);
            s_axis_tlast  : in  std_logic;
            s_axis_tuser  : in  std_logic_vector(  1 downto 0);
            m_axis_tvalid : out std_logic;
            m_axis_tready : in  std_logic;
            m_axis_tdata  : out std_logic_vector(255 downto 0);
            m_axis_tkeep  : out std_logic_vector(31 downto 0);
            m_axis_tlast  : out std_logic;
            m_axis_tuser  : out std_logic_vector(  1 downto 0)
        );
    end component;
    
    ---- fifo to store headers
    component header_fifo
        port (
            rst    : in  std_logic                      := '0';
            wr_clk : in  std_logic                      := '0';
            rd_clk : in  std_logic                      := '0';
            din    : in  std_logic_vector(255 downto 0) := (others => '0');
            wr_en  : in  std_logic                      := '0';
            rd_en  : in  std_logic                      := '0';
            dout   : out std_logic_vector(255 downto 0) := (others => '0');
            full   : out std_logic                      := '0';
            empty  : out std_logic                      := '1';
            underflow : OUT STD_LOGIC;
            rd_data_count : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
            wr_data_count : OUT STD_LOGIC_VECTOR(5 DOWNTO 0)
        );
    end component;
    ----------------------------------------------------------------------------
    
    
    
    ----------------------------------------------------------------------------
    --
    -- Signal declaration
    --
    ----------------------------------------------------------------------------
    ---- other constants
    constant BUFFER_SIZE       : integer := 64;
    constant QUADS             : integer := 2;
    
    ---- generate_testdata signals
    type TBCounterVec is array(natural range <>) of natural range 0 to 3564;
    type TOCounterVec is array(natural range <>) of natural;
    type TEvtWordsVec is array(natural range <>) of natural range 0 to 5;
    signal ocounter : TOCounterVec(7 downto 0) := (others => 65280);
    signal bcounter : TBCounterVec(7 downto 0) := (0, 0, 0, 0, 0, 0, 0, 0);
    signal evt_word : TEvtWordsVec(7 downto 0) := (others => 0);
    
    ---- comma-gap-cleaner signals
    -- First buffer to invalidate padding words in comma gap
    -- DEPTH = 24 (1 PADDING word every 24 COMMA word in comma gap) x 8 links
    signal q_padding_buf   : TLinkBuffer(23 downto 0);
    -- Second buffer to handle special sequences of words at end of orbit
    -- DEPTH = 6 (minimum length to catch all the special sequences) x 8 links
    signal q_buffer        : TLinkBuffer( 5 downto 0);

    ---- aligner/zs/fifo_filler data signals
    signal q_inputs      : ldata(N_STREAMS-1 downto 0);
    -- signal q_gap_cleaner : ldata(N_STREAMS-1 downto 0);
    signal d_align       : ldata(N_STREAMS-1 downto 0);
    signal d_zs          : adata(N_STREAMS-1 downto 0);
    signal q_zs          : adata(N_STREAMS-1 downto 0);
    signal d_package     : adata(N_STREAMS-1 downto 0);
    
    ---- aligner/zs/fifo_filler control signals
    signal d_ctrl_zs      : acontrol;
    signal q_ctrl_zs      : acontrol;
    signal d_ctrl_package : acontrol;
    
    ---- fifo filler signals
    signal s_axis_tvalid           : std_logic;
    signal s_axis_tready           : std_logic;
    signal s_axis_tlast            : std_logic;
    signal s_axis_tdata            : std_logic_vector(32*N_STREAMS-1 downto 0);
    signal s_axis_tuser            : std_logic_vector(             1 downto 0);
    signal s_axis_tvalid_reg       : std_logic;
    signal s_axis_tlast_reg        : std_logic;
    signal s_axis_tdata_reg        : std_logic_vector(32*N_STREAMS-1 downto 0);
    signal s_axis_tuser_reg        : std_logic_vector(             1 downto 0);
    signal axi_prog_full           : std_logic;

    type t_FillerState is (FILLING, DROPPING);
    signal fillerState             : t_FillerState;

    signal d1                      : adata(N_STREAMS-1 downto 0);
    signal d_ctrl_d1, d_ctrl_d2    : acontrol;
    signal d_ctrl_d3               : acontrol;

    ---- counters
    signal s_dropped_orbit_counter : unsigned(63 downto 0);
    signal s_orbit_counter         : unsigned(63 downto 0);
    signal orbits_in_packet        : unsigned(15 downto 0);
    signal orbit_length_int        : unsigned(63 downto 0);
    signal packet_length_int       : unsigned(63 downto 0);
    signal packet_length           : unsigned(63 downto 0);
    signal s_orbit_length_cnt      : unsigned(63 downto 0);
    
    signal rst_aligner             : std_logic;
    signal s_axi_backpressure_seen : std_logic;
    signal rst_n                   : std_logic;

    ---- from first big data FIFO to small distr RAM FIFO
    signal f1tofd1_axis_tvalid : std_logic;
    signal f1tofd1_axis_tready : std_logic;
    signal f1tofd1_axis_tlast  : std_logic;
    signal f1tofd1_axis_tdata  : std_logic_vector(255 downto 0);
    signal f1tofd1_axis_tkeep  : std_logic_vector( 31 downto 0);
    signal f1tofd1_axis_tuser  : std_logic_vector(  1 downto 0);
    ---- from smal distr RAM FIFO to second big (packet) FIFO
    signal fd1tof2_axis_tvalid : std_logic;
    signal fd1tof2_axis_tready : std_logic;
    signal fd1tof2_axis_tlast  : std_logic;
    signal fd1tof2_axis_tdata  : std_logic_vector(255 downto 0);
    signal fd1tof2_axis_tkeep  : std_logic_vector( 31 downto 0);
    signal fd1tof2_axis_tuser  : std_logic_vector(  1 downto 0);
    ---- from second big (packet) FIFO to second small distr FIFO
    signal f2tofd2_axis_tvalid : std_logic;
    signal f2tofd2_axis_tready : std_logic;
    signal f2tofd2_axis_tlast  : std_logic;
    signal f2tofd2_axis_tdata  : std_logic_vector(255 downto 0);
    signal f2tofd2_axis_tkeep  : std_logic_vector( 31 downto 0);
    signal f2tofd2_axis_tuser  : std_logic_vector(  1 downto 0);
    ---- output
    signal m_axis_tvalid : std_logic;
    signal m_axis_tdata  : std_logic_vector(255 downto 0);
    signal m_axis_tkeep  : std_logic_vector( 31 downto 0);
    signal m_axis_tlast  : std_logic;
    signal m_axis_tready : std_logic := '0';
    signal m_axis_tready_d : std_logic := '0';
    signal m_axis_tuser  : std_logic_vector(  1 downto 0);
    ---- register output
    signal m_axis_tvalid_d1 : std_logic;
    signal m_axis_tdata_d1  : std_logic_vector(255 downto 0);
    signal m_axis_tkeep_d1  : std_logic_vector( 31 downto 0);
    signal m_axis_tlast_d1  : std_logic;
    signal m_axis_tready_d1 : std_logic := '0';
    signal m_axis_tuser_d1  : std_logic_vector(  1 downto 0);
    ---- register output
    signal m_axis_tvalid_d2 : std_logic;
    signal m_axis_tdata_d2  : std_logic_vector(255 downto 0);
    signal m_axis_tkeep_d2  : std_logic_vector( 31 downto 0);
    signal m_axis_tlast_d2  : std_logic;
    signal m_axis_tready_d2 : std_logic := '0';
    signal m_axis_tuser_d2  : std_logic_vector(  1 downto 0);
    ---- header fifo
    signal header_din   : std_logic_vector(255 downto 0) := (others => '0');
    signal header_wen   : std_logic                      := '0';
    signal header_ren   : std_logic                      := '0';
    signal header_dout  : std_logic_vector(255 downto 0);
    signal header_full  : std_logic;
    signal header_empty : std_logic;
    signal header_underflow : std_logic;
    signal header_rd_data_count : std_logic_vector(5 downto 0);
    signal header_wr_data_count : std_logic_vector(5 downto 0);
    
    signal packet_end   : std_logic;
    signal packet_end_d : std_logic_vector(22 downto 0);
    
    signal filler_orbit_counter              : unsigned(63 downto 0);
    signal filler_dropped_orbit_counter      : unsigned(63 downto 0);
    signal axi_backpressure_seen             : std_logic;
    signal waiting_orbit_end                 : std_logic;

    signal orbit_length                      : unsigned(63 downto 0);
    signal orbit_length_cnt                  : unsigned(63 downto 0);
    signal orbit_exceeds_size                : std_logic;
    signal enable_autorealign                : std_logic := '1';
    signal autorealign_counter               : unsigned(63 downto 0);
    
    signal filler_state : std_logic;
    
    signal s_wait_orbit_end : std_logic;
    ----------------------------------------------------------------------------

begin

    ----------------------------------------------------------------------------
    --
    -- Generate test data
    --
    ----------------------------------------------------------------------------
    create_testdata : process(clk_link)
        variable send_padding  : std_logic_vector(QUADS*4-1 downto 0);
    begin
        if clk_link'event and clk_link = '1' then
            -- Create test data now
            for i in (N_STREAMS-1) downto 0 loop  -- Number of channels
                if ((bcounter(i) +2*i) mod 4 = 0) and (evt_word(i) = 0) and (send_padding(i) = '0') then
                    send_padding(i) := '1';
                elsif evt_word(i) < 5 then
                    send_padding(i) := '0';
                    evt_word(i)     <= evt_word(i)+1;
                elsif evt_word(i) = 5 then
                    send_padding(i) := '0';
                    evt_word(i)     <= 0;
                    if bcounter(i) < GEN_ORBIT_FULL_LENGTH then
                        bcounter(i) <= bcounter(i)+1;
                    elsif bcounter(i) = GEN_ORBIT_FULL_LENGTH then
                        bcounter(i) <= 1;
                        ocounter(i) <= ocounter(i)+1;
                    else
                        bcounter(i) <= 1;
                    end if;
                else
                    send_padding(i) := '0';
                    evt_word(i)     <= 0;
                end if;

                if send_padding(i) = '1' then
                    q_inputs(i).data     <= x"F7F7F7F7";  -- Padding
                    q_inputs(i).valid    <= '1';
                    q_inputs(i).strobe   <= '0';
                    q_inputs(i).done     <= '0';
                
                elsif bcounter(i) > 0 and bcounter(i) < GEN_ORBIT_DATA_LENGTH then  -- Arbitrary obit gap
                    if evt_word(i) = 0 then
                        q_inputs(i).data <= std_logic_vector(to_unsigned(ocounter(i), 32));
                    elsif evt_word(i) = 1 then
                        q_inputs(i).data <= std_logic_vector(to_unsigned(bcounter(i), 32));
                    elsif (evt_word(i) = 2) or (evt_word(i) = 4) then
                        q_inputs(i).data <= "000000001" & "1100" & "100000000" & "0000000000";
                    elsif (evt_word(i) = 3) or (evt_word(i) = 5) then
                        q_inputs(i).data <= (others => '1');
                    else
                        q_inputs(i).data <= (others => '1');
                    end if;

                    q_inputs(i).done   <= '0';
                    q_inputs(i).valid  <= '1';
                    q_inputs(i).strobe <= '1';
                    
                elsif bcounter(i) = GEN_ORBIT_DATA_LENGTH and (evt_word(i) = 1 or evt_word(i) = 2) then
                    q_inputs(i).done   <= '0';
                    q_inputs(i).strobe <= '1';
                    q_inputs(i).valid  <= '1';
                    q_inputs(i).data   <= "10101010101010101010101010101010";  -- If I see this pattern I'm transmitting "CRCs"
                
                else
                    q_inputs(i).done   <= '0';
                    q_inputs(i).strobe <= '1';
                    q_inputs(i).valid  <= '0';
                    q_inputs(i).data   <= x"505050BC";  -- COMMA
                end if;
            end loop;

        end if;
    end process;
    
    q_inputs_o <= q_inputs;
    ----------------------------------------------------------------------------
    
    
    
    
    
    ----------------------------------------------------------------------------
    --
    -- Comma Gap Cleaner
    --
    ----------------------------------------------------------------------------
    q_padding_buf(0)(N_STREAMS-1 downto 0) <= q_inputs;

    invalidate_padding_in_comma_gap : process(clk_link)
        -- variable crcs_read : std_logic_vector(4*QUADS - 1 downto 0) := (others => '0');
    begin
        if clk_link'event and clk_link = '1' then
            q_padding_buf(q_padding_buf'high downto 1) <= q_padding_buf(q_padding_buf'high-1 downto 0);
            
            for i in q_inputs'range loop
                if  (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 2)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 3)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 4)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 5)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 6)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 7)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 8)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 9)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-10)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-11)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-12)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-13)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-14)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-15)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-16)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-17)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-18)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-19)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-20)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-21)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-22)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-23)(i).valid = '0')
                then
                    q_buffer(0)(i).data   <= q_padding_buf(q_padding_buf'high-1)(i).data;
                    q_buffer(0)(i).strobe <= q_padding_buf(q_padding_buf'high-1)(i).strobe;
                    q_buffer(0)(i).done   <= q_padding_buf(q_padding_buf'high-1)(i).done;
                    q_buffer(0)(i).valid  <= '0';
                    
                else
                    q_buffer(0)(i) <= q_padding_buf(q_padding_buf'high-1)(i);
                end if;
            end loop;

            q_buffer(q_buffer'high downto 1) <= q_buffer(q_buffer'high-1 downto 0);
            
            for i in q_inputs'range loop

                -- Invalidate padding word before COMMA -> CRC -> LID -> COMMA -> GAP
                if ((q_buffer(5)(i).valid = '1' and q_buffer(5)(i).strobe = '1') and    -- DATA word
                    (q_buffer(4)(i).valid = '1' and q_buffer(4)(i).strobe = '0') and    -- PADDING word
                    (q_buffer(3)(i).valid = '0' and q_buffer(3)(i).strobe = '1') and    -- COMMA word
                    (q_buffer(2)(i).valid = '0' and q_buffer(2)(i).strobe = '1') and    -- CRC word
                    (q_buffer(1)(i).valid = '0' and q_buffer(1)(i).strobe = '1') and    -- LID word
                    (q_buffer(0)(i).valid = '0' and q_buffer(0)(i).strobe = '1'))       -- COMMA word (before comma gap)
                then
                    -- Invalidate PADDING word (index 4) in the special sequence n°(1)
                    d_align(i).data   <= q_buffer(4)(i).data;
                    d_align(i).strobe <= q_buffer(4)(i).strobe;
                    d_align(i).done   <= q_buffer(4)(i).done;
                    d_align(i).valid  <= '0';
                else
                    d_align(i) <= q_buffer(4)(i);
                end if;
            end loop;

            
        end if;
    end process;
    
    q_cgc_o <= d_align;
    ----------------------------------------------------------------------------
    
    
    
    
    
    ----------------------------------------------------------------------------
    --
    -- Aligner
    --
    ----------------------------------------------------------------------------
    align : entity work.bx_aware_aligner
        generic map (
            NSTREAMS => N_STREAMS
        )
        port map (
            clk_wr            => clk_link,
            clk_rd            => clk_axi,
            rst               => rst_global,
            enable            => (others => '1'),
            waiting_orbit_end => waiting_orbit_end,
            d                 => d_align,
            q                 => d_zs,
            q_ctrl            => d_ctrl_zs
        );
    
    q_align_o      <= d_zs;
    q_align_ctrl_o <= d_ctrl_zs;
    ----------------------------------------------------------------------------
    
    
    
    
    
    ----------------------------------------------------------------------------
    --
    -- Autorealign controller (if misalignment is found, trigger a reset)
    --
    ----------------------------------------------------------------------------
    auto_realign_controller_1 : entity work.auto_realign_controller
        port map (
            axi_clk           => clk_axi,
            rst_in            => rst_global,
            enabled           => enable_autorealign,
            clk_aligner       => clk_link,
            rst_aligner_out   => rst_aligner,
            misalignment_seen => orbit_exceeds_size,
            autoreset_count   => autorealign_counter
        );
    ----------------------------------------------------------------------------
    
    
    
    
    
    ----------------------------------------------------------------------------
    --
    -- Zero-Suppression (ZS)
    --
    ----------------------------------------------------------------------------
    zs : entity work.zs
        generic map (
            NSTREAMS => N_STREAMS
        )
        port map (
            clk    => clk_axi,
            rst    => rst_global,
            d      => d_zs,
            d_ctrl => d_ctrl_zs,
            q      => q_zs,
            q_ctrl => q_ctrl_zs
        );
    
    d_package      <= q_zs      when do_zs = '1' else d_zs;
    d_ctrl_package <= q_ctrl_zs when do_zs = '1' else d_ctrl_zs;
    
    q_zs_o      <= d_package;
    q_zs_ctrl_o <= d_ctrl_package;
    ----------------------------------------------------------------------------
    
    
    
    
    
    ----------------------------------------------------------------------------
    --
    -- Fifo-Filler
    --
    ----------------------------------------------------------------------------
    fifo_filler : entity work.fifo_filler
        generic map (
            NSTREAMS => N_STREAMS
        )
        port map (
            d_clk                  => clk_axi,
            rst                    => rst_global,
            orbits_per_packet      => unsigned(orbits_per_packet),
            d                      => d_package,
            d_ctrl                 => d_ctrl_package,
            m_aclk                 => clk_axi,
            m_axis_tvalid          => s_axis_c2h_tvalid_0,
            m_axis_tready          => s_axis_c2h_tready_0,
            m_axis_tdata           => s_axis_c2h_tdata_0,
            m_axis_tkeep           => s_axis_c2h_tkeep_0,
            m_axis_tlast           => s_axis_c2h_tlast_0,
            dropped_orbit_counter  => filler_dropped_orbit_counter,
            orbit_counter          => filler_orbit_counter,
            axi_backpressure_seen  => axi_backpressure_seen,
            orbit_length           => orbit_length,
            orbit_length_cnt       => orbit_length_cnt,
            orbit_exceeds_size     => orbit_exceeds_size,
            in_autorealign_counter => autorealign_counter
        );
    ----------------------------------------------------------------------------

end Behavioral;
