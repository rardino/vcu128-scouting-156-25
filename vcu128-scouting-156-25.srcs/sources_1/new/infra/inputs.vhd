-- inputs
--
-- Optical serial inputs to 40 MHz scouting demonstrator
--
-- D. R. May 2018
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

use work.constants.all;
use work.datatypes.all;

entity inputs is
    generic (
        ORBIT        : natural := ORBIT_LENGTH;
        BYPASS_LINKS : boolean := false;
        QUADS        : natural := N_REGION
    );
    port (
        clk_axi        : in  std_logic;
        rst            : in  std_logic;
        
        GEN_ORBIT_FULL_LENGTH : integer := ORBIT;
        GEN_ORBIT_DATA_LENGTH : integer := 156;
        

        -- Serial data ports for transceiver channel 0
        ch0_gtyrxn_in  : in  std_logic;
        ch0_gtyrxp_in  : in  std_logic;
        ch0_gtytxn_out : out std_logic;
        ch0_gtytxp_out : out std_logic;

        -- Serial data ports for transceiver channel 1
        ch1_gtyrxn_in  : in  std_logic;
        ch1_gtyrxp_in  : in  std_logic;
        ch1_gtytxn_out : out std_logic;
        ch1_gtytxp_out : out std_logic;

        -- Serial data ports for transceiver channel 2
        ch2_gtyrxn_in  : in  std_logic;
        ch2_gtyrxp_in  : in  std_logic;
        ch2_gtytxn_out : out std_logic;
        ch2_gtytxp_out : out std_logic;

        -- Serial data ports for transceiver channel 3
        ch3_gtyrxn_in  : in  std_logic;
        ch3_gtyrxp_in  : in  std_logic;
        ch3_gtytxn_out : out std_logic;
        ch3_gtytxp_out : out std_logic;

        -- Serial data ports for transceiver channel 4
        ch4_gtyrxn_in  : in  std_logic;
        ch4_gtyrxp_in  : in  std_logic;
        ch4_gtytxn_out : out std_logic;
        ch4_gtytxp_out : out std_logic;

        -- Serial data ports for transceiver channel 5
        ch5_gtyrxn_in  : in  std_logic;
        ch5_gtyrxp_in  : in  std_logic;
        ch5_gtytxn_out : out std_logic;
        ch5_gtytxp_out : out std_logic;

        -- Serial data ports for transceiver channel 6
        ch6_gtyrxn_in  : in  std_logic;
        ch6_gtyrxp_in  : in  std_logic;
        ch6_gtytxn_out : out std_logic;
        ch6_gtytxp_out : out std_logic;

        -- Serial data ports for transceiver channel 7
        ch7_gtyrxn_in  : in  std_logic;
        ch7_gtyrxp_in  : in  std_logic;
        ch7_gtytxn_out : out std_logic;
        ch7_gtytxp_out : out std_logic;

        -- mgtrefclk0                                 : in  std_logic;
        mgtrefclk0_x0y11_int                       : in  std_logic;
        -- mgtrefclk0_x0y10_int                       : in  std_logic;
        clk_freerun                                : in  std_logic;
        clk_freerun_buf                            : out std_logic;
        
--        init_done_int                              : out std_logic_vector(0 downto 0);
--        init_retry_ctr_int                         : out std_logic_vector(3 downto 0);
--        gtpowergood_vio_sync                       : out std_logic_vector(4*QUADS - 1 downto 0);
--        txpmaresetdone_vio_sync                    : out std_logic_vector(4*QUADS - 1 downto 0);
--        rxpmaresetdone_vio_sync                    : out std_logic_vector(4*QUADS - 1 downto 0);
--        gtwiz_reset_tx_done_vio_sync               : out std_logic_vector(0 downto 0);
--        gtwiz_reset_rx_done_vio_sync               : out std_logic_vector(0 downto 0);
--        hb_gtwiz_reset_all_vio_int                 : in  std_logic_vector(0 downto 0);
--        hb0_gtwiz_reset_tx_pll_and_datapath_int    : in  std_logic_vector(0 downto 0);
--        hb0_gtwiz_reset_tx_datapath_int            : in  std_logic_vector(0 downto 0);
--        hb_gtwiz_reset_rx_pll_and_datapath_vio_int : in  std_logic_vector(0 downto 0);
--        hb_gtwiz_reset_rx_datapath_vio_int         : in  std_logic_vector(0 downto 0);
--        link_down_latched_reset_vio_int            : in  std_logic_vector(0 downto 0);

        clk                                        : out std_logic;
        q                                          : out ldata(4*QUADS - 1 downto 0);
        oCommaDet                                  : out std_logic_vector(4*QUADS - 1 downto 0);

        oLinkData : out std_logic_vector(8 * 32 - 1 downto 0);

        cdr_stable  : out std_logic_vector(0 downto 0);
--        rxctrl0_out : out std_logic_vector(63 downto 0);
--        rxctrl1_out : out std_logic_vector(63 downto 0);
--        rxctrl2_out : out std_logic_vector(31 downto 0);
--        rxctrl3_out : out std_logic_vector(31 downto 0);
        rxctrl0_out : out std_logic_vector(127 downto 0);
        rxctrl1_out : out std_logic_vector(127 downto 0);
        rxctrl2_out : out std_logic_vector( 63 downto 0);
        rxctrl3_out : out std_logic_vector( 63 downto 0);
        
        oRxbyteisaligned : out std_logic_vector(4*QUADS - 1 downto 0);
        
        rx_buf_status_out                   : out std_logic_vector(4*3*QUADS - 1 downto 0);

        hb_gtwiz_reset_rx_datapath_out      : out std_logic_vector(0 downto 0);
        hb_gtwiz_reset_all_out              : out std_logic_vector(0 downto 0);
        hb_gtwiz_reset_all_init_out         : out std_logic_vector(0 downto 0);
        hb_gtwiz_reset_rx_datapath_init_out : out std_logic_vector(0 downto 0);
        gtwiz_reset_tx_pll_and_datapath_out : out std_logic_vector(0 downto 0);
        gtwiz_reset_tx_datapath_out         : out std_logic_vector(0 downto 0);
        gtwiz_reset_rx_pll_and_datapath_out : out std_logic_vector(0 downto 0);
        gtwiz_reset_rx_datapath_out         : out std_logic_vector(0 downto 0);
        gtwiz_reset_tx_done_out             : out std_logic_vector(0 downto 0);
        gtwiz_reset_rx_done_out             : out std_logic_vector(0 downto 0);
        gtwiz_userclk_tx_reset_out          : out std_logic_vector(0 downto 0);
        gtwiz_userclk_rx_reset_out          : out std_logic_vector(0 downto 0);

        txpmaresetdone_out                  : out std_logic_vector(4*QUADS - 1 downto 0);
        rxpmaresetdone_out                  : out std_logic_vector(4*QUADS - 1 downto 0);
        
        gtpowergood_out                     : out std_logic_vector(4*QUADS - 1 downto 0);
        init_done_out                       : out std_logic_vector(0 downto 0);
        
        
        
        
        ifcomma   : out std_logic_vector(4*QUADS - 1 downto 0);
        ifpadding : out std_logic_vector(4*QUADS - 1 downto 0);
        ifinvalid : out std_logic_vector(4*QUADS - 1 downto 0);
        ifdata    : out std_logic_vector(4*QUADS - 1 downto 0);
        ifelse    : out std_logic_vector(4*QUADS - 1 downto 0);
        
        cdr_stable_o : out std_logic_vector(1 downto 0);
        cnt_o        : out std_logic_vector(12 downto 0);
        en_cnt_o     : out std_logic;
        cdr_inhibit_o: out std_logic
        
        -- bx_cnt      : out TBCounterVec(7 downto 0)
    );
end inputs;

architecture Behavioral of inputs is

    signal rx_data : std_logic_vector(4*QUADS * 32 - 1 downto 0);
    signal tx_data : std_logic_vector(4*QUADS * 32 - 1 downto 0);
    signal usr_clk : std_logic;

    --R
--    type TBCounterVec is array(natural range <>) of natural range 0 to 3564;
--    type TOCounterVec is array(natural range <>) of natural;
--    type TEvtWordsVec is array(natural range <>) of natural range 0 to 5;
    signal ocounter : TOCounterVec(7 downto 0) := (others => 65280);
    signal bcounter : TBCounterVec(7 downto 0) := (0, 0, 0, 0, 0, 0, 0, 0);
    signal evt_word : TEvtWordsVec(7 downto 0) := (others => 0);

    signal sCommaDet : std_logic_vector(4*QUADS - 1 downto 0);
    signal sCharisk  : std_logic_vector(63 downto 0);

    signal rxctrl0_int : std_logic_vector(QUADS * 64 - 1 downto 0);
    signal rxctrl1_int : std_logic_vector(QUADS * 64 - 1 downto 0);
    signal rxctrl2_int : std_logic_vector(QUADS * 32 - 1 downto 0);
    signal rxctrl3_int : std_logic_vector(QUADS * 32 - 1 downto 0);
    
    signal sRxbyteisaligned : std_logic_vector(QUADS * 4 - 1 downto 0);
    
    -- DEBUG
    signal s_ifcomma   : std_logic_vector(4*QUADS - 1 downto 0);
    signal s_ifpadding : std_logic_vector(4*QUADS - 1 downto 0);
    signal s_ifinvalid : std_logic_vector(4*QUADS - 1 downto 0);
    signal s_ifdata    : std_logic_vector(4*QUADS - 1 downto 0);
    signal s_ifelse    : std_logic_vector(4*QUADS - 1 downto 0);
    
--    signal rx_buf_status_int                   : std_logic_vector(4*3*QUADS - 1 downto 0);
                                    
--    signal hb_gtwiz_reset_rx_datapath_int      : std_logic_vector(0 downto 0);
--    signal hb_gtwiz_reset_all_int              : std_logic_vector(0 downto 0);
--    signal hb_gtwiz_reset_all_init_int         : std_logic_vector(0 downto 0);
--    signal hb_gtwiz_reset_rx_datapath_init_int : std_logic_vector(0 downto 0);
--    signal gtwiz_reset_tx_pll_and_datapath_int : std_logic_vector(0 downto 0);
--    signal gtwiz_reset_tx_datapath_int         : std_logic_vector(0 downto 0);
--    signal gtwiz_reset_rx_pll_and_datapath_int : std_logic_vector(0 downto 0);
--    signal gtwiz_reset_rx_datapath_int         : std_logic_vector(0 downto 0);
--    signal gtwiz_reset_tx_done_int             : std_logic_vector(0 downto 0);
--    signal gtwiz_reset_rx_done_int             : std_logic_vector(0 downto 0);
--    signal gtwiz_userclk_tx_reset_int          : std_logic_vector(0 downto 0);
--    signal gtwiz_userclk_rx_reset_int          : std_logic_vector(0 downto 0);
                                        
--    signal txpmaresetdone_int                  : std_logic_vector(4*QUADS - 1 downto 0);
--    signal rxpmaresetdone_int                  : std_logic_vector(4*QUADS - 1 downto 0);
    
    signal s_cdr_stable   : std_logic;
    signal s_cdr_stable_d : std_logic;
    signal cdr_inhibit    : std_logic := '0';
    signal cdr_stable_out : std_logic;
    signal en_cnt         : std_logic := '0';
    
    shared variable cnt : integer := 0;

begin

    gty_wrapper : entity work.gtwizard_ultrascale_0_example_top
        port map (
            -- Differential reference clock inputs
            -- mgtrefclk0 => mgtrefclk0,
            mgtrefclk0_x0y11_int => mgtrefclk0_x0y11_int,
            -- mgtrefclk0_x0y10_int => mgtrefclk0_x0y10_int,
   
            -- Serial data ports for transceiver channel 0
            ch0_gtyrxn_in  => ch0_gtyrxn_in,
            ch0_gtyrxp_in  => ch0_gtyrxp_in,
            ch0_gtytxn_out => ch0_gtytxn_out,
            ch0_gtytxp_out => ch0_gtytxp_out,
   
            -- Serial data ports for transceiver channel 1
            ch1_gtyrxn_in  => ch1_gtyrxn_in,
            ch1_gtyrxp_in  => ch1_gtyrxp_in,
            ch1_gtytxn_out => ch1_gtytxn_out,
            ch1_gtytxp_out => ch1_gtytxp_out,
   
            -- Serial data ports for transceiver channel 2
            ch2_gtyrxn_in  => ch2_gtyrxn_in,
            ch2_gtyrxp_in  => ch2_gtyrxp_in,
            ch2_gtytxn_out => ch2_gtytxn_out,
            ch2_gtytxp_out => ch2_gtytxp_out,
   
            -- Serial data ports for transceiver channel 3
            ch3_gtyrxn_in  => ch3_gtyrxn_in,
            ch3_gtyrxp_in  => ch3_gtyrxp_in,
            ch3_gtytxn_out => ch3_gtytxn_out,
            ch3_gtytxp_out => ch3_gtytxp_out,
   
            -- Serial data ports for transceiver channel 4
            ch4_gtyrxn_in  => ch4_gtyrxn_in,
            ch4_gtyrxp_in  => ch4_gtyrxp_in,
            ch4_gtytxn_out => ch4_gtytxn_out,
            ch4_gtytxp_out => ch4_gtytxp_out,
   
            -- Serial data ports for transceiver channel 5
            ch5_gtyrxn_in  => ch5_gtyrxn_in,
            ch5_gtyrxp_in  => ch5_gtyrxp_in,
            ch5_gtytxn_out => ch5_gtytxn_out,
            ch5_gtytxp_out => ch5_gtytxp_out,
   
            -- Serial data ports for transceiver channel 6
            ch6_gtyrxn_in  => ch6_gtyrxn_in,
            ch6_gtyrxp_in  => ch6_gtyrxp_in,
            ch6_gtytxn_out => ch6_gtytxn_out,
            ch6_gtytxp_out => ch6_gtytxp_out,
   
            -- Serial data ports for transceiver channel 7
            ch7_gtyrxn_in  => ch7_gtyrxn_in,
            ch7_gtyrxp_in  => ch7_gtyrxp_in,
            ch7_gtytxn_out => ch7_gtytxn_out,
            ch7_gtytxp_out => ch7_gtytxp_out,
            
            -- reset helper block(s)
            hb_gtwiz_reset_clk_freerun_in       => clk_freerun,
            hb_gtwiz_reset_all_in               => rst,
            link_down_latched_reset_in          => rst,
            gtwiz_userdata_rx_out               => rx_data(4*QUADS * 32 - 1 downto 0),
            
            gtwiz_userclk_rx_usrclk_out         => usr_clk,
            
            rxctrl0_out                         => rxctrl0_int, -- If RXCTRL3 is low: Corresponding rxdata byte is a KWORD
            rxctrl1_out                         => rxctrl1_int,
            rxctrl2_out                         => rxctrl2_int, -- Corresponding byte is a comma
            rxctrl3_out                         => rxctrl3_int, -- Can't decode the word
            
            rxbyteisaligned_out                 => sRxbyteisaligned,
            gtwiz_reset_rx_cdr_stable_out(0)    => cdr_stable_out,
            
            rx_buf_status_out                   => rx_buf_status_out,
            
            hb_gtwiz_reset_rx_datapath_out      => hb_gtwiz_reset_rx_datapath_out,
            hb_gtwiz_reset_all_out              => hb_gtwiz_reset_all_out,
            hb_gtwiz_reset_all_init_out         => hb_gtwiz_reset_all_init_out,
            hb_gtwiz_reset_rx_datapath_init_out => hb_gtwiz_reset_rx_datapath_init_out,
            gtwiz_reset_tx_pll_and_datapath_out => gtwiz_reset_tx_pll_and_datapath_out,
            gtwiz_reset_tx_datapath_out         => gtwiz_reset_tx_datapath_out,
            gtwiz_reset_rx_pll_and_datapath_out => gtwiz_reset_rx_pll_and_datapath_out,
            gtwiz_reset_rx_datapath_out         => gtwiz_reset_rx_datapath_out,
            gtwiz_reset_tx_done_out             => gtwiz_reset_tx_done_out,
            gtwiz_reset_rx_done_out             => gtwiz_reset_rx_done_out,
            gtwiz_userclk_tx_reset_out          => gtwiz_userclk_tx_reset_out,
            gtwiz_userclk_rx_reset_out          => gtwiz_userclk_rx_reset_out,
   
            txpmaresetdone_out                  => txpmaresetdone_out,
            rxpmaresetdone_out                  => rxpmaresetdone_out,
            
            gtpowergood_out                     => gtpowergood_out,
            init_done_out                       => init_done_out
        );



    clk           <= usr_clk;
    oCommaDet     <= sCommaDet;
    cdr_stable(0) <= cdr_stable_out;



    data_assignment : process(rx_data, rxctrl0_int, rxctrl2_int, rxctrl3_int, sRxbyteisaligned)
    begin
        for i in 0 to 4*QUADS-1 loop  -- TODO: This needs to be made aware of the number of quads.
            -- if BYPASS_LINKS = false then
            q(i).data <= rx_data(i*32 + 31 downto i*32);
            q(i).done <= '0';
            
            -- Comma word (0x505050BC)
            if rxctrl3_int(i*8) = '0' and rxctrl0_int(i*16) = '1' and rxctrl2_int(i*8) = '1' and sRxbyteisaligned = x"FF" then
                sCommaDet(i) <= '1';
                q(i).valid   <= '0';
                q(i).strobe  <= '1';
                -- DEBUG
                s_ifcomma(i)   <= '1';
                s_ifpadding(i) <= '0';
                s_ifinvalid(i) <= '0';
                s_ifdata(i)    <= '0';
                s_ifelse(i)    <= '0';
            
            -- Padding word (0xF7F7F7F7)
            elsif rxctrl3_int(i*8 + 3 downto i*8) = "0000" and rxctrl0_int(i*16 + 3 downto i*16) = "1111" and sRxbyteisaligned = x"FF" then
                sCommaDet(i) <= '0';
                q(i).valid   <= '1';
                q(i).strobe  <= '0';
                -- DEBUG
                s_ifcomma(i)   <= '0';
                s_ifpadding(i) <= '1';
                s_ifinvalid(i) <= '0';
                s_ifdata(i)    <= '0';
                s_ifelse(i)    <= '0';
            
            -- Invalid word
            elsif rxctrl3_int(i*8 + 3 downto i*8) = "1111" and rxctrl0_int(i*16 + 3 downto i*16) = "0000" then
                sCommaDet(i) <= '0';
                q(i).valid   <= '0';
                q(i).strobe  <= '0';
                -- DEBUG
                s_ifcomma(i)   <= '0';
                s_ifpadding(i) <= '0';
                s_ifinvalid(i) <= '1';
                s_ifdata(i)    <= '0';
                s_ifelse(i)    <= '0';
            
            -- Data word
            elsif rxctrl3_int(i*8 + 3 downto i*8) = "0000" and rxctrl0_int(i*16 + 3 downto i*16) = "0000" and rxctrl2_int(i*8) = '0' and sRxbyteisaligned = x"FF" then
                sCommaDet(i) <= '0';
                q(i).valid   <= '1';
                q(i).strobe  <= '1';
                -- DEBUG
                s_ifcomma(i)   <= '0';
                s_ifpadding(i) <= '0';
                s_ifinvalid(i) <= '0';
                s_ifdata(i)    <= '1';
                s_ifelse(i)    <= '0';
            
            -- Others
            else
                sCommaDet(i) <= '0';
                -- q(i).valid   <= '1';
                -- q(i).strobe  <= '1';
                q(i).valid   <= '0';
                q(i).strobe  <= '0';
                -- DEBUG
                s_ifcomma(i)   <= '0';
                s_ifpadding(i) <= '0';
                s_ifinvalid(i) <= '0';
                s_ifdata(i)    <= '0';
                s_ifelse(i)    <= '1';
            end if;
        end loop;
    end process;
    oLinkData(4*QUADS*32 - 1 downto 0) <= rx_data;
    
    rxctrl0_out <= rxctrl0_int;
    rxctrl1_out <= rxctrl1_int;
    rxctrl2_out <= rxctrl2_int;
    rxctrl3_out <= rxctrl3_int;
    
    oRxbyteisaligned <= sRxbyteisaligned;
    
    ifcomma   <= s_ifcomma;
    ifpadding <= s_ifpadding;
    ifinvalid <= s_ifinvalid;
    ifdata    <= s_ifdata;
    ifelse    <= s_ifelse;
    
    
    cdr_stable_o(0) <= s_cdr_stable;
    cdr_stable_o(1) <= s_cdr_stable_d;
    cnt_o           <= std_logic_vector(to_unsigned(cnt,13));
    en_cnt_o        <= en_cnt;
    cdr_inhibit_o   <= cdr_inhibit;

end Behavioral;
