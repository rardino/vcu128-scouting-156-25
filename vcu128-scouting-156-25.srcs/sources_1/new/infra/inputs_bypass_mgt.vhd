-- inputs
--
-- Optical serial inputs to 40 MHz scouting demonstrator
--
-- D. R. May 2018
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

use work.constants.all;
use work.datatypes.all;

entity inputs_bypass_mgt is
    generic (
        ORBIT        : natural := ORBIT_LENGTH;
        BYPASS_LINKS : boolean := false;
        QUADS        : natural := N_REGION
    );
    port (
        clk_axi        : in  std_logic;
        rst            : in  std_logic;
        
        GEN_ORBIT_FULL_LENGTH : integer := ORBIT;
        GEN_ORBIT_DATA_LENGTH : integer := 156;

        -- mgtrefclk0                                 : in  std_logic;
        mgtrefclk0_x0y11_int                       : in  std_logic;
        -- mgtrefclk0_x0y10_int                       : in  std_logic;
        clk_freerun                                : in  std_logic;

        clk                                        : out std_logic;
        q                                          : out ldata(4*QUADS - 1 downto 0);
        oCommaDet                                  : out std_logic_vector(4*QUADS - 1 downto 0);

        oLinkData : out std_logic_vector(8 * 32 - 1 downto 0);

        cdr_stable  : out std_logic_vector(0 downto 0);
        
        rxctrl0_out : out std_logic_vector(127 downto 0);
        rxctrl1_out : out std_logic_vector(127 downto 0);
        rxctrl2_out : out std_logic_vector( 63 downto 0);
        rxctrl3_out : out std_logic_vector( 63 downto 0);
        
        oRxbyteisaligned : out std_logic_vector(4*QUADS - 1 downto 0);
        
        rx_buf_status_out                   : out std_logic_vector(4*3*QUADS - 1 downto 0);

        hb_gtwiz_reset_rx_datapath_out      : out std_logic_vector(0 downto 0);
        hb_gtwiz_reset_all_out              : out std_logic_vector(0 downto 0);
        hb_gtwiz_reset_all_init_out         : out std_logic_vector(0 downto 0);
        hb_gtwiz_reset_rx_datapath_init_out : out std_logic_vector(0 downto 0);
        gtwiz_reset_tx_pll_and_datapath_out : out std_logic_vector(0 downto 0);
        gtwiz_reset_tx_datapath_out         : out std_logic_vector(0 downto 0);
        gtwiz_reset_rx_pll_and_datapath_out : out std_logic_vector(0 downto 0);
        gtwiz_reset_rx_datapath_out         : out std_logic_vector(0 downto 0);
        gtwiz_reset_tx_done_out             : out std_logic_vector(0 downto 0);
        gtwiz_reset_rx_done_out             : out std_logic_vector(0 downto 0);
        gtwiz_userclk_tx_reset_out          : out std_logic_vector(0 downto 0);
        gtwiz_userclk_rx_reset_out          : out std_logic_vector(0 downto 0);

        txpmaresetdone_out                  : out std_logic_vector(4*QUADS - 1 downto 0);
        rxpmaresetdone_out                  : out std_logic_vector(4*QUADS - 1 downto 0);
        
        gtpowergood_out                     : out std_logic_vector(4*QUADS - 1 downto 0);
        init_done_out                       : out std_logic_vector(0 downto 0);
        
        
        
        
        ifcomma   : out std_logic_vector(4*QUADS - 1 downto 0);
        ifpadding : out std_logic_vector(4*QUADS - 1 downto 0);
        ifinvalid : out std_logic_vector(4*QUADS - 1 downto 0);
        ifdata    : out std_logic_vector(4*QUADS - 1 downto 0);
        ifelse    : out std_logic_vector(4*QUADS - 1 downto 0);
        
        cdr_stable_o : out std_logic_vector(1 downto 0);
        cnt_o        : out std_logic_vector(12 downto 0);
        en_cnt_o     : out std_logic;
        cdr_inhibit_o: out std_logic
        
        -- bx_cnt      : out TBCounterVec(7 downto 0)
    );
end inputs_bypass_mgt;

architecture Behavioral of inputs_bypass_mgt is

    signal rx_data : std_logic_vector(4*QUADS * 32 - 1 downto 0);
    signal tx_data : std_logic_vector(4*QUADS * 32 - 1 downto 0);
    signal usr_clk : std_logic;

    --R
--    type TBCounterVec is array(natural range <>) of natural range 0 to 3564;
--    type TOCounterVec is array(natural range <>) of natural;
--    type TEvtWordsVec is array(natural range <>) of natural range 0 to 5;
    signal ocounter : TOCounterVec(7 downto 0) := (others => 65280);
    signal bcounter : TBCounterVec(7 downto 0) := (0, 0, 0, 0, 0, 0, 0, 0);
    signal evt_word : TEvtWordsVec(7 downto 0) := (others => 0);

    signal sCommaDet : std_logic_vector(4*QUADS - 1 downto 0);
    signal q_int     : ldata(7 downto 0);
    signal sCharisk  : std_logic_vector(63 downto 0);

    signal rxctrl0_int : std_logic_vector(QUADS * 64 - 1 downto 0);
    signal rxctrl1_int : std_logic_vector(QUADS * 64 - 1 downto 0);
    signal rxctrl2_int : std_logic_vector(QUADS * 32 - 1 downto 0);
    signal rxctrl3_int : std_logic_vector(QUADS * 32 - 1 downto 0);
    
    signal sRxbyteisaligned : std_logic_vector(QUADS * 4 - 1 downto 0);
    
    -- DEBUG
    signal s_ifcomma   : std_logic_vector(4*QUADS - 1 downto 0);
    signal s_ifpadding : std_logic_vector(4*QUADS - 1 downto 0);
    signal s_ifinvalid : std_logic_vector(4*QUADS - 1 downto 0);
    signal s_ifdata    : std_logic_vector(4*QUADS - 1 downto 0);
    signal s_ifelse    : std_logic_vector(4*QUADS - 1 downto 0);
    
    signal s_cdr_stable   : std_logic;
    signal s_cdr_stable_d : std_logic;
    signal cdr_inhibit    : std_logic := '0';
    signal cdr_stable_out : std_logic;
    signal en_cnt         : std_logic := '0';
    
    shared variable cnt : integer := 0;

begin

    clk           <= usr_clk;
    oCommaDet     <= sCommaDet;
    cdr_stable(0) <= cdr_stable_out;
    
    
    
    bypass_mgt : if BYPASS_LINKS = true generate
        usr_clk <= clk_axi;
    end generate;



    data_assignment : process(q_int)
    begin
        for i in 0 to 4*QUADS-1 loop  -- TODO: This needs to be made aware of the number of quads.
            if BYPASS_LINKS = true then
                q(i) <= q_int(i);
            else
            -- if BYPASS_LINKS = false then
                q(i).data <= rx_data(i*32 + 31 downto i*32);
                q(i).done <= '0';
                
                -- Comma word (0x505050BC)
                if rxctrl3_int(i*8) = '0' and rxctrl0_int(i*16) = '1' and rxctrl2_int(i*8) = '1' and sRxbyteisaligned = x"FF" then
                    sCommaDet(i) <= '1';
                    q(i).valid   <= '0';
                    q(i).strobe  <= '1';
                    -- DEBUG
                    s_ifcomma(i)   <= '1';
                    s_ifpadding(i) <= '0';
                    s_ifinvalid(i) <= '0';
                    s_ifdata(i)    <= '0';
                    s_ifelse(i)    <= '0';
                
                -- Padding word (0xF7F7F7F7)
                elsif rxctrl3_int(i*8 + 3 downto i*8) = "0000" and rxctrl0_int(i*16 + 3 downto i*16) = "1111" and sRxbyteisaligned = x"FF" then
                    sCommaDet(i) <= '0';
                    q(i).valid   <= '1';
                    q(i).strobe  <= '0';
                    -- DEBUG
                    s_ifcomma(i)   <= '0';
                    s_ifpadding(i) <= '1';
                    s_ifinvalid(i) <= '0';
                    s_ifdata(i)    <= '0';
                    s_ifelse(i)    <= '0';
                
                -- Invalid word
                elsif rxctrl3_int(i*8 + 3 downto i*8) = "1111" and rxctrl0_int(i*16 + 3 downto i*16) = "0000" then
                    sCommaDet(i) <= '0';
                    q(i).valid   <= '0';
                    q(i).strobe  <= '0';
                    -- DEBUG
                    s_ifcomma(i)   <= '0';
                    s_ifpadding(i) <= '0';
                    s_ifinvalid(i) <= '1';
                    s_ifdata(i)    <= '0';
                    s_ifelse(i)    <= '0';
                
                -- Data word
                elsif rxctrl3_int(i*8 + 3 downto i*8) = "0000" and rxctrl0_int(i*16 + 3 downto i*16) = "0000" and rxctrl2_int(i*8) = '0' and sRxbyteisaligned = x"FF" then
                    sCommaDet(i) <= '0';
                    q(i).valid   <= '1';
                    q(i).strobe  <= '1';
                    -- DEBUG
                    s_ifcomma(i)   <= '0';
                    s_ifpadding(i) <= '0';
                    s_ifinvalid(i) <= '0';
                    s_ifdata(i)    <= '1';
                    s_ifelse(i)    <= '0';
                
                -- Others
                else
                    sCommaDet(i) <= '0';
                    -- q(i).valid   <= '1';
                    -- q(i).strobe  <= '1';
                    q(i).valid   <= '0';
                    q(i).strobe  <= '0';
                    -- DEBUG
                    s_ifcomma(i)   <= '0';
                    s_ifpadding(i) <= '0';
                    s_ifinvalid(i) <= '0';
                    s_ifdata(i)    <= '0';
                    s_ifelse(i)    <= '1';
                end if;
            end if;
        end loop;
    end process;
    oLinkData(4*QUADS*32 - 1 downto 0) <= rx_data;
    
    rxctrl0_out <= rxctrl0_int;
    rxctrl1_out <= rxctrl1_int;
    rxctrl2_out <= rxctrl2_int;
    rxctrl3_out <= rxctrl3_int;
    
    oRxbyteisaligned <= sRxbyteisaligned;
    
    ifcomma   <= s_ifcomma;
    ifpadding <= s_ifpadding;
    ifinvalid <= s_ifinvalid;
    ifdata    <= s_ifdata;
    ifelse    <= s_ifelse;
    
    
    cdr_stable_o(0) <= s_cdr_stable;
    cdr_stable_o(1) <= s_cdr_stable_d;
    cnt_o           <= std_logic_vector(to_unsigned(cnt,13));
    en_cnt_o        <= en_cnt;
    cdr_inhibit_o   <= cdr_inhibit;





    generate_testdata : process(usr_clk)
        variable send_padding  : std_logic_vector(QUADS*4-1 downto 0);
    begin
        if usr_clk'event and usr_clk = '1' then
            -- Create test data now
            for i in QUADS*4-1 downto 0 loop  -- Number of channels
                if ((bcounter(i)+2*i) mod 4 = 0) and (evt_word(i) = 0) and (send_padding(i) = '0') then
                    send_padding(i) := '1';
                elsif evt_word(i) < 5 then
                    send_padding(i) := '0';
                    evt_word(i)     <= evt_word(i)+1;
                elsif evt_word(i) = 5 then
                    send_padding(i) := '0';
                    evt_word(i)     <= 0;
                    if bcounter(i) < GEN_ORBIT_FULL_LENGTH then
                        bcounter(i) <= bcounter(i)+1;
                    elsif bcounter(i) = GEN_ORBIT_FULL_LENGTH then
                        bcounter(i) <= 1;
                        ocounter(i) <= ocounter(i)+1;
                    else
                        bcounter(i) <= 1;
                    end if;
                else
                    send_padding(i) := '0';
                    evt_word(i)     <= 0;
                end if;

                if send_padding(i) = '1' then
                    q_int(i).data                  <= x"F7F7F7F7";  -- Padding
                    q_int(i).valid                 <= '1';
                    q_int(i).strobe                <= '0';
                    q_int(i).done                  <= '0';
                    tx_data(i*32 + 31 downto i*32) <= x"F7F7F7F7";  -- Padding
                    sCharisk(i*8 + 7 downto i*8)   <= "00001111";
                
                elsif bcounter(i) > 0 and bcounter(i) < GEN_ORBIT_DATA_LENGTH then  -- Arbitrary orbit gap
                    if evt_word(i) = 0 then
                        tx_data(i*32 + 31 downto i*32) <= std_logic_vector(to_unsigned(bcounter(i), 32));
                        q_int(i).data                  <= std_logic_vector(to_unsigned(bcounter(i), 32));
                    elsif evt_word(i) = 1 then
                        tx_data(i*32 + 31 downto i*32) <= std_logic_vector(to_unsigned(ocounter(i), 32));
                        q_int(i).data                  <= std_logic_vector(to_unsigned(ocounter(i), 32));
                    elsif (evt_word(i) = 2) or (evt_word(i) = 4) then
                        tx_data(i*32 + 31 downto i*32) <= "000000001" & "1100" & "100000000" & "0000000000";
                        q_int(i).data                  <= "000000001" & "1100" & "100000000" & "0000000000";
                    elsif (evt_word(i) = 3) or (evt_word(i) = 5) then
                        tx_data(i*32 + 31 downto i*32) <= (others => '1');
                        q_int(i).data                  <= (others => '1');
                    else
                        tx_data(i*32 + 31 downto i*32) <= (others => '1');
                        q_int(i).data                  <= (others => '1');
                    end if;

                    q_int(i).done                <= '0';
                    q_int(i).valid               <= '1';
                    q_int(i).strobe              <= '1';
                    sCharisk(i*8 + 7 downto i*8) <= "00000000";
                elsif bcounter(i) = GEN_ORBIT_DATA_LENGTH and (evt_word(i) = 1 or evt_word(i) = 2) then
                    q_int(i).done                  <= '0';
                    q_int(i).strobe                <= '1';
                    q_int(i).valid                 <= '1';
                    q_int(i).data                  <= "10101010101010101010101010101010";  -- If I see this pattern I'm transmitting "CRCs".
                    tx_data(i*32 + 31 downto i*32) <= "10101010101010101010101010101010";
                    sCharisk(i*8 + 7 downto i*8)   <= "00000000";
                else
                    q_int(i).done                  <= '0';
                    q_int(i).strobe                <= '1';
                    q_int(i).valid                 <= '0';
                    q_int(i).data                  <= x"505050BC";  -- COMMA
                    tx_data(i*32 + 31 downto i*32) <= x"505050BC";  -- COMMA
                    sCharisk(i*8 + 7 downto i*8)   <= "00000001";
                end if;
            end loop;

        end if;
    end process;

end Behavioral;
