----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/11/2018 11:35:55 AM
-- Design Name: 
-- Module Name: fifo_filler_header - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use work.datatypes.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fifo_filler_header is
    generic (
        NSTREAMS : integer := 8
    );
    port (
        d_clk : in std_logic;
        rst   : in std_logic;

        orbits_per_packet : in unsigned (15 downto 0);

        d      : in adata(NSTREAMS -1 downto 0);
        d_ctrl : in acontrol;

        m_aclk        : in  std_logic;
        m_axis_tvalid : out std_logic;
        m_axis_tready : in  std_logic;
        m_axis_tdata  : out std_logic_vector(255 downto 0);
        m_axis_tkeep  : out std_logic_vector( 31 downto 0);
        m_axis_tlast  : out std_logic := '0';

        dropped_orbit_counter  : out unsigned(63 downto 0);
        orbit_counter          : out unsigned(63 downto 0);
        axi_backpressure_seen  : out std_logic;
        orbit_length           : out unsigned(63 downto 0);
        orbit_length_cnt       : out unsigned(63 downto 0); -- new
        orbit_exceeds_size     : out std_logic;
        in_autorealign_counter : in  unsigned(63 downto 0);
        
        -- DEBUG
        header_dout_o          : out std_logic_vector(255 downto 0);
        header_ren_o           : out std_logic

        -- '1' while orbit larger than 3600*6 = 21600 clocks,   
        -- this may happen when the alignment logic fails 

    );
end fifo_filler_header;





architecture Behavioral of fifo_filler_header is

    -- 1 MB data fifo with prog full
    component fifo_generator_0
        port (
            m_aclk         : in  std_logic;
            s_aclk         : in  std_logic;
            s_aresetn      : in  std_logic;
            s_axis_tvalid  : in  std_logic;
            s_axis_tready  : out std_logic;
            s_axis_tdata   : in  std_logic_vector(255 downto 0);
            s_axis_tkeep   : in  std_logic_vector( 31 downto 0);
            s_axis_tlast   : in  std_logic;
            s_axis_tuser   : in  std_logic_vector(  1 downto 0);
            m_axis_tvalid  : out std_logic;
            m_axis_tready  : in  std_logic;
            m_axis_tdata   : out std_logic_vector(255 downto 0);
            m_axis_tkeep   : out std_logic_vector( 31 downto 0);
            m_axis_tlast   : out std_logic;
            m_axis_tuser   : out std_logic_vector(  1 downto 0);
            axis_prog_full : out std_logic
        );
    end component;

    -- 1 MB packet FIFO same clock  
    component fifo_generator_1
        port (
            s_aclk        : in  std_logic;
            s_aresetn     : in  std_logic;
            s_axis_tvalid : in  std_logic;
            s_axis_tready : out std_logic;
            s_axis_tdata  : in  std_logic_vector(255 downto 0);
            s_axis_tkeep  : in  std_logic_vector( 31 downto 0);
            s_axis_tlast  : in  std_logic;
            s_axis_tuser  : in  std_logic_vector(  1 downto 0);
            m_axis_tvalid : out std_logic;
            m_axis_tready : in  std_logic;
            m_axis_tdata  : out std_logic_vector(255 downto 0);
            m_axis_tkeep  : out std_logic_vector( 31 downto 0);
            m_axis_tlast  : out std_logic;
            m_axis_tuser  : out std_logic_vector(  1 downto 0)
        );
    end component;

    component fifo_generator_2
        port (
            s_aclk        : in  std_logic;
            s_aresetn     : in  std_logic;
            s_axis_tvalid : in  std_logic;
            s_axis_tready : out std_logic;
            s_axis_tdata  : in  std_logic_vector(255 downto 0);
            s_axis_tkeep  : in  std_logic_vector(31 downto 0);
            s_axis_tlast  : in  std_logic;
            s_axis_tuser  : in  std_logic_vector(  1 downto 0);
            m_axis_tvalid : out std_logic;
            m_axis_tready : in  std_logic;
            m_axis_tdata  : out std_logic_vector(255 downto 0);
            m_axis_tkeep  : out std_logic_vector(31 downto 0);
            m_axis_tlast  : out std_logic;
            m_axis_tuser  : out std_logic_vector(  1 downto 0)
        );
    end component;
    
    -- fifo to store headers
    component header_fifo
        port (
            rst    : in  std_logic                      := '0';
            wr_clk : in  std_logic                      := '0';
            rd_clk : in  std_logic                      := '0';
            din    : in  std_logic_vector(255 downto 0) := (others => '0');
            wr_en  : in  std_logic                      := '0';
            rd_en  : in  std_logic                      := '0';
            dout   : out std_logic_vector(255 downto 0) := (others => '0');
            full   : out std_logic                      := '0';
            empty  : out std_logic                      := '1';
            underflow     : out std_logic;
            rd_data_count : out std_logic_vector(5 downto 0);
            wr_data_count : out std_logic_vector(5 downto 0)
       );
    end component;

    signal s_axis_tvalid     : std_logic;
    signal s_axis_tready     : std_logic;
    signal s_axis_tlast      : std_logic;
    signal s_axis_tdata      : std_logic_vector(255 downto 0);
    signal s_axis_tuser      : std_logic_vector(  1 downto 0);
    signal s_axis_tvalid_reg : std_logic;
    signal s_axis_tlast_reg  : std_logic;
    signal s_axis_tdata_reg  : std_logic_vector(255 downto 0);
    signal s_axis_tuser_reg  : std_logic_vector(  1 downto 0);
    signal axi_prog_full     : std_logic;

    type t_FillerState is (FILLING, DROPPING);
    signal fillerState : t_FillerState;

    signal d1                              : adata(NSTREAMS -1 downto 0);
    signal d_ctrl_d1, d_ctrl_d2, d_ctrl_d3 : acontrol;

    signal s_dropped_orbit_counter : unsigned(64-1 downto 0);
    signal s_orbit_counter         : unsigned(64-1 downto 0);

    signal orbits_in_packet : unsigned (15 downto 0);

    signal s_axi_backpressure_seen : std_logic;
    signal orbit_length_int        : unsigned(63 downto 0);
    signal s_orbit_length_cnt      : unsigned(63 downto 0); -- new
    signal packet_length_int       : unsigned(63 downto 0); -- new
    signal packet_length           : unsigned(63 downto 0); -- new

    signal rst_n : std_logic;

    -- from small distr RAM FIFO to first big data FIFO
    signal fd0tof1_axis_tvalid : std_logic;
    signal fd0tof1_axis_tready : std_logic;
    signal fd0tof1_axis_tlast  : std_logic;
    signal fd0tof1_axis_tdata  : std_logic_vector(255 downto 0);
    signal fd0tof1_axis_tkeep  : std_logic_vector( 31 downto 0);
    signal fd0tof1_axis_tuser  : std_logic_vector(  1 downto 0);
    -- from first big data FIFO to small distr RAM FIFO
    signal f1tofd1_axis_tvalid : std_logic;
    signal f1tofd1_axis_tready : std_logic;
    signal f1tofd1_axis_tlast  : std_logic;
    signal f1tofd1_axis_tdata  : std_logic_vector(255 downto 0);
    signal f1tofd1_axis_tkeep  : std_logic_vector( 31 downto 0);
    signal f1tofd1_axis_tuser  : std_logic_vector(  1 downto 0);
    -- from smal distr RAM FIFO to second big (packet) FIFO
    signal fd1tof2_axis_tvalid : std_logic;
    signal fd1tof2_axis_tready : std_logic;
    signal fd1tof2_axis_tlast  : std_logic;
    signal fd1tof2_axis_tdata  : std_logic_vector(255 downto 0);
    signal fd1tof2_axis_tkeep  : std_logic_vector( 31 downto 0);
    signal fd1tof2_axis_tuser  : std_logic_vector(  1 downto 0);
    -- from second big (packet) FIFO to second small distr FIFO
    signal f2tofd2_axis_tvalid : std_logic;
    signal f2tofd2_axis_tready : std_logic;
    signal f2tofd2_axis_tlast  : std_logic;
    signal f2tofd2_axis_tdata  : std_logic_vector(255 downto 0);
    signal f2tofd2_axis_tkeep  : std_logic_vector( 31 downto 0);
    signal f2tofd2_axis_tuser  : std_logic_vector(  1 downto 0);
    -- register output
    signal s_axis_c2h_tvalid_d1 : std_logic;
    signal s_axis_c2h_tdata_d1  : std_logic_vector(255 downto 0);
    signal s_axis_c2h_tkeep_d1  : std_logic_vector( 31 downto 0);
    signal s_axis_c2h_tlast_d1  : std_logic;
    signal s_axis_c2h_tready_d1 : std_logic := '0';
    signal s_axis_c2h_tuser_d1  : std_logic_vector(  1 downto 0);
    -- register output
    signal s_axis_c2h_tvalid_d2 : std_logic;
    signal s_axis_c2h_tdata_d2  : std_logic_vector(255 downto 0);
    signal s_axis_c2h_tkeep_d2  : std_logic_vector( 31 downto 0);
    signal s_axis_c2h_tlast_d2  : std_logic;
    signal s_axis_c2h_tready_d2 : std_logic := '0';
    signal s_axis_c2h_tuser_d2  : std_logic_vector(  1 downto 0);
        -- header fifo
    signal header_din   : std_logic_vector(255 downto 0) := (others => '0');
    signal header_wen   : std_logic                      := '0';
    signal header_ren   : std_logic                      := '0';
    signal header_dout  : std_logic_vector(255 downto 0);
    signal header_full  : std_logic;
    signal header_empty : std_logic;
    
    -- R
    signal m_axis_tvalid_reg : std_logic;
    signal m_axis_tdata_reg  : std_logic_vector(255 downto 0);
    signal m_axis_tkeep_reg  : std_logic_vector( 31 downto 0);
    signal m_axis_tlast_reg  : std_logic;
    
    --
    -- signal wait_orbit_end : std_logic; --





begin

    filler : process (d_clk) is
        variable b_wait_orbit_end     : boolean;
        variable b_orbit_exceeds_size : boolean;
    begin
        if (d_clk'event and d_clk = '1') then
            if (rst = '1') then
                fillerState             <= FILLING;
                -- orbit counters
                s_orbit_counter         <= to_unsigned(0, 64);
                s_dropped_orbit_counter <= to_unsigned(0, 64);
                orbits_in_packet        <= to_unsigned(0, 16);
                -- backpressure
                s_axi_backpressure_seen <= '0';
                -- orbit length
                orbit_exceeds_size      <= '0';
                orbit_length_int        <= to_unsigned(0, 64);
                s_orbit_length_cnt      <= to_unsigned(0, 64);
                -- packet length
                packet_length_int       <= to_unsigned(0, 64);
                -- data
                s_axis_tdata            <= (others => '0');
                s_axis_tuser            <= (others => '0');
                s_axis_tlast            <= '0';
                s_axis_tvalid           <= '0';
                -- header fifo
                header_din              <= (others => '0');
                -- header_dout             <= (others => '0');
                header_wen              <= '0';
                header_ren              <= '0';
                --
                b_wait_orbit_end        := true; --R
                b_orbit_exceeds_size    := false; --R
                -- s_axis_c2h_tuser_d1     <= (others => '0');
                -- s_axis_c2h_tdata_d1     <= (others => '0');
                -- s_axis_c2h_tvalid_d1    <= '0';
                -- s_axis_c2h_tlast_d1     <= '0';
                -- s_axis_c2h_tkeep_d1     <= (others => '0');
                -- m_axis_tdata            <= (others => '0');
                -- m_axis_tvalid           <= '0';
                -- m_axis_tlast            <= '0';
                -- m_axis_tkeep            <= (others => '0');
                
            else

                d1        <= d;
                d_ctrl_d1 <= d_ctrl;
                d_ctrl_d2 <= d_ctrl_d1;
                d_ctrl_d3 <= d_ctrl_d2;

                -- register FIFO inputs so that timing is easier to meet 
                s_axis_tvalid_reg <= s_axis_tvalid;
                s_axis_tlast_reg  <= s_axis_tlast;
                s_axis_tdata_reg  <= s_axis_tdata;
                s_axis_tuser_reg  <= s_axis_tuser;
                
                -- disable write to header fifo
                if header_wen = '1' then
                    header_wen <= '0';
                end if;

                -- start of orbit
                if d_ctrl_d1.valid = '1' and d_ctrl_d2.valid = '0' then
                    orbit_length_int   <= to_unsigned(1, 64);
                    s_orbit_length_cnt <= to_unsigned(1, 64); -- new
                end if;

                -- orbit on-going
                if d_ctrl_d1.valid = '1' and d_ctrl_d2.valid = '1' and d_ctrl_d1.strobe = '1' then
                    orbit_length_int   <= orbit_length_int   + 1;
                    s_orbit_length_cnt <= s_orbit_length_cnt + 1;

                    -- check if orbit length exceeds size
                    if orbit_length_int > 21600 then
                        orbit_exceeds_size   <= '1';
                        b_orbit_exceeds_size := true;
                    else
                        orbit_exceeds_size   <= '0';
                        b_orbit_exceeds_size := false;
                    end if;
                end if;

                -- end of orbit
                if d_ctrl_d1.valid = '0' and d_ctrl_d2.valid = '1' then
                    orbit_length         <= orbit_length_int;
                    orbit_length_int     <= to_unsigned(0, 64);
                    s_orbit_length_cnt   <= to_unsigned(0, 64); -- new
                    orbit_exceeds_size   <= '0';
                    b_orbit_exceeds_size := false;
                end if;
                
                if d_ctrl_d2.valid = '0' and d_ctrl_d3.valid = '1' then
                    b_wait_orbit_end   := false; --R
                end if;

                --===============================================================
                -- FSM to fill/drop data
                --===============================================================
                case fillerState is
                    --===========================================================
                    -- FSM in FILLER state
                    --===========================================================
                    when FILLING =>
                        if s_axis_tvalid_reg = '1' and s_axis_tready = '0' then
                            s_axi_backpressure_seen <= '1';
                        end if;
                        
                        if d_ctrl.valid = '1' and d_ctrl_d1.valid = '0' and std_logic_vector(orbits_in_packet) = x"0000" then
                            --R
                            if b_wait_orbit_end then
                                s_axis_tvalid <= '0';
                            else
                                s_axis_tvalid <= '1';
                            end if;
                            --end R
                            -- s_axis_tvalid <= '1' and (not b_wait_orbit_end); --R
                            s_axis_tlast  <= '0';  -- d_ctrl.last; --- mhmmm
                            s_axis_tuser  <= "10"; -- header
                            s_axis_tdata  <= x"0000000000000000" &
                                             x"0000000000000000" &
                                             x"0000000000000000" &
                                             x"feedbeeffeedbeef";
                        else
                            
                            --R
                            if b_wait_orbit_end then
                                s_axis_tvalid <= '0';
                            else
                                s_axis_tvalid <= d_ctrl_d1.valid and d_ctrl_d1.strobe;
                            end if;
                            --end R
                            -- s_axis_tvalid <= d_ctrl_d1.valid and d_ctrl_d1.strobe and (not b_wait_orbit_end); --R
                            s_axis_tlast  <= '0';  -- d_ctrl.last; --- mhmmm
                            s_axis_tuser  <= "11"; -- data
                            s_axis_tdata  <= (others => '0');
                            for i in d'range loop
                                s_axis_tdata(i*32 + 31 downto i*32) <= d1(i);
                            end loop;
                        end if;

                        --=======================================================
                        -- packet length in frames and packet end
                        --=======================================================
                        -- start of orbit
                        if d_ctrl_d1.valid = '1' and d_ctrl_d2.valid = '0' and (not b_wait_orbit_end) then --R
                        
                            -- start of packet
                            if std_logic_vector(orbits_in_packet) = x"0000" then
                                packet_length_int <= to_unsigned(1, 64);
                            
                            -- packet on-going
                            else
                                packet_length_int <= packet_length_int + 1;
                            end if;
                        
                        -- middle of packet and orbit
                        elsif d_ctrl_d1.valid = '1' and d_ctrl_d2.valid = '1' and d_ctrl_d1.strobe = '1' and (not b_wait_orbit_end) then --R
                            packet_length_int <= packet_length_int + 1;

                        -- end of orbit
                        elsif d_ctrl_d1.valid = '0' and d_ctrl_d2.valid = '1' and (not b_wait_orbit_end) then
                            s_orbit_counter <= s_orbit_counter + 1;
                            if (orbits_in_packet < orbits_per_packet-1) then
                                if (not b_orbit_exceeds_size) then
                                    orbits_in_packet <= orbits_in_packet + 1;
                                end if;
                             
                            -- end of packet
                            else
                                s_axis_tvalid     <= '1';
                                s_axis_tlast      <= '1';
                                s_axis_tuser      <= "01"; -- trailer
                                s_axis_tdata      <= std_logic_vector(s_orbit_counter + 1)     &
                                                     std_logic_vector(s_dropped_orbit_counter) &
                                                     std_logic_vector(in_autorealign_counter)  &
                                                     x"deadbeefdeadbeef";                           -- end of packet marker
                                orbits_in_packet  <= to_unsigned(0, 16);
                                packet_length     <= packet_length_int;
                                packet_length_int <= to_unsigned(0, 64);
                                
                                header_din        <= std_logic_vector(s_orbit_counter + 1)     &
                                                     std_logic_vector(s_dropped_orbit_counter) &
                                                     std_logic_vector(packet_length_int)       &
                                                     x"feedbeeffeedbeef";
                                header_wen        <= '1';
                            end if;
                            
                            -- check if fifo is full
                            if axi_prog_full = '1' then
                                fillerState <= DROPPING;
                            end if;
                        end if;
                        --=======================================================
                    --===========================================================
                        
                    --===========================================================
                    -- FSM in DROPPING state
                    --===========================================================
                    when DROPPING =>
                        -- send null and invalid data
                        s_axis_tvalid <= '0';
                        s_axis_tlast  <= '0';
                        s_axis_tdata  <= (others => '0');
                        
                        -- end of orbit
                        if d_ctrl_d1.valid = '0' and d_ctrl_d2.valid = '1' and (not b_wait_orbit_end) then --R
                            s_orbit_counter         <= s_orbit_counter + 1;
                            s_dropped_orbit_counter <= s_dropped_orbit_counter + 1;
                            if axi_prog_full = '1' then
                                fillerState <= DROPPING;
                            end if;
                        end if;
                        
                        -- start of orbit
                        if d_ctrl.valid = '1' and d_ctrl_d1.valid = '0' and (not b_wait_orbit_end) then --R
                            if axi_prog_full = '0' then
                                fillerState <= FILLING;
                                
                                if std_logic_vector(orbits_in_packet) = x"0000" then
                                    s_axis_tvalid <= '1';
                                    s_axis_tlast  <= '0';  -- d_ctrl.last; --- mhmmm
                                    s_axis_tuser  <= "10";
                                    s_axis_tdata  <= x"0000000000000000" &
                                                     x"0000000000000000" &
                                                     x"0000000000000000" &
                                                     x"feedbeeffeedbeef";
                                end if;
                            end if;
                        end if;
                    --===========================================================

                end case fillerState;
                --===============================================================
                
                --===============================================================
                -- READ and ADD HEADER from parallel fifo
                --===============================================================
                if header_ren = '1' then
                    header_ren <= '0';
                end if;

                if m_axis_tready = '1' then
                    if s_axis_c2h_tuser_d1 = "10" then 
                        header_ren    <= '1';
                        m_axis_tdata  <= header_dout;
                        m_axis_tvalid <= '1';
                        m_axis_tlast  <= '0';
                        m_axis_tkeep  <= (others => '1');
                    else
                        header_ren    <= '0';
                        m_axis_tdata  <= s_axis_c2h_tdata_d1;
                        if s_axis_c2h_tuser_d1 /= "00" then
                            m_axis_tvalid <= s_axis_c2h_tvalid_d1;
                        else
                            m_axis_tvalid <= '0';
                        end if;
                        m_axis_tlast  <= s_axis_c2h_tlast_d1;
                        m_axis_tkeep  <= s_axis_c2h_tkeep_d1;
                    end if;
                -- end if;
                -- else
                --     m_axis_tdata     <= x"0000000000000000" &
                --                         x"0000000000000000" &
                --                         x"0000000000000000" &
                --                         x"beafbeafbeafbeaf";
                --     m_axis_tvalid    <= '0';
                --     m_axis_tlast     <= '0';
                --     m_axis_tkeep     <= (others => '1');
                end if;
                --===============================================================
            end if;
        end if;
    end process filler;

    --===========================================================================
    -- OUTPUT signals
    --===========================================================================
    orbit_length_cnt      <= s_orbit_length_cnt;
    orbit_counter         <= s_orbit_counter;
    dropped_orbit_counter <= s_dropped_orbit_counter;
    axi_backpressure_seen <= s_axi_backpressure_seen;
    header_dout_o         <= header_dout;
    header_ren_o          <= header_ren;
    --===========================================================================



    --===========================================================================
    -- FIFOs instantiations
    --===========================================================================
    rst_n <= not rst;
    
    -- small FIFO to achcive timing between fifo filler and first big FIFO
    distrRAM_fftof1 : fifo_generator_2
        port map (
            s_aclk        => m_aclk,
            s_aresetn     => rst_n,
            s_axis_tvalid => s_axis_tvalid_reg,
            s_axis_tready => s_axis_tready,
            s_axis_tdata  => s_axis_tdata_reg,
            s_axis_tkeep  => (others => '1'),
            s_axis_tlast  => s_axis_tlast_reg,
            s_axis_tuser  => s_axis_tuser_reg,
            
            m_axis_tvalid => fd0tof1_axis_tvalid,
            m_axis_tready => fd0tof1_axis_tready,
            m_axis_tdata  => fd0tof1_axis_tdata,
            m_axis_tkeep  => fd0tof1_axis_tkeep,
            m_axis_tlast  => fd0tof1_axis_tlast,
            m_axis_tuser  => fd0tof1_axis_tuser
        );

    -- 1 MB data fifo with prog full
--    fifo : fifo_generator_0
--        port map(
--            s_aclk         => d_clk,
--            m_aclk         => m_aclk,
--            s_aresetn      => rst_n,
--            s_axis_tvalid  => s_axis_tvalid_reg,
--            s_axis_tready  => s_axis_tready,
--            s_axis_tdata   => s_axis_tdata_reg,
--            s_axis_tkeep   => (others => '1'),
--            s_axis_tlast   => s_axis_tlast_reg,
--            s_axis_tuser   => s_axis_tuser_reg,
--            axis_prog_full => axi_prog_full,

--            m_axis_tvalid => f1tofd1_axis_tvalid,
--            m_axis_tready => f1tofd1_axis_tready,
--            m_axis_tdata  => f1tofd1_axis_tdata,
--            m_axis_tkeep  => f1tofd1_axis_tkeep,
--            m_axis_tlast  => f1tofd1_axis_tlast,
--            m_axis_tuser  => f1tofd1_axis_tuser
--        );
    fifo : fifo_generator_0
        port map(
            s_aclk         => d_clk,
            m_aclk         => m_aclk,
            s_aresetn      => rst_n,
            s_axis_tvalid  => fd0tof1_axis_tvalid,
            s_axis_tready  => fd0tof1_axis_tready,
            s_axis_tdata   => fd0tof1_axis_tdata,
            s_axis_tkeep   => fd0tof1_axis_tkeep,
            s_axis_tlast   => fd0tof1_axis_tlast,
            s_axis_tuser   => fd0tof1_axis_tuser,
            axis_prog_full => axi_prog_full,

            m_axis_tvalid => f1tofd1_axis_tvalid,
            m_axis_tready => f1tofd1_axis_tready,
            m_axis_tdata  => f1tofd1_axis_tdata,
            m_axis_tkeep  => f1tofd1_axis_tkeep,
            m_axis_tlast  => f1tofd1_axis_tlast,
            m_axis_tuser  => f1tofd1_axis_tuser
        );

    -- small FIFO to achcive timing between two big FIFOs
    distrRAM_fifo12 : fifo_generator_2
        port map (
            s_aclk        => m_aclk,
            s_aresetn     => rst_n,
            s_axis_tvalid => f1tofd1_axis_tvalid,
            s_axis_tready => f1tofd1_axis_tready,
            s_axis_tdata  => f1tofd1_axis_tdata,
            s_axis_tkeep  => f1tofd1_axis_tkeep,
            s_axis_tlast  => f1tofd1_axis_tlast,
            s_axis_tuser  => f1tofd1_axis_tuser,
            
            m_axis_tvalid => fd1tof2_axis_tvalid,
            m_axis_tready => fd1tof2_axis_tready,
            m_axis_tdata  => fd1tof2_axis_tdata,
            m_axis_tkeep  => fd1tof2_axis_tkeep,
            m_axis_tlast  => fd1tof2_axis_tlast,
            m_axis_tuser  => fd1tof2_axis_tuser
        );

    -- 1 MB packet FIFO same clock
    fifo2 : fifo_generator_1
        port map (
            s_aclk        => m_aclk,
            s_aresetn     => rst_n,
            s_axis_tvalid => fd1tof2_axis_tvalid,
            s_axis_tready => fd1tof2_axis_tready,
            s_axis_tdata  => fd1tof2_axis_tdata,
            s_axis_tkeep  => fd1tof2_axis_tkeep,
            s_axis_tlast  => fd1tof2_axis_tlast,
            s_axis_tuser  => fd1tof2_axis_tuser,

            m_axis_tvalid => f2tofd2_axis_tvalid,
            m_axis_tready => f2tofd2_axis_tready,
            m_axis_tdata  => f2tofd2_axis_tdata,
            m_axis_tkeep  => f2tofd2_axis_tkeep,
            m_axis_tlast  => f2tofd2_axis_tlast,
            m_axis_tuser  => f2tofd2_axis_tuser
        );

    -- small FIFO to achcive timing between two big FIFOs
    distrRAM_fifo2dma : fifo_generator_2
        port map (
            s_aclk        => m_aclk,
            s_aresetn     => rst_n,
            s_axis_tvalid => f2tofd2_axis_tvalid,
            s_axis_tready => f2tofd2_axis_tready,
            s_axis_tdata  => f2tofd2_axis_tdata,
            s_axis_tkeep  => f2tofd2_axis_tkeep,
            s_axis_tlast  => f2tofd2_axis_tlast,
            s_axis_tuser  => f2tofd2_axis_tuser,
            
            m_axis_tvalid => s_axis_c2h_tvalid_d1,
            m_axis_tready => m_axis_tready, -- s_axis_c2h_tready_d1,
            m_axis_tdata  => s_axis_c2h_tdata_d1,
            m_axis_tkeep  => s_axis_c2h_tkeep_d1,
            m_axis_tlast  => s_axis_c2h_tlast_d1,
            m_axis_tuser  => s_axis_c2h_tuser_d1
        );
    
    distRAM_header_fifo : header_fifo
        port map (
            rst    => rst,
            wr_clk => d_clk,
            rd_clk => m_aclk,
            din    => header_din,
            wr_en  => header_wen,
            rd_en  => header_ren,
            dout   => header_dout,
            full   => header_full,
            empty  => header_empty
        );
    --===========================================================================

end Behavioral;
