----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 10/10/2018 05:05:10 PM
-- Design Name:
-- Module Name: comma_gap_cleaner - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

use work.vcu128_top_decl.all;
use work.datatypes.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity comma_gap_cleaner is
    generic (
        QUADS   : natural := N_REGION
        );
    port (
        clk     : in  std_logic;
        rst     : in  std_logic;
        d       : in  ldata(4*QUADS - 1 downto 0);
        q       : out ldata(4*QUADS - 1 downto 0);
        lid     : out TAuxInfo(4*QUADS - 1 downto 0);
        crc     : out TAuxInfo(4*QUADS - 1 downto 0)
        );
end comma_gap_cleaner;

architecture Behavioral of comma_gap_cleaner is

    -- First buffer to invalidate padding words in comma gap
    -- DEPTH = 24 (1 PADDING word every 24 COMMA word in comma gap) x 8 links
    signal q_padding_buf   : TLinkBuffer(23 downto 0);

    -- Second buffer to handle special sequences of words at end of orbit
    -- DEPTH = 6 (minimum length to catch all the special sequences) x 8 links
    signal q_buffer        : TLinkBuffer( 5 downto 0);

begin

    -- Send input data to first buffer
    q_padding_buf(0)(4*QUADS - 1 downto 0) <= d;


    -- Padding and special words in the comma gap make everything tedious, so we try to catch them here and just invalidate them.
    -- While we're at it we read the contents of the special words (CRCs and LIDs) and make them available to the top.
    -- Words are then sent to the second buffer (q_buffer)
    invalidate_padding_in_comma_gap : process(clk)
        variable crcs_read : std_logic_vector(4*QUADS - 1 downto 0) := (others => '0');
    begin
        if clk'event and clk = '1' then
            q_padding_buf(q_padding_buf'high downto 1) <= q_padding_buf(q_padding_buf'high-1 downto 0);

            for i in q'range loop
                if  (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 2)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 3)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 4)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 5)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 6)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 7)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 8)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high- 9)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-10)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-11)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-12)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-13)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-14)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-15)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-16)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-17)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-18)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-19)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-20)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-21)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-22)(i).valid = '0') or
                    (q_buffer(0)(i).valid = '0' and q_padding_buf(q_padding_buf'high-23)(i).valid = '0')
                then
                    q_buffer(0)(i).data   <= q_padding_buf(q_padding_buf'high-1)(i).data;
                    q_buffer(0)(i).strobe <= q_padding_buf(q_padding_buf'high-1)(i).strobe;
                    q_buffer(0)(i).done   <= q_padding_buf(q_padding_buf'high-1)(i).done;
                    q_buffer(0)(i).valid  <= '0';

                    if q_padding_buf(q_padding_buf'high-1)(i).strobe = '1' and q_padding_buf(q_padding_buf'high-1)(i).valid = '1' then -- It's either the CRC or LID field if it's both valid and not a padding word
                        if crcs_read(i) = '1' then
                            lid(i) <= q_padding_buf(q_padding_buf'high-1)(i).data;
                            crcs_read(i) := '0';
                        else
                            crc(i) <= q_padding_buf(q_padding_buf'high-1)(i).data;
                            crcs_read(i) := '1';
                        end if;
                    end if;

                else
                    q_buffer(0)(i) <= q_padding_buf(q_padding_buf'high-1)(i);
                end if;

            end loop;
        end if;
    end process;


    -- At the end of orbit, some patterns may cause problems to the following aligner logic.
    -- Here we handle these special cases:
    -- (1) DATA -> PADDING -> COMMA -> CRC -> LID -> COMMA -> GAP    : causes the aligner to go into idle state due to valid padding word
    -- (2) ...
    protect_comma_gap : process(clk)
    begin
        if clk'event and clk = '1' then
            q_buffer(q_buffer'high downto 1) <= q_buffer(q_buffer'high-1 downto 0);

            for i in q'range loop

                -- Invalidate padding word before COMMA -> CRC -> LID -> COMMA -> GAP
                if ((q_buffer(5)(i).valid = '1' and q_buffer(5)(i).strobe = '1') and    -- DATA word
                    (q_buffer(4)(i).valid = '1' and q_buffer(4)(i).strobe = '0') and    -- PADDING word
                    (q_buffer(3)(i).valid = '0' and q_buffer(3)(i).strobe = '1') and    -- COMMA word
                    (q_buffer(2)(i).valid = '0' and q_buffer(2)(i).strobe = '1') and    -- CRC word
                    (q_buffer(1)(i).valid = '0' and q_buffer(1)(i).strobe = '1') and    -- LID word
                    (q_buffer(0)(i).valid = '0' and q_buffer(0)(i).strobe = '1'))       -- COMMA word (before comma gap)
                then
                    -- Invalidate PADDING word (index 4) in the special sequence n°(1)
                    q(i).data   <= q_buffer(4)(i).data;
                    q(i).strobe <= q_buffer(4)(i).strobe;
                    q(i).done   <= q_buffer(4)(i).done;
                    q(i).valid  <= '0';
                else
                    q(i) <= q_buffer(4)(i);
                end if;
            end loop;
        end if;
    end process;

end Behavioral;
