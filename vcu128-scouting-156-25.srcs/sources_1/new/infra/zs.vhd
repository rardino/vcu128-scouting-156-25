library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.datatypes.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity zs is
    generic (
        NSTREAMS : integer 
    );
    port (
        clk     : in  std_logic;
        rst     : in  std_logic;
        d       : in  adata(NSTREAMS-1 downto 0);
        d_ctrl  : in  acontrol;
        q       : out adata(NSTREAMS-1 downto 0);
        q_ctrl  : out acontrol;
        zs_sel  : in  std_logic := '0';
        zs_cuts : in  CutList := (others => to_unsigned(0, 9))
    );
end zs;

architecture Behavioral of zs is

    signal d1, d2, d3, d4, d5, d6 : adata(NSTREAMS-1 downto 0);
    signal d_ctrl1, d_ctrl2, d_ctrl3, d_ctrl4, d_ctrl5, d_ctrl6 : acontrol;
    
    signal zs_cuts_reg : CutList;

begin

    zs: process(clk, rst) is
      variable suppress : boolean;
    begin
        if rst = '1' then
            d1      <= (others => AWORD_NULL);
            d_ctrl1 <= ('0', '0', '0', '0');
            d2      <= (others => AWORD_NULL);
            d_ctrl2 <= ('0', '0', '0', '0');
            d3      <= (others => AWORD_NULL);
            d_ctrl3 <= ('0', '0', '0', '0');
            d4      <= (others => AWORD_NULL);
            d_ctrl4 <= ('0', '0', '0', '0');
            d5      <= (others => AWORD_NULL);
            d_ctrl5 <= ('0', '0', '0', '0');
        else
            if clk'event and clk = '1' then
                zs_cuts_reg <= zs_cuts;
                
                q       <= d5;
                q_ctrl  <= d_ctrl5;
                d5      <= d4;
                d_ctrl5 <= d_ctrl4;
                d4      <= d3;
                d_ctrl4 <= d_ctrl3;
                d3      <= d2;
                d_ctrl3 <= d_ctrl2;
                d2      <= d1;
                d_ctrl2 <= d_ctrl1;
                d1      <= d;
                d_ctrl1 <= d_ctrl;
                
                if d_ctrl5.valid ='1' and d_ctrl5.strobe='1' and d_ctrl5.bx_start = '1' then
                
                    suppress := true;
                    case zs_sel is
                        when '0' => -- UGMT
                            for i in d'range loop
                              if d3(i)(18 downto 10) /= "000000000" or   -- pt is in bits 18 downto 10
                                 d1(i)(18 downto 10) /= "000000000" then -- d3 has 1st word of 1st muon, d1 has 1st word of 2nd muon
                                   suppress := false;
                              end if;   
                            end loop;
                        
                        when '1' => -- DEMUX
                            for i in d'range loop
                                if unsigned(d5(i)(8 downto 0)) > zs_cuts_reg(i) or
                                   unsigned(d4(i)(8 downto 0)) > zs_cuts_reg(i) or
                                   unsigned(d3(i)(8 downto 0)) > zs_cuts_reg(i) or
                                   unsigned(d2(i)(8 downto 0)) > zs_cuts_reg(i) or
                                   unsigned(d1(i)(8 downto 0)) > zs_cuts_reg(i) or
                                   unsigned(d(i)(8 downto 0))  > zs_cuts_reg(i) then
                                    suppress := false;
                                end if;
                            end loop;
                        
                        when others =>
                            suppress := false;
                    end case zs_sel;
                    
                    if suppress then
                        q_ctrl.strobe <= '0';
                        d_ctrl5.strobe <= '0';
                        d_ctrl4.strobe <= '0';
                        d_ctrl3.strobe <= '0';
                        d_ctrl2.strobe <= '0';
                        d_ctrl1.strobe <= '0';
                    end if;
               end if;
            end if;
        end if;
    end process zs;

end Behavioral;
