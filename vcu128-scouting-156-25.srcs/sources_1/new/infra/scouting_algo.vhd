library ieee;
use ieee.std_logic_1164.all;

library unisim;
use unisim.vcomponents.all;

use work.datatypes.all;

entity algo is
    port (
        clk    : in  std_logic;
        rst    : in  std_logic;
        d      : in  adata;
        d_ctrl : in  acontrol;
        q      : out adata;
        q_ctrl : out acontrol
        );
end algo;

architecture Behavioral of algo is
begin

    -- TODO: Actual algo

    q      <= d;
    q_ctrl <= d_ctrl;

end Behavioral;
