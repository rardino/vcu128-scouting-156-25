-- vcu128_top_decl
--
-- Constants for the whole device

library IEEE;
use IEEE.std_logic_1164.all;

package vcu128_top_decl is

    -- version
    -- First octet identifies the board, then major, minor, patch
    constant ALGO_REV               : std_logic_vector(31 downto 0) := X"01" & X"00" & X"00" & X"01";
    -- orbit length
    constant LHC_BUNCH_COUNT        : integer                       := 3564;
    constant ORBIT_LENGTH           : natural                       := 3564;
    -- number of transceivers
    constant N_REGION               : positive                      := 2;
    -- register address width (for axi lite interface)
    constant REGISTER_ADDRESS_WIDTH : natural                       := 4;

end vcu128_top_decl;
