------------------------------------------------------
-- remove CMC slink status word until the first Header on L0 & L1 before each event
--
--  Ver 1.00
--
-- Dominique Gigi Jan 2011
------------------------------------------------------
--
--
--
--
------------------------------------------------------
LIBRARY ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


entity resetn_gen is
    generic (
        resync  : boolean := false
    );
    port (
        HRD_resetn  : in std_logic;
        clock       : in std_logic;
        func_reset  : in std_logic;

        clock_rst   : in std_logic;
        resetn      : out std_logic;
        done        : out std_logic
    );
end resetn_gen;





architecture behavioral of resetn_gen is

component resetn_resync is
    port (
        aresetn     : in std_logic;
        clock       : in std_logic;
        Resetn_sync : out std_logic;
        Resetp_sync : out std_logic
    );
end component;

signal local_reset      : std_logic := '1';
signal local_reset_cnt  : std_logic_vector(7 downto 0) := x"00";
signal rst_resync       : std_logic;

--############################################
--##########  code start here ################
--############################################
begin

-- reset Procedure
process(HRD_resetn,clock)
begin
    if HRD_resetn = '0' then
        local_reset <= '0';
    elsif rising_edge(clock) then
        if func_reset = '1' then
            local_reset <= '0';
        elsif local_reset_cnt = x"f0" then
            local_reset <= '1';
        end if;
    end if;
end process;

process(HRD_resetn,clock)
begin
    if HRD_resetn = '0' then
        local_reset_cnt         <= (others => '0');
    elsif rising_edge(clock) then
        if func_reset  = '1' then
            local_reset_cnt <= (others => '0');
        elsif local_reset = '0' then
            local_reset_cnt <= local_reset_cnt + '1';
        end if;
    end if;
end process;

resync_rst_i1:resetn_resync
port map(
        aresetn                         => local_reset,
        clock                           => clock_rst,
        Resetn_sync                     => rst_resync
        );

resetn  <= rst_resync when resync else local_reset;

done    <= local_reset;

end behavioral;