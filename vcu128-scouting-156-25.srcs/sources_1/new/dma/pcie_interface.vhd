----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 07/15/2021 12:07:31 PM
-- Design Name:
-- Module Name: top - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
library UNISIM;

use IEEE.std_logic_1164.all;
use UNISIM.vcomponents.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pcie_interface is
    generic (
        PL_LINK_CAP_MAX_LINK_WIDTH   : integer := 8;            -- 1- X1; 2 - X2; 4 - X4; 8 - X8
        PL_SIM_FAST_LINK_TRAINING    : boolean := false;        -- Simulation Speedup
        PL_LINK_CAP_MAX_LINK_SPEED   : integer := 4;            -- 1- GEN1; 2 - GEN2; 4 - GEN3
        C_DATA_WIDTH                 : integer := 256;
        EXT_PIPE_SIM                 : boolean := false;        -- This Parameter has effect on selecting Enable External PIPE Interface in GUI.
        C_ROOT_PORT                  : boolean := false;        -- PCIe block is in root port mode
        C_DEVICE_NUMBER              : integer := 0;            -- Device number for Root Port configurations only
        AXIS_CCIX_RX_TDATA_WIDTH     : integer := 256; 
        AXIS_CCIX_TX_TDATA_WIDTH     : integer := 256;
        AXIS_CCIX_RX_TUSER_WIDTH     : integer := 46;
        AXIS_CCIX_TX_TUSER_WIDTH     : integer := 46
    );
    port (
        pci_exp_rst_n  : in  std_logic;
        pci_exp_clk_n  : in  std_logic;
        pci_exp_clk_p  : in  std_logic;

        pci_exp_tx_n   : out std_logic_vector(PL_LINK_CAP_MAX_LINK_WIDTH-1 downto 0);
        pci_exp_tx_p   : out std_logic_vector(PL_LINK_CAP_MAX_LINK_WIDTH-1 downto 0);
        pci_exp_rx_n   : in  std_logic_vector(PL_LINK_CAP_MAX_LINK_WIDTH-1 downto 0);
        pci_exp_rx_p   : in  std_logic_vector(PL_LINK_CAP_MAX_LINK_WIDTH-1 downto 0);
        
        -- AXI Lite Interface
        -- AXI Master Write Address Channel
        m_axil_awaddr  : out std_logic_vector(31 downto 0);
        m_axil_awprot  : out std_logic_vector( 2 downto 0);
        m_axil_awvalid : out std_logic;
        m_axil_awready : in  std_logic;
        -- AXI Master Write Data Channel
        m_axil_wdata   : out std_logic_vector(31 downto 0);
        m_axil_wstrb   : out std_logic_vector( 3 downto 0);
        m_axil_wvalid  : out std_logic;
        m_axil_wready  : in  std_logic;
        -- AXI Master Write Response Channel
        m_axil_bvalid  : in  std_logic;
        m_axil_bready  : out std_logic;
        -- AXI Master Read Address Channel
        m_axil_araddr  : out std_logic_vector(31 downto 0);
        m_axil_arprot  : out std_logic_vector( 2 downto 0);
        m_axil_arvalid : out std_logic;
        m_axil_arready : in  std_logic;
        -- AXI Master Read Data Channel
        m_axil_rdata   : in  std_logic_vector(31 downto 0);
        m_axil_rresp   : in  std_logic_vector( 1 downto 0);
        m_axil_rvalid  : in  std_logic;
        m_axil_rready  : out std_logic;
        m_axil_bresp   : in  std_logic_vector( 1 downto 0);
        
        -- AXI Stream
        axi_clk             : out std_logic;
        axi_rstn            : out std_logic;
        pci_exp_lnk_up      : out std_logic;
        m_axis_h2c_tdata_0  : out std_logic_vector(C_DATA_WIDTH-1 downto 0);
        m_axis_h2c_tlast_0  : out std_logic;
        m_axis_h2c_tvalid_0 : out std_logic;
        m_axis_h2c_tready_0 : in  std_logic;
        m_axis_h2c_tkeep_0  : out std_logic_vector(C_DATA_WIDTH/8-1 downto 0);
        s_axis_c2h_tdata_0  : in  std_logic_vector(C_DATA_WIDTH-1 downto 0);
        s_axis_c2h_tlast_0  : in  std_logic;
        s_axis_c2h_tvalid_0 : in  std_logic;
        s_axis_c2h_tready_0 : out std_logic;
        s_axis_c2h_tkeep_0  : in  std_logic_vector(C_DATA_WIDTH/8-1 downto 0)

--        usr_clk        : out std_logic;
--        usr_rst_n      : out std_logic;
--        usr_func_wr    : out std_logic_vector(65535 downto 0);
--        usr_wren       : out std_logic;
--        usr_data_wr    : out std_logic_vector(C_DATA_WIDTH/8-1 downto 0);

--        usr_func_rd    : out std_logic_vector(65535 downto 0);
--        usr_rden       : out std_logic;
--        usr_data_rd    : in  std_logic_vector(C_DATA_WIDTH/8-1 downto 0);
--        usr_rd_val     : in  std_logic
    );
end pcie_interface;





architecture Behavioral of pcie_interface is

    component xdma_0
        port (
            sys_clk                    : in  std_logic;
            sys_clk_gt                 : in  std_logic;
            sys_rst_n                  : in  std_logic;
            user_lnk_up                : out std_logic;
            pci_exp_txp                : out std_logic_vector(PL_LINK_CAP_MAX_LINK_WIDTH-1 downto 0);
            pci_exp_txn                : out std_logic_vector(PL_LINK_CAP_MAX_LINK_WIDTH-1 downto 0);
            pci_exp_rxp                : in  std_logic_vector(PL_LINK_CAP_MAX_LINK_WIDTH-1 downto 0);
            pci_exp_rxn                : in  std_logic_vector(PL_LINK_CAP_MAX_LINK_WIDTH-1 downto 0);
            axi_aclk                   : out std_logic;
            axi_aresetn                : out std_logic;
            usr_irq_req                : in  std_logic_vector(0 downto 0);
            usr_irq_ack                : out std_logic_vector(0 downto 0);
            msi_enable                 : out std_logic;
            msi_vector_width           : out std_logic_vector(2 downto 0);
            m_axil_awaddr              : out std_logic_vector(C_DATA_WIDTH/8-1 downto 0);
            m_axil_awprot              : out std_logic_vector(2 downto 0);
            m_axil_awvalid             : out std_logic;
            m_axil_awready             : in  std_logic;
            m_axil_wdata               : out std_logic_vector(C_DATA_WIDTH/8-1 downto 0);
            m_axil_wstrb               : out std_logic_vector(3 downto 0);
            m_axil_wvalid              : out std_logic;
            m_axil_wready              : in  std_logic;
            m_axil_bvalid              : in  std_logic;
            m_axil_bresp               : in  std_logic_vector(1 downto 0);
            m_axil_bready              : out std_logic;
            m_axil_araddr              : out std_logic_vector(C_DATA_WIDTH/8-1 downto 0);
            m_axil_arprot              : out std_logic_vector(2 downto 0);
            m_axil_arvalid             : out std_logic;
            m_axil_arready             : in  std_logic;
            m_axil_rdata               : in  std_logic_vector(C_DATA_WIDTH/8-1 downto 0);
            m_axil_rresp               : in  std_logic_vector(1 downto 0);
            m_axil_rvalid              : in  std_logic;
            m_axil_rready              : out std_logic;
            cfg_mgmt_addr              : in  std_logic_vector(18 downto 0);
            cfg_mgmt_write             : in  std_logic;
            cfg_mgmt_write_data        : in  std_logic_vector(31 downto 0);
            cfg_mgmt_byte_enable       : in  std_logic_vector(3 downto 0);
            cfg_mgmt_read              : in  std_logic;
            cfg_mgmt_read_data         : out std_logic_vector(31 downto 0);
            cfg_mgmt_read_write_done   : out std_logic;
            s_axis_c2h_tdata_0         : in  std_logic_vector(C_DATA_WIDTH-1 downto 0);
            s_axis_c2h_tlast_0         : in  std_logic;
            s_axis_c2h_tvalid_0        : in  std_logic;
            s_axis_c2h_tready_0        : out std_logic;
            s_axis_c2h_tkeep_0         : in  std_logic_vector(C_DATA_WIDTH/8-1 downto 0);
            m_axis_h2c_tdata_0         : out std_logic_vector(C_DATA_WIDTH-1 downto 0);
            m_axis_h2c_tlast_0         : out std_logic;
            m_axis_h2c_tvalid_0        : out std_logic;
            m_axis_h2c_tready_0        : in  std_logic;
            m_axis_h2c_tkeep_0         : out std_logic_vector(C_DATA_WIDTH/8-1 downto 0);
            int_usp_qpll0lock_out      : out std_logic_vector(1 downto 0);
            int_usp_qpll0outrefclk_out : out std_logic_vector(1 downto 0);
            int_usp_qpll0outclk_out    : out std_logic_vector(1 downto 0);
            int_usp_qpll1lock_out      : out std_logic_vector(1 downto 0);
            int_usp_qpll1outrefclk_out : out std_logic_vector(1 downto 0);
            int_usp_qpll1outclk_out    : out std_logic_vector(1 downto 0)
        );
    end component;


    signal pci_exp_clk_ref : std_logic;
    signal pci_exp_clk_gt  : std_logic;
    
    signal pci_exp_rst_n_c : std_logic;
    
    signal usr_clk    : std_logic;
    signal usr_resetn : std_logic;
    signal usr_lnk_up : std_logic;

    signal usr_irq_req : std_logic_vector(0 downto 0) := (0 => '0');
    signal usr_irq_ack : std_logic_vector(0 downto 0);
    
    signal msi_enable       : std_logic;
    signal msi_vector_width : std_logic_vector(2 downto 0);




begin



    IBUFDS_GTE4_inst : IBUFDS_GTE4
        port map (
            O       => pci_exp_clk_gt,
            ODIV2   => pci_exp_clk_ref,
            CEB     => '0',
            I       => pci_exp_clk_p,
            IB      => pci_exp_clk_n
        );
    
    IBUF_inst : IBUF
        port map (
            O => pci_exp_rst_n_c,
            I => pci_exp_rst_n
        );


    xdma_axi : xdma_0
        port map (
            -- PCIe clk and rst
            sys_clk                  => pci_exp_clk_ref,
            sys_clk_gt               => pci_exp_clk_gt,
            sys_rst_n                => pci_exp_rst_n_c,
            -- tx
            pci_exp_txp              => pci_exp_tx_p,
            pci_exp_txn              => pci_exp_tx_n,
            -- rx
            pci_exp_rxp              => pci_exp_rx_p,
            pci_exp_rxn              => pci_exp_rx_n,
            
            -- AXI global
            axi_aclk                 => usr_clk,
            axi_aresetn              => usr_resetn,
            user_lnk_up              => usr_lnk_up,
            
            -- AXI Stream
            s_axis_c2h_tdata_0       => s_axis_c2h_tdata_0,
            s_axis_c2h_tlast_0       => s_axis_c2h_tlast_0,
            s_axis_c2h_tvalid_0      => s_axis_c2h_tvalid_0,
            s_axis_c2h_tready_0      => s_axis_c2h_tready_0,
            s_axis_c2h_tkeep_0       => s_axis_c2h_tkeep_0,
            m_axis_h2c_tdata_0       => m_axis_h2c_tdata_0,
            m_axis_h2c_tlast_0       => m_axis_h2c_tlast_0,
            m_axis_h2c_tvalid_0      => m_axis_h2c_tvalid_0,
            m_axis_h2c_tready_0      => m_axis_h2c_tready_0,
            m_axis_h2c_tkeep_0       => m_axis_h2c_tkeep_0,
            
            -- AXI Lite Interface
            -- AXI Master Write Address Channel
            m_axil_awaddr            => m_axil_awaddr,
            m_axil_awprot            => m_axil_awprot,
            m_axil_awvalid           => m_axil_awvalid,
            m_axil_awready           => m_axil_awready,
            -- AXI Master Write Data Channel
            m_axil_wdata             => m_axil_wdata,
            m_axil_wstrb             => m_axil_wstrb,
            m_axil_wvalid            => m_axil_wvalid,
            m_axil_wready            => m_axil_wready,
            -- AXI Master Write Response Channel
            m_axil_bvalid            => m_axil_bvalid,
            m_axil_bresp             => m_axil_bresp,
            m_axil_bready            => m_axil_bready,
            -- AXI Master Read Address Channel
            m_axil_araddr            => m_axil_araddr,
            m_axil_arprot            => m_axil_arprot,
            m_axil_arvalid           => m_axil_arvalid,
            m_axil_arready           => m_axil_arready,
            m_axil_rdata             => m_axil_rdata,
            -- AXI Master Read Data Channel
            m_axil_rresp             => m_axil_rresp,
            m_axil_rvalid            => m_axil_rvalid,
            m_axil_rready            => m_axil_rready,
            
            usr_irq_req              => usr_irq_req,
            usr_irq_ack              => usr_irq_ack,
            msi_enable               => msi_enable,
            msi_vector_width         => msi_vector_width,
            
            cfg_mgmt_addr            => (others => '0'),
            cfg_mgmt_write           => '0',
            cfg_mgmt_write_data      => (others => '0'),
            cfg_mgmt_byte_enable     => (others => '0'),
            cfg_mgmt_read            => '0'
            -- cfg_mgmt_read_data       => (others => '0'),
            -- cfg_mgmt_read_write_done => (others => '0'),
        );


    axi_clk        <= usr_clk;
    axi_rstn       <= usr_resetn;
    pci_exp_lnk_up <= usr_lnk_up;


end Behavioral;