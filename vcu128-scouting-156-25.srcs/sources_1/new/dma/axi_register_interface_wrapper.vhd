library IEEE;
use IEEE.std_logic_1164.all;

library unisim;
use unisim.vcomponents.all;

use work.datatypes.all;

entity axi_register_interface_wrapper is
  generic (
    REG_DATA_WIDTH : integer;
    REG_ADDR_WIDTH : integer
  );
  port (
    -- register interface
    moni_reg : in SRegister(2 ** (REG_ADDR_WIDTH) - 1 downto 0);
    ctrl_reg : out SRegister(2 ** (REG_ADDR_WIDTH) - 1 downto 0);
    -- axi interface
    S_AXI_ACLK    : in std_logic;
    S_AXI_ARESETN : in std_logic;
    S_AXI_ARADDR  : in std_logic_vector(31 downto 0);
    S_AXI_ARPROT  : in std_logic_vector(2 downto 0);
    S_AXI_ARREADY : out std_logic;
    S_AXI_ARVALID : in std_logic;
    S_AXI_AWADDR  : in std_logic_vector(31 downto 0);
    S_AXI_AWPROT  : in std_logic_vector(2 downto 0);
    S_AXI_AWREADY : out std_logic;
    S_AXI_AWVALID : in std_logic;
    S_AXI_BREADY  : in std_logic;
    S_AXI_BRESP   : out std_logic_vector(1 downto 0);
    S_AXI_BVALID  : out std_logic;
    S_AXI_RDATA   : out std_logic_vector(REG_DATA_WIDTH - 1 downto 0);
    S_AXI_RREADY  : in std_logic;
    S_AXI_RRESP   : out std_logic_vector(1 downto 0);
    S_AXI_RVALID  : out std_logic;
    S_AXI_WDATA   : in std_logic_vector(REG_DATA_WIDTH - 1 downto 0);
    S_AXI_WREADY  : out std_logic;
    S_AXI_WSTRB   : in std_logic_vector((REG_DATA_WIDTH/8) - 1 downto 0);
    S_AXI_WVALID  : in std_logic
  );
end axi_register_interface_wrapper;

architecture Behavioral of axi_register_interface_wrapper is

  constant axi_addr_width : natural := REG_ADDR_WIDTH + 3;

  constant num_half_registers : natural := 2 ** REG_ADDR_WIDTH;
  constant num_axi_registers  : natural := 2 * num_half_registers;

  signal axi_read_registers  : SRegister(num_axi_registers - 1 downto 0);
  signal axi_write_registers : SRegister(num_axi_registers - 1 downto 0);

  component axi_regfile_v1_0_S00_AXI
    generic (
      C_S_AXI_DATA_WIDTH : integer;
      C_S_AXI_ADDR_WIDTH : integer
    );
    port (
      -- register interface
      slv_read : in SRegister(num_axi_registers - 1 downto 0);
      slv_reg  : out SRegister(num_axi_registers - 1 downto 0);
      -- axi interface
      S_AXI_ACLK    : in std_logic;
      S_AXI_ARESETN : in std_logic;
      S_AXI_ARADDR  : in std_logic_vector(REG_ADDR_WIDTH + 2 downto 0); -- Two more bits for byte addressing, one more due to doubling of registers.
      S_AXI_ARPROT  : in std_logic_vector(2 downto 0);
      S_AXI_ARREADY : out std_logic;
      S_AXI_ARVALID : in std_logic;
      S_AXI_AWADDR  : in std_logic_vector(REG_ADDR_WIDTH + 2 downto 0); -- Two more bits for byte addressing, one more due to doubling of registers.
      S_AXI_AWPROT  : in std_logic_vector(2 downto 0);
      S_AXI_AWREADY : out std_logic;
      S_AXI_AWVALID : in std_logic;
      S_AXI_BREADY  : in std_logic;
      S_AXI_BRESP   : out std_logic_vector(1 downto 0);
      S_AXI_BVALID  : out std_logic;
      S_AXI_RDATA   : out std_logic_vector(REG_DATA_WIDTH - 1 downto 0);
      S_AXI_RREADY  : in std_logic;
      S_AXI_RRESP   : out std_logic_vector(1 downto 0);
      S_AXI_RVALID  : out std_logic;
      S_AXI_WDATA   : in std_logic_vector(REG_DATA_WIDTH - 1 downto 0);
      S_AXI_WREADY  : out std_logic;
      S_AXI_WSTRB   : in std_logic_vector((REG_DATA_WIDTH/8) - 1 downto 0);
      S_AXI_WVALID  : in std_logic
    );
  end component;

begin

  -- We want to connect the lower part of the axi_write_registers interface to the lower part of the axi_read_registers interface, in that way we get a readback for writes.
  axi_read_registers(num_half_registers - 1 downto 0)                   <= axi_write_registers(num_half_registers - 1 downto 0); -- Connected to lower part of axi_write_registers
  axi_read_registers(axi_read_registers'high downto num_half_registers) <= moni_reg;                                             -- Connected to the monitoring inputs

  -- We want to leave the higher part of axi_write_registers unconnected, because the higher part of axi_read_registers comes from our monitoring registers.
  ctrl_reg <= axi_write_registers(num_half_registers - 1 downto 0);

  axi_register_interface : axi_regfile_v1_0_S00_AXI
  generic map(
    C_S_AXI_DATA_WIDTH => REG_DATA_WIDTH,
    C_S_AXI_ADDR_WIDTH => axi_addr_width -- Two more bits for byte addressing, one more due to doubling of registers.
  )
  port map(
    -- register interface
    slv_read => axi_read_registers,  -- Input
    slv_reg  => axi_write_registers, -- Output
    -- axi interface
    S_AXI_ACLK    => S_AXI_ACLK,
    S_AXI_ARESETN => S_AXI_ARESETN,
    S_AXI_ARADDR  => S_AXI_ARADDR(axi_addr_width - 1 downto 0),
    S_AXI_ARPROT  => S_AXI_ARPROT,
    S_AXI_ARREADY => S_AXI_ARREADY,
    S_AXI_ARVALID => S_AXI_ARVALID,
    S_AXI_AWADDR  => S_AXI_AWADDR(axi_addr_width - 1 downto 0),
    S_AXI_AWPROT  => S_AXI_AWPROT,
    S_AXI_AWREADY => S_AXI_AWREADY,
    S_AXI_AWVALID => S_AXI_AWVALID,
    S_AXI_BREADY  => S_AXI_BREADY,
    S_AXI_BRESP   => S_AXI_BRESP,
    S_AXI_BVALID  => S_AXI_BVALID,
    S_AXI_RDATA   => S_AXI_RDATA,
    S_AXI_RREADY  => S_AXI_RREADY,
    S_AXI_RRESP   => S_AXI_RRESP,
    S_AXI_RVALID  => S_AXI_RVALID,
    S_AXI_WDATA   => S_AXI_WDATA,
    S_AXI_WREADY  => S_AXI_WREADY,
    S_AXI_WSTRB   => S_AXI_WSTRB,
    S_AXI_WVALID  => S_AXI_WVALID
  );

end Behavioral;
