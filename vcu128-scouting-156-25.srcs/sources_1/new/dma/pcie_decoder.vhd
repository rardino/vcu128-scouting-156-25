----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 07/16/2021 11:59:29 AM
-- Design Name:
-- Module Name: pcie_decoder - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pcie_decoder is
    port(
        ADD     : in std_logic_vector(18 downto 3);
        BA      : in std_logic;
        LD_ADD  : in std_logic;
        CLOCK   : in std_logic;
        RESET   : in std_logic;

        SEL_FUNC: out std_logic_vector(65535 downto 0)
    );
end pcie_decoder;

architecture Behavioral of pcie_decoder is

signal reg : std_logic_vector(65535 downto 0);

begin


process(CLOCK,RESET,ADD)
variable address : integer;

    begin
    address := to_integer(unsigned(ADD(18 downto 3))); -- 32-bit registers

    if rising_edge(clock) then
        if (LD_ADD = '1' and BA = '1') then
            reg             <= (others => '0');
            reg(address)    <= '1';
        end if;
    end if;
end process;

SEL_func <= reg;


end Behavioral;