library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

package scouting_register_constants is

    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    --| Monitoring registers
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    -- cdr_stable
    constant cdr_stable_addr             : natural := 0;
    constant cdr_stable_offset           : natural := 0;
    constant cdr_stable_width            : natural := 1;
    -- Rxbyteisaligned
    constant RxByteIsAligned_addr        : natural := 0;
    constant RxByteIsAligned_offset      : natural := 1;
    constant RxByteIsAligned_width       : natural := 8;
    -- gtpowergood
    constant GtPowerGood_addr            : natural := 0;
    constant GtPowerGood_offset          : natural := 9;
    constant GtPowerGood_width           : natural := 8;
    -- gtwiz_reset_tx_done
    constant GtTxResetDone_addr          : natural := 0;
    constant GtTxResetDone_offset        : natural := 17;
    constant GtTxResetDone_width         : natural := 1;
    -- gtwiz_reset_rx_done
    constant GtRxResetDone_addr          : natural := 0;
    constant GtRxResetDone_offset        : natural := 18;
    constant GtRxResetDone_width         : natural := 1;
    -- txpmaresetdone
    constant TxPmaResetDone_addr         : natural := 0;
    constant TxPmaResetDone_offset       : natural := 19;
    constant TxPmaResetDone_width        : natural := 1;
    -- rxpmaresetdone
    constant RxPmaResetDone_addr         : natural := 0;
    constant RxPmaResetDone_offset       : natural := 20;
    constant RxPmaResetDone_width        : natural := 1;
    -- gtwiz_reset_tx_pll_and_datapath
    constant GtResetTxPllDatapath_addr   : natural := 0;
    constant GtResetTxPllDatapath_offset : natural := 21;
    constant GtResetTxPllDatapath_width  : natural := 1;
    -- gtwiz_reset_tx_datapath
    constant GtResetTxDatapath_addr      : natural := 0;
    constant GtResetTxDatapath_offset    : natural := 22;
    constant GtResetTxDatapath_width     : natural := 1;
    -- gtwiz_reset_rx_datapath
    constant GtResetRxDatapath_addr      : natural := 0;
    constant GtResetRxDatapath_offset    : natural := 23;
    constant GtResetRxDatapath_width     : natural := 1;
    -- init_done_out
    constant InitDone_addr               : natural := 0;
    constant InitDone_offset             : natural := 24;
    constant InitDone_width              : natural := 1;
    -- clk_link_freq
    constant FreqInput_addr              : natural := 1;
    constant FreqInput_offset            : natural := 0;
    constant FreqInput_width             : natural := 32;
    -- filler_orbit_counter
    constant FillerOrbitsSeen_addr       : natural := 2;
    constant FillerOrbitsSeen_offset     : natural := 0;
    constant FillerOrbitsSeen_width      : natural := 32;
    -- filler_dropped_orbit_counter
    constant FillerOrbitsDropped_addr    : natural := 3;
    constant FillerOrbitsDropped_offset  : natural := 0;
    constant FillerOrbitsDropped_width   : natural := 32;
    -- axi_backpressure_seen
    constant AxiBackpressureSeen_addr    : natural := 4;
    constant AxiBackpressureSeen_offset  : natural := 0;
    constant AxiBackpressureSeen_width   : natural := 1;
    -- orbit_exceeds_size
    constant OrbitExceedsSize_addr       : natural := 4;
    constant OrbitExceedsSize_offset     : natural := 1;
    constant OrbitExceedsSize_width      : natural := 1;
    -- waiting_orbit_end
    constant WaitingForOrbitEnd_addr     : natural := 4;
    constant WaitingForOrbitEnd_offset   : natural := 2;
    constant WaitingForOrbitEnd_width    : natural := 1;
    -- data_rd
    constant I2CvalueRead_addr           : natural := 4;
    constant I2CvalueRead_offset         : natural := 3;
    constant I2CvalueRead_width          : natural := 8;
    -- autorealign_counter
    constant Autorealigns_addr           : natural := 5;
    constant Autorealigns_offset         : natural := 0;
    constant Autorealigns_width          : natural := 32;
    -- orbit_length
    constant OrbitLength_addr            : natural := 6;
    constant OrbitLength_offset          : natural := 0;
    constant OrbitLength_width           : natural := 32;
    -- ALGO_REV
    constant FwRev_addr                  : natural := 7;
    constant FwRev_offset                : natural := 0;
    constant FwRev_width                 : natural := 32;
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+





    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    --| Control registers
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    constant zs_threshold_width        : natural := 9;
    -- zs threshold for egammas
    constant zs_threshold_eg_addr      : natural := 0;
    constant zs_threshold_eg_offset    : natural := zs_threshold_width * 0;
    constant zs_threshold_eg_width     : natural := zs_threshold_width;
    -- zs threshold for jets
    constant zs_threshold_jet_addr     : natural := 0;
    constant zs_threshold_jet_offset   : natural := zs_threshold_width * 1;
    constant zs_threshold_jet_width    : natural := zs_threshold_width;
    -- zs threshold for taus
    constant zs_threshold_tau_addr     : natural := 1;
    constant zs_threshold_tau_offset   : natural := zs_threshold_width * 0;
    constant zs_threshold_tau_width    : natural := zs_threshold_width;
    -- zs threshold for et sums
    constant zs_threshold_sum_addr     : natural := 1;
    constant zs_threshold_sum_offset   : natural := zs_threshold_width * 1;
    constant zs_threshold_sum_width    : natural := zs_threshold_width;
    -- zs empty
    constant zs_threshold_empty_addr   : natural := 1;
    constant zs_threshold_empty_offset : natural := zs_threshold_width * 2;
    constant zs_threshold_empty_width  : natural := zs_threshold_width;
    -- zs type selection (0 => UGMT, 1 => DEMUX)
    constant zs_sel_addr               : natural := 1;
    constant zs_sel_offset             : natural := zs_threshold_width * 3;
    constant zs_sel_width              : natural := 1;
    -- link enable (1) or mask (0)
    constant link_enable_addr          : natural := 2;
    constant link_enable_offset        : natural := 0;
    constant link_enable_width         : natural := 8;
    -- led control (debug)
    constant led_enable_addr           : natural := 2;
    constant led_enable_offset         : natural := 8;
    constant led_enable_width          : natural := 8;
    --+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

end scouting_register_constants;
