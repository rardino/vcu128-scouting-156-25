-- Common data types
--
-- D. R., May 2018
library IEEE;
use IEEE.std_logic_1164.all;

use IEEE.numeric_std.all;

use work.vcu128_top_decl.all;

package datatypes is

    constant LWORD_WIDTH : integer := 32;
    type lword is
    record
        data   : std_logic_vector(LWORD_WIDTH - 1 downto 0);
        valid  : std_logic;
        done   : std_logic;
        strobe : std_logic;
    end record;

    type ldata is array(natural range <>) of lword;
    constant LWORD_NULL : lword             := ((others => '0'), '0', '0', '0');
    constant LWORD_PAD  : lword             := (x"F7F7F7F7", '1', '0', '1');
    constant LDATA_NULL : ldata(0 downto 0) := (0       => LWORD_NULL);

    subtype aword is std_logic_vector(LWORD_WIDTH - 1 downto 0);
    type adata is array(natural range <>) of aword;
    type acontrol is
    record
        valid    : std_logic;
        strobe   : std_logic;
        bx_start : std_logic;
        last     : std_logic;
    end record;
    constant AWORD_NULL : aword             := (others => '0');
    constant AWORD_PAD  : aword             := x"F7F7F7F7";
    constant ADATA_NULL : adata(0 downto 0) := (0      => AWORD_NULL);
    
    subtype aword_with_last is std_logic_vector(32 downto 0);
    type adata_with_last is array (natural range <>) of aword_with_last;

    type TAuxInfo    is array (natural range <>) of std_logic_vector(31 downto 0);
    type SRegister   is array (natural range <>) of std_logic_vector(31 downto 0);
    
    type TBCounterVec is array(natural range <>) of natural range 0 to 3564;
    type TOCounterVec is array(natural range <>) of natural;
    type TEvtWordsVec is array(natural range <>) of natural range 0 to 5;
    
    type TLinkBuffer is array (natural range <>) of ldata(7 downto 0);
    
    type CutList is array((N_REGION * 4) - 1 downto 0) of unsigned(8 downto 0);

end datatypes;
