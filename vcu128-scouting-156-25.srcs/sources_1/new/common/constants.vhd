library ieee;
use ieee.std_logic_1164.all;

package constants is

    -- constant ALGO_REV        : std_logic_vector(31 downto 0) := X"00" & X"00" & X"00" & X"01";
    constant LHC_BUNCH_COUNT        : integer  := 3564;
    constant ORBIT_LENGTH           : natural  := 3564;
    
    constant N_REGION               : positive := 2;
    
    constant REGISTER_ADDRESS_WIDTH : natural  := 4;

end constants;
