library ieee;
library unisim;
library xpm;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use unisim.vcomponents.all;
use xpm.vcomponents.all;
-- use work.all;
use work.vcu128_top_decl.all;
use work.datatypes.all;
use work.scouting_register_constants.all;






entity vcu128_top is
    Port(
        -- 100 MHz clk
        clk_ddr4_p     : in std_logic;
        clk_ddr4_n     : in std_logic;
        
        -- MGT ref clocks
        mgtrefclk0_x0y11_p : in std_logic;
        mgtrefclk0_x0y11_n : in std_logic;
        mgtrefclk0_x0y10_p : in std_logic;
        mgtrefclk0_x0y10_n : in std_logic;
        
        -- Serial data ports for transceiver channel 0
        ch0_gtyrxn_in  : in  std_logic;
        ch0_gtyrxp_in  : in  std_logic;
        ch0_gtytxn_out : out std_logic;
        ch0_gtytxp_out : out std_logic;

        -- Serial data ports for transceiver channel 1
        ch1_gtyrxn_in  : in  std_logic;
        ch1_gtyrxp_in  : in  std_logic;
        ch1_gtytxn_out : out std_logic;
        ch1_gtytxp_out : out std_logic;

        -- Serial data ports for transceiver channel 2
        ch2_gtyrxn_in  : in  std_logic;
        ch2_gtyrxp_in  : in  std_logic;
        ch2_gtytxn_out : out std_logic;
        ch2_gtytxp_out : out std_logic;

        -- Serial data ports for transceiver channel 3
        ch3_gtyrxn_in  : in  std_logic;
        ch3_gtyrxp_in  : in  std_logic;
        ch3_gtytxn_out : out std_logic;
        ch3_gtytxp_out : out std_logic;

        -- Serial data ports for transceiver channel 4
        ch4_gtyrxn_in  : in  std_logic;
        ch4_gtyrxp_in  : in  std_logic;
        ch4_gtytxn_out : out std_logic;
        ch4_gtytxp_out : out std_logic;

        -- Serial data ports for transceiver channel 5
        ch5_gtyrxn_in  : in  std_logic;
        ch5_gtyrxp_in  : in  std_logic;
        ch5_gtytxn_out : out std_logic;
        ch5_gtytxp_out : out std_logic;

        -- Serial data ports for transceiver channel 6
        ch6_gtyrxn_in  : in  std_logic;
        ch6_gtyrxp_in  : in  std_logic;
        ch6_gtytxn_out : out std_logic;
        ch6_gtytxp_out : out std_logic;

        -- Serial data ports for transceiver channel 7
        ch7_gtyrxn_in  : in  std_logic;
        ch7_gtyrxp_in  : in  std_logic;
        ch7_gtytxn_out : out std_logic;
        ch7_gtytxp_out : out std_logic;
        
        -- i2c
        sda            : inout std_logic;
        scl            : inout std_logic;
        
        -- pci express
        pci_exp_rst_n  : in std_logic;
        pci_exp_clk_n  : in std_logic;
        pci_exp_clk_p  : in std_logic;

        pci_exp_txn    : out std_logic_vector(7 downto 0);
        pci_exp_txp    : out std_logic_vector(7 downto 0);
        pci_exp_rxn    : in  std_logic_vector(7 downto 0);
        pci_exp_rxp    : in  std_logic_vector(7 downto 0);

        -- leds (debug)
        led_enable     : out std_logic_vector(7 downto 0)
    );
end vcu128_top;





architecture Behavioral of vcu128_top is

    ---------------------------------------------------------------------------
    --
    -- Components
    --
    ---------------------------------------------------------------------------
    ---- ila for link and xdma debug
    -- link debug ila
--    component link_debug_ila
--        port (
--            clk         : in  std_logic;
--            trig_in     : in  std_logic;
--            trig_in_ack : out std_logic;
--            probe0      : in  std_logic_vector(31 downto 0); 
--            probe1      : in  std_logic_vector(31 downto 0); 
--            probe2      : in  std_logic_vector(31 downto 0); 
--            probe3      : in  std_logic_vector(31 downto 0); 
--            probe4      : in  std_logic_vector(31 downto 0); 
--            probe5      : in  std_logic_vector(31 downto 0); 
--            probe6      : in  std_logic_vector(31 downto 0); 
--            probe7      : in  std_logic_vector(31 downto 0); 
--            probe8      : in  std_logic_vector( 7 downto 0); 
--            probe9      : in  std_logic_vector( 7 downto 0); 
--            probe10     : in  std_logic_vector( 7 downto 0); 
--            probe11     : in  std_logic_vector( 7 downto 0); 
--            probe12     : in  std_logic_vector( 0 downto 0); 
--            probe13     : in  std_logic_vector( 0 downto 0); 
--            probe14     : in  std_logic_vector( 7 downto 0); 
--            probe15     : in  std_logic_vector( 7 downto 0); 
--            probe16     : in  std_logic_vector( 7 downto 0); 
--            probe17     : in  std_logic_vector( 7 downto 0); 
--            probe18     : in  std_logic_vector( 7 downto 0);
--            probe19     : in  std_logic_vector( 7 downto 0);
--            probe20     : in  std_logic_vector(31 downto 0);
--            probe21     : in  std_logic_vector( 7 downto 0);
--            probe22     : in  std_logic_vector( 7 downto 0)
--        );
--    end component;
    
    -- xdma debug ila
--    component xdma_debug_ila
--        port (
--            clk         : in  std_logic;
--            trig_in     : in  std_logic;
--            trig_in_ack : out std_logic;
--            probe0      : in  std_logic_vector( 31 downto 0);
--            probe1      : in  std_logic_vector( 31 downto 0);
--            probe2      : in  std_logic_vector( 31 downto 0);
--            probe3      : in  std_logic_vector( 31 downto 0);
--            probe4      : in  std_logic_vector(  3 downto 0);
--            probe5      : in  std_logic_vector( 31 downto 0);
--            probe6      : in  std_logic_vector(  0 downto 0);
--            probe7      : in  std_logic_vector(255 downto 0);
--            probe8      : in  std_logic_vector(  0 downto 0);
--            probe9      : in  std_logic_vector(  0 downto 0);
--            probe10     : in  std_logic_vector(  0 downto 0);
--            probe11     : in  std_logic_vector(255 downto 0);
--            probe12     : in  std_logic_vector(  0 downto 0)
--            -- probe11     : in  std_logic_vector( 22 downto 0);
--            -- probe12     : in  std_logic_vector(  0 downto 0);
--            -- probe13     : in  std_logic_vector(  0 downto 0)
--        );
--    end component;
    
    
    
    ---- vio for monitoring
    component vio_0
        port (
            clk         : in  std_logic;
            probe_in0   : in  std_logic_vector( 7 downto 0);
            probe_in1   : in  std_logic_vector( 0 downto 0);
            probe_in2   : in  std_logic_vector( 0 downto 0);
            probe_in3   : in  std_logic_vector( 0 downto 0);
            probe_in4   : in  std_logic_vector( 0 downto 0);
            probe_in5   : in  std_logic_vector( 0 downto 0);
            probe_in6   : in  std_logic_vector( 0 downto 0);
            probe_in7   : in  std_logic_vector( 0 downto 0);
            probe_in8   : in  std_logic_vector(63 downto 0);
            probe_in9   : in  std_logic_vector(63 downto 0);
            probe_in10  : in  std_logic_vector(31 downto 0);
            probe_in11  : in  std_logic_vector(31 downto 0);
            probe_in12  : in  std_logic_vector(31 downto 0);
            probe_in13  : in  std_logic_vector(63 downto 0);
            probe_in14  : in  std_logic_vector(63 downto 0);
            probe_out0  : out std_logic_vector( 0 downto 0);
            probe_out1  : out std_logic_vector( 0 downto 0);
            probe_out2  : out std_logic_vector( 7 downto 0);
            probe_out3  : out std_logic_vector( 0 downto 0);
            probe_out4  : out std_logic_vector( 0 downto 0);
            probe_out5  : out std_logic_vector(15 downto 0);
            probe_out6  : out std_logic_vector(15 downto 0);
            probe_out7  : out std_logic_vector(15 downto 0)
        );
    end component;
    
    
    
    ---- MMCM to generate i2c and free-running clocks
    component clk_wiz_0
        port (
            -- Clock in ports
            clk_in1  : in  std_logic;
            -- Clock out ports
            clk_out1 : out std_logic;
            clk_out2 : out std_logic;
            -- Status and control signals
            reset    : in  std_logic;
            locked   : out std_logic
        );
    end component;
    ---------------------------------------------------------------------------
    
    
    
    
    
    ---------------------------------------------------------------------------
    --
    -- Signals declaration
    --
    ---------------------------------------------------------------------------
    ---- MGT CLOCK signals
    -- x0y11
    signal mgtrefclk0_x0y11_int      : std_logic;
    signal mgtrefclk0_x0y11_int_buf  : std_logic;
    signal mgtrefclk0_x0y11_int_free : std_logic;
    -- x0y10
    signal mgtrefclk0_x0y10_int      : std_logic;
    signal mgtrefclk0_x0y10_int_buf  : std_logic;
    signal mgtrefclk0_x0y10_int_free : std_logic;
    
    
    ---- CLK signals
    signal clk_ddr4 : std_logic;
    signal clk_free : std_logic;
    signal clk_link : std_logic;
    signal clk_i2c  : std_logic;
    signal clk_axi  : std_logic;
    signal locked   : std_logic;
    
    
    ---- I2C signals (unused for vcu128)
    signal str_rd         : std_logic;
    signal str_wr         : std_logic;
    signal data_rd        : std_logic_vector(7 downto 0);
    signal enable_i2c_gen : std_logic;
    signal rst_i2c_gen    : std_logic;
    signal wr_i2c_gen     : std_logic;
    
    
    ---- RST signals
    signal rst_global   : std_logic;
    signal rst_algo     : std_logic;
    signal rst_i2c      : std_logic;
    signal rst_packager : std_logic;
    signal rst_aligner  : std_logic;
    
    
    ---- VIO signals (synchronized with xpm cdc)
    signal sRxbyteisaligned_vio      : std_logic_vector(4*N_REGION - 1 downto 0);
    signal cdr_stable_vio            : std_logic_vector( 0 downto 0);
    signal GEN_ORBIT_FULL_LENGTH_vio : std_logic_vector(15 downto 0);
    signal GEN_ORBIT_DATA_LENGTH_vio : std_logic_vector(15 downto 0);
    
    
    ---- GTWIZARD signals
    signal gtpowergood_int                            : std_logic_vector(4*N_REGION    - 1 downto 0);
    signal init_done_int                              : std_logic_vector(0 downto 0);
    signal init_retry_ctr_int                         : std_logic_vector(3 downto 0);
    signal link_down_latched_reset_vio_int            : std_logic_vector(0 downto 0);
    signal hb0_gtwiz_reset_tx_pll_and_datapath_int    : std_logic_vector(0 downto 0);
    signal hb0_gtwiz_reset_tx_datapath_int            : std_logic_vector(0 downto 0);
    signal hb_gtwiz_reset_rx_pll_and_datapath_vio_int : std_logic_vector(0 downto 0);
    signal hb_gtwiz_reset_rx_datapath_vio_int         : std_logic_vector(0 downto 0);

    signal hb_gtwiz_reset_clk_freerun_int             : std_logic;
    
    signal rx_data                                    : std_logic_vector(4*N_REGION*32 - 1 downto 0);
    signal sRxbyteisaligned                           : std_logic_vector(4*N_REGION    - 1 downto 0);
    signal sCommaDet                                  : std_logic_vector(4*N_REGION    - 1 downto 0);
    signal cdr_stable                                 : std_logic_vector(0 downto 0);

    signal rxctrl0_int                                : std_logic_vector(4*N_REGION*16 - 1 downto 0);
    signal rxctrl1_int                                : std_logic_vector(4*N_REGION*16 - 1 downto 0);
    signal rxctrl2_int                                : std_logic_vector(4*N_REGION* 8 - 1 downto 0);
    signal rxctrl3_int                                : std_logic_vector(4*N_REGION* 8 - 1 downto 0);


    ---- GAP_CLEANER/ALIGNER/ZS/ALGO signals
    signal lid : TAuxInfo(4*N_REGION - 1 downto 0);
    signal crc : TAuxInfo(4*N_REGION - 1 downto 0);
    
    signal stream_enable_mask     : std_logic_vector(4*N_REGION - 1 downto 0);
    signal stream_enable_mask_vio : std_logic_vector(7 downto 0);
    
    signal d_gap_cleaner          : ldata(3 + (N_REGION - 1) * 4 downto 0);
    signal d_align                : ldata(3 + (N_REGION - 1) * 4 downto 0);
    
    signal d_algo                 : adata(4*N_REGION -1 downto 0);
    signal d_zs, q_zs             : adata(4*N_REGION -1 downto 0);
    signal d_package              : adata(4*N_REGION -1 downto 0);
    
    signal d_ctrl_algo            : acontrol;
    signal d_ctrl_zs, q_ctrl_zs   : acontrol;
    signal d_ctrl_package         : acontrol;
    
    signal do_zs                  : std_logic;
    
    signal zs_threshold_eg        : unsigned(8 downto 0);
    signal zs_threshold_jet       : unsigned(8 downto 0);
    signal zs_threshold_tau       : unsigned(8 downto 0);
    signal zs_threshold_sum       : unsigned(8 downto 0);
    signal zs_threshold_empty     : unsigned(8 downto 0);
    
    signal zs_sel                 : std_logic := '0';
    signal zs_cuts                : CutList := (others => to_unsigned(0, 9));
    
    
    ---- FIFO FILLER signals
    signal filler_orbit_counter              : unsigned(63 downto 0);
    signal filler_dropped_orbit_counter      : unsigned(63 downto 0);
    signal axi_backpressure_seen             : std_logic;
    signal orbits_per_packet                 : std_logic_vector(15 downto 0);
    signal waiting_orbit_end                 : std_logic;

    signal orbit_length                      : unsigned(63 downto 0);
    signal orbit_length_cnt                  : unsigned(63 downto 0);
    signal orbit_exceeds_size                : std_logic;
    signal enable_autorealign                : std_logic;
    signal autorealign_counter               : unsigned(63 downto 0);
    
    
    ---- AXI signals
    signal axi_rstn                                 : std_logic;
    signal pci_exp_lnk_up                           : std_logic := '0';
    -- AXI lite
    signal m_axil_awaddr,  m_axil_araddr            : std_logic_vector(31 downto 0);
    signal m_axil_awprot,  m_axil_arprot            : std_logic_vector( 2 downto 0);
    signal m_axil_awvalid, m_axil_awready           : std_logic;                     -- write address ready/valid
    signal m_axil_arvalid, m_axil_arready           : std_logic;
    signal m_axil_wdata,   m_axil_rdata             : std_logic_vector(31 downto 0);
    signal m_axil_wstrb                             : std_logic_vector(3 downto 0);
    signal m_axil_wvalid,  m_axil_wready            : std_logic;                     -- write data valid/ready
    signal m_axil_rvalid,  m_axil_rready            : std_logic;
    signal m_axil_bvalid,  m_axil_bready            : std_logic;                     -- write response valid/ready
    signal m_axil_rresp,   m_axil_bresp             : std_logic_vector(1 downto 0);
    -- AXI stream
    signal s_axis_c2h_tdata_0,  m_axis_h2c_tdata_0  : std_logic_vector(255 downto 0);
    signal s_axis_c2h_tlast_0,  m_axis_h2c_tlast_0  : std_logic;
    signal s_axis_c2h_tvalid_0, m_axis_h2c_tvalid_0 : std_logic;
    signal s_axis_c2h_tready_0, m_axis_h2c_tready_0 : std_logic;
    signal s_axis_c2h_tkeep_0,  m_axis_h2c_tkeep_0  : std_logic_vector(31 downto 0);
    -- AXI register interface
    signal moni_reg                                 : SRegister(2 ** REGISTER_ADDRESS_WIDTH - 1 downto 0);
    signal ctrl_reg                                 : SRegister(2 ** REGISTER_ADDRESS_WIDTH - 1 downto 0);


    ---- PCIE INTERFACE signals (unused in this version)
    signal usr_func_wr      : std_logic_vector(65535 downto 0);
    signal usr_wren         : std_logic;
    signal usr_data_wr      : std_logic_vector(31 downto 0);
    -- signal usr_be           : std_logic_vector(7 downto 0);
    signal usr_func_rd      : std_logic_vector(65535 downto 0);
    signal usr_rden         : std_logic;
    signal usr_data_rd      : std_logic_vector(31 downto 0);
    signal usr_rd_val       : std_logic := '1';
    
    signal delay_retreive_data_back : std_logic_vector(2 downto 0);


    ---- DEBUG signals
    -- leds
    signal led_status_reg                      : std_logic_vector(7 downto 0);
    
    -- clk frequency measurement
    signal freq_clk_free                       : std_logic_vector(31 downto 0);
    signal freq_clk_i2c                        : std_logic_vector(31 downto 0);
    signal freq_clk_link                       : std_logic_vector(31 downto 0);
    signal freq_clk_x0y11                      : std_logic_vector(31 downto 0);
    signal freq_clk_axi                        : std_logic_vector(31 downto 0);
    
    -- ila trigger (from vio), unused
    signal trigger_ila                         : std_logic;
    
    -- input data type decoding
    signal s_ifcomma                           : std_logic_vector(4*N_REGION - 1 downto 0);
    signal s_ifpadding                         : std_logic_vector(4*N_REGION - 1 downto 0);
    signal s_ifinvalid                         : std_logic_vector(4*N_REGION - 1 downto 0);
    signal s_ifdata                            : std_logic_vector(4*N_REGION - 1 downto 0);
    signal s_ifelse                            : std_logic_vector(4*N_REGION - 1 downto 0);
    
    -- gtwizard link resets     
    signal hb_gtwiz_reset_rx_datapath_ila      : std_logic_vector(0 downto 0);
    signal hb_gtwiz_reset_all_ila              : std_logic_vector(0 downto 0);
    signal hb_gtwiz_reset_all_init_ila         : std_logic_vector(0 downto 0);
    signal hb_gtwiz_reset_rx_datapath_init_ila : std_logic_vector(0 downto 0);

    signal gtwiz_reset_tx_pll_and_datapath_ila : std_logic_vector(0 downto 0);
    signal gtwiz_reset_tx_datapath_ila         : std_logic_vector(0 downto 0);
    signal gtwiz_reset_rx_pll_and_datapath_ila : std_logic_vector(0 downto 0);
    signal gtwiz_reset_rx_datapath_ila         : std_logic_vector(0 downto 0);
    signal gtwiz_reset_tx_done_ila             : std_logic_vector(0 downto 0);
    signal gtwiz_reset_rx_done_ila             : std_logic_vector(0 downto 0);

    signal gtwiz_userclk_tx_reset_ila          : std_logic_vector(0 downto 0);
    signal gtwiz_userclk_rx_reset_ila          : std_logic_vector(0 downto 0);
                                        
    signal txpmaresetdone_ila                  : std_logic_vector(4*N_REGION - 1 downto 0);
    signal rxpmaresetdone_ila                  : std_logic_vector(4*N_REGION - 1 downto 0);
    
    signal init_done_ila                       : std_logic_vector(0 downto 0);
    
    -- status of elastic buffer, unused
    signal rx_buf_status_ila                   : std_logic_vector(4*3*N_REGION - 1 downto 0);
    
    -- aligner logic debug
    signal fifo_wr_en_ila : std_logic_vector(4*N_REGION - 1 downto 0);
    signal last_ila       : std_logic_vector(4*N_REGION - 1 downto 0);
    
    -- fifo filler debug
    signal header_dout_ila   : std_logic_vector(255 downto 0);
    signal header_ren_ila    : std_logic;
    ---------------------------------------------------------------------------

begin

    ---------------------------------------------------------------------------
    --
    -- CLK buffers, MMCM, I2C driver for MGT clk programming
    --
    ---------------------------------------------------------------------------
    ---- Differential reference clock buffer for mgtrefclk0_x0y11
    IBUFDS_GTE4_MGTREFCLK0_X0Y11_INST : IBUFDS_GTE4
        generic map (
            REFCLK_EN_TX_PATH  => '0',
            REFCLK_HROW_CK_SEL => "00",
            REFCLK_ICNTL_RX    => "00"
        )
        port map (
            I     => mgtrefclk0_x0y11_p,
            IB    => mgtrefclk0_x0y11_n,
            CEB   => '0',
            O     => mgtrefclk0_x0y11_int,
            ODIV2 => mgtrefclk0_x0y11_int_buf
        );
    
    BUFG_GT_X0Y11_INST : BUFG_GT
        port map (
            CE      => '1',
            CEMASK  => '0',
            CLR     => '0',
            CLRMASK => '0',
            DIV     => "000",
            I       => mgtrefclk0_x0y11_int_buf,
            O       => mgtrefclk0_x0y11_int_free
        );
    
    
    ---- Differential reference clock buffer for mgtrefclk0_x0y10
    IBUFDS_GTE4_MGTREFCLK0_X0Y10_INST : IBUFDS_GTE4
        generic map (
            REFCLK_EN_TX_PATH  => '0',
            REFCLK_HROW_CK_SEL => "00",
            REFCLK_ICNTL_RX    => "00"
        )
        port map (
            I     => mgtrefclk0_x0y10_p,
            IB    => mgtrefclk0_x0y10_n,
            CEB   => '0',
            O     => mgtrefclk0_x0y10_int,
            ODIV2 => mgtrefclk0_x0y10_int_buf
        );
    
    BUFG_GT_X0Y10_INST : BUFG_GT
        port map (
            CE      => '1',
            CEMASK  => '0',
            CLR     => '0',
            CLRMASK => '0',
            DIV     => "000",
            I       => mgtrefclk0_x0y10_int_buf,
            O       => mgtrefclk0_x0y10_int_free
        );


    ---- DDR4 100 MHz clock
    IBUFDS_DDR4_100MHZ : IBUFDS
        port map(
            o       => clk_ddr4,
            I       => clk_ddr4_p,
            IB      => clk_ddr4_n
        );
    
    
    ---- MMCM for free running and i2c clocks generation
    clk_wiz : clk_wiz_0
        port map (
            -- Clock in ports
            clk_in1   => mgtrefclk0_x0y10_int_free,
            -- Clock out ports  
            clk_out1  => clk_free,
            clk_out2  => clk_i2c,
            -- Status and control signals                
            reset     => '0',
            locked    => locked
        );
    
    
    ---- Set MGT clock via I2C 
--    i2c_i : entity work.i2c_driver
--        port map (
--            clk     => clk_i2c, -- 50 MHz from MMCM
--            reset   => rst_i2c,
--            str_wr  => str_wr,
--            str_rd  => str_rd,
--            data_rd => data_rd,
--            sda     => sda,
--            scl     => scl
--        );
    ---------------------------------------------------------------------------
    
    



    ---------------------------------------------------------------------------
    --
    -- Debug Module instantiations
    --
    ---------------------------------------------------------------------------
    ---- ILAs for link and xdma debug
--    link_debug_ila_0 : link_debug_ila
--        port map (
--            clk                    => clk_link,
--            trig_in                => '0',
--            trig_in_ack            => open,
--            -- d_gap_cleaner data
--            probe0                 => d_gap_cleaner(0).data,
--            probe1                 => d_gap_cleaner(1).data,
--            probe2                 => d_gap_cleaner(2).data,
--            probe3                 => d_gap_cleaner(3).data,
--            probe4                 => d_gap_cleaner(4).data,
--            probe5                 => d_gap_cleaner(5).data,
--            probe6                 => d_gap_cleaner(6).data,
--            probe7                 => d_gap_cleaner(7).data,
--            -- d_gap_cleaner valid
--            probe8(0)              => d_gap_cleaner(0).valid,
--            probe8(1)              => d_gap_cleaner(1).valid,
--            probe8(2)              => d_gap_cleaner(2).valid,
--            probe8(3)              => d_gap_cleaner(3).valid,
--            probe8(4)              => d_gap_cleaner(4).valid,
--            probe8(5)              => d_gap_cleaner(5).valid,
--            probe8(6)              => d_gap_cleaner(6).valid,
--            probe8(7)              => d_gap_cleaner(7).valid,
--            -- d_gap_cleaner strobe
--            probe9(0)              => d_gap_cleaner(0).strobe,
--            probe9(1)              => d_gap_cleaner(1).strobe,
--            probe9(2)              => d_gap_cleaner(2).strobe,
--            probe9(3)              => d_gap_cleaner(3).strobe,
--            probe9(4)              => d_gap_cleaner(4).strobe,
--            probe9(5)              => d_gap_cleaner(5).strobe,
--            probe9(6)              => d_gap_cleaner(6).strobe,
--            probe9(7)              => d_gap_cleaner(7).strobe,
--            -- d_align valid
--            probe10(0)             => d_align(0).valid,
--            probe10(1)             => d_align(1).valid,
--            probe10(2)             => d_align(2).valid,
--            probe10(3)             => d_align(3).valid,
--            probe10(4)             => d_align(4).valid,
--            probe10(5)             => d_align(5).valid,
--            probe10(6)             => d_align(6).valid,
--            probe10(7)             => d_align(7).valid,
--            -- d_align strobe
--            probe11(0)             => d_align(0).strobe,
--            probe11(1)             => d_align(1).strobe,
--            probe11(2)             => d_align(2).strobe,
--            probe11(3)             => d_align(3).strobe,
--            probe11(4)             => d_align(4).strobe,
--            probe11(5)             => d_align(5).strobe,
--            probe11(6)             => d_align(6).strobe,
--            probe11(7)             => d_align(7).strobe,
--            -- rst aligner bit
--            probe12(0)             => rst_aligner,
--            -- gtwiz_reset_rx_cdr_stable bit (!)
--            probe13                => cdr_stable,
--            -- rxbyteisaligned (!)
--            probe14                => sRxbyteisaligned,
--            -- data type decodings
--            probe15                => s_ifcomma,
--            probe16                => s_ifpadding,
--            probe17                => s_ifinvalid,
--            probe18                => s_ifdata,
--            probe19                => s_ifelse,
--            -- orbit length counter (just for raw tests, not in the same clk domain)
--            probe20                => std_logic_vector(orbit_length_cnt(31 downto 0)),
--            probe21                => fifo_wr_en_ila,
--            probe22                => last_ila
--        );
    
--    xdma_debug_ila_0 : xdma_debug_ila
--        port map (
--            clk                    => clk_axi,
--            trig_in                => '0',
--            trig_in_ack            => open,
--            -- d_gap_cleaner data
--            probe0                 => d_package(0),
--            probe1                 => d_package(1),
--            probe2                 => d_package(2),
--            probe3                 => d_package(3),
--            probe4(0)              => d_ctrl_package.valid,
--            probe4(1)              => d_ctrl_package.strobe,
--            probe4(2)              => d_ctrl_package.bx_start,
--            probe4(3)              => d_ctrl_package.last,
--            probe5                 => std_logic_vector(orbit_length_cnt(31 downto 0)),
--            probe6(0)              => rst_aligner,
--            probe7                 => s_axis_c2h_tdata_0,
--            probe8(0)              => s_axis_c2h_tvalid_0,
--            probe9(0)              => s_axis_c2h_tready_0,
--            probe10(0)             => s_axis_c2h_tlast_0,
--            probe11                => header_dout_ila,
--            probe12(0)             => header_ren_ila
--        );


    ---- VIO for monitoring
    central_vio : vio_0
        port map (
            clk          => clk_axi,
            -- input ports
            probe_in0     => sRxbyteisaligned_vio,
            probe_in1     => cdr_stable_vio,
            probe_in2(0)  => rst_aligner,
            probe_in3     => hb0_gtwiz_reset_tx_pll_and_datapath_int,
            probe_in4     => hb0_gtwiz_reset_tx_datapath_int,
            probe_in5     => hb_gtwiz_reset_rx_datapath_vio_int,
            probe_in6(0)  => rst_algo,
            probe_in7(0)  => rst_packager,
            probe_in8     => std_logic_vector(autorealign_counter),
            probe_in9     => std_logic_vector(orbit_length),
            probe_in10    => freq_clk_link,
            probe_in11    => freq_clk_x0y11,
            probe_in12    => freq_clk_axi,
            probe_in13    => std_logic_vector(filler_orbit_counter),
            probe_in14    => std_logic_vector(filler_dropped_orbit_counter),
            -- output ports
            probe_out0(0) => rst_global,
            probe_out1(0) => rst_i2c,
            probe_out2    => stream_enable_mask_vio,
            probe_out3(0) => enable_autorealign,
            probe_out4(0) => do_zs,
            probe_out5    => orbits_per_packet,
            probe_out6    => GEN_ORBIT_FULL_LENGTH_vio,
            probe_out7    => GEN_ORBIT_DATA_LENGTH_vio
        );
    
    xpm_cdc_array_single_sRxbyteisaligned : xpm_cdc_array_single
        generic map (
            DEST_SYNC_FF   => 2,      -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 0,      -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0,      -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 1,      -- DECIMAL; 0=do not register input, 1=register input
            WIDTH          => sRxbyteisaligned'length    -- DECIMAL; range: 1-1024
        )
        port map (
            dest_out => sRxbyteisaligned_vio,    -- WIDTH-bit output: src_in synchronized to the destination clock domain. This
                                                 -- output is registered.
            dest_clk => clk_axi,                 -- 1-bit input: Clock signal for the destination clock domain.
            src_clk  => clk_link,                -- 1-bit input: optional; required when SRC_INPUT_REG = 1
            src_in   => sRxbyteisaligned         -- WIDTH-bit input: Input single-bit array to be synchronized to destination clock
                                                 -- domain. It is assumed that each bit of the array is unrelated to the others.
                                                 -- This is reflected in the constraints applied to this macro. To transfer a binary
                                                 -- value losslessly across the two clock domains, use the XPM_CDC_GRAY macro
                                                 -- instead.
        );
    
    xpm_cdc_array_single_cdr_stable : xpm_cdc_array_single
        generic map (
            DEST_SYNC_FF   => 2,
            INIT_SYNC_FF   => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG  => 1,
            WIDTH          => cdr_stable'length
        )
        port map (
            dest_out => cdr_stable_vio,
            dest_clk => clk_axi,
            src_clk  => mgtrefclk0_x0y10_int_free,
            src_in   => cdr_stable
        );
    
    xpm_cdc_array_single_stream_enable_mask : xpm_cdc_array_single
        generic map (
            DEST_SYNC_FF   => 2,
            INIT_SYNC_FF   => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG  => 1,
            WIDTH          => stream_enable_mask_vio'length
        )
        port map (
            dest_out => stream_enable_mask,
            dest_clk => clk_link,
            src_clk  => clk_axi,
            src_in   => stream_enable_mask_vio
        );
    ---------------------------------------------------------------------------
    
    
    
    
    
    ---------------------------------------------------------------------------
    --
    -- Reset controllers and freq measurement
    --
    ---------------------------------------------------------------------------
    ---- RESET block
    global_reset : entity work.reset
        port map (
            clk_free     => clk_free,
            clk_i2c      => clk_i2c,
            clk_algo     => clk_link,
            clk_axi      => clk_axi,
            rst_global   => rst_global,
            enable_i2c   => enable_i2c_gen,
            rst_i2c      => rst_i2c_gen,
            write_i2c    => wr_i2c_gen,
            rst_pll      => hb0_gtwiz_reset_tx_pll_and_datapath_int(0),
            rst_tx       => hb0_gtwiz_reset_tx_datapath_int(0),
            rst_rx       => hb_gtwiz_reset_rx_datapath_vio_int(0),
            rst_algo     => rst_algo,
            rst_packager => rst_packager
        );


    ---- frequency measurements
    freq_meas_clk_link : entity work.freq_meas
        generic map (
            DELAY_THRS_1 => x"09502F90",
            DELAY_THRS_2 => x"09502F95"
        )
        port map (
            clk      => mgtrefclk0_x0y10_int_free,
            rst      => rst_packager,
            clk_meas => clk_link,
            freq     => freq_clk_link
        );
    
    freq_meas_clk_x0y11 : entity work.freq_meas
        generic map (
            DELAY_THRS_1 => x"09502F90",
            DELAY_THRS_2 => x"09502F95"
        )
        port map (
            clk      => mgtrefclk0_x0y10_int_free,
            rst      => rst_packager,
            clk_meas => mgtrefclk0_x0y11_int_free,
            freq     => freq_clk_x0y11
        );
    
    freq_meas_clk_axi : entity work.freq_meas
        generic map (
            DELAY_THRS_1 => x"09502F90",
            DELAY_THRS_2 => x"09502F95"
        )
        port map (
            clk      => mgtrefclk0_x0y10_int_free,
            rst      => rst_packager,
            clk_meas => clk_axi,
            freq     => freq_clk_axi
        );
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- Input data receiver block (GTY wrapper)
    --
    ---------------------------------------------------------------------------
    inputs : entity work.inputs
        generic map (
            BYPASS_LINKS => false
        )
        port map (
            clk_axi        => clk_axi,
            rst            => rst_global,
            
            GEN_ORBIT_FULL_LENGTH => to_integer(unsigned(GEN_ORBIT_FULL_LENGTH_vio)),
            GEN_ORBIT_DATA_LENGTH => to_integer(unsigned(GEN_ORBIT_DATA_LENGTH_vio)),
            
            -- mgtrefclk0_x0y10_int => mgtrefclk0_x0y10_int,
            mgtrefclk0_x0y11_int => mgtrefclk0_x0y11_int,
            
            -- Serial data ports for transceiver channel 0
            ch0_gtyrxn_in  => ch0_gtyrxn_in,
            ch0_gtyrxp_in  => ch0_gtyrxp_in,
            ch0_gtytxn_out => ch0_gtytxn_out,
            ch0_gtytxp_out => ch0_gtytxp_out,

            -- Serial data ports for transceiver channel 1
            ch1_gtyrxn_in  => ch1_gtyrxn_in,
            ch1_gtyrxp_in  => ch1_gtyrxp_in,
            ch1_gtytxn_out => ch1_gtytxn_out,
            ch1_gtytxp_out => ch1_gtytxp_out,

            -- Serial data ports for transceiver channel 2
            ch2_gtyrxn_in  => ch2_gtyrxn_in,
            ch2_gtyrxp_in  => ch2_gtyrxp_in,
            ch2_gtytxn_out => ch2_gtytxn_out,
            ch2_gtytxp_out => ch2_gtytxp_out,

            -- Serial data ports for transceiver channel 3
            ch3_gtyrxn_in  => ch3_gtyrxn_in,
            ch3_gtyrxp_in  => ch3_gtyrxp_in,
            ch3_gtytxn_out => ch3_gtytxn_out,
            ch3_gtytxp_out => ch3_gtytxp_out,

            -- Serial data ports for transceiver channel 4
            ch4_gtyrxn_in  => ch4_gtyrxn_in,
            ch4_gtyrxp_in  => ch4_gtyrxp_in,
            ch4_gtytxn_out => ch4_gtytxn_out,
            ch4_gtytxp_out => ch4_gtytxp_out,

            -- Serial data ports for transceiver channel 5
            ch5_gtyrxn_in  => ch5_gtyrxn_in,
            ch5_gtyrxp_in  => ch5_gtyrxp_in,
            ch5_gtytxn_out => ch5_gtytxn_out,
            ch5_gtytxp_out => ch5_gtytxp_out,

            -- Serial data ports for transceiver channel 6
            ch6_gtyrxn_in  => ch6_gtyrxn_in,
            ch6_gtyrxp_in  => ch6_gtyrxp_in,
            ch6_gtytxn_out => ch6_gtytxn_out,
            ch6_gtytxp_out => ch6_gtytxp_out,

            -- Serial data ports for transceiver channel 7
            ch7_gtyrxn_in  => ch7_gtyrxn_in,
            ch7_gtyrxp_in  => ch7_gtyrxp_in,
            ch7_gtytxn_out => ch7_gtytxn_out,
            ch7_gtytxp_out => ch7_gtytxp_out,

            -- free-running clock
            clk_freerun                         => mgtrefclk0_x0y10_int_free, -- clk_free,
            clk_freerun_buf                     => hb_gtwiz_reset_clk_freerun_int,  -- Clock after being globally buffered

            -- debug: elastic buffer
            rx_buf_status_out                   => rx_buf_status_ila,
            -- debug: rx/tx reset (helper block)
            hb_gtwiz_reset_rx_datapath_out      => hb_gtwiz_reset_rx_datapath_ila,
            hb_gtwiz_reset_all_out              => hb_gtwiz_reset_all_ila,
            hb_gtwiz_reset_all_init_out         => hb_gtwiz_reset_all_init_ila,
            hb_gtwiz_reset_rx_datapath_init_out => hb_gtwiz_reset_rx_datapath_init_ila,
            -- debug: rx/tx reset
            gtwiz_reset_tx_pll_and_datapath_out => gtwiz_reset_tx_pll_and_datapath_ila,
            gtwiz_reset_tx_datapath_out         => gtwiz_reset_tx_datapath_ila,
            gtwiz_reset_rx_pll_and_datapath_out => gtwiz_reset_rx_pll_and_datapath_ila,
            gtwiz_reset_rx_datapath_out         => gtwiz_reset_rx_datapath_ila,
            gtwiz_reset_tx_done_out             => gtwiz_reset_tx_done_ila,
            gtwiz_reset_rx_done_out             => gtwiz_reset_rx_done_ila,
            gtwiz_userclk_tx_reset_out          => gtwiz_userclk_tx_reset_ila,
            gtwiz_userclk_rx_reset_out          => gtwiz_userclk_rx_reset_ila,
            -- debug: rx/tx pma reset
            txpmaresetdone_out                  => txpmaresetdone_ila,
            rxpmaresetdone_out                  => rxpmaresetdone_ila,
            -- debug: initialization done
            init_done_out                       => init_done_ila,
            -- debug: check byte alignment and clock data recovery (CDR) stable
            oRxbyteisaligned                    => sRxbyteisaligned,
            cdr_stable                          => cdr_stable,
            
            -- debug: decoded rx words type
            ifcomma                             => s_ifcomma,  
            ifpadding                           => s_ifpadding,
            ifinvalid                           => s_ifinvalid,
            ifdata                              => s_ifdata,
            ifelse                              => s_ifelse,
            
            -- rx words
            oLinkData                           => rx_data,
            
            -- decoding signals
            rxctrl0_out                         => rxctrl0_int,
            rxctrl1_out                         => rxctrl1_int,
            rxctrl2_out                         => rxctrl2_int,
            rxctrl3_out                         => rxctrl3_int,

            clk                                 => clk_link,
            q                                   => d_gap_cleaner,
            oCommaDet                           => sCommaDet
        );
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- Comma-Gap-Cleaner + Aligner + Zero-Suppression + Fifo Chain
    --
    ---------------------------------------------------------------------------
    ---- Comma-Gap-Cleaner
    gap_cleaner : entity work.comma_gap_cleaner
        port map (
            clk => clk_link,
            rst => rst_algo,
            d   => d_gap_cleaner,
            q   => d_align
        );
    
    
    ---- Autorealign controller (if misalignment is found, trigger a reset)
    auto_realign_controller_1 : entity work.auto_realign_controller
        port map (
            axi_clk           => clk_axi,
            rst_in            => rst_packager,
            enabled           => enable_autorealign,
            clk_aligner       => clk_link,
            rst_aligner_out   => rst_aligner,
            misalignment_seen => orbit_exceeds_size,
            autoreset_count   => autorealign_counter
        );
    
    
    ---- Aligner (and link masking)
    align : entity work.bx_aware_aligner
        generic map (
            NSTREAMS => 4* N_REGION
        )
        port map (
            clk_wr            => clk_link,
            clk_rd            => clk_axi,
            rst               => rst_aligner,
            enable            => stream_enable_mask,
            waiting_orbit_end => waiting_orbit_end,
            d                 => d_align,
            q                 => d_zs,
            q_ctrl            => d_ctrl_zs,
            -- DEBUG
            fifo_wr_en_o      => fifo_wr_en_ila,
            last_o            => last_ila
        );


    ---- Zero-Suppression
    zs_cuts <= (
        zs_threshold_tau,
        zs_threshold_tau,
        zs_threshold_sum,
        zs_threshold_empty,
        zs_threshold_eg,
        zs_threshold_eg,
        zs_threshold_jet,
        zs_threshold_jet
    );
    
    zs : entity work.zs
        generic map (
            NSTREAMS => 4*N_REGION
        )
        port map (
            clk     => clk_axi,
            rst     => rst_packager,
            d       => d_zs,
            d_ctrl  => d_ctrl_zs,
            q       => q_zs,
            q_ctrl  => q_ctrl_zs,
            zs_sel  => zs_sel,
            zs_cuts => zs_cuts
        );
    
    
    ---- Fifo Chain before DMA (and check if zs is enabled)
    ---- Instantiate work.fifo_filler_header if you want header functionalities
    d_package      <= q_zs      when do_zs = '1' else d_zs;
    d_ctrl_package <= q_ctrl_zs when do_zs = '1' else d_ctrl_zs;
    
    fifo_filler : entity work.fifo_filler
        generic map (
            NSTREAMS => 4* N_REGION
        )
        port map (
            d_clk                  => clk_axi,
            rst                    => rst_packager,
            orbits_per_packet      => unsigned(orbits_per_packet),
            d                      => d_package,
            d_ctrl                 => d_ctrl_package,
            m_aclk                 => clk_axi,
            m_axis_tvalid          => s_axis_c2h_tvalid_0,
            m_axis_tready          => s_axis_c2h_tready_0,
            m_axis_tdata           => s_axis_c2h_tdata_0,
            m_axis_tkeep           => s_axis_c2h_tkeep_0,
            m_axis_tlast           => s_axis_c2h_tlast_0,
            dropped_orbit_counter  => filler_dropped_orbit_counter,
            orbit_counter          => filler_orbit_counter,
            axi_backpressure_seen  => axi_backpressure_seen,
            orbit_length           => orbit_length,
            orbit_length_cnt       => orbit_length_cnt,
            orbit_exceeds_size     => orbit_exceeds_size,
            in_autorealign_counter => autorealign_counter
            -- header_dout_o          => header_dout_ila,
            -- header_ren_o           => header_ren_ila
        );
    ---------------------------------------------------------------------------



    
    
    ---------------------------------------------------------------------------
    --
    -- DMA engine and AXI Lite interface
    --
    ---------------------------------------------------------------------------
    ---- AXI Lite register interface
    axi_register_interface : entity work.axi_register_interface_wrapper
        generic map(
            REG_DATA_WIDTH => 32,
            REG_ADDR_WIDTH => REGISTER_ADDRESS_WIDTH
        )
        port map(
            -- register interface
            moni_reg      => moni_reg,
            ctrl_reg      => ctrl_reg,
            -- axi interface
            s_axi_aclk    => clk_axi,
            s_axi_aresetn => axi_rstn,
            s_axi_araddr  => m_axil_araddr,
            s_axi_arprot  => m_axil_arprot,
            s_axi_arready => m_axil_arready,
            s_axi_arvalid => m_axil_arvalid,
            s_axi_awaddr  => m_axil_awaddr,
            s_axi_awprot  => m_axil_awprot,
            s_axi_awready => m_axil_awready,
            s_axi_awvalid => m_axil_awvalid,
            s_axi_bready  => m_axil_bready,
            s_axi_bresp   => m_axil_bresp,
            s_axi_bvalid  => m_axil_bvalid,
            s_axi_rdata   => m_axil_rdata,
            s_axi_rready  => m_axil_rready,
            s_axi_rresp   => m_axil_rresp,
            s_axi_rvalid  => m_axil_rvalid,
            s_axi_wdata   => m_axil_wdata,
            s_axi_wready  => m_axil_wready,
            s_axi_wstrb   => m_axil_wstrb,
            s_axi_wvalid  => m_axil_wvalid
        );

    ---- PCIe interface and DMA engine
    dma_engine_i : entity work.pcie_interface
        generic map(
            PL_LINK_CAP_MAX_LINK_WIDTH => 8,
            PL_SIM_FAST_LINK_TRAINING  => false,
            PL_LINK_CAP_MAX_LINK_SPEED => 4,
            C_DATA_WIDTH               => 256,
            EXT_PIPE_SIM               => false,
            C_ROOT_PORT                => false,
            C_DEVICE_NUMBER            => 0
        )
        port map (
            -- PCIe clk and rst
            pci_exp_rst_n => pci_exp_rst_n,
            pci_exp_clk_p => pci_exp_clk_p,
            pci_exp_clk_n => pci_exp_clk_n,
            -- tx
            pci_exp_tx_p  => pci_exp_txp,
            pci_exp_tx_n  => pci_exp_txn,
            -- rx
            pci_exp_rx_p  => pci_exp_rxp,
            pci_exp_rx_n  => pci_exp_rxn,
            
            -- AXI clk and rstn
            axi_clk        => clk_axi,
            axi_rstn       => axi_rstn,
            pci_exp_lnk_up => pci_exp_lnk_up,
            
            -- AXI Lite Interface
            -- AXI Master Write Address Channel
            m_axil_awaddr  => m_axil_awaddr,
            m_axil_awprot  => m_axil_awprot,
            m_axil_awvalid => m_axil_awvalid,
            m_axil_awready => m_axil_awready,
            -- AXI Master Write Data Channel
            m_axil_wdata   => m_axil_wdata,
            m_axil_wstrb   => m_axil_wstrb,
            m_axil_wvalid  => m_axil_wvalid,
            m_axil_wready  => m_axil_wready,
            -- AXI Master Write Response Channel
            m_axil_bvalid  => m_axil_bvalid,
            m_axil_bready  => m_axil_bready,
            -- AXI Master Read Address Channel
            m_axil_araddr  => m_axil_araddr,
            m_axil_arprot  => m_axil_arprot,
            m_axil_arvalid => m_axil_arvalid,
            m_axil_arready => m_axil_arready,
            -- AXI Master Read Data Channel
            m_axil_rdata   => m_axil_rdata,
            m_axil_rresp   => m_axil_rresp,
            m_axil_rvalid  => m_axil_rvalid,
            m_axil_rready  => m_axil_rready,
            m_axil_bresp   => m_axil_bresp,

            -- AXI Stream
            -- tdata
            s_axis_c2h_tdata_0  => s_axis_c2h_tdata_0,
            m_axis_h2c_tdata_0  => m_axis_h2c_tdata_0,
            -- tlast
            s_axis_c2h_tlast_0  => s_axis_c2h_tlast_0,
            m_axis_h2c_tlast_0  => m_axis_h2c_tlast_0,
            -- tvalid
            s_axis_c2h_tvalid_0 => s_axis_c2h_tvalid_0,
            m_axis_h2c_tvalid_0 => m_axis_h2c_tvalid_0,
            -- tready
            s_axis_c2h_tready_0 => s_axis_c2h_tready_0,
            m_axis_h2c_tready_0 => m_axis_h2c_tready_0,
            -- tkeep
            s_axis_c2h_tkeep_0  => s_axis_c2h_tkeep_0,
            m_axis_h2c_tkeep_0  => m_axis_h2c_tkeep_0
        );
    ---------------------------------------------------------------------------





    ---------------------------------------------------------------------------
    --
    -- Register Control (through AXI Lite)
    --
    ---------------------------------------------------------------------------
    ---- monitoring registers
    moni_reg(cdr_stable_addr)(cdr_stable_offset)                                                                                     <= cdr_stable(0);
    moni_reg(RxByteIsAligned_addr)(RxByteIsAligned_width + RxByteIsAligned_offset - 1 downto RxByteIsAligned_offset)                 <= sRxbyteisaligned;
    moni_reg(GtPowerGood_addr)(GtPowerGood_width + GtPowerGood_offset - 1 downto GtPowerGood_offset)                                 <= gtpowergood_int;
    moni_reg(GtTxResetDone_addr)(GtTxResetDone_offset)                                                                               <= gtwiz_reset_tx_done_ila(0);
    moni_reg(GtRxResetDone_addr)(GtRxResetDone_offset)                                                                               <= gtwiz_reset_rx_done_ila(0);
    moni_reg(TxPmaResetDone_addr)(TxPmaResetDone_offset)                                                                             <= txpmaresetdone_ila(0);
    moni_reg(RxPmaResetDone_addr)(RxPmaResetDone_offset)                                                                             <= rxpmaresetdone_ila(0);
    moni_reg(GtResetTxPllDatapath_addr)(GtResetTxPllDatapath_offset)                                                                 <= gtwiz_reset_tx_pll_and_datapath_ila(0);
    moni_reg(GtResetTxDatapath_addr)(GtResetTxDatapath_offset)                                                                       <= gtwiz_reset_tx_datapath_ila(0);
    moni_reg(GtResetRxDatapath_addr)(GtResetRxDatapath_offset)                                                                       <= gtwiz_reset_rx_datapath_ila(0);
    moni_reg(InitDone_addr)(InitDone_offset)                                                                                         <= init_done_int(0);
    moni_reg(FreqInput_addr)(FreqInput_width + FreqInput_offset - 1 downto FreqInput_offset)                                         <= freq_clk_link;
    moni_reg(FillerOrbitsSeen_addr)(FillerOrbitsSeen_width + FillerOrbitsSeen_offset - 1 downto FillerOrbitsSeen_offset)             <= std_logic_vector(filler_orbit_counter(31 downto 0));
    moni_reg(FillerOrbitsDropped_addr)(FillerOrbitsDropped_width + FillerOrbitsDropped_offset - 1 downto FillerOrbitsDropped_offset) <= std_logic_vector(filler_dropped_orbit_counter(31 downto 0));
    moni_reg(AxiBackpressureSeen_addr)(AxiBackpressureSeen_offset)                                                                   <= axi_backpressure_seen;
    moni_reg(OrbitExceedsSize_addr)(OrbitExceedsSize_offset)                                                                         <= orbit_exceeds_size;
    moni_reg(WaitingForOrbitEnd_addr)(WaitingForOrbitEnd_offset)                                                                     <= waiting_orbit_end;
    moni_reg(I2CvalueRead_addr)(I2CvalueRead_width + I2CvalueRead_offset - 1 downto I2CvalueRead_offset)                             <= data_rd;
    moni_reg(Autorealigns_addr)(Autorealigns_width + Autorealigns_offset - 1 downto Autorealigns_offset)                             <= std_logic_vector(autorealign_counter(31 downto 0));
    moni_reg(OrbitLength_addr)(OrbitLength_width + OrbitLength_offset - 1 downto OrbitLength_offset)                                 <= std_logic_vector(orbit_length(31 downto 0));
    moni_reg(FwRev_addr)(FwRev_width + FwRev_offset - 1 downto FwRev_offset)                                                         <= ALGO_REV;
    
    ---- control registers
    zs_threshold_eg    <= unsigned(ctrl_reg(zs_threshold_eg_addr)(zs_threshold_eg_width + zs_threshold_eg_offset - 1 downto zs_threshold_eg_offset));
    zs_threshold_jet   <= unsigned(ctrl_reg(zs_threshold_jet_addr)(zs_threshold_jet_width + zs_threshold_jet_offset - 1 downto zs_threshold_jet_offset));
    zs_threshold_tau   <= unsigned(ctrl_reg(zs_threshold_tau_addr)(zs_threshold_tau_width + zs_threshold_tau_offset - 1 downto zs_threshold_tau_offset));
    zs_threshold_sum   <= unsigned(ctrl_reg(zs_threshold_sum_addr)(zs_threshold_sum_width + zs_threshold_sum_offset - 1 downto zs_threshold_sum_offset));
    zs_threshold_empty <= unsigned(ctrl_reg(zs_threshold_empty_addr)(zs_threshold_empty_width + zs_threshold_empty_offset - 1 downto zs_threshold_empty_offset));
    zs_sel             <= ctrl_reg(zs_sel_addr)(zs_sel_offset);
    -- stream_enable_mask <= ctrl_reg(link_enable_addr)(link_enable_width - 1 downto link_enable_offset);
    led_enable         <= ctrl_reg(led_enable_addr)(led_enable_width + led_enable_offset - 1 downto led_enable_offset);
    ---------------------------------------------------------------------------

end Behavioral;