// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Mon Feb 21 14:21:24 2022
// Host        : daqlab40-skylake16 running 64-bit CentOS Linux release 7.9.2009 (Core)
// Command     : write_verilog -force -mode synth_stub
//               /home/rardino/vcu128-scouting-156-25/vcu128-scouting-156-25.srcs/sources_1/ip/gtwizard_ultrascale_0/gtwizard_ultrascale_0_stub.v
// Design      : gtwizard_ultrascale_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xcvu37p-fsvh2892-2L-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "gtwizard_ultrascale_0_gtwizard_top,Vivado 2019.2" *)
module gtwizard_ultrascale_0(gtwiz_userclk_tx_active_in, 
  gtwiz_userclk_rx_active_in, gtwiz_reset_clk_freerun_in, gtwiz_reset_all_in, 
  gtwiz_reset_tx_pll_and_datapath_in, gtwiz_reset_tx_datapath_in, 
  gtwiz_reset_rx_pll_and_datapath_in, gtwiz_reset_rx_datapath_in, 
  gtwiz_reset_rx_cdr_stable_out, gtwiz_reset_tx_done_out, gtwiz_reset_rx_done_out, 
  gtwiz_userdata_tx_in, gtwiz_userdata_rx_out, gtrefclk00_in, qpll0outclk_out, 
  qpll0outrefclk_out, drpaddr_in, drpclk_in, drpdi_in, drpen_in, drpwe_in, eyescanreset_in, 
  gtyrxn_in, gtyrxp_in, loopback_in, rx8b10ben_in, rxcommadeten_in, rxlpmen_in, 
  rxmcommaalignen_in, rxpcommaalignen_in, rxpolarity_in, rxrate_in, rxusrclk_in, 
  rxusrclk2_in, tx8b10ben_in, txctrl0_in, txctrl1_in, txctrl2_in, txdiffctrl_in, txpolarity_in, 
  txpostcursor_in, txprecursor_in, txusrclk_in, txusrclk2_in, drpdo_out, drprdy_out, 
  gtpowergood_out, gtytxn_out, gtytxp_out, rxbufstatus_out, rxbyteisaligned_out, 
  rxbyterealign_out, rxcommadet_out, rxctrl0_out, rxctrl1_out, rxctrl2_out, rxctrl3_out, 
  rxoutclk_out, rxpmaresetdone_out, txoutclk_out, txpmaresetdone_out)
/* synthesis syn_black_box black_box_pad_pin="gtwiz_userclk_tx_active_in[0:0],gtwiz_userclk_rx_active_in[0:0],gtwiz_reset_clk_freerun_in[0:0],gtwiz_reset_all_in[0:0],gtwiz_reset_tx_pll_and_datapath_in[0:0],gtwiz_reset_tx_datapath_in[0:0],gtwiz_reset_rx_pll_and_datapath_in[0:0],gtwiz_reset_rx_datapath_in[0:0],gtwiz_reset_rx_cdr_stable_out[0:0],gtwiz_reset_tx_done_out[0:0],gtwiz_reset_rx_done_out[0:0],gtwiz_userdata_tx_in[255:0],gtwiz_userdata_rx_out[255:0],gtrefclk00_in[1:0],qpll0outclk_out[1:0],qpll0outrefclk_out[1:0],drpaddr_in[79:0],drpclk_in[7:0],drpdi_in[127:0],drpen_in[7:0],drpwe_in[7:0],eyescanreset_in[7:0],gtyrxn_in[7:0],gtyrxp_in[7:0],loopback_in[23:0],rx8b10ben_in[7:0],rxcommadeten_in[7:0],rxlpmen_in[7:0],rxmcommaalignen_in[7:0],rxpcommaalignen_in[7:0],rxpolarity_in[7:0],rxrate_in[23:0],rxusrclk_in[7:0],rxusrclk2_in[7:0],tx8b10ben_in[7:0],txctrl0_in[127:0],txctrl1_in[127:0],txctrl2_in[63:0],txdiffctrl_in[39:0],txpolarity_in[7:0],txpostcursor_in[39:0],txprecursor_in[39:0],txusrclk_in[7:0],txusrclk2_in[7:0],drpdo_out[127:0],drprdy_out[7:0],gtpowergood_out[7:0],gtytxn_out[7:0],gtytxp_out[7:0],rxbufstatus_out[23:0],rxbyteisaligned_out[7:0],rxbyterealign_out[7:0],rxcommadet_out[7:0],rxctrl0_out[127:0],rxctrl1_out[127:0],rxctrl2_out[63:0],rxctrl3_out[63:0],rxoutclk_out[7:0],rxpmaresetdone_out[7:0],txoutclk_out[7:0],txpmaresetdone_out[7:0]" */;
  input [0:0]gtwiz_userclk_tx_active_in;
  input [0:0]gtwiz_userclk_rx_active_in;
  input [0:0]gtwiz_reset_clk_freerun_in;
  input [0:0]gtwiz_reset_all_in;
  input [0:0]gtwiz_reset_tx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_tx_datapath_in;
  input [0:0]gtwiz_reset_rx_pll_and_datapath_in;
  input [0:0]gtwiz_reset_rx_datapath_in;
  output [0:0]gtwiz_reset_rx_cdr_stable_out;
  output [0:0]gtwiz_reset_tx_done_out;
  output [0:0]gtwiz_reset_rx_done_out;
  input [255:0]gtwiz_userdata_tx_in;
  output [255:0]gtwiz_userdata_rx_out;
  input [1:0]gtrefclk00_in;
  output [1:0]qpll0outclk_out;
  output [1:0]qpll0outrefclk_out;
  input [79:0]drpaddr_in;
  input [7:0]drpclk_in;
  input [127:0]drpdi_in;
  input [7:0]drpen_in;
  input [7:0]drpwe_in;
  input [7:0]eyescanreset_in;
  input [7:0]gtyrxn_in;
  input [7:0]gtyrxp_in;
  input [23:0]loopback_in;
  input [7:0]rx8b10ben_in;
  input [7:0]rxcommadeten_in;
  input [7:0]rxlpmen_in;
  input [7:0]rxmcommaalignen_in;
  input [7:0]rxpcommaalignen_in;
  input [7:0]rxpolarity_in;
  input [23:0]rxrate_in;
  input [7:0]rxusrclk_in;
  input [7:0]rxusrclk2_in;
  input [7:0]tx8b10ben_in;
  input [127:0]txctrl0_in;
  input [127:0]txctrl1_in;
  input [63:0]txctrl2_in;
  input [39:0]txdiffctrl_in;
  input [7:0]txpolarity_in;
  input [39:0]txpostcursor_in;
  input [39:0]txprecursor_in;
  input [7:0]txusrclk_in;
  input [7:0]txusrclk2_in;
  output [127:0]drpdo_out;
  output [7:0]drprdy_out;
  output [7:0]gtpowergood_out;
  output [7:0]gtytxn_out;
  output [7:0]gtytxp_out;
  output [23:0]rxbufstatus_out;
  output [7:0]rxbyteisaligned_out;
  output [7:0]rxbyterealign_out;
  output [7:0]rxcommadet_out;
  output [127:0]rxctrl0_out;
  output [127:0]rxctrl1_out;
  output [63:0]rxctrl2_out;
  output [63:0]rxctrl3_out;
  output [7:0]rxoutclk_out;
  output [7:0]rxpmaresetdone_out;
  output [7:0]txoutclk_out;
  output [7:0]txpmaresetdone_out;
endmodule
