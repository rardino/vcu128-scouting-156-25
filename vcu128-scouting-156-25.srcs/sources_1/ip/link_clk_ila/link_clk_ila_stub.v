// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Wed Oct 20 20:08:01 2021
// Host        : daqlab40-skylake16 running 64-bit CentOS Linux release 7.9.2009 (Core)
// Command     : write_verilog -force -mode synth_stub
//               /home/rardino/vcu128-scouting/vcu128-scouting.srcs/sources_1/ip/link_clk_ila/link_clk_ila_stub.v
// Design      : link_clk_ila
// Purpose     : Stub declaration of top-level module interface
// Device      : xcvu37p-fsvh2892-2L-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "ila,Vivado 2019.2" *)
module link_clk_ila(clk, trig_in, trig_in_ack, probe0, probe1, probe2, 
  probe3, probe4, probe5, probe6, probe7, probe8, probe9, probe10, probe11, probe12, probe13, probe14, 
  probe15, probe16, probe17, probe18, probe19, probe20, probe21, probe22, probe23, probe24, probe25, 
  probe26)
/* synthesis syn_black_box black_box_pad_pin="clk,trig_in,trig_in_ack,probe0[31:0],probe1[31:0],probe2[31:0],probe3[31:0],probe4[31:0],probe5[31:0],probe6[31:0],probe7[31:0],probe8[31:0],probe9[31:0],probe10[31:0],probe11[31:0],probe12[31:0],probe13[31:0],probe14[31:0],probe15[31:0],probe16[7:0],probe17[7:0],probe18[7:0],probe19[7:0],probe20[0:0],probe21[127:0],probe22[127:0],probe23[63:0],probe24[63:0],probe25[0:0],probe26[7:0]" */;
  input clk;
  input trig_in;
  output trig_in_ack;
  input [31:0]probe0;
  input [31:0]probe1;
  input [31:0]probe2;
  input [31:0]probe3;
  input [31:0]probe4;
  input [31:0]probe5;
  input [31:0]probe6;
  input [31:0]probe7;
  input [31:0]probe8;
  input [31:0]probe9;
  input [31:0]probe10;
  input [31:0]probe11;
  input [31:0]probe12;
  input [31:0]probe13;
  input [31:0]probe14;
  input [31:0]probe15;
  input [7:0]probe16;
  input [7:0]probe17;
  input [7:0]probe18;
  input [7:0]probe19;
  input [0:0]probe20;
  input [127:0]probe21;
  input [127:0]probe22;
  input [63:0]probe23;
  input [63:0]probe24;
  input [0:0]probe25;
  input [7:0]probe26;
endmodule
